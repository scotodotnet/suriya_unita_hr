﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="New_PF_List.aspx.cs" Inherits="New_PF_List" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="SalPay" runat="server">
                 <ContentTemplate>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>


 <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Report</a></li>
				<li class="active">Pay Slip</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Pay Slip</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Pay Slip</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        
                      
                        
                        <!-- begin row -->
                        <div class="row">
                          <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonths" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ExpField" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_LeaveField" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ManField" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                      
                             <div class="col-md-4">
								<div class="form-group">
								 <label>Fin. Year</label>
								 <asp:DropDownList runat="server" ID="ddlFinance" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
								</div>
                               </div>
                        
                        </div>
                    <!-- end row -->
                       <div class="row">
                       
                       <div class="col-md-2">
                       
                       </div>
                            
                               <div class="col-md-10">
                               <br />
                               
                               
                                   <asp:Button ID="btnPFList" runat="server" Text="Generate PF" 
                                       class="btn btn-success" onclick="btnPFList_Click" />
                                   <asp:Button ID="btnESI" runat="server" Text="Generate ESI" class="btn btn-info" 
                                       onclick="btnESI_Click"/>
                                       
                               </div>
                               
                       </div>
                       
                           <!-- begin row -->  
                        <div class="row">
                        <div class="col-md-2"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-10">
								 <div class="form-group">
									
									<%--<asp:Button runat="server" id="btnAllSalary" Text="All Salary Details" class="btn btn-success" OnClick="btnAllSalary_Click" Visible="false"/>--%>
									<%--<asp:Button runat="server" id="btnBankSalary" Text="Bank Salary" class="btn btn-success" onclick="btnBankSalary_Click"/>--%>
									<%--<asp:Button runat="server" id="btnOtherBankSalary" Text="Other Bank Salary" class="btn btn-success" onclick="btnOtherBankSalary_Click"/>--%>
									<%--<asp:Button runat="server" id="btnCivilAbstract" Text="Civil Abstract" class="btn btn-success" OnClick="btnCivilAbstract_Click"/>--%>
									<%--<asp:Button runat="server" id="btnLeaveWages" Text="NFH Worked Wages" class="btn btn-success" ValidationGroup="Validate_LeaveField" OnClick="btnLeaveWages_Click" Visible="false"/>--%>
									<%--<asp:Button runat="server" id="BtnDeptManDays" Text="Department Mandays Wages" class="btn btn-success" onclick="BtnDeptManDays_Click" ValidationGroup="Validate_ManField" />--%>
									<%--<asp:Button runat="server" id="btnConfirm" Text="Confirmation" class="btn btn-success" OnClick="btnConfirm_Click" Visible="false"/>--%>
									
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                            
                         </div>
                        <!-- end row --> 
                        
                        <asp:Panel runat="server" ID="PnlTrPF" Visible="false">
                        <!--  STAFF SALARY Details GridView Start  -->
                                            <tr id="TrPF" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="GvPF" runat="server" AutoGenerateColumns="false" 
                                                        onselectedindexchanged="gvSalary_SelectedIndexChanged">
                                                        <Columns>
                                                         
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>UAN</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUAN" runat="server" Text='<%# Eval("UAN") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Member Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Gross Wages</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>EPF Wages</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEPFWages" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>EPS Wages</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEPSWages" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>EDLI Wages</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEDLI" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>EPF Contribution remitted</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWH_Work_Days" runat="server" Text='<%# Eval("PF") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                          
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>EPS Contribution remitted </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLeaveCredit" runat="server" text='<%# Eval("EmployeerPFTwo") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>EPF and EPS Diff remitted</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLOPDays" runat="server" Text='<%# Eval("EmployeerPFone") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NCP Days </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLOPAmount" runat="server" Text='0'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Refund of Advances</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFixed" runat="server" Text='0'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!--  STAFF SALARY Details GridView End  -->
                       </asp:Panel>
                       
                       <asp:Panel runat="server" ID="PnlESI" Visible="false">
                        <!--  STAFF SALARY Details GridView Start  -->
                                            <tr id="TrESI" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="GvESI" runat="server" AutoGenerateColumns="false" 
                                                        onselectedindexchanged="gvSalary_SelectedIndexChanged">
                                                        <Columns>
                                                         
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>UAN</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUAN" runat="server" Text='<%# Eval("UAN") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Member Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Gross Wages</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>EPF Wages</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEPFWages" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>EPS Wages</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEPSWages" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>EDLI Wages</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEDLI" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ESI Contribution remitted</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWH_Work_Days" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                         
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NCP Days </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLOPAmount" runat="server" Text='0'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Refund of Advances</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFixed" runat="server" Text='0'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!--  STAFF SALARY Details GridView End  -->
                       </asp:Panel>
                       
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->



 </ContentTemplate>
                 <Triggers>
                    <asp:PostBackTrigger ControlID="btnPFList" />
                    <asp:PostBackTrigger ControlID="btnESI" />
                 </Triggers>
              </asp:UpdatePanel>  
</asp:Content>

