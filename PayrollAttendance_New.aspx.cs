﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
public partial class PayrollAttendance_New : System.Web.UI.Page
{

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string Division;
    string OT_Eligible = "";
    string Wages_Type = "";
    string OT_Above_Eight = "0";
    string OT_Above_Four = "0";
    string OT_Below_Four = "0";
    string OT_Total_Hours = "0";

    string OT_Hours = "0";
    DataTable Log_DS = new DataTable();
    DataTable dt_OT = new DataTable();
    DataTable dt_OT_Eligible = new DataTable();

    BALDataAccess objdata = new BALDataAccess();


    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable NFH_DS = new DataTable();
    DataTable NFH_Type_Ds = new DataTable();
    DataTable WH_DS = new DataTable();
    DataTable dt_Manula_OT = new DataTable();
    string Manul_OT;
    DateTime NFH_Date = new DateTime();
    DateTime DOJ_Date_Format = new DateTime();
    string qry_nfh = "";
    string SSQL = "";
    DateTime Week_Off_Date = new DateTime();
    DateTime WH_DOJ_Date_Format = new DateTime();
    Boolean Check_Week_Off_DOJ = false;
    double NFH_Present_Check = 0;
    decimal NFH_Days_Count = 0;
    decimal AEH_NFH_Days_Count = 0;
    decimal LBH_NFH_Days_Count = 0;
    decimal NFH_Days_Present_Count = 0;
    decimal WH_Count = 0;
    decimal WH_Present_Count = 0;
    decimal Present_Days_Count = 0;
    int Fixed_Work_Days = 0;
    decimal Spinning_Incentive_Days = 0;

    decimal NFH_Double_Wages_Checklist = 0;
    decimal NFH_Double_Wages_Statutory = 0;
    decimal NFH_Double_Wages_Manual = 0;
    decimal NFH_WH_Days_Mins = 0;
    decimal NFH_Single_Wages = 0;
    int Month_Mid_Total_Days_Count;
    string NFH_Type_Str = "";
    string NFH_Name_Get_Str = "";
    string NFH_Dbl_Wages_Statutory_Check = "";
    string Emp_WH_Day = "";
    string DOJ_Date_Str = "";


    string NFH_Date_P_Date = "";
    DateTime date1;
    DateTime date2;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    string WagesValue;
    string oldWages;
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Payroll Attendance ";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            oldWages = Request.QueryString["Wages"].ToString();
            if (oldWages == "FITTER _ ELECTRICIANS")
            {
                WagesValue = oldWages.Replace('_', '&');
            }
            else
            {
                WagesValue = Request.QueryString["Wages"].ToString();
            }

            Division = Request.QueryString["Division"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["Todate"].ToString();

            mEmployeeDT.Columns.Add("Machine ID");
            mEmployeeDT.Columns.Add("Token No");
            mEmployeeDT.Columns.Add("EmpName");
            mEmployeeDT.Columns.Add("Days");
            mEmployeeDT.Columns.Add("H.Allowed");
            mEmployeeDT.Columns.Add("N/FH");
            mEmployeeDT.Columns.Add("Manual OT");
            mEmployeeDT.Columns.Add("SPG Allow");
            mEmployeeDT.Columns.Add("Canteen Days Minus");
            mEmployeeDT.Columns.Add("OT Hours");
            mEmployeeDT.Columns.Add("W.H");
            mEmployeeDT.Columns.Add("Fixed W.Days");
            mEmployeeDT.Columns.Add("NFH W.Days");
            mEmployeeDT.Columns.Add("Total Month Days");
            mEmployeeDT.Columns.Add("NFH Worked Days");
            mEmployeeDT.Columns.Add("NFH D.W Statutory");
            mEmployeeDT.Columns.Add("AEH");
            mEmployeeDT.Columns.Add("LBH");
            mEmployeeDT.Columns.Add("Half Night");
            mEmployeeDT.Columns.Add("Full Night");

            if (SessionUserType == "2")
            {
                NonAdminPayrollAttn();
            }
            else
            {
                PayrollAttn();
            }
        }
    }
    public void NonAdminPayrollAttn()
    {

        try
        {
            int intI;
            SSQL = "";
            SSQL = "Select LTD.MachineID,LTD.ExistingCode,EM.FirstName,sum(LTD.Present) as Days,";
            SSQL = SSQL + " EM.DOJ,EM.Weekoff from LogTime_Days LTD inner join Employee_Mst EM on";
            SSQL = SSQL + " EM.CompCode=LTD.CompCode And EM.LocCode=LTD.LocCode And LTD.MachineID=EM.MachineID";
            SSQL = SSQL + " where LTD.CompCode='" + SessionCcode + "' And LTD.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.Wages='" + WagesType + "' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103))";
            SSQL = SSQL + " and CONVERT(DATETIME,LTD.Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And  CONVERT(DATETIME,LTD.Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
            SSQL = SSQL + " And LTD.Present!='0.0' and EM.Eligible_PF='1' ";
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And EM.Division = '" + Division + "'";
            }
            //If Trim(txtDivision.Text) <> "" Then SSQL = SSQL + " And EM.Division = '" + txtDivision.Text + "'";

            SSQL = SSQL + " Group by LTD.MachineID,LTD.ExistingCode,EM.FirstName,EM.DOJ,EM.Weekoff ";
            SSQL = SSQL + " Order by LTD.ExistingCode Asc";

            Log_DS = objdata.RptEmployeeMultipleDetails(SSQL);

            intI = 2;
            for (int i = 0; i < Log_DS.Rows.Count; i++)
            {

                // 'Final Output Variable Declaration
                NFH_Days_Count = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;
                NFH_Days_Present_Count = 0;
                WH_Count = 0;
                WH_Present_Count = 0;
                Present_Days_Count = Convert.ToDecimal(Log_DS.Rows[i]["Days"]);
                Fixed_Work_Days = 0;
                Spinning_Incentive_Days = 0;

                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;
                NFH_Single_Wages = 0;

                NFH_Type_Str = "";
                NFH_Name_Get_Str = "";
                NFH_Dbl_Wages_Statutory_Check = "";
                Emp_WH_Day = "";
                DOJ_Date_Str = "";

                DOJ_Date_Str = Log_DS.Rows[i]["DOJ"].ToString();
                Emp_WH_Day = Log_DS.Rows[i]["WeekOff"].ToString();

                //  'NFH Check Start
                string Machineid = Log_DS.Rows[i]["MachineID"].ToString();
                if (Machineid == "1886")
                {
                    Machineid = "1886";
                }

                qry_nfh = "Select NFHDate from NFH_Mst where  CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103) ";
                qry_nfh = qry_nfh + " And  CONVERT(DATETIME,DateStr,103) <=  CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                NFH_DS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                if (NFH_DS.Rows.Count > 0)
                {

                    //  'Date OF Joining Check Start
                    for (int k = 0; k < NFH_DS.Rows.Count; k++)
                    {
                        NFH_Date = Convert.ToDateTime(NFH_DS.Rows[k]["NFHDate"]);
                        // 'NFH Day Present Check

                        NFH_Date_P_Date = String.Format(Convert.ToString(NFH_Date), "yyyy/MM/dd");

                        SSQL = "";
                        SSQL = "Select * from LogTime_Days where Attn_Date=CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_P_Date).ToString("dd/MM/yyyy") + "',103)  ";
                        SSQL = SSQL + "  And MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count != 0)
                        {

                            NFH_Present_Check = Convert.ToDouble(mDataSet.Rows[0]["Present"]);
                        }
                        else
                        {

                            NFH_Present_Check = 0;

                        }
                        //'Get NFh Type

                        SSQL = "Select * from NFH_Mst where CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_P_Date).ToString("dd/MM/yyyy") + "',103) ";
                        NFH_Type_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (NFH_Type_Ds.Rows.Count != 0)
                        {

                            NFH_Type_Str = NFH_Type_Ds.Rows[0]["NFH_Type"].ToString();
                            NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            NFH_Name_Get_Str = NFH_Type_Ds.Rows[0]["Form25DisplayText"].ToString();

                        }
                        else
                        {
                            NFH_Type_Str = "";
                            NFH_Dbl_Wages_Statutory_Check = "";
                        }
                        if (DOJ_Date_Str != "")
                        {

                            DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (DOJ_Date_Format.Date < NFH_Date.Date)
                            {
                                if (NFH_Type_Str == "WH Minus")
                                {
                                    NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;
                                }
                                else
                                {

                                    NFH_Days_Present_Count = NFH_Days_Present_Count + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Type_Str == "Double Wages Checklist")
                                    {

                                        NFH_Double_Wages_Checklist = NFH_Double_Wages_Checklist + Convert.ToDecimal(NFH_Present_Check);

                                        if (NFH_Dbl_Wages_Statutory_Check == "Yes")
                                        {
                                            NFH_Double_Wages_Statutory = NFH_Double_Wages_Statutory + Convert.ToDecimal(NFH_Present_Check);

                                        }
                                    }

                                    if (NFH_Type_Str == "Double Wages Manual")
                                    {
                                        NFH_Double_Wages_Manual = NFH_Double_Wages_Manual + Convert.ToDecimal(NFH_Present_Check);
                                    }

                                    NFH_Days_Count = NFH_Days_Count + 1;
                                    if (NFH_Name_Get_Str == "AEH")
                                    {
                                        AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;
                                    }
                                    if (NFH_Name_Get_Str == "LBH")
                                    {
                                        LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;
                                    }

                                }
                            }
                        }




                        else
                        {
                            NFH_Days_Count = NFH_Days_Count + 1;

                            if (NFH_Name_Get_Str == "AEH")
                            {
                                AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;

                            }
                            if (NFH_Name_Get_Str == "LBH")
                            {
                                LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;

                            }

                            if (NFH_Type_Str == "WH Minus")
                            {
                                NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;
                            }
                            else
                            {
                                NFH_Days_Present_Count = NFH_Days_Present_Count + Convert.ToDecimal(NFH_Present_Check);

                                if (NFH_Type_Str == "Double Wages Checklist")
                                {
                                    NFH_Double_Wages_Checklist = NFH_Double_Wages_Checklist + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Dbl_Wages_Statutory_Check == "Yes")
                                    {
                                        NFH_Double_Wages_Statutory = NFH_Double_Wages_Statutory + Convert.ToDecimal(NFH_Present_Check);
                                    }
                                }
                                if (NFH_Type_Str == "Double Wages Manual")
                                {
                                    NFH_Double_Wages_Manual = NFH_Double_Wages_Manual + Convert.ToDecimal(NFH_Present_Check);
                                }
                            }



                        }
                    }
                }// NFH Check END


                //'Week of Check

                Check_Week_Off_DOJ = false;

                SSQL = "Select Present,Attn_Date from LogTime_Days where MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And upper(DATENAME(weekday,Attn_Date))=upper('" + Emp_WH_Day + "')";
                SSQL = SSQL + " And Attn_Date >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And Attn_Date <=CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                WH_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < WH_DS.Rows.Count; j++)
                {

                    Week_Off_Date = Convert.ToDateTime(WH_DS.Rows[j]["Attn_Date"]);
                    if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                    {
                        if (DOJ_Date_Str != "")
                        {

                            WH_DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (WH_DOJ_Date_Format.Date <= Week_Off_Date.Date)
                            {

                                Check_Week_Off_DOJ = true;
                            }
                            else
                            {
                                Check_Week_Off_DOJ = false;
                            }
                        }
                        else
                        {
                            Check_Week_Off_DOJ = true;
                        }
                    }
                    else
                    {
                        Check_Week_Off_DOJ = true;
                    }

                    if (Check_Week_Off_DOJ == true)
                    {

                        WH_Count = WH_Count + 1;
                        //'NFH Day Check
                        // String.Format(Convert.ToString(dtime), "yyyy/MM/dd");

                        string NFH_Date_WH_Date = Convert.ToDateTime(Week_Off_Date).AddDays(0).ToString("yyyy/MM/dd");
                        SSQL = "Select * from NFH_Mst where CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_WH_Date).ToString("dd/MM/yyyy") + "',103) ";
                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count > 0)
                        {
                            if (mDataSet.Rows.Count != 0)
                            {
                                NFH_Type_Str = mDataSet.Rows[0]["NFH_Type"].ToString();
                                NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            }
                            else
                            {
                                NFH_Type_Str = "";
                                NFH_Dbl_Wages_Statutory_Check = "";
                            }
                            if (NFH_Type_Str == "WH Minus")
                            {
                                WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"].ToString());
                            }


                        }
                        else
                        {
                            WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"].ToString());
                        }
                    }
                }




                //'Spinning Incentive Check
                if (WagesValue.ToUpper() == "REGULAR".ToUpper() || WagesValue.ToUpper() == "HOSTEL".ToUpper())
                {
                    //'Check Spinning Days Start
                    Boolean Check_Spinning_Eligible = false;
                    DataTable DS_Spinning_Incv = new DataTable();
                    // 'Spinning Wages Check
                    int Month_Int = 1;
                    string Months_Full_Str = "";
                    string Fin_Year = "";
                    string Spin_Wages = WagesValue;
                    string Spin_Machine_ID_Str = Log_DS.Rows[i]["MachineID"].ToString();

                    string[] date = FromDate.Split('/');
                    string mon = date[1];
                    Month_Int = Convert.ToInt32(mon.ToString());
                    if (Month_Int >= 4)
                    {
                        Fin_Year = date[2] + "-" + (Convert.ToInt32(date[2]) + 1);
                    }
                    else
                    {
                        Fin_Year = (Convert.ToInt32(date[2]) - 1) + "-" + date[2];
                    }


                    if (Month_Int == 01)
                    {
                        Months_Full_Str = "January";
                    }

                    else if (Month_Int == 02)
                    {
                        Months_Full_Str = "February";
                    }
                    else if (Month_Int == 03)
                    {
                        Months_Full_Str = "March";
                    }
                    else if (Month_Int == 04)
                    {
                        Months_Full_Str = "April";
                    }
                    else if (Month_Int == 05)
                    {
                        Months_Full_Str = "May";
                    }
                    else if (Month_Int == 06)
                    {
                        Months_Full_Str = "June";
                    }
                    else if (Month_Int == 07)
                    {
                        Months_Full_Str = "July";
                    }
                    else if (Month_Int == 08)
                    {
                        Months_Full_Str = "August";
                    }
                    else if (Month_Int == 09)
                    {
                        Months_Full_Str = "September";
                    }
                    else if (Month_Int == 10)
                    {
                        Months_Full_Str = "October";
                    }
                    else if (Month_Int == 11)
                    {
                        Months_Full_Str = "November";
                    }
                    else if (Month_Int == 12)
                    {
                        Months_Full_Str = "December";
                    }

                    Months_Full_Str = Left_Val(Months_Full_Str, 3);
                    SSQL = "Select * from SpinIncentiveDetPart where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Months='" + Months_Full_Str + "' And FinYear='" + Fin_Year + "' And Wages='" + Spin_Wages + "' And MachineID='" + Spin_Machine_ID_Str + "'";
                    DS_Spinning_Incv = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DS_Spinning_Incv.Rows.Count != 0)
                    {
                        Check_Spinning_Eligible = true;
                    }
                    else
                    {
                        Check_Spinning_Eligible = false;
                    }
                    if (Check_Spinning_Eligible == true)
                    {
                        if (WagesValue.ToUpper() == "HOSTEL".ToUpper())
                        {
                            Spinning_Incentive_Days = Convert.ToDecimal(Log_DS.Rows[i]["Days"].ToString());
                        }
                        else
                        {
                            if (WagesValue.ToUpper() == "REGULAR".ToUpper())
                            {
                                SSQL = "Select isnull(sum(Present),0) as Days from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And MachineID='" + Log_DS.Rows[i]["MachineID"] + "'";
                                SSQL = SSQL + " And Attn_Date >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                                SSQL = SSQL + " And Attn_Date <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                                SSQL = SSQL + " And (Shift='SHIFT2' or Shift='SHIFT3')";
                                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                                if (mDataSet.Rows.Count != 0)
                                {
                                    Spinning_Incentive_Days = Convert.ToDecimal(mDataSet.Rows[0]["Days"].ToString());
                                }
                                else
                                {
                                    Spinning_Incentive_Days = 0;
                                }
                            }
                            else
                            {
                                Spinning_Incentive_Days = 0;
                            }

                        }

                    }

                    else
                    {
                        Spinning_Incentive_Days = 0;

                    }
                }
                else
                {
                    Spinning_Incentive_Days = 0;

                }



                //'Total Days Get

                date1 = Convert.ToDateTime(FromDate);
                // string dd =string.Format("{dd/MM/yyyy}",TxtTo.Text);
                date2 = Convert.ToDateTime(ToDate);
                int dayCount = (int)((date2 - date1).TotalDays);
                int Total_Days_Count = dayCount;
                Total_Days_Count = Total_Days_Count + 1;
                Month_Mid_Total_Days_Count = 0;

                // 'Check DOJ Date to Report Date
                DateTime Report_Date = new DateTime();
                DateTime DOJ_Date_Format_Check = new DateTime();
                if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                {
                    if (DOJ_Date_Str != "")
                    {
                        Report_Date = Convert.ToDateTime(FromDate.ToString()); //'Report Date
                        DOJ_Date_Format_Check = Convert.ToDateTime(DOJ_Date_Str);
                        if (DOJ_Date_Format_Check <= Report_Date)
                        {
                            Month_Mid_Total_Days_Count = Total_Days_Count;
                        }
                        else
                        {
                            Month_Mid_Total_Days_Count = dayCount;
                            Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                        }

                    }
                    else
                    {
                        Month_Mid_Total_Days_Count = Total_Days_Count;
                    }

                }
                else
                {

                }

                if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                {
                    Fixed_Work_Days = Month_Mid_Total_Days_Count - Convert.ToInt32(WH_Count);
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count + NFH_Days_Present_Count;
                }
                else
                {
                    Fixed_Work_Days = Month_Mid_Total_Days_Count;
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count;
                }
                if (Present_Days_Count != 0 && NFH_Days_Count != 0 && NFH_WH_Days_Mins != 0)
                {
                    double NFH_Week_Off_Minus_Days = Convert.ToDouble(NFH_WH_Days_Mins);
                    double NFH_Final_Disp_Days = 0;
                    if (NFH_Week_Off_Minus_Days <= Convert.ToDouble(WH_Present_Count))
                    {
                        WH_Present_Count = WH_Present_Count - Convert.ToDecimal(NFH_Week_Off_Minus_Days);
                    }
                    else
                    {
                        // 'NFH Days Minus for Working Days
                        double Balance_NFH_Days_Minus_IN_WorkDays = 0;

                        Balance_NFH_Days_Minus_IN_WorkDays = NFH_Week_Off_Minus_Days - Convert.ToDouble(WH_Present_Count);
                        Present_Days_Count = Present_Days_Count - Convert.ToDecimal(Balance_NFH_Days_Minus_IN_WorkDays);
                        WH_Present_Count = 0;
                    }
                }



                string NFH_Doubale_Wages_Attn_Inct = (NFH_Double_Wages_Checklist + NFH_Double_Wages_Manual + NFH_WH_Days_Mins).ToString();

                if (Present_Days_Count != 0)
                {


                    mEmployeeDT.NewRow();
                    mEmployeeDT.Rows.Add();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Machine ID"] = Log_DS.Rows[i]["MachineID"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Token No"] = Log_DS.Rows[i]["ExistingCode"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["EmpName"] = Log_DS.Rows[i]["FirstName"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Days"] = Present_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["H.Allowed"] = "0";
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["N/FH"] = NFH_Days_Count;

                    if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                    {
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Manual OT"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["SPG Allow"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Canteen Days Minus"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Hours"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["W.H"] = WH_Present_Count;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Fixed W.Days"] = Fixed_Work_Days;
                    }
                    else
                    {
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Manual OT"] = WH_Present_Count;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["SPG Allow"] = Spinning_Incentive_Days;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Canteen Days Minus"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Hours"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["W.H"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Fixed W.Days"] = "0";
                    }



                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH W.Days"] = NFH_Double_Wages_Checklist;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Total Month Days"] = Month_Mid_Total_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH Worked Days"] = NFH_Doubale_Wages_Attn_Inct;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH D.W Statutory"] = NFH_Double_Wages_Statutory;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AEH"] = AEH_NFH_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["LBH"] = LBH_NFH_Days_Count;
                    intI = intI + 1;
                }



                NFH_Days_Count = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;
                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;


            }


        }




        catch (Exception ex)
        {

        }


        UploadDataTableToExcel(mEmployeeDT);

    }

    public void PayrollAttn()
    {

        try
        {
            int intI;
            SSQL = "";
            SSQL = "Select LTD.MachineID,LTD.ExistingCode,EM.FirstName,sum(LTD.Present) as Days,Sum(LTD.Wh_Present_Count) as WH, '0' as HalfNight,";
            SSQL = SSQL + " '0' as FullNight, EM.DOJ,EM.Weekoff from LogTime_Days LTD inner join Employee_Mst EM on";
            SSQL = SSQL + " EM.CompCode=LTD.CompCode And EM.LocCode=LTD.LocCode And LTD.MachineID=EM.MachineID";
            SSQL = SSQL + " where LTD.CompCode='" + SessionCcode + "' And LTD.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.Wages='" + WagesValue + "' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103))";
            SSQL = SSQL + " and CONVERT(DATETIME,LTD.Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,LTD.Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
            SSQL = SSQL + " And LTD.Present!='0.0'";

            if (Division != "-Select-")
            {
                SSQL = SSQL + " And EM.Division = '" + Division + "'";
            }
            //If Trim(txtDivision.Text) <> "" Then SSQL = SSQL + " And EM.Division = '" + txtDivision.Text + "'";

            SSQL = SSQL + " Group by LTD.MachineID,LTD.ExistingCode,EM.FirstName,EM.DOJ,EM.Weekoff ";
            SSQL = SSQL + " Order by cast(LTD.ExistingCode as int) Asc";

            Log_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            //Check OT Below 8 houes
            
            intI = 2;
            for (int i = 0; i < Log_DS.Rows.Count; i++)
            {

                string Machineid = Log_DS.Rows[i]["MachineID"].ToString();

                SSQL = "";
                SSQL = "select sum(CASE when Shift='SHIFT3' then present else '0' end) as FullNight,sum(CASE when Shift='SHIFT2' then (present) else '0' end) as HalfNight";
                SSQL = SSQL + " from Logtime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + Machineid + "'";
                SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                SSQL = SSQL + " And Present!='0.0'";
                SSQL = SSQL + " Group by MachineID, Shift ";
                DataTable dt_FullHalfNight = new DataTable();
                dt_FullHalfNight = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_FullHalfNight.Rows.Count > 0)
                {
                    for(int k = 0; k < dt_FullHalfNight.Rows.Count; k++)
                    {
                        Log_DS.Rows[i]["HalfNight"] = (Convert.ToDecimal(Log_DS.Rows[i]["HalfNight"]) + Convert.ToDecimal(dt_FullHalfNight.Rows[k]["HalfNight"]));
                        Log_DS.Rows[i]["FullNight"] = (Convert.ToDecimal(Log_DS.Rows[i]["FullNight"]) + Convert.ToDecimal(dt_FullHalfNight.Rows[k]["FullNight"]));
                    }
                }

                if (Machineid == "505")
                {
                    string StopQuery = "";
                }

                // 'Final Output Variable Declaration
                NFH_Days_Count = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;
                NFH_Days_Present_Count = 0;
                WH_Count = 0;
                WH_Present_Count = 0;
                Present_Days_Count = Convert.ToDecimal(Log_DS.Rows[i]["Days"]);


                SSQL = "";
                SSQL = "select OTEligible,Wages from Employee_Mst where MachineID='" + Log_DS.Rows[i]["MachineID"].ToString() + "'";
                dt_OT_Eligible = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_OT_Eligible.Rows.Count > 0)
                {
                    OT_Eligible = dt_OT_Eligible.Rows[0]["OTEligible"].ToString();
                    Wages_Type = dt_OT_Eligible.Rows[0]["Wages"].ToString();
                }

                SSQL = "";
                SSQL = SSQL + "select isnull(sum(NoHrs), '0') as NoHrs from [" + SessionEpay + "]..OverTime where EmpNo='" + Log_DS.Rows[i]["MachineID"].ToString() + "' ";
                SSQL = SSQL + " and CONVERT(DATETIME,TransDate,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,TransDate,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                
                if (dt_OT.Rows.Count > 0) { OT_Above_Eight = dt_OT.Rows[0]["NoHrs"].ToString(); } else { OT_Above_Eight = "0"; }
                OT_Total_Hours = (Convert.ToDecimal(OT_Above_Eight)).ToString();

                OT_Hours = OT_Total_Hours;

                //if (OT_Eligible.Trim() == "1")
                //{
                //    //OT Hours Above working 8 hours 

                    


                //    //if ((Wages_Type == "STAFF") || (Wages_Type == "FITTER & ELECTRICIANS"))
                //    //{
                //    //    // Aboue 9 Hours
                //    //    SSQL = "";
                //    //    SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(9 as decimal(18,1)))), '0') as OT_8Hours ";
                //    //    SSQL = SSQL + " from LogTime_Days ";
                //    //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                //    //    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    //    SSQL = SSQL + " And Present ='1' and MachineID='" + Log_DS.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 9";
                //    //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    //    if (dt_OT.Rows.Count > 0) { OT_Above_Eight = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Eight = "0"; }


                //    //    // Aboue 4 Hours and Below 8 Hours
                //    //    SSQL = "";
                //    //    SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(4 as decimal(18,1)))), '0') as OT_8Hours ";
                //    //    SSQL = SSQL + " from LogTime_Days ";
                //    //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                //    //    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    //    SSQL = SSQL + " And (Present ='1' or Present ='0.5') and MachineID='" + Log_DS.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 4 and Total_Hrs < 7";
                //    //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    //    if (dt_OT.Rows.Count > 0) { OT_Above_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Four = "0"; }


                //    //    // Below 4
                //    //    SSQL = "";
                //    //    SSQL = "Select isnull(SUM(CAST(Total_Hrs as decimal(18,1))), '0') as OT_8Hours ";
                //    //    SSQL = SSQL + " from LogTime_Days ";
                //    //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                //    //    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    //    SSQL = SSQL + " And TypeName!='Leave' and MachineID='" + Log_DS.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs < 4 ";
                //    //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    //    //if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }


                //    //    if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }
                //    //}
                //    //else if ((Wages_Type == "SECURITY") || (Wages_Type == "DRIVERS"))
                //    //{
                //    //    // Aboue 12 Hours
                //    //    SSQL = "";
                //    //    SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(12 as decimal(18,1)))), '0') as OT_8Hours ";
                //    //    SSQL = SSQL + " from LogTime_Days ";
                //    //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                //    //    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    //    SSQL = SSQL + " And Present ='1' and MachineID='" + Log_DS.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 13";
                //    //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    //    if (dt_OT.Rows.Count > 0) { OT_Above_Eight = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Eight = "0"; }


                //    //    // Aboue 4 Hours and Below 8 Hours
                //    //    SSQL = "";
                //    //    SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(6 as decimal(18,1)))), '0') as OT_8Hours ";
                //    //    SSQL = SSQL + " from LogTime_Days ";
                //    //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                //    //    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    //    SSQL = SSQL + " And (Present ='1' or Present ='0.5') and MachineID='" + Log_DS.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 6 and Total_Hrs < 12";
                //    //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    //    if (dt_OT.Rows.Count > 0) { OT_Above_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Four = "0"; }


                //    //    // Below 4
                //    //    SSQL = "";
                //    //    SSQL = "Select isnull(SUM(CAST(Total_Hrs as decimal(18,1))), '0') as OT_8Hours ";
                //    //    SSQL = SSQL + " from LogTime_Days ";
                //    //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                //    //    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    //    SSQL = SSQL + " And TypeName!='Leave' and MachineID='" + Log_DS.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs < 6 ";
                //    //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    //    //if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }


                //    //    if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }
                //    //}
                //    //else
                //    //{
                //    //    // Aboue 8 Hours
                //    //    SSQL = "";
                //    //    SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(8 as decimal(18,1)))), '0') as OT_8Hours ";
                //    //    SSQL = SSQL + " from LogTime_Days ";
                //    //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                //    //    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    //    SSQL = SSQL + " And Present ='1' and MachineID='" + Log_DS.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 8";
                //    //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    //    if (dt_OT.Rows.Count > 0) { OT_Above_Eight = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Eight = "0"; }


                //    //    // Aboue 4 Hours and Below 8 Hours
                //    //    SSQL = "";
                //    //    SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(4 as decimal(18,1)))), '0') as OT_8Hours ";
                //    //    SSQL = SSQL + " from LogTime_Days ";
                //    //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                //    //    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    //    SSQL = SSQL + " And (Present ='1' or Present ='0.5') and MachineID='" + Log_DS.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs > 4 and Total_Hrs < 7";
                //    //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    //    if (dt_OT.Rows.Count > 0) { OT_Above_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Four = "0"; }


                //    //    // Below 4
                //    //    SSQL = "";
                //    //    SSQL = "Select isnull(SUM(CAST(Total_Hrs as decimal(18,1))), '0') as OT_8Hours ";
                //    //    SSQL = SSQL + " from LogTime_Days ";
                //    //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                //    //    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    //    SSQL = SSQL + " And TypeName!='Leave' and MachineID='" + Log_DS.Rows[i]["MachineID"].ToString() + "' and  Total_Hrs < 4 ";
                //    //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    //    if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }
                //    //}

                //    ////Get Manual OT
                //    //SSQL = "";
                //    //SSQL = SSQL + "select isnull(Sum(OTHrs),'0') as OTHrs from Weekly_OTHours where MachineID='" + Machineid + "'  ";
                //    //SSQL = SSQL + " and CONVERT(DATETIME,OTDate_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                //    //SSQL = SSQL + " And CONVERT(DATETIME,OTDate_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    //dt_Manula_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    //if (dt_Manula_OT.Rows.Count > 0) { Manul_OT = dt_Manula_OT.Rows[0]["OTHrs"].ToString(); } else { Manul_OT = "0"; }


                //    //OT_Total_Hours = (Convert.ToDecimal(OT_Above_Eight) + Convert.ToDecimal(OT_Above_Four) + Convert.ToDecimal(OT_Below_Four)).ToString();

                //    //OT_Hours = OT_Total_Hours;
                //    //if (dt_OT.Rows.Count > 0)
                //    //{
                //    //    OT_Hours = dt_OT.Rows[0]["OT_8Hours"].ToString();
                //    //}
                //    //else
                //    //{
                //    //    OT_Hours = "0";
                //    //}
                //}
                //else
                //{
                //    OT_Hours = "0";
                //}

                

                //OT_Hours = "0";


                
                Fixed_Work_Days = 0;
                Spinning_Incentive_Days = 0;

                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;
                NFH_Single_Wages = 0;

                NFH_Type_Str = "";
                NFH_Name_Get_Str = "";
                NFH_Dbl_Wages_Statutory_Check = "";
                Emp_WH_Day = "";
                DOJ_Date_Str = "";

                DOJ_Date_Str = Log_DS.Rows[i]["DOJ"].ToString();
                Emp_WH_Day = Log_DS.Rows[i]["WeekOff"].ToString();

                //  'NFH Check Start
                

                qry_nfh = "Select NFHDate from NFH_Mst where  CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103) ";
                qry_nfh = qry_nfh + " And  CONVERT(DATETIME,DateStr,103) <=  CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                NFH_DS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                if (NFH_DS.Rows.Count > 0)
                {

                    //  'Date OF Joining Check Start
                    for (int k = 0; k < NFH_DS.Rows.Count; k++)
                    {
                        NFH_Date = Convert.ToDateTime(NFH_DS.Rows[k]["NFHDate"]);
                        // 'NFH Day Present Check

                        NFH_Date_P_Date = String.Format(Convert.ToString(NFH_Date), "yyyy/MM/dd");

                        SSQL = "";
                        SSQL = "Select * from LogTime_Days where Attn_Date=CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_P_Date).ToString("dd/MM/yyyy") + "',103)  ";
                        SSQL = SSQL + "  And MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count != 0)
                        {

                            NFH_Present_Check = Convert.ToDouble(mDataSet.Rows[0]["Present"]);
                        }
                        else
                        {

                            NFH_Present_Check = 0;

                        }
                        //'Get NFh Type

                        SSQL = "Select * from NFH_Mst where CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_P_Date).ToString("dd/MM/yyyy") + "',103) ";
                        NFH_Type_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (NFH_Type_Ds.Rows.Count != 0)
                        {

                            NFH_Type_Str = NFH_Type_Ds.Rows[0]["NFH_Type"].ToString();
                            NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            NFH_Name_Get_Str = NFH_Type_Ds.Rows[0]["Form25DisplayText"].ToString();

                        }
                        else
                        {
                            NFH_Type_Str = "";
                            NFH_Dbl_Wages_Statutory_Check = "";
                        }
                        if (DOJ_Date_Str != "")
                        {

                            DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (DOJ_Date_Format.Date < NFH_Date.Date)
                            {
                                if (NFH_Type_Str == "WH Minus")
                                {
                                    NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;

                                }
                                else
                                {

                                    NFH_Days_Present_Count = NFH_Days_Present_Count + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Type_Str == "Double Wages Checklist")
                                    {

                                        NFH_Double_Wages_Checklist = NFH_Double_Wages_Checklist + Convert.ToDecimal(NFH_Present_Check);

                                        if (NFH_Dbl_Wages_Statutory_Check == "Yes")
                                        {
                                            NFH_Double_Wages_Statutory = NFH_Double_Wages_Statutory + Convert.ToDecimal(NFH_Present_Check);

                                        }
                                    }

                                    if (NFH_Type_Str == "Double Wages Manual")
                                    {
                                        NFH_Double_Wages_Manual = NFH_Double_Wages_Manual + Convert.ToDecimal(NFH_Present_Check);
                                    }

                                    NFH_Days_Count = NFH_Days_Count + 1;
                                    if (NFH_Name_Get_Str == "AEH")
                                    {
                                        AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;
                                    }
                                    if (NFH_Name_Get_Str == "LBH")
                                    {
                                        LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;
                                    }

                                }
                            }
                        }
                        else
                        {
                            NFH_Days_Count = NFH_Days_Count + 1;

                            if (NFH_Name_Get_Str == "AEH")
                            {
                                AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;

                            }
                            if (NFH_Name_Get_Str == "LBH")
                            {
                                LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;

                            }

                            if (NFH_Type_Str == "WH Minus")
                            {
                                NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;
                            }
                            else
                            {
                                NFH_Days_Present_Count = NFH_Days_Present_Count + Convert.ToDecimal(NFH_Present_Check);

                                if (NFH_Type_Str == "Double Wages Checklist")
                                {
                                    NFH_Double_Wages_Checklist = NFH_Double_Wages_Checklist + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Dbl_Wages_Statutory_Check == "Yes")
                                    {
                                        NFH_Double_Wages_Statutory = NFH_Double_Wages_Statutory + Convert.ToDecimal(NFH_Present_Check);
                                    }
                                }
                                if (NFH_Type_Str == "Double Wages Manual")
                                {
                                    NFH_Double_Wages_Manual = NFH_Double_Wages_Manual + Convert.ToDecimal(NFH_Present_Check);
                                }
                            }
                        }
                    }
                }// NFH Check END


                //'Week of Check

                Check_Week_Off_DOJ = false;

                SSQL = "Select Present,Attn_Date from LogTime_Days where MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And upper(DATENAME(weekday,Attn_Date))=upper('" + Emp_WH_Day + "')";
                SSQL = SSQL + " And Attn_Date >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And Attn_Date <=CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                WH_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < WH_DS.Rows.Count; j++)
                {

                    Week_Off_Date = Convert.ToDateTime(WH_DS.Rows[j]["Attn_Date"]);
                    if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                    {
                        if (DOJ_Date_Str != "")
                        {

                            WH_DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (WH_DOJ_Date_Format.Date <= Week_Off_Date.Date)
                            {

                                Check_Week_Off_DOJ = true;
                            }
                            else
                            {
                                Check_Week_Off_DOJ = false;
                            }
                        }
                        else
                        {
                            Check_Week_Off_DOJ = true;
                        }
                    }
                    else
                    {
                        Check_Week_Off_DOJ = true;
                    }

                    if (Check_Week_Off_DOJ == true)
                    {

                        WH_Count = WH_Count + 1;
                        //'NFH Day Check
                        // String.Format(Convert.ToString(dtime), "yyyy/MM/dd");
                        
                        string NFH_Date_WH_Date = Convert.ToDateTime(Week_Off_Date).AddDays(0).ToString("yyyy/MM/dd");
                        SSQL = "Select * from NFH_Mst where CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_WH_Date).ToString("dd/MM/yyyy") + "',103) ";
                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count > 0)
                        {
                            if (mDataSet.Rows.Count != 0)
                            {
                                NFH_Type_Str = mDataSet.Rows[0]["NFH_Type"].ToString();
                                NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            }
                            else
                            {
                                NFH_Type_Str = "";
                                NFH_Dbl_Wages_Statutory_Check = "";
                            }
                            if (NFH_Type_Str == "WH Minus")
                            {
                                WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"].ToString());
                            }


                        }
                        else
                        {
                            WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"].ToString());
                        }
                    }
                }

                WH_Present_Count = Convert.ToDecimal(Log_DS.Rows[i]["WH"].ToString());
                
                //'Spinning Incentive Check
                if (WagesValue.ToUpper() == "REGULAR".ToUpper() || WagesValue.ToUpper() == "HOSTEL".ToUpper())
                {
                    //'Check Spinning Days Start
                    Boolean Check_Spinning_Eligible = false;
                    DataTable DS_Spinning_Incv = new DataTable();
                    // 'Spinning Wages Check
                    int Month_Int = 1;
                    string Months_Full_Str = "";
                    string Fin_Year = "";
                    string Spin_Wages = WagesType;
                    string Spin_Machine_ID_Str = Log_DS.Rows[i]["MachineID"].ToString();

                    string[] date = FromDate.Split('/');
                    string mon = date[1];
                    Month_Int = Convert.ToInt32(mon.ToString());
                    if (Month_Int >= 4)
                    {
                        Fin_Year = date[2] + "-" + (Convert.ToInt32(date[2]) + 1);
                    }
                    else
                    {
                        Fin_Year = (Convert.ToInt32(date[2]) - 1) + "-" + date[2];
                    }


                    if (Month_Int == 01)
                    {
                        Months_Full_Str = "January";
                    }

                    else if (Month_Int == 02)
                    {
                        Months_Full_Str = "February";
                    }
                    else if (Month_Int == 03)
                    {
                        Months_Full_Str = "March";
                    }
                    else if (Month_Int == 04)
                    {
                        Months_Full_Str = "April";
                    }
                    else if (Month_Int == 05)
                    {
                        Months_Full_Str = "May";
                    }
                    else if (Month_Int == 06)
                    {
                        Months_Full_Str = "June";
                    }
                    else if (Month_Int == 07)
                    {
                        Months_Full_Str = "July";
                    }
                    else if (Month_Int == 08)
                    {
                        Months_Full_Str = "August";
                    }
                    else if (Month_Int == 09)
                    {
                        Months_Full_Str = "September";
                    }
                    else if (Month_Int == 10)
                    {
                        Months_Full_Str = "October";
                    }
                    else if (Month_Int == 11)
                    {
                        Months_Full_Str = "November";
                    }
                    else if (Month_Int == 12)
                    {
                        Months_Full_Str = "December";
                    }

                    Months_Full_Str = Left_Val(Months_Full_Str, 3);
                    SSQL = "Select * from SpinIncentiveDetPart where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Months='" + Months_Full_Str + "' And FinYear='" + Fin_Year + "' And Wages='" + Spin_Wages + "' And MachineID='" + Spin_Machine_ID_Str + "'";
                    DS_Spinning_Incv = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DS_Spinning_Incv.Rows.Count != 0)
                    {
                        Check_Spinning_Eligible = true;
                    }
                    else
                    {
                        Check_Spinning_Eligible = false;
                    }
                    if (Check_Spinning_Eligible == true)
                    {
                        if (WagesValue.ToUpper() == "HOSTEL".ToUpper())
                        {
                            Spinning_Incentive_Days = Convert.ToDecimal(Log_DS.Rows[i]["Days"].ToString());
                        }
                        else
                        {
                            if (WagesValue.ToUpper() == "REGULAR".ToUpper())
                            {
                                SSQL = "Select isnull(sum(Present),0) as Days from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And MachineID='" + Log_DS.Rows[i]["MachineID"] + "'";
                                SSQL = SSQL + " And Attn_Date >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                                SSQL = SSQL + " And Attn_Date <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                                SSQL = SSQL + " And (Shift='SHIFT2' or Shift='SHIFT3')";
                                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                                if (mDataSet.Rows.Count != 0)
                                {
                                    Spinning_Incentive_Days = Convert.ToDecimal(mDataSet.Rows[0]["Days"].ToString());
                                }
                                else
                                {
                                    Spinning_Incentive_Days = 0;
                                }
                            }
                            else
                            {
                                Spinning_Incentive_Days = 0;
                            }

                        }

                    }

                    else
                    {
                        Spinning_Incentive_Days = 0;

                    }
                }
                else
                {
                    Spinning_Incentive_Days = 0;

                }

                //'Total Days Get

                date1 = Convert.ToDateTime(FromDate);
                // string dd =string.Format("{dd/MM/yyyy}",TxtTo.Text);
                date2 = Convert.ToDateTime(ToDate);
                int dayCount = (int)((date2 - date1).TotalDays);
                int Total_Days_Count = dayCount;
                Total_Days_Count = Total_Days_Count + 1;
                Month_Mid_Total_Days_Count = 0;

                // 'Check DOJ Date to Report Date
                DateTime Report_Date = new DateTime();
                DateTime DOJ_Date_Format_Check = new DateTime();
                if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                {
                    if (DOJ_Date_Str != "")
                    {
                        Report_Date = Convert.ToDateTime(FromDate.ToString()); //'Report Date
                        DOJ_Date_Format_Check = Convert.ToDateTime(DOJ_Date_Str);
                        if (DOJ_Date_Format_Check <= Report_Date)
                        {
                            Month_Mid_Total_Days_Count = Total_Days_Count;
                        }
                        else
                        {
                            //Month_Mid_Total_Days_Count = dayCount;
                            Month_Mid_Total_Days_Count = (int)((date2 - DOJ_Date_Format_Check).TotalDays);
                            Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                        }

                    }
                    else
                    {
                        Month_Mid_Total_Days_Count = Total_Days_Count;
                    }

                }
                else
                {
                    var lastDayOfMonth = new DateTime(date1.Year, date1.Month, DateTime.DaysInMonth(date1.Year, date1.Month));
                    Month_Mid_Total_Days_Count = Convert.ToInt32(lastDayOfMonth.ToString("dd"));
                }
                if (Log_DS.Rows[i]["MachineID"].ToString() == "2004")
                {
                    string stop = "";
                }
                if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                {
                    Fixed_Work_Days = Month_Mid_Total_Days_Count - Convert.ToInt32(WH_Count);
                    //Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    //Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count + NFH_Days_Present_Count;
                }
                else
                {
                   
                    Fixed_Work_Days = Total_Days_Count;
                   // Present_Days_Count = Present_Days_Count - WH_Present_Count;
                   // Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count;
                }
                if (Present_Days_Count != 0 && NFH_Days_Count != 0 && NFH_WH_Days_Mins != 0)
                {
                    double NFH_Week_Off_Minus_Days = Convert.ToDouble(NFH_WH_Days_Mins);
                    double NFH_Final_Disp_Days = 0;
                    if (NFH_Week_Off_Minus_Days <= Convert.ToDouble(WH_Present_Count))
                    {
                        WH_Present_Count = WH_Present_Count - Convert.ToDecimal(NFH_Week_Off_Minus_Days);
                    }
                    else
                    {
                        // 'NFH Days Minus for Working Days
                        double Balance_NFH_Days_Minus_IN_WorkDays = 0;

                        Balance_NFH_Days_Minus_IN_WorkDays = NFH_Week_Off_Minus_Days - Convert.ToDouble(WH_Present_Count);
                        Present_Days_Count = Present_Days_Count - Convert.ToDecimal(Balance_NFH_Days_Minus_IN_WorkDays);
                        WH_Present_Count = 0;
                    }
                }
                string FullNight = "";
                if (WagesValue == "ORISSA")
                {
                    SSQL = "";
                    SSQL = "Select sum(present) as present from [" + SessionEpay + "]..FullNight_Days where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " and EmpNo='" + Log_DS.Rows[i]["MachineID"].ToString() + "' and Convert(datetime,Attn_date,103)>=Convert(datetime,'" + FromDate + "',103)";
                    SSQL = SSQL + " and Convert(datetime,Attn_date,103)<=Convert(datetime,'" + ToDate + "',103)";
                    SSQL = SSQL + " group by EMpno";
                    DataTable dt_orissa_FullNight = new DataTable();
                    dt_orissa_FullNight = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_orissa_FullNight.Rows.Count > 0)
                    {
                        FullNight = (Convert.ToDecimal(dt_orissa_FullNight.Rows[0]["present"].ToString())).ToString();
                    }else
                    {
                        FullNight= "0.00";
                    }
                }else
                {
                    FullNight= Log_DS.Rows[i]["FullNight"].ToString();
                }
                string NFH_Doubale_Wages_Attn_Inct = (NFH_Double_Wages_Checklist + NFH_Double_Wages_Manual + NFH_WH_Days_Mins).ToString();

                if (Present_Days_Count != 0)
                {


                    mEmployeeDT.NewRow();
                    mEmployeeDT.Rows.Add();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count-1]["Machine ID"] = Log_DS.Rows[i]["MachineID"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Token No"] = Log_DS.Rows[i]["ExistingCode"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["EmpName"] = Log_DS.Rows[i]["FirstName"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Days"] = Present_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["H.Allowed"] = "0";
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["N/FH"] = NFH_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Half Night"] = Log_DS.Rows[i]["HalfNight"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Full Night"] = FullNight;

                    if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                    {
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Manual OT"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["SPG Allow"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Canteen Days Minus"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Hours"] = OT_Hours;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count-1]["W.H"] = WH_Present_Count;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count-1]["Fixed W.Days"] = Fixed_Work_Days;
                    }
                    else
                    {
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Manual OT"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["SPG Allow"] = Spinning_Incentive_Days;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Canteen Days Minus"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Hours"] = OT_Hours;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["W.H"] = WH_Present_Count;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Fixed W.Days"] = Fixed_Work_Days;
                    }



                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH W.Days"] = NFH_Double_Wages_Checklist;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Total Month Days"] = Month_Mid_Total_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH Worked Days"] = NFH_Doubale_Wages_Attn_Inct;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH D.W Statutory"] = NFH_Double_Wages_Statutory;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AEH"] = AEH_NFH_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["LBH"] = LBH_NFH_Days_Count;
                    intI = intI + 1;
                }



                NFH_Days_Count = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;
                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;
                Manul_OT = "0";


            }
        }
        catch (Exception ex)
        {

        }
        UploadDataTableToExcel(mEmployeeDT);
    }
 
    protected void UploadDataTableToExcel(DataTable dtRecords)
    {
        string XlsPath = Server.MapPath(@"~/Add_data/EmployeePayroll.xls");
        string attachment = string.Empty;
        if (XlsPath.IndexOf("\\") != -1)
        {
            string[] strFileName = XlsPath.Split(new char[] { '\\' });
            attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
        }
        else
            attachment = "attachment; filename=" + XlsPath;
        try
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            string tab = string.Empty;

            foreach (DataColumn datacol in dtRecords.Columns)
            {
                Response.Write(tab + datacol.ColumnName);
                tab = "\t";
            }
            Response.Write("\n");

            foreach (DataRow dr in dtRecords.Rows)
            {
                tab = "";
                for (int j = 0; j < dtRecords.Columns.Count; j++)
                {
                    Response.Write(tab + Convert.ToString(dr[j]));
                    tab = "\t";
                }

                Response.Write("\n");
            }
            Response.End();
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }
    }
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
}