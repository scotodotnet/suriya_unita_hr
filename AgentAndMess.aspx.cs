﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
public partial class AgentAndMess : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string NetBase;
    string NetFDA;
    string NetVDA;
    string NetHRA;
    string Nettotal;
    string NetPFEarnings;
    string NetPF;
    string NetESI;
    string NetUnion;
    string NetAdvance;
    string NetAll1;
    string NetAll2;
    string NetAll3;
    string NetAll4;
    string NetAll5;
    string NetDed1;
    string NetDed2;
    string NetDed3;
    string NetDed4;
    string NetDed5;
    string HomeDays;
    string Halfnight;
    string FullNight;
    string DayIncentive;
    string Spinning;
    string ThreeSided;
    string NetLOP;
    string NetStamp;
    string NetTotalDeduction;
    string NetOT;
    string NetAmt;
    string Network;
    string totNFh;
    string totweekoff;
    string totCL;
    string totwork;
    string Roundoff;
    DateTime MyDate;
    DateTime MyDate1;
    DateTime MyDate2;
    static decimal AdvAmt;
    static string ID;
    static string Adv_id = "";
    static string Dec_mont = "0";
    static string Adv_BalanceAmt = "0";
    static string Adv_due = "0";
    static string Increment_mont = "0";
    static decimal val;
    static string EmployeeDays = "0";
    string MyMonth;
    static string cl = "0";
    string TempDate;
    static string Fixedsal = "0";
    static string FixedOT = "0";
    static string Tot_OThr = "0";
    static string Emp_ESI_Code = "";
    static string NetPay_Grand_Total = "0";
    static string NetPay_Grand_Total_Words = "";
    static bool isUK = false;
    string SessionPayroll;

    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_WagesType();
            //Load_Department();
            //Load_BankName();
            Months_load();
            //ESICode_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                //txtLCYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            //Load_Division_Name();
            //Master.Visible = false;
            if (SessionUserType == "2")
            {
                //IFUser_Fields_Hide();
            }
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Suriya_UnitA_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlAgentName.Items.Clear();
        string Category_Str = "0";

        if (rbtnReportType.SelectedValue == "1")
        {
            // query = "select (AgentName) as Names from MstAgent ";
            query = "Select EmpNo, FirstName as Names from Employee_Mst where DeptName='AGENT' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' And IsActive='Yes'";
            dtdsupp = objdata.RptEmployeeMultipleDetails(query);
            ddlAgentName.DataSource = dtdsupp;
            DataRow dr = dtdsupp.NewRow();
            dr["EmpNo"] = "0";
            dr["Names"] = "-Select-";
            dtdsupp.Rows.InsertAt(dr, 0);
            ddlAgentName.DataTextField = "Names";
            ddlAgentName.DataValueField = "EmpNo";
            ddlAgentName.DataBind();
        }
        else
        {
            query = "select (MessAgent) as Names from MessTime_Mst ";
            dtdsupp = objdata.RptEmployeeMultipleDetails(query);
            ddlAgentName.DataSource = dtdsupp;
            DataRow dr = dtdsupp.NewRow();
            dr["Names"] = "0";
            dr["Names"] = "-Select-";
            dtdsupp.Rows.InsertAt(dr, 0);
            ddlAgentName.DataTextField = "Names";
            ddlAgentName.DataValueField = "Names";
            ddlAgentName.DataBind();
        }
       
    }

    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
    
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
    }
    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);
            }

            if (i != 0)
                sb.Append(words3[i - 1]);
        }

        return sb.ToString().TrimEnd();
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
    
    }
    protected void rbtnReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }
    protected void btnExport_Click1(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }
            if (rbtnReportType.SelectedValue == "1")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Mess Category..,');", true);
                ErrFlag = true;
            }

            if ((ddlAgentName.SelectedValue == "-Select-") || (ddlAgentName.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            
            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }

            string Other_State = "";
            string Non_Other_State = "";

            if (!ErrFlag)
            {

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,SalDet.New_Mess_Days,(SalDet.ProfessionalTax) as New_Mess_Amt," +
                                  " SalDet.WorkedDays,MM.MessAmt,EmpDet.AgentName " +
                                  
                                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo inner join MessTime_Mst MM on EmpDet.MessAgent=MM.MessAgent " +
                                  " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.MessAgent='" + ddlAgentName.SelectedItem.Text + "' and " +
                                  " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' " +
                                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and SalDet.New_Mess_Days<>'0.0' ";

                query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA,SalDet.Basic_SM," +
               " SalDet.New_Mess_Amt,SalDet.New_Mess_Days,SalDet.WorkedDays,MM.MessAmt,SalDet.ProfessionalTax,EmpDet.AgentName " +
               " Order by EmpDet.ExistingCode Asc";

                DataTable dt_1 = new DataTable();
                dt_1 = objdata.RptEmployeeMultipleDetails(query);

                GVMess.DataSource = dt_1;
                GVMess.DataBind();

                string attachment = "attachment;filename=Mess.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                //DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);

                GVMess.RenderControl(htextw);

                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='9'>");
                Response.Write("" + CmpName + " - MESS REPORT");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='9'>");
                Response.Write("" + SessionLcode + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='9'>");
                Response.Write("" + Cmpaddress + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                string Salary_Head = "";

                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='9'>");
                Response.Write("Mess Month of " + txtfrom.Text + " - " + txtTo.Text + " / "  + ddlAgentName.SelectedItem.Text);
                Response.Write(Salary_Head);
                Response.Write("</td>");
                Response.Write("</tr>");
                //}
                Response.Write("</table>");

                Response.Write(stw.ToString());

                Int32 Grand_Tot_End = 0;

                Response.Write("<table border='1'>");
                Response.Write("<tr Font-Bold='true'>");
                Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                Response.Write("Grand Total");
                Response.Write("</td>");

                Grand_Tot_End = GVMess.Rows.Count + 5;
                Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                

                Response.Write("</tr></table>");

                Response.Write("</tr>");
                Response.Write("</table>");

                Response.End();
                Response.Clear();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
                
            }
        }
        catch (Exception)
        {
            throw;
        }
    }
    protected void btnSearch_Click1(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if ((ddlAgentName.SelectedValue == "-Select-") || (ddlAgentName.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }

            if (rbtnReportType.SelectedValue == "2")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Agent Category..,');", true);
                ErrFlag = true;
            }

            string Other_State = "";
            string Non_Other_State = "";

            if (!ErrFlag)
            {

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,SalDet.New_Agent_Amt,SalDet.New_Agent_Days," +
                                  " SalDet.WorkedDays,MM.Commission " +

                                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo inner join MstAgent MM on EmpDet.AgentName=MM.AgentName " +
                                  " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.AgentName='" + ddlAgentName.SelectedItem.Text + "' and " +
                                  " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' " +
                                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and SalDet.New_Agent_Days <> '0.0' ";

                query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA,SalDet.Basic_SM," +
               " SalDet.New_Agent_Amt,SalDet.New_Agent_Days,SalDet.WorkedDays,MM.Commission " +
               " Order by EmpDet.ExistingCode Asc";

                DataTable dt_1 = new DataTable();
                dt_1 = objdata.RptEmployeeMultipleDetails(query);
                if (dt_1.Rows.Count > 0)
                {
                    GVAgent.DataSource = dt_1;
                    GVAgent.DataBind();

                    //Agent Advance Check Start
                    query = "";
                    query = "Select * from [" + SessionPayroll + "]..AdvancePayment where EmpNo='" + ddlAgentName.SelectedValue + "' and Completed='S' and  ReductionMonth!='0' and Convert(datetime,GETDATE(),103)>=Convert(datetime,CreateDate,103) and Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    DataTable Advance_Dt = new DataTable();
                    DataTable AdvanveCheck_DT = new DataTable();
                    string MonthlyDeduction = "0";
                    Advance_Dt = objdata.RptEmployeeMultipleDetails(query);
                    if (Advance_Dt != null && Advance_Dt.Rows.Count > 0)
                    {
                        query = "";
                        query = "select * from [" + SessionPayroll + "]..Advancerepayment where EmpNo='" + ddlAgentName.SelectedValue + "' and Months='" + ddlMonths.SelectedItem.Text + "' and AdvanceId='" + Advance_Dt.Rows[0]["ID"] + "'";
                        AdvanveCheck_DT = objdata.RptEmployeeMultipleDetails(query);
                        if (AdvanveCheck_DT.Rows.Count == 0)
                        {
                            query = "";
                            query = "Insert into [" + SessionPayroll + "]..Advancerepayment(EmpNo,TransDate,Months,Amount,AdvanceId,Ccode,Lcode) ";
                            query = query + " values('" + ddlAgentName.SelectedValue + "',getdate(),'" + ddlMonths.SelectedItem.Text + "','" + Advance_Dt.Rows[0]["MonthlyDeduction"] + "','" + Advance_Dt.Rows[0]["ID"] + "','" + SessionCcode + "' ,'" + SessionLcode + "')";
                            objdata.RptEmployeeMultipleDetails(query);


                            query = "";
                            query = "select isnull(sum(Amount),'0') as BalanceAmt from [" + SessionPayroll + "]..Advancerepayment where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and AdvanceID='" + Advance_Dt.Rows[0]["ID"] + "' and EmpNo='" + ddlAgentName.SelectedValue + "'";
                            AdvanveCheck_DT = objdata.RptEmployeeMultipleDetails(query);
                            if (AdvanveCheck_DT.Rows.Count > 0)
                            {
                                if (Convert.ToDecimal(AdvanveCheck_DT.Rows[0]["BalanceAmt"]) >= Convert.ToDecimal(Advance_Dt.Rows[0]["Amount"]))
                                {
                                    query = "";
                                    query = "update [" + SessionPayroll + "]..AdvancePayment set Completed='Y', BalanceAmount='0',IncreaseMonth=IncreaseMonth+1,ReductionMonth=ReductionMonth-1 where ID='" + Advance_Dt.Rows[0]["ID"] + "' and EmpNo='" + ddlAgentName.SelectedValue + "'";
                                    objdata.RptEmployeeMultipleDetails(query);
                                }
                                else
                                {
                                    query = "";
                                    query = "update [" + SessionPayroll + "]..AdvancePayment set Completed='S', BalanceAmount='" + Convert.ToDecimal(Convert.ToDecimal(Advance_Dt.Rows[0]["Amount"]) - Convert.ToDecimal(AdvanveCheck_DT.Rows[0]["BalanceAmt"])) + "',IncreaseMonth=(IncreaseMonth+1),ReductionMonth=(ReductionMonth-1) where ID='" + Advance_Dt.Rows[0]["ID"] + "' and EmpNo='" + ddlAgentName.SelectedValue + "'";
                                    objdata.RptEmployeeMultipleDetails(query);
                                    MonthlyDeduction = Advance_Dt.Rows[0]["MonthlyDeduction"].ToString();
                                }
                            }
                        }
                    }//Agent Advance Check End

                    string attachment = "attachment;filename=AgentCommission.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    //DataTable dt = new DataTable();
                    query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dt = objdata.RptEmployeeMultipleDetails(query);
                    if (dt.Rows.Count > 0)
                    {
                        CmpName = dt.Rows[0]["Cname"].ToString();
                        Cmpaddress = (dt.Rows[0]["Address1"].ToString() + " - " + dt.Rows[0]["Pincode"].ToString());
                    }

                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);

                    GVAgent.RenderControl(htextw);

                    Response.Write("<table>");
                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='8'>");
                    Response.Write("" + CmpName + " - AGENT COMMISION REPORT");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='8'>");
                    Response.Write("" + SessionLcode + "");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='8'>");
                    Response.Write("" + Cmpaddress + "");
                    Response.Write("</td>");
                    Response.Write("</tr>");

                    string Salary_Head = "";

                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='8'>");
                    Response.Write("COMMISION Month of " + txtfrom.Text + " - " + txtTo.Text + " / " + ddlAgentName.SelectedItem.Text);
                    Response.Write(Salary_Head);
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    //}
                    Response.Write("</table>");

                    Response.Write(stw.ToString());

                    Int32 Grand_Tot_End = 0;

                    Response.Write("<table border='1'>");
                    Response.Write("<tr Font-Bold='true'>");
                    Response.Write("<td font-Bold='true' align='right' colspan='4'>");
                    Response.Write("Grand Total");
                    Response.Write("</td>");

                    Grand_Tot_End = GVAgent.Rows.Count + 5;

                    Response.Write("<td>=sum(E6:E" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td></td>");
                    Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("</tr><tr>");
                    Response.Write("<td font-Bold='true' align='right' colspan='6'>");
                    Response.Write("Advance Amt");
                    Response.Write("</td>");
                    Response.Write("<td>" + MonthlyDeduction + " </td>");
                    Response.Write("</tr><tr>");
                    Response.Write("<td font-Bold='true' align='right' colspan='6'>");
                    Response.Write("Net Total");
                    Response.Write("</td>");
                    Response.Write("<td>=G" + (Grand_Tot_End + 1).ToString() + "-" + MonthlyDeduction + " </td>");
                    Response.Write("</tr></table>");

                    Response.Write("</tr>");
                    Response.Write("</table>");

                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page,this.GetType(),"alert", "SaveMsgAlert('No Data Found')", true);
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
    }
}
