﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
public partial class WeeklyOTDeptwise : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;

    double Final_Count;

    string PerDayAmt;

    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable OTHrs_DS = new DataTable();


    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;
    ReportDocument report = new ReportDocument();
    DataSet ds = new DataSet();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- WEEKLY OT HOURS";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Division = Request.QueryString["Division"].ToString();
            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
            //WagesType = Request.QueryString["Wages"].ToString();

            FromDate = Request.QueryString["FromDate"].ToString();
            //ToDate = Request.QueryString["ToDate"].ToString();

            if (SessionUserType == "2")
            {
                NonAdminGetAttdTable_Weekly_OTHOURS();
            }

            else
            {
                //GetAttdTable_Weekly_OTHOURS();

                SSQL = "";
                SSQL = "select (EM.DeptName) as Designation ,EM.DeptCode,Count(*) as Col3,'' as  Dept,'' as Col1,'' as Col2 from LogTime_Days LD";
                SSQL = SSQL + " inner join Employee_Mst EM on EM.MachineID = LD.MachineID";
                SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
                SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + FromDate + "',103)";
                SSQL = SSQL + " And (Present_Absent='Present' or Present_Absent='Half Day')  group by EM.DeptName,EM.DeptCode order by Em.DeptName asc   ";
                // 
                AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);

                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                {
                    SSQL = "";
                    SSQL = SSQL + "select (EM.DeptName) AS Dept,Count(*) as Col1,sum(OT.NoHrs) as Col2 ";
                    SSQL = SSQL + "from Employee_Mst EM inner join [Suriya_UnitA_Epay]..OverTime OT on EM.EmpNo=OT.EmpNo where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' ";
                    SSQL = SSQL + "and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CONVERT(DATETIME,TransDate,103)= convert(datetime,'" + FromDate + "',103) and EM.DeptName='" + AutoDTable.Rows[i]["Designation"].ToString().Trim() + "' and NoHrs<>'0'";
                    SSQL = SSQL + "group by EM.DeptName order by EM.DeptName asc ";

                    mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (mDataSet.Rows.Count > 0)
                    {
                        AutoDTable.Rows[i]["Dept"] = mDataSet.Rows[0]["Dept"].ToString();
                        AutoDTable.Rows[i]["Col1"] = mDataSet.Rows[0]["Col1"].ToString();
                        AutoDTable.Rows[i]["Col2"] = mDataSet.Rows[0]["Col2"].ToString();

                    }
                    else
                    {
                        AutoDTable.Rows[i]["Dept"] = "0";
                        AutoDTable.Rows[i]["Col1"] = "0";
                        AutoDTable.Rows[i]["Col2"] = "0";
                    
                    }
                    
                }


                

            }


            ds.Tables.Add(AutoDTable);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/ManualOTDept.rpt"));

            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt.Rows[0]["CompName"].ToString();

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.DataDefinition.FormulaFields["Ccode"].Text = "'" + name.ToString() + "'";
            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";


            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
    }
    public void GetAttdTable_Weekly_OTHOURS()
    {
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("FromDate");
        AutoDTable.Columns.Add("ToDate");
        AutoDTable.Columns.Add("Col1");
        AutoDTable.Columns.Add("Col2");
        AutoDTable.Columns.Add("Col3");
        AutoDTable.Columns.Add("Col4");
        AutoDTable.Columns.Add("Col5");
        AutoDTable.Columns.Add("Col6");
        AutoDTable.Columns.Add("Col7");
        AutoDTable.Columns.Add("Basic");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("OTHrs");
        AutoDTable.Columns.Add("Designation");
        AutoDTable.Columns.Add("WagesType");
        AutoDTable.Columns.Add("TokenNo");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("EmpName");
        AutoDTable.Columns.Add("SPGTotalHrs");
        AutoDTable.Columns.Add("OTHrs_Rate");
        AutoDTable.Columns.Add("BasicAmt");

        DataTable DateTable = new DataTable();
        DataRow datetRow;
        int i;
        DateTable.Columns.Add("OTDate");


        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);
      

        DataRow dtRow;


        //Get Weekly OT Employee Details
        DateTime Fromdayy = Convert.ToDateTime(date1.AddDays(0).ToShortDateString());
        string Date1 = Fromdayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
        DateTime Todayy = Convert.ToDateTime(Date2.AddDays(0).ToShortDateString());
        string date2 = Todayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);


        SSQL = "";
        SSQL = SSQL + "select EM.DeptName,Count(*) as TotPer,sum(OT.NoHrs) as TotHours ";
        SSQL = SSQL + "from Employee_Mst EM inner join [Suriya_UnitA_Epay]..OverTime OT on EM.EmpNo=OT.EmpNo where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + "and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CONVERT(DATETIME,TransDate,103)= convert(datetime,'" + FromDate + "',103)";
        SSQL = SSQL + "group by EM.DeptName order by EM.DeptName asc ";

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        for (i = 0; i < mDataSet.Rows.Count; i++)
        {
            dtRow = AutoDTable.NewRow();
            dtRow["CompanyName"] = SessionCcode;
            dtRow["LocationName"] = SessionLcode;
            dtRow["FromDate"] = FromDate;
            dtRow["ToDate"] = ToDate;
            dtRow["OTHrs"] = mDataSet.Rows[i]["OTHrs"].ToString();
            dtRow["Basic"] = mDataSet.Rows[i]["Basic"].ToString();
            dtRow["Dept"] = mDataSet.Rows[i]["DeptName"].ToString();
            dtRow["Designation"] = mDataSet.Rows[i]["Designation"].ToString();
            dtRow["WagesType"] = mDataSet.Rows[i]["WagesType"].ToString();
            dtRow["TokenNo"] = mDataSet.Rows[i]["TokenNo"].ToString();
            dtRow["MachineID"] = mDataSet.Rows[i]["MachineID"].ToString();
            dtRow["EmpName"] = mDataSet.Rows[i]["EmpName"].ToString();

            AutoDTable.Rows.Add(dtRow);
        }

    }



    public void NonAdminGetAttdTable_Weekly_OTHOURS()
    {

        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("FromDate");
        AutoDTable.Columns.Add("ToDate");
        AutoDTable.Columns.Add("Col1");
        AutoDTable.Columns.Add("Col2");
        AutoDTable.Columns.Add("Col3");
        AutoDTable.Columns.Add("Col4");
        AutoDTable.Columns.Add("Col5");
        AutoDTable.Columns.Add("Col6");
        AutoDTable.Columns.Add("Col7");
        AutoDTable.Columns.Add("Basic");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Designation");
        AutoDTable.Columns.Add("WagesType");
        AutoDTable.Columns.Add("TokenNo");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("EmpName");
        AutoDTable.Columns.Add("SPGTotalHrs");

        DataTable DateTable = new DataTable();
        DataRow datetRow;
        int i;
        DateTable.Columns.Add("OTDate");


        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);
        for (i = 0; i < 7; i++)
        {
            datetRow = DateTable.NewRow();
            date1 = Convert.ToDateTime(FromDate);
            DateTime dayy = Convert.ToDateTime(date1.AddDays(i).ToShortDateString());
            datetRow["OTDate"] = Convert.ToDateTime(date1.AddDays(i).ToString("yyyy/MM/dd"));
            DateTable.Rows.Add(datetRow);

        }

        DataRow dtRow;


        //Get Weekly OT Employee Details
        DateTime Fromdayy = Convert.ToDateTime(date1.AddDays(0).ToShortDateString());
        string Date1 = Fromdayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
        DateTime Todayy = Convert.ToDateTime(Date2.AddDays(0).ToShortDateString());
        string date2 = Todayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);


        SSQL = "Select Distinct WOT.MachineID,WOT.ExistingCode as TokenNo,WOT.EmpName,WOT.Wages as WagesType,ED.DeptName,ED.Designation,ED.BaseSalary as Basic,";
        SSQL = SSQL + "0 as Col1,0 as Col2,0 as Col3,0 as Col4,0 as Col5,0 as Col6,0 as Col7 from Weekly_OTHours WOT, Employee_Mst ED where";
        SSQL = SSQL + " ED.CompCode=WOT.CompCode and ED.LocCode=WOT.LocCode and ED.MachineID=WOT.MachineID and ED.ExistingCode=WOT.ExistingCode";
        SSQL = SSQL + " And WOT.CompCode='" + SessionCcode + "' And WOT.LocCode='" + SessionLcode + "' And WOT.Wages='" + WagesType + "'";
        SSQL = SSQL + " And ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' and ED.Eligible_PF='1'";
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " and WOT.OTDate between CONVERT(datetime,'" + Date1 + "',103) and CONVERT(datetime,'" + date2 + "',103)";

        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And ED.Division = '" + Division + "'";

        }
        SSQL = SSQL + " Order By WOT.ExistingCode Asc";

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        for (i = 0; i < mDataSet.Rows.Count; i++)
        {
            dtRow = AutoDTable.NewRow();
            dtRow["CompanyName"] = SessionCcode;
            dtRow["LocationName"] = SessionLcode;
            dtRow["FromDate"] = FromDate;
            dtRow["ToDate"] = ToDate;
            dtRow["Col1"] = mDataSet.Rows[i]["Col1"].ToString();
            dtRow["Col2"] = mDataSet.Rows[i]["Col2"].ToString();
            dtRow["Col3"] = mDataSet.Rows[i]["Col3"].ToString();
            dtRow["Col4"] = mDataSet.Rows[i]["Col4"].ToString();
            dtRow["Col5"] = mDataSet.Rows[i]["Col5"].ToString();
            dtRow["Col6"] = mDataSet.Rows[i]["Col6"].ToString();
            dtRow["Col7"] = mDataSet.Rows[i]["Col7"].ToString();
            dtRow["Basic"] = mDataSet.Rows[i]["Basic"].ToString();
            dtRow["Dept"] = mDataSet.Rows[i]["DeptName"].ToString();
            dtRow["Designation"] = mDataSet.Rows[i]["Designation"].ToString();
            dtRow["WagesType"] = mDataSet.Rows[i]["WagesType"].ToString();
            dtRow["TokenNo"] = mDataSet.Rows[i]["TokenNo"].ToString();
            dtRow["MachineID"] = mDataSet.Rows[i]["MachineID"].ToString();
            dtRow["EmpName"] = mDataSet.Rows[i]["EmpName"].ToString();


            //Get Spinning OT Hrs Start
            SSQL = "Select isnull(sum(cast(OTHrs as decimal(18,2))),0) as SPGTotalHrs from Weekly_OTHours where CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "'";
            SSQL = SSQL + " And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "' and SPG_Inc='True'";
            SSQL = SSQL + " and OTDate between CONVERT(datetime,'" + Date1 + "',103) and CONVERT(datetime,'" + date2 + "',103)";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["SPGTotalHrs"] = OTHrs_DS.Rows[0]["SPGTotalHrs"].ToString();
            }
            else
            {
                dtRow["SPGTotalHrs"] = 0;
            }
            //Get Spinning OT Hrs End

            //Day1 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[0][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col1"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }

            //Day2 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[1][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col2"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }
            //Day3 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[2][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col3"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }
            //Day4 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[3][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col4"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }
            //Day5 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[4][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col5"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }
            //Day6 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[5][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col6"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }
            //Day7 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[6][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col7"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }
            AutoDTable.Rows.Add(dtRow);
        }

    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }

}
