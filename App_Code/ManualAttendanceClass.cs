﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for ManualAttendanceClass
/// </summary>
public class ManualAttendanceClass
{
	public ManualAttendanceClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string _Ccode;

    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }

    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }


    private string _TktNo;

    public string TktNo
    {
        get { return _TktNo; }
        set { _TktNo = value; }
    }

    private string _ExistingCode;

    public string ExistingCode
    {
        get { return _ExistingCode; }
        set { _ExistingCode = value; }
    }

    private string _MachineNo;

    public string MachineNo
    {
        get { return _MachineNo; }
        set { _MachineNo = value; }
    }

    private string _EmpName;

    public string EmpName
    {
        get { return _EmpName; }
        set { _EmpName = value; }
    }

    private string _AttendanceStatus;

    public string AttendanceStatus
    {
        get { return _AttendanceStatus; }
        set { _AttendanceStatus = value; }
    }


    private string _TimeIn;

    public string TimeIn
    {
        get { return _TimeIn; }
        set { _TimeIn = value; }
    }

    private string _TimeOut;

    public string TimeOut
    {
        get { return _TimeOut; }
        set { _TimeOut = value; }
    }

    private string _AttendanceDate;

    public string AttendanceDate
    {
        get { return _AttendanceDate; }
        set { _AttendanceDate = value; }
    }

    private string _CompanyCode;

    public string CompanyCode
    {
        get { return _CompanyCode; }
        set { _CompanyCode = value; }
    }

    private string _LocationCode;

    public string LocationCode
    {
        get { return _LocationCode; }
        set { _LocationCode = value; }
    }
    private string _LogTimeIN;

    public string LogTimeIN
    {
        get { return _LogTimeIN; }
        set { _LogTimeIN = value; }
    }


    private string _LogTimeOUT;

    public string LogTimeOUT
    {
        get { return _LogTimeOUT; }
        set { _LogTimeOUT = value; }
    }

    private string _CurrentDate;

    public string CurrentDate
    {
        get { return _CurrentDate; }
        set { _CurrentDate = value; }
    }

    private string _UserName;

    public string UserName
    {
        get { return _UserName; }
        set { _UserName = value; }
    }

    private string _EncryptID;

    public string EncryptID
    {
        get { return _EncryptID; }
        set { _EncryptID = value; }
    }

    private string _CompensationDate;

    public string CompensationDate
    {
        get { return _CompensationDate; }
        set { _CompensationDate = value; }
    }

    private string _CoAgainstDate;

    public string CoAgainstDate
    {
        get { return _CoAgainstDate; }
        set { _CoAgainstDate = value; }
    }

}
