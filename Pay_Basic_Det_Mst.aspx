﻿
<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="Pay_Basic_Det_Mst.aspx.cs" Inherits="Pay_Basic_Det_Mst" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>

<asp:UpdatePanel ID="Basic" runat="server">
   <ContentTemplate>
     <!-- begin #content -->
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
	    <li class="active">Salary Category</li>
	</ol>
	<h1 class="page-header">Salary Category</h1>
	<div class="row">
        <div class="col-md-12">
		    <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Salary Category</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <h5>STAFF And REGULAR WORKER BASIC DETAILS</h5>
                        <div class="row">
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Category</label>
								    <asp:DropDownList ID="ddlCategory" runat="server" class="form-control select2"  AutoPostBack="true"
                                        onselectedindexchanged="ddlCategory_SelectedIndexChanged">
								       <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
								       <asp:ListItem Text="STAFF" Value="1"></asp:ListItem>
								       <asp:ListItem Text="LABOUR" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                    
                                
								</div>
                            </div>
                            <div class="col-md-3">
								<div class="form-group">
								    <label>Employee Type</label>
								    <asp:DropDownList runat="server" ID="ddlEmployeeType" AutoPostBack="true"
                                        class="form-control select2" style="width:100%;" 
                                        onselectedindexchanged="ddlEmployeeType_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                             <div class="col-md-3">
							    <div class="form-group">
								    <label>Basic And DA</label>
								    <asp:TextBox runat="server" ID="txtBasic" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                          
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>HRA</label>
								    <asp:TextBox runat="server" ID="txtHRA" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                        </div>
                       <div class="row">
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Bonus %</label>
								    <asp:TextBox runat="server" ID="txtBonusPer" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                       </div>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                        onclick="btnClear_Click" />
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        
                         <div class="row">
                           <div class="col-md-3">
                            <label id="lblHead" runat="server" visible="false"> <h5>PF BASIC DETAILS</h5></label>
                           </div>
                        </div>
                        <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 
                               <asp:Label ID="lblCateName" Visible="false" runat="server" Text="Category"></asp:Label>
								 <asp:DropDownList runat="server" ID="ddlcategoryPF" class="form-control select2" Visible="false"
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlcategory_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="STAFF">STAFF</asp:ListItem>
								 <asp:ListItem Value="LABOUR">LABOUR</asp:ListItem>
								</asp:DropDownList>
							
								</div>
                               </div>
                           <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <asp:Label ID="lblEMpType" Visible="false" runat="server" Text="Employee Type"></asp:Label>
								 <asp:DropDownList runat="server" ID="txtEmployeeType" class="form-control select2" 
                                        style="width:100%;" Visible="false" AutoPostBack="true"
                                        onselectedindexchanged="txtEmployeeType_SelectedIndexChanged">
								 </asp:DropDownList>
							
								</div>
                               </div>
                           <!-- end col-4 -->
                           <div class="col-md-3">
							    <div class="form-group">
								    <asp:Label ID="lblDays" Visible="false" runat="server" Text="PF Days"></asp:Label>
								    <asp:TextBox runat="server" ID="txtPfDays" Text="0.00" class="form-control" style="width:100%;" Visible="false"></asp:TextBox>
								</div>
                            </div>
                        </div>
                        <div class="row">
                         <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnPFsave" Text="Save" class="btn btn-success" 
                                        Visible="false" onclick="btnPFsave_Click" />
									<asp:Button runat="server" id="btnPFclear" Text="Clear" class="btn btn-danger"  
                                        Visible="false" onclick="btnPFclear_Click"
                                         />
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
   </ContentTemplate>
</asp:UpdatePanel>


</asp:Content>

