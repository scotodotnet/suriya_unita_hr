﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class ONDutyReport : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    string SessionUserType;
    string SSQL = "";

    DataTable dt = new DataTable();
    DataTable dsEmployee = new DataTable();
    DataTable mDataSet = new DataTable();
    Int32 Total_Calculate = 0;
    string[] Time_Minus_Value_Check;

    int shiftCount;
    string Final_Shift_Final = "";
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string FindWages;

    DataTable dt_OT = new DataTable();
    string OT_Above_Eight = "0";
    string OT_Above_Four = "0";
    string OT_Below_Four = "0";
    string OT_Total_Hours = "0";
    string oldWages;
    string EmpCode;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-OT Report Between Dates";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            //SessionUserType = Session["UserType"].ToString();


            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            FindWages = Request.QueryString["wages"].ToString();

            oldWages = Request.QueryString["Wages"].ToString();
            EmpCode = Request.QueryString["EmpCode"].ToString();

            if (oldWages == "FITTER _ ELECTRICIANS")
            {
                FindWages = oldWages.Replace('_', '&');
            }
            else
            {
                FindWages = Request.QueryString["Wages"].ToString();
            }


            System.Web.UI.WebControls.DataGrid grid =
                                  new System.Web.UI.WebControls.DataGrid();

            //AutoDTable.Columns.Add("DeptName");
            //AutoDTable.Columns.Add("MachineID");
            //AutoDTable.Columns.Add("MachineID_Enc");
            //AutoDTable.Columns.Add("EmpNo");
            //AutoDTable.Columns.Add("ExistingCode");
            //AutoDTable.Columns.Add("FirstName");
            //AutoDTable.Columns.Add("Wages");

            //DataCells.Columns.Add("DeptName");
            //DataCells.Columns.Add("EmpNo");
            //DataCells.Columns.Add("ExistingCode");
            //DataCells.Columns.Add("FirstName");



            //Fill_Day_Attd_Between_Dates_OT();


            SSQL = "select TokenNo,ExistingCode,EmpName,ONDutyFromDate,ONDutyToDate,Days,FromArea,ToArea from OnDuty_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            if (FromDate != "" && ToDate != "")
            {
                SSQL = SSQL + " And convert(datetime,ONDutyFromDate,103)>=CONVERT(datetime,'" + FromDate + "',103)";
                SSQL = SSQL + " And convert(datetime,ONDutyToDate,103)<=CONVERT(datetime,'" + ToDate + "',103)";
            }
            if (EmpCode != "")
            {
                SSQL = SSQL + " And TokenNo='" + EmpCode + "'";
            }
            DataCells = objdata.RptEmployeeMultipleDetails(SSQL);


            grid.DataSource = DataCells;
            grid.DataBind();

            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt.Rows[0]["CompName"].ToString();


            string attachment = "attachment;filename=ONDutyReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\">" + name + "</a>");
            Response.Write("  ");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\">ON DUTY REPORT</a>");
            Response.Write("  ");
            Response.Write("</td>");
            Response.Write("</tr>");
            if (FromDate != "" && ToDate != "")
            {
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='8'>");
                Response.Write("<a style=\"font-weight:bold\">FROM:" + FromDate + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">TO:" + ToDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
    }
   
}
