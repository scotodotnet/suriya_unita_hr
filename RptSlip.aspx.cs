﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
public partial class RptSlip : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string NetBase;
    string NetFDA;
    string NetVDA;
    string NetHRA;
    string Nettotal;
    string NetPFEarnings;
    string NetPF;
    string NetESI;
    string NetUnion;
    string NetAdvance;
    string NetAll1;
    string NetAll2;
    string NetAll3;
    string NetAll4;
    string NetAll5;
    string NetDed1;
    string NetDed2;
    string NetDed3;
    string NetDed4;
    string NetDed5;
    string HomeDays;
    string Halfnight;
    string FullNight;
    string DayIncentive;
    string Spinning;
    string ThreeSided;
    string NetLOP;
    string NetStamp;
    string NetTotalDeduction;
    string NetOT;
    string NetAmt;
    string Network;
    string totNFh;
    string totweekoff;
    string totCL;
    string totwork;
    string Roundoff;
    DateTime MyDate;
    DateTime MyDate1;
    DateTime MyDate2;
    static decimal AdvAmt;
    static string ID;
    static string Adv_id = "";
    static string Dec_mont = "0";
    static string Adv_BalanceAmt = "0";
    static string Adv_due = "0";
    static string Increment_mont = "0";
    static decimal val;
    static string EmployeeDays = "0";
    string MyMonth;
    static string cl = "0";
    string TempDate;
    static string Fixedsal = "0";
    static string FixedOT = "0";
    static string Tot_OThr = "0";
    static string Emp_ESI_Code = "";
    static string NetPay_Grand_Total = "0";
    static string NetPay_Grand_Total_Words = "";
    static bool isUK = false;
    string SessionPayroll;

    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_Report_Type();
            Load_WagesType();
            Agent_load();
            //Load_Department();
            Load_BankName();
            Months_load();
            //ESICode_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                //txtLCYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            //Load_Division_Name();
            //Master.Visible = false;
            if (SessionUserType == "2")
            {
                IFUser_Fields_Hide();
            }
        }
    }
    protected void btnPayslip_Excel(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            string Other_State = "";
            string Non_Other_State = "";

            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlcategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                string Payslip_Format_Type = "";
                if (SessionUserType == "2")
                {
                    Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                }
                else
                {
                    Payslip_Format_Type = "";
                }

                string Str_PFType = "";
                if (SessionUserType == "2")
                {
                    Str_PFType = RdpIFPF.SelectedValue.ToString();
                }
                else
                {
                    Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                }

                string Str_ChkLeft = "";
                //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                Str_ChkLeft = "0";


                if (SessionUserType == "2")
                {
                    report_head = "PAYSLIP FOR THE MONTH OF " + ddlMonths.SelectedItem.Text + " " + YR.ToString();
                }
                else
                {
                    report_head = "PAYSLIP FOR THE DATE OF " + txtfrom.Text.ToString() + " - " + txtTo.Text.ToString();
                }

                Basic_Report_Date = "";
                Basic_Report_Type = "OLD";

                DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                //Check PF Category
                if ((txtEmployeeType.SelectedValue == "101") || (txtEmployeeType.SelectedValue == "111") || (txtEmployeeType.SelectedValue == "91") || (txtEmployeeType.SelectedValue == "61") || (txtEmployeeType.SelectedValue == "71") || (txtEmployeeType.SelectedValue == "81"))
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                           " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive," +
                           " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                           " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                           " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                           " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                           " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                           " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                           " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                           " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                           " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                           " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                           " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                           " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and SalDet.New_Allowance_Amt<>'0'";

                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive," +
                   " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.Manual_OT,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                   " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                   " Order by cast(EmpDet.ExistingCode as int) Asc";
                }
                else if ((txtEmployeeType.SelectedValue == "121") || (txtEmployeeType.SelectedValue == "131"))
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                              " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive," +
                              " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                              " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                              " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                              " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                              " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                              " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                              " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                              " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                              " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                              " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And convert(datetime,AttnDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,AttnDet.ToDate,103)<=convert(datetime,'" + txtTo.Text + "',103) And " +
                              " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                              " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and convert(datetime,SalDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,SalDet.ToDate,103)<=convert(datetime,'" + txtTo.Text + "',103) and SalDet.New_Allowance_Amt<>'0' ";

                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive," +
                   " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                   " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                   " Order by cast(EmpDet.ExistingCode as int) Asc";
                }
                else if ((txtEmployeeType.SelectedValue == "21") || (txtEmployeeType.SelectedValue == "31") || (txtEmployeeType.SelectedValue == "41") || (txtEmployeeType.SelectedValue == "51"))
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,(EmpDet.BaseSalary + EmpDet.VPF) as Actual,(EmpDet.VPF) as Fixed," +
                              " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA) as NonPFWages," +
                              " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                              " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                              " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                              " (SalDet.New_Cash_Amt) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt " +
                              " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                              " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                              " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                              " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                              " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                              " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                              " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                              " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and SalDet.New_Allowance_Amt<>'0'";

                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt," +
                   " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt,EmpDet.BaseSalary,EmpDet.VPF," +
                   " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                   " Order by cast(EmpDet.ExistingCode as int) Asc";
                }
              else
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,(EmpDet.BaseSalary) as Actual,(EmpDet.VPF) as Fixed," +
                              " SUM(isnull(CAST(SalDet.WorkedDays as decimal(18,2)),'0')) as WorkedDays,EmpDet.DeptName as Department,(Sum(isnull(CAST(SalDet.OTHoursNew as decimal(18,2)),'0')) + SUM(isnull(CAST(SalDet.Manual_OT as decimal(18,2)),'0'))) as OTHoursNew,SUM(isnull(CAST(SalDet.BasicandDA as decimal(18,2)),'0')) as NonPFWages,(SUM(isnull(CAST(SalDet.OTHoursAmtNew as decimal(18,2)),'0'))+SUM(isnull(CAST(SalDet.FullNightAmt as decimal(18,2)),'0'))+SUM(isnull(CAST(Allowances1 as decimal(18,2)),'0'))) as TA," +
                              " SUM(isnull(CAST(SalDet.OTHoursAmtNew as decimal(18,2)),'0')) as OTHoursAmtNew,(SUM(isnull(CAST(SalDet.BasicandDA as decimal(18,2)),'0')) + SUM(isnull(CAST(SalDet.OTHoursAmtNew as decimal(18,2)),'0'))) as GrossEarningsOT,SUM(isnull(CAST(SalDet.ProvidentFund as decimal(18,2)),'0')) as PF,SUM(isnull(CAST(SalDet.Advance as decimal(18,2)),'0')) as Advance,SUm(isnull(CAST(SalDet.ESI as decimal(18,2)),'0')) as ESI,(SUM(isnull(CAST(SalDet.ProvidentFund as decimal(18,2)),'0'))+SUM(isnull(CAST(SalDet.ESI as decimal(18,2)),'0'))) as TotolESIPF,SUM(isnull(CAST(SalDet.New_Allowance_Amt as decimal(18,2)),'0')) as New_Allowance_Amt," +
                              " (SUM(isnull(CAST(SalDet.Advance as decimal(18,2)),'0')) + SUM(isnull(CAST(SalDet.Deduction3 as decimal(18,2)),'0')) + SUM(isnull(CAST(SalDet.Deduction4 as decimal(18,2)),'0')) + SUM(isnull(CAST(SalDet.DedOthers1 as decimal(18,2)),'0')) + SUM(isnull(CAST(SalDet.DedOthers2 as decimal(18,2)),'0'))) as TotDed,SUM(isnull(CAST(SalDet.TotalDeductions as decimal(18,2)),'0')) as TotalDeductions,SUM(isnull(CAST(SalDet.DayIncentive as decimal(18,2)),'0')) as DayIncentive,SUM(isnull(CAST(SalDet.Allowances1 as decimal(18,2)),'0')) as Allowances1,SUM(isnull(CAST(SalDet.FullNightAmt as decimal(18,2)),'0')) as FullNightAmt," +
                              " SUM(isnull(CAST(SalDet.Deduction3 as decimal(18,2)),'0')) as Deduction3,SUM(isnull(CAST(SalDet.Deduction4 as decimal(18,2)),'0')) as Deduction4,SUM(isnull(CAST(SalDet.Deduction5 as decimal(18,2)),'0')) as Deduction5,SUM(isnull(CAST(SalDet.DedOthers1 as decimal(18,2)),'0')) as DedOthers1,SUM(isnull(CAST(SalDet.DedOthers2 as decimal(18,2)),'0')) as DedOthers2,SUM(isnull(CAST(SalDet.Basic_SM as decimal(18,2)),'0')) as Basic_SM,SUM(isnull(CAST(SalDet.Advance as decimal(18,2)),'0')) as Advance,SUM(isnull(CAST(SalDet.RoundOffNetPay as decimal(18,2)),'0')) as RoundOffNetPay, " +
                              " SUM(isnull(CAST(SalDet.New_Cash_Amt as decimal(18,2)),'0')) as NetPay,SUM(isnull(CAST(SalDet.New_Cash_Amt as decimal(18,2)),'0')) as New_Cash_Amt,SUM(isnull(CAST(SalDet.New_Bank_Amt as decimal(18,2)),'0')) as New_Bank_Amt,SUM(isnull(CAST(SalDet.New_Tot_Amt as decimal(18,2)),'0')) as New_Tot_Amt " +
                              " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                              " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                              " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' " +
                              " and SalDet.Lcode='" + SessionLcode + "' " +
                              " and EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                              " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') " +
                              " and Convert(date,SalDet.FromDate,103)>=Convert(date,'" + txtfrom.Text + "',103) and Convert(date,SalDet.ToDate,103)<=Convert(date,'" + txtTo.Text + "',103)";

                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName," +
                   " EmpDet.DeptName,EmpDet.BaseSalary,EmpDet.VPF " +
                   " Order by cast(EmpDet.ExistingCode as int) Asc";
                }

                DataTable dt_1 = new DataTable();
                dt_1 = objdata.RptEmployeeMultipleDetails(query);

                Decimal NetGrTot = Convert.ToDecimal(NetPay_Grand_Total.ToString());
                NetPay_Grand_Total_Words = NumerictoNumber(Convert.ToInt32(NetGrTot), isUK).ToString() + " " + "Only";


                //GvAllowance.DataSource = dt_1;
                //GvAllowance.DataBind();

                GVStaff.DataSource = dt_1;
                GVStaff.DataBind();




                string attachment = "attachment;filename=Payslip.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                //DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                NetBase = "0";
                NetFDA = "0";
                NetVDA = "0";
                Nettotal = "0";
                NetPFEarnings = "0";
                NetPF = "0";
                NetESI = "0";
                NetUnion = "0";
                NetAdvance = "0";
                NetAll1 = "0";
                NetAll2 = "0";
                NetAll3 = "0";
                NetAll4 = "0";
                NetDed1 = "0";
                NetDed2 = "0";
                NetDed3 = "0";
                NetDed4 = "0";
                NetAll5 = "0";
                NetDed5 = "0";
                NetLOP = "0";
                NetStamp = "0";
                NetTotalDeduction = "0";
                NetOT = "0";
                NetAmt = "0";
                Network = "0";
                totCL = "0";
                totNFh = "0";
                totweekoff = "0";
                Roundoff = "0";
                Fixedsal = "0";
                FixedOT = "0";
                Tot_OThr = "0";
                //HomeDays = "0";
                //Halfnight = "0";
                //FullNight = "0";
                //Spinning = "0";
                //DayIncentive = "0";
                //ThreeSided = "0";
                //totwork = "0";

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);

                GVStaff.RenderControl(htextw);
                

                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='"+GVStaff.Columns.Count+"'>");
                Response.Write("" + CmpName + " - PAYSLIP REPORT");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='"+ GVStaff.Columns.Count + "'>");
                Response.Write("" + SessionLcode + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='"+ GVStaff.Columns.Count + "'>");
                Response.Write("" + Cmpaddress + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                string Salary_Head = "";

                //if ((txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11"))
                //{
                //    Salary_Head = "" + txtEmployeeType.SelectedItem.Text + " WORKERS ALLOWANCE REPORT FOR THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text;
                //}
                //else if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //{
                //    Salary_Head = "" + txtEmployeeType.SelectedItem.Text + " WORKERS ALLOWANCE REPORT FOR THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text;
                //}

                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='"+ GVStaff.Columns.Count + "'>");
                //Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
                Response.Write(report_head);
                Response.Write("</td>");
                Response.Write("</tr>");
                //}
                Response.Write("</table>");

                Response.Write(stw.ToString());

                //Response.Write("<table border='1'>");
                //Response.Write("<tr Font-Bold='true'>");
                //Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                //Response.Write("Grand Total");
                //Response.Write("</td>");

                Int32 Grand_Tot_End = 0;

                //Check PF Category

                Response.Write("<table border='1'>");
                Response.Write("<tr Font-Bold='true'>");
                Response.Write("<td font-Bold='true' align='right' colspan='4'>");
                Response.Write("Grand Total");
                Response.Write("</td>");

                Grand_Tot_End = GVStaff.Rows.Count + 5;


                Response.Write("<td>=sum(E6:E" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("</tr></table>");

                Response.Write("</tr>");
                Response.Write("</table>");

                Response.End();
                Response.Clear();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);



            }

        }
        catch (Exception)
        {

            throw;
        }
    }
    public void IFUser_Fields_Hide()
    {
        //IF_Dept_Hide.Visible = false;
        IF_FromDate_Hide.Visible = false;
        IF_ToDate_Hide.Visible = false;
        //IF_Left_Employee_Hide.Visible = false;
        //IF_State_Hide.Visible = false;
        //btnBankSalary.Visible = false;
        //btnCivilAbstract.Visible = false;
        //BtnDeptManDays.Visible = false;
        //IF_Leave_Credit.Visible = false;
        //rdbPayslipFormat.Visible = false;
        rdbPayslipIFFormat.Visible = true;
        RdbCashBank.SelectedValue = "2";
        RdbPFNonPF.SelectedValue = "1";
        IF_Salary_Through_Hide.Visible = false;
        IF_PF_NON_PF_Hide.Visible = false;
        IF_Report_Type.Visible = true;
        //IF_rdbPayslipFormat_Hide.Visible = false;
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Suriya_UnitA_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private void Load_BankName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtBankName.Items.Clear();
        query = "Select distinct BankName from MstBank order by BankName Asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtBankName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["BankName"] = "-Select-";
        dr["BankName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtBankName.DataTextField = "BankName";
        txtBankName.DataValueField = "BankName";
        txtBankName.DataBind();
    }
    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        string Category_Str = "0";
        if (ddlcategory.SelectedItem.Text == "STAFF")
        {
            Category_Str = "1";
        }
        else if (ddlcategory.SelectedItem.Text == "LABOUR")
        {
            Category_Str = "2";
        }
        query = "select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + Category_Str + "'";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }
    private void Agent_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlAgentName.Items.Clear();
        query = "Select AgentName from MstAgent order by AgentName ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlAgentName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["AgentName"] = "0";
        dr["AgentName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlAgentName.DataTextField = "AgentName";
        ddlAgentName.DataValueField = "AgentName";
        ddlAgentName.DataBind();
    }

    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";  

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            string Other_State = "";
            string Non_Other_State = "";

            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlcategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                string Payslip_Format_Type = "";
                if (SessionUserType == "2")
                {
                    Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                }
                else
                {
                    Payslip_Format_Type = "";
                }

                string Str_PFType = "";
                if (SessionUserType == "2")
                {
                    Str_PFType = RdpIFPF.SelectedValue.ToString();
                }
                else
                {
                    Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                }

                string Str_ChkLeft = "";
                //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                Str_ChkLeft = "0";

               
                if (SessionUserType == "2")
                {
                    report_head = "PAYSLIP FOR THE MONTH OF " + ddlMonths.SelectedItem.Text + " " + YR.ToString();
                }
                else
                {
                    report_head = "PAYSLIP FOR THE MONTH OF " + txtfrom.ToString() + " - " + txtTo.ToString();
                }

                Basic_Report_Date = "";
                Basic_Report_Type = "OLD";

                DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                //Check PF Category
                if (Str_PFType.ToString() != "0")
                {
                    if (Str_PFType.ToString() == "1")
                    {
                        if ((txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8")) //Tamil Girls Worker
                        {
                            query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                                   " SalDet.LOPDays,(SalDet.FFDA) as Basic_SMM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Basic_SM," +
                                   " SalDet.GrossEarnings,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4) as DedPF," +
                                   " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4)) as NetpayIF,EmpDet.DeptName as Department,SalDet.WorkedDays,SalDet.Deduction3, SalDet.Deduction4 " +
                                   " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                   " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                   " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                   " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                                   " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                                   " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                   " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                                   " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";

                            query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA,SalDet.Basic_SM," +
                           " EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA," +
                           " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.Deduction3,SalDet.Deduction4 " +
                           " Order by cast(EmpDet.ExistingCode as int) Asc";
                        }
                        else if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13"))
                        {
                            query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                                      " SalDet.LOPDays,(SalDet.FFDA) as Basic_SMM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Basic_SM," +
                                      " SalDet.GrossEarnings,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4) as DedPF," +
                                      " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4)) as NetpayIF,EmpDet.DeptName as Department,SalDet.WorkedDays,SalDet.Deduction3, SalDet.Deduction4 " +
                                      " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                      " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                      " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                      " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                                      " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                                      " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And convert(datetime,SalDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,SalDet.ToDate,103)<=convert(datetime,'" + txtTo.Text + "',103) And " +
                                      " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                                      " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' and convert(datetime,AttnDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,AttnDet.ToDate,103)<=convert(datetime,'" + txtTo.Text + "',103) ";

                            query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA,SalDet.Basic_SM," +
                           " EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA," +
                           " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.Deduction3,SalDet.Deduction4 " +
                           " Order by cast(EmpDet.ExistingCode as int) Asc";
                        }
                        else if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                        {
                            query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                                      " SalDet.LOPDays,(SalDet.FFDA) as Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Deduction3, SalDet.Deduction4, " +
                                      " SalDet.GrossEarnings,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4) as DedPF," +
                                      " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4)) as NetpayIF,EmpDet.DeptName as Department,SalDet.WorkedDays " +
                                      " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                      " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                      " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                      " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                                      " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                                      " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                      " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                                      " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";

                            query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA," +
                           " EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Deduction3,SalDet.Deduction4, " +
                           " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI " +
                           " Order by cast(EmpDet.ExistingCode as int) Asc";
                        }
                    }
                    else
                    {
                        //Non PF Employee
                        query = query + " and (EmpDet.Eligible_PF='2')";
                    }
                }
                else
                {
                    if ((txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                    {
                        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                               " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive," +
                               " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                               " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                               " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                               " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                               " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                               " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                               " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                               " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                               " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                               " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                               " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                               " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes')";

                        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive," +
                       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.Manual_OT,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                       " Order by cast(EmpDet.ExistingCode as int) Asc";
                    }
                    else if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13"))
                    {
                        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                                  " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive," +
                                  " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                                  " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                                  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                                  " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                  " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                                  " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And convert(datetime,AttnDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,AttnDet.ToDate,103)<=convert(datetime,'" + txtTo.Text + "',103) And " +
                                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and convert(datetime,SalDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,SalDet.ToDate,103)<=convert(datetime,'" + txtTo.Text + "',103) ";

                        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive," +
                       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                       " Order by cast(EmpDet.ExistingCode as int) Asc";
                    }
                    else if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                    {
                        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,(EmpDet.BaseSalary + EmpDet.VPF) as Actual,(EmpDet.VPF) as Fixed," +
                                  " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA) as NonPFWages," +
                                  " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                                  " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                                  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                                  " (SalDet.New_Cash_Amt) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt " +
                                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                  " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                                  " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes')";

                        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt," +
                       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt,EmpDet.BaseSalary,EmpDet.VPF," +
                       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                       " Order by cast(EmpDet.ExistingCode as int) Asc";
                    }
                }

                DataTable dt_1 = new DataTable();
                dt_1 = objdata.RptEmployeeMultipleDetails(query);

                Decimal NetGrTot = Convert.ToDecimal(NetPay_Grand_Total.ToString());
                NetPay_Grand_Total_Words = NumerictoNumber(Convert.ToInt32(NetGrTot), isUK).ToString() + " " + "Only";

                
                   //Check PF Category
                   if (Str_PFType.ToString() == "1")
                    {
                        //PF Employee List
                        GVHostel.DataSource = dt_1;
                        GVHostel.DataBind();
                    }
                    else
                    {
                        //Non PF Employee List
                        if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "6"))
                        {
                            GVCivil.DataSource = dt_1;
                            GVCivil.DataBind();
                        }
                        else if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                        {
                            gvStaffNonPF.DataSource = dt_1;
                            gvStaffNonPF.DataBind();
                        }
                    }
                
               

                string attachment = "attachment;filename=Payslip.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                //DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                NetBase = "0";
                NetFDA = "0";
                NetVDA = "0";
                Nettotal = "0";
                NetPFEarnings = "0";
                NetPF = "0";
                NetESI = "0";
                NetUnion = "0";
                NetAdvance = "0";
                NetAll1 = "0";
                NetAll2 = "0";
                NetAll3 = "0";
                NetAll4 = "0";
                NetDed1 = "0";
                NetDed2 = "0";
                NetDed3 = "0";
                NetDed4 = "0";
                NetAll5 = "0";
                NetDed5 = "0";
                NetLOP = "0";
                NetStamp = "0";
                NetTotalDeduction = "0";
                NetOT = "0";
                NetAmt = "0";
                Network = "0";
                totCL = "0";
                totNFh = "0";
                totweekoff = "0";
                Roundoff = "0";
                Fixedsal = "0";
                FixedOT = "0";
                Tot_OThr = "0";
                //HomeDays = "0";
                //Halfnight = "0";
                //FullNight = "0";
                //Spinning = "0";
                //DayIncentive = "0";
                //ThreeSided = "0";
                //totwork = "0";

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);

                //Check PF Category
                    if (Str_PFType.ToString() == "1")
                    {
                        //PF Employee
                        if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7"))
                        {
                            GVHostel.RenderControl(htextw);
                        }
                    }
                    else
                    {
                        //Non PF Employee
                        if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "6"))
                        {
                            GVCivil.RenderControl(htextw);
                        }
                        else if((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                        {
                            gvStaffNonPF.RenderControl(htextw);
                        }
                    }
                
                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='19'>");
                Response.Write("" + CmpName + " - PAYSLIP REPORT");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='19'>");
                Response.Write("" + SessionLcode + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='19'>");
                Response.Write("" + Cmpaddress + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                string Salary_Head = "";

                if ((txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11"))
                {
                    Salary_Head = "" + txtEmployeeType.SelectedItem.Text + " WORKERS REPORT FOR THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text;
                }
                else if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                {
                    Salary_Head = "" + txtEmployeeType.SelectedItem.Text + " WORKERS REPORT FOR THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text;
                }

                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='19'>");
                //Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
                Response.Write(Salary_Head);
                Response.Write("</td>");
                Response.Write("</tr>");
                //}
                Response.Write("</table>");

                Response.Write(stw.ToString());

                //Response.Write("<table border='1'>");
                //Response.Write("<tr Font-Bold='true'>");
                //Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                //Response.Write("Grand Total");
                //Response.Write("</td>");

                Int32 Grand_Tot_End = 0;

                //Check PF Category
               
                    if (Str_PFType.ToString() == "1")
                    {
                        //PF Employee
                        if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                        {
                            Response.Write("<table border='1'>");
                            Response.Write("<tr Font-Bold='true'>");
                            Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                            Response.Write("Grand Total");
                            Response.Write("</td>");

                            Grand_Tot_End = GVHostel.Rows.Count + 5;
                        }

                        if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                        {
                            Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                         
                        }
                    }
                    else
                    {
                        //Non PF Employee
                        if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                        {
                            Response.Write("<table border='1'>");
                            Response.Write("<tr Font-Bold='true'>");
                            Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                            Response.Write("Grand Total");
                            Response.Write("</td>");

                            Grand_Tot_End = GVCivil.Rows.Count + 5;

                            Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                            Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");

                            Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                        }
                        else
                        {
                            Response.Write("<table border='1'>");
                            Response.Write("<tr Font-Bold='true'>");
                            Response.Write("<td font-Bold='true' align='right' colspan='6'>");
                            Response.Write("Grand Total");
                            Response.Write("</td>");

                            Grand_Tot_End = gvStaffNonPF.Rows.Count + 5;

                            //Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                            Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");

                            Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                        }



                        if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                        {
                            
                        }
                        //want to put grand total
                        

                    }
                

               

                Response.Write("</tr></table>");

                Response.Write("</tr>");
                Response.Write("</table>");

                Response.End();
                Response.Clear();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);

               
            
            }
          
        }
        catch (Exception)
        {
            
            throw;
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
    }
    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);

            }

            if (i != 0)
                sb.Append(words3[i - 1]);

        }

        return sb.ToString().TrimEnd();



    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        
        bool ErrFlag = false;
        string CmpName = "";
        string Cmpaddress = "";
        int YR = 0;
        string SalaryType = "";
        string query = "";
        string ExemptedStaff = "";
        string report_head = "";
        string Basic_Report_Date = "";
        string Basic_Report_Type = ""; 


        if (ddlMonths.SelectedItem.Text == "January")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonths.SelectedItem.Text == "February")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonths.SelectedItem.Text == "March")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
        }
        DataTable dt_Bank = new DataTable();
        query = "select * from MstBank where BankName='" + txtBankName.SelectedItem.Text + "'";
        dt_Bank = objdata.RptEmployeeMultipleDetails(query);

        

        if (ddlcategory.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
            ErrFlag = true;
        }
        //else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
        //    ErrFlag = true;
        //}

        else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            try
            {
                if (RdbCashBank.SelectedValue != "2")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Bank Option');", true);
                    ErrFlag = true;
                }
                else if (txtBankName.SelectedValue == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Bank Name');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {

                    string Wages = "";
                    if(txtEmployeeType.SelectedItem.Text!="-Select-")
                    {
                        Wages = " and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'";
                    }
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,EmpDet.Designation,EmpDet.AccountNo," +
                                         " SalDet.New_Bank_Amt,SalDet.New_Allowance_Amt,'' as Charges,'' as BankAmt" +
                                         " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                         " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                         " inner Join MstBank MB on MB.BankName = EmpDet.BankName " +
                                         " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                         " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                                         " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                                         " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                         " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "'" + Wages +" "+
                                         " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Salary_Through='2' and EmpDet.Eligible_PF='1' and  EmpDet.BankName='" + txtBankName.SelectedItem.Text + "'";

                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA,EmpDet.BankName,EmpDet.BranchCode,EmpDet.IFSC_Code," +
                   " EmpDet.DeptName,EmpDet.Designation,EmpDet.AccountNo,SalDet.New_Bank_Amt,SalDet.New_Allowance_Amt" +

                   " Order by cast(EmpDet.ExistingCode as int) Asc";

                    DataTable dt_1 = new DataTable();
                    dt_1 = objdata.RptEmployeeMultipleDetails(query);

                    Decimal NetGrTot = Convert.ToDecimal(NetPay_Grand_Total.ToString());
                    NetPay_Grand_Total_Words = NumerictoNumber(Convert.ToInt32(NetGrTot), isUK).ToString() + " " + "Only";
                    
                    for (int i = 0; i < dt_1.Rows.Count; i++)
                    {
                        if (Convert.ToDecimal(dt_1.Rows[i]["New_Bank_Amt"].ToString()) > Convert.ToDecimal(dt_Bank.Rows[0]["AmtCharge"].ToString()))
                        {
                            dt_1.Rows[i]["Charges"] = dt_Bank.Rows[0]["MaxCharge"].ToString();
                            dt_1.Rows[i]["BankAmt"] = (Convert.ToDecimal(dt_1.Rows[i]["New_Bank_Amt"].ToString()) + Convert.ToDecimal(dt_Bank.Rows[0]["MaxCharge"].ToString())).ToString();
                            dt_1.Rows[i]["BankAmt"] = (Convert.ToDecimal(dt_1.Rows[i]["BankAmt"].ToString()) + Convert.ToDecimal(dt_1.Rows[i]["New_Allowance_Amt"].ToString())).ToString();
                        }
                        else
                        {
                            dt_1.Rows[i]["Charges"] = dt_Bank.Rows[0]["MinCharge"].ToString();
                            dt_1.Rows[i]["BankAmt"] = (Convert.ToDecimal(dt_1.Rows[i]["New_Bank_Amt"].ToString()) + Convert.ToDecimal(dt_Bank.Rows[0]["MinCharge"].ToString())).ToString();
                            dt_1.Rows[i]["BankAmt"] = (Convert.ToDecimal(dt_1.Rows[i]["BankAmt"].ToString()) + Convert.ToDecimal(dt_1.Rows[i]["New_Allowance_Amt"].ToString())).ToString();
                        }
                    }
                    
                    BankGV.DataSource = dt_1;
                    BankGV.DataBind();
                    
                    string attachment = "attachment;filename=NEFT.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    //DataTable dt = new DataTable();
                    DataTable dt = new DataTable();
                    query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dt = objdata.RptEmployeeMultipleDetails(query);
                    if (dt.Rows.Count > 0)
                    {
                        CmpName = dt.Rows[0]["Cname"].ToString();
                        Cmpaddress = (dt.Rows[0]["Address1"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                    }
                    
                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);

                    BankGV.RenderControl(htextw);

                    Response.Write("<table>");
                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='9'>");
                    Response.Write("" + CmpName + " - NEFT TRANSFER REPORT");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='9'>");
                    Response.Write("" + SessionLcode + "");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='9'>");
                    Response.Write("" + Cmpaddress + "");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    string Salary_Head = "";




                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='9'>");
                    //Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
                    Response.Write(Salary_Head);
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    //}
                    Response.Write("</table>");

                    Response.Write(stw.ToString());

                    //Response.Write("<table border='1'>");
                    //Response.Write("<tr Font-Bold='true'>");
                    //Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                    //Response.Write("Grand Total");
                    //Response.Write("</td>");

                    Int32 Grand_Tot_End = 0;

                    Response.Write("<table border='1'>");
                    Response.Write("<tr Font-Bold='true'>");
                    Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                    Response.Write("Grand Total");
                    Response.Write("</td>");

                    Grand_Tot_End = BankGV.Rows.Count + 5;

                    Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");

                    Response.Write("</tr></table>");

                    Response.Write("</tr>");
                    Response.Write("</table>");

                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);


                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
    protected void gvSalary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void rbtnReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Report_Type();
    }
    public void Load_Report_Type()
    {
        if (rbtnReportType.SelectedValue == "1")
        {
            Agent_load();
            ddlAgentName.Enabled = false;
            ddlcategory.Enabled = true;
            txtEmployeeType.Enabled = true;
        }
        else
        {
            Load_WagesType();
            ddlAgentName.Enabled = true;
            ddlcategory.Enabled = false;
            txtEmployeeType.Enabled = false;
        
        }
    }
    protected void btnPayslip_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }
            if (rbtnReportType.SelectedValue == "2")
            {
                if (ddlAgentName.SelectedValue == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                    ErrFlag = true;
                }
            }
            else
            {
                if (ddlcategory.SelectedValue == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                    ErrFlag = true;
                }
                else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                    ErrFlag = true;
                }
            }

            if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            string Other_State = "";
            string Non_Other_State = "";

            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlcategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                string Payslip_Format_Type = "";

                if (SessionUserType == "2")
                {
                    Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                }
                else
                {
                    Payslip_Format_Type = "";
                }

                string Str_PFType = "";
                if (SessionUserType == "2")
                {
                    Str_PFType = RdpIFPF.SelectedValue.ToString();
                }
                else
                {
                    Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                }

                string Report_Types = rbtnReportType.SelectedValue;
                string AgentName = ddlAgentName.SelectedItem.Text;

                string Str_ChkLeft = "";
                //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                Str_ChkLeft = "0";

                ResponseHelper.Redirect("NewViewReport.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&AgentName=" + AgentName + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + Str_PFType + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + Payslip_Format_Type.ToString() + "&PFTypePost=" + Str_PFType.ToString() + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=Payslip" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");
            }

        }
        catch (Exception Ex)
        {

            throw;
        }
    }
    protected void btnAllPF_Click(object sender, EventArgs e)
    {
        try
        {
             bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";


            string Report_Types = "PF List";

            if (btnAllPFList.SelectedValue == "0")
            {
                ExemptedStaff = btnAllPFList.SelectedValue;
            }
            else if (btnAllPFList.SelectedValue == "1")
            {
                ExemptedStaff = btnAllPFList.SelectedValue;
            }
            else
            {
                ExemptedStaff = btnAllPFList.SelectedValue;
            }


            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }
            if (!ErrFlag)
            {
                ResponseHelper.Redirect("RptAdbanceShow.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&BName=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&TypePF=" + ExemptedStaff, "_blank", "");
            }
        }
        catch (Exception Ex)
        {
            
            throw;
        }
    }
    protected void btnCompanyPay_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }
            if (rbtnReportType.SelectedValue == "2")
            {
                if (ddlAgentName.SelectedValue == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                    ErrFlag = true;
                }
            }
            else
            {
                if (ddlcategory.SelectedValue == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                    ErrFlag = true;
                }
                else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                    ErrFlag = true;
                }
            }

            if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            string Other_State = "";
            string Non_Other_State = "";

            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlcategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                string Payslip_Format_Type = "";

                if (SessionUserType == "2")
                {
                    Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                }
                else
                {
                    Payslip_Format_Type = "";
                }

                string Str_PFType = "";
                if (SessionUserType == "2")
                {
                    Str_PFType = RdpIFPF.SelectedValue.ToString();
                }
                else
                {
                    Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                }

                string Report_Types = rbtnReportType.SelectedValue;
                string AgentName = ddlAgentName.SelectedItem.Text;
                string ReportName = "Company Payslip";
                string Str_ChkLeft = "";
                //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                Str_ChkLeft = "0";

                ResponseHelper.Redirect("RptAdbanceShow.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&AgentName=" + AgentName + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + Str_PFType + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + Payslip_Format_Type.ToString() + "&PFTypePost=" + Str_PFType.ToString() + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=Payslip" + "&OtherState=" + Other_State + "&BName=" + ReportName + "&NonOtherState=" + Non_Other_State, "_blank", "");
            }

        }
        catch (Exception Ex)
        {

            throw;
        }
    }
}
