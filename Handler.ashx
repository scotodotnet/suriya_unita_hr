﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.IO;

public class Handler : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        string f = context.Request.QueryString.Get("f");

 
        //f = "//PARTHI-PC/ParthiShare/Emp_Scan/UNIT_I//Photos/" + f + ".jpg";
        System.Drawing.Image image = System.Drawing.Image.FromFile(f);
        context.Response.Clear();
        context.Response.ClearHeaders();
        image.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
        context.Response.ContentType = "image/jpeg";
        HttpContext.Current.ApplicationInstance.CompleteRequest();
    }
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}