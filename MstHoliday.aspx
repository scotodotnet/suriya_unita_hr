﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstHoliday.aspx.cs" Inherits="MstHoliday" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">HoliDay</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">HoliDay </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">HoliDay</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Year</label>
								  <asp:DropDownList runat="server" ID="ddlYear" class="form-control select2" style="width:100%;" 
								    AutoPostBack="true" onselectedindexchanged="ddlYear_SelectedIndexChanged">
								  </asp:DropDownList>
								  <asp:HiddenField ID="txtID" runat="server" />
								  <asp:RequiredFieldValidator ControlToValidate="ddlYear" Display="Dynamic" InitialValue="-Select-" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                               <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>LeaveDate</label>
								  <asp:TextBox runat="server" ID="txtLeaveDate" class="form-control datepicker"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtLeaveDate" Display="Dynamic"  ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>Description</label>
								  <asp:TextBox runat="server" ID="txtDesc" class="form-control"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtDesc" Display="Dynamic"  ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                            
                              </div>
                        <!-- end row -->
                         <div class="row">
                          <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>NFH Type</label>
								  <asp:DropDownList runat="server" ID="ddlNFHType" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlNFHType_SelectedIndexChanged">
								  <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								  <asp:ListItem Value="Double Wages Manual">Double Wages Manual</asp:ListItem>
                                  <%--<asp:ListItem Value="Double Wages Checklist">Double Wages Checklist</asp:ListItem>
                                  <asp:ListItem Value="Single Wages">Single Wages</asp:ListItem>
                                  <asp:ListItem Value="WH Minus">WH Minus</asp:ListItem>--%>
								  </asp:DropDownList>
								  <asp:RequiredFieldValidator ControlToValidate="ddlNFHType" Display="Dynamic" InitialValue="-Select-" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                               <!-- begin col-4 -->
                          <div class="col-md-2">
                               <div class="form-group">
                                   <div class="col-md-12">
                                   </br>
                                        <label class="checkbox-inline">
                                            <asp:CheckBox ID="chkDoubleWages" Visible="false" runat="server" Enabled="false" /><%--Double Wages Statutory--%>
                                        </label>
                                       
                                    </div>
                                </div>
                             </div>
                           <!-- end col-4 -->
                              <!-- begin col-4 -->
                          <div class="col-md-2">
                               <div class="form-group">
                                   <div class="col-md-12">
                                    </br>
                                        <label class="checkbox-inline">
                                            <asp:CheckBox ID="chkForm25" runat="server" Visible="false" /><%--Form25 NFHPresent--%>
                                        </label>
                                       
                                    </div>
                                </div>
                             </div>
                           <!-- end col-4 --> 
                             <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <%--<label>Form25 Text Display</label>--%>
								  <asp:TextBox runat="server" ID="txtForm25Text" Visible="false" class="form-control"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->     
                           </div>
                       
                         <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="ValidateDept_Field" onclick="btnSave_Click"/>
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                        
                         <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Description</th>
                                                <th>NFH Type</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("DateStr")%></td>
                                        <td><%# Eval("Description")%></td>
                                        <td><%# Eval("NFH_Type")%></td>
                                        <td>
                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("ID")%>'></asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("ID")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this NFH details?');">
                                                    </asp:LinkButton>
                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        
                        
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

