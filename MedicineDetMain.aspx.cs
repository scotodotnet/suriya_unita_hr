﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MedicineDetMain : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SessionAdmin;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "HR | Medical Details";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Menu-EmployeeProfile"));
            //li.Attributes.Add("class", "has-sub active open");
        }
        LoadDataDisplay();
    }

    public void LoadDataDisplay()
    {
        string query = "";
        DataTable DT = new DataTable();

        query = "Select Distinct TransID,TransDate,GatePassID from MedicalDetails Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater1.DataSource = DT;
        Repeater1.DataBind();

    }

    protected void GridEditEntryClick(object sender, CommandEventArgs e)
    {
        Session["TransID"] = e.CommandName.ToString();
        Response.Redirect("MedicalDetails.aspx");
    }

    protected void lbtnAdd_Click(object sender, EventArgs e)
    {
        Session.Remove("TransID");
        Response.Redirect("MedicalDetails.aspx");
    }
}
