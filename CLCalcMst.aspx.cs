﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class CLCalcMst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    string MonthID;
    string Month;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Holiday Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Fin_Year_Add();

            Load_Data_EmpDet();
        }

        Load_Data_NFH();
    }
    private void Load_Data_EmpDet()
    {

        string TableName;

        TableName = "Employee_Mst";


        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtExistingCode.Items.Clear();
        query = "Select ExistingCode from " + TableName + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtExistingCode.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtExistingCode.DataTextField = "ExistingCode";
        txtExistingCode.DataValueField = "ExistingCode";
        txtExistingCode.DataBind();
    }
    protected void txtExistingCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();
        string TableName;

        TableName = "Employee_Mst";

        if (txtExistingCode.SelectedItem.Text != "-Select-")
        {
            query = "Select * from " + TableName + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And ExistingCode='" + txtExistingCode.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {

                txtMachineID.Value = DT.Rows[0]["EmpNo"].ToString();
                txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
                txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();
            }
            else
            {
                txtExistingCode.SelectedValue = "-Select-";
                txtEmpName.Text = "";
                txtDepartment.Text = "";
                txtMachineID.Value = "";
            }
        }
        else
        {
            txtExistingCode.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtDepartment.Text = "";
            txtMachineID.Value = "";
        }
    }
    public void Fin_Year_Add()
    {
        int CurrentYear;


        CurrentYear = System.DateTime.Now.Year;

        ddlYear.Items.Clear();
        ddlYear.Items.Add("-Select-");
        for (int i = 0; i < 11; i++)
        {
            ddlYear.Items.Add(Convert.ToString(CurrentYear));
            CurrentYear = CurrentYear - 1;
        }

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;
        string Double_Wages_Statutory = "";
        string Form25_NFH_Present = "";

        DataTable DT_NFH = new DataTable();

        if (txtCLdays.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the CL Days...');", true);
        }
        
        if (txtFromDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the From Date...');", true);
        }

        if (txtToDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the To Date...');", true);
        }

        int ServerYr = Convert.ToInt32(ddlYear.SelectedItem.Text);
        int yr = Convert.ToDateTime(txtFromDate.Text).Year;

        if (ServerYr > yr)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Select Year Properly.');", true);
        }
        else
        {
            yr = Convert.ToDateTime(txtFromDate.Text).Year;
            MonthID = Convert.ToDateTime(txtFromDate.Text).Month.ToString();
            if (MonthID == "1")
            {
                Month = "January";
            }
            else if (MonthID == "2")
            {
                Month = "February";
            }
            else if (MonthID == "3")
            {
                Month = "March";
            }
            else if (MonthID == "4")
            {
                Month = "April";
            }
            else if (MonthID == "5")
            {
                Month = "May";
            }
            else if (MonthID == "6")
            {
                Month = "June";
            }
            else if (MonthID == "7")
            {
                Month = "July";
            }
            else if (MonthID == "8")
            {
                Month = "August";
            }
            else if (MonthID == "9")
            {
                Month = "September";
            }
            else if (MonthID == "10")
            {
                Month = "October";
            }
            else if (MonthID == "11")
            {
                Month = "November";
            }
            else if (MonthID == "12")
            {
                Month = "December";
            }
        }



        if (!ErrFlag)
        {
            if (btnSave.Text == "Save")
            {
                query = "Select * from CLMst where EmpNo='" + txtExistingCode.SelectedValue + "'";
                DT_NFH = objdata.RptEmployeeMultipleDetails(query);
                if (DT_NFH.Rows.Count != 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Employee Already Registered');", true);
                }
                else
                {
                    query = "Insert into CLMst (EmpNo,EmpName,DeptName,Months,Years,FromDate,ToDate,AddDays,MinDays,Ccode,Lcode) ";
                    query = query + "values ('" + txtExistingCode.SelectedValue + "','" + txtEmpName.Text + "','" + txtDepartment.Text + "','" + Month + "','" + yr + "',";
                    query = query + "'" + txtFromDate.Text + "','" + txtToDate.Text + "','" + txtCLdays.Text + "','0','" + SessionCcode + "','" + SessionLcode + "')";
                    objdata.RptEmployeeMultipleDetails(query);

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
                }
            }
            

        }
        Load_Data_NFH();
        Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    

    private void Clear_All_Field()
    {
        ddlYear.SelectedValue = "-Select-";
        txtCLdays.Text = "0";
        txtDepartment.Text = "";
        txtEmpName.Text = "";
        txtFromDate.Text = "";
        txtMachineID.Value = "";
        txtToDate.Text = "";


        //txtDesc.Text = "";
        //ddlNFHType.SelectedValue = "-Select-";
        //chkDoubleWages.Checked = false;
        //chkDoubleWages.Enabled = false;
        //chkForm25.Checked = false;
        //txtForm25Text.Text = "";
        //txtLeaveDate.Text = "";
        //txtID.Value = "";
        //btnSave.Text = "Save";
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from NFH_Mst where ID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from NFH_Mst where ID='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Holiday Details Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Holiday Details Not Found');", true);
        }
        Load_Data_NFH();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from NFH_Mst where ID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtID.Value = DT.Rows[0]["ID"].ToString();
            ddlYear.SelectedValue = DT.Rows[0]["Year"].ToString();
            //txtLeaveDate.Text = DT.Rows[0]["DateStr"].ToString();
            //txtDesc.Text = DT.Rows[0]["Description"].ToString();
            //ddlNFHType.SelectedValue = DT.Rows[0]["NFH_Type"].ToString();

            //if (DT.Rows[0]["Dbl_Wages_Statutory"].ToString() == "Yes")
            //{
            //    chkDoubleWages.Checked = true;
            //}
            //else
            //{
            //    chkDoubleWages.Checked = false;
            //}
            //if (DT.Rows[0]["Form25_NFH_Present"].ToString() == "Yes")
            //{
            //    chkForm25.Checked = true;
            //}
            //else
            //{
            //    chkForm25.Checked = false;
            //}
            //txtForm25Text.Text = DT.Rows[0]["Form25DisplayText"].ToString();

            //if (ddlNFHType.SelectedItem.Text == "Double Wages Checklist")
            //{
            //    chkDoubleWages.Enabled = true;
            //}
            //else
            //{
            //    chkDoubleWages.Enabled = false;

            //}

            btnSave.Text = "Update";
        }
        else
        {
            ddlYear.SelectedValue = "-Select-";
            //txtDesc.Text = "";
            //ddlNFHType.SelectedValue = "-Select-";
            //chkDoubleWages.Checked = false;
            //chkDoubleWages.Enabled = false;
            //chkForm25.Checked = false;
            //txtForm25Text.Text = "";
            txtID.Value = "";
            btnSave.Text = "Save";
        }
    }

    private void Load_Data_NFH()
    {
        string query = "";
        DataTable AutoDTable = new DataTable();
        DataTable DT = new DataTable();
        AutoDTable.Columns.Add("EmpNo");
        AutoDTable.Columns.Add("EmpName");
        AutoDTable.Columns.Add("DeptName");
        AutoDTable.Columns.Add("Months");
        AutoDTable.Columns.Add("Years");
        AutoDTable.Columns.Add("FromDate");
        AutoDTable.Columns.Add("ToDate");
        AutoDTable.Columns.Add("AddDays");
        AutoDTable.Columns.Add("MinDays");


        query = "select EmpNo,EmpName,DeptName,Months,Years,FromDate,ToDate,AddDays,'' as MinDays from CLMst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and AddDays<>'0' ";
        //query = query + "(sum(isnull(AddDays , '0'))  - sum(isnull(MinDays , '0')) ) as MinDays  ";
        //query = query + "group by EmpNo,EmpName,DeptName,Months,Years,FromDate,ToDate,AddDays,MinDays  having (sum(isnull(AddDays , '0'))  - sum(isnull(MinDays , '0')) ) > 0";
        ////query = "Select EmpNo,EmpName,convert(varchar(10),FromDate,103) as FromDate,convert(varchar(10),ToDate,103) as ToDate,AddDays,MinDays from CLMst ";
        DT = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT.Rows.Count; i++)
        {
            query = "select (sum(isnull(AddDays , '0'))  - sum(isnull(MinDays , '0')) ) as MDays from CLMst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and EmpNo='" + DT.Rows[i]["EmpNo"].ToString() + "' ";
            query = query + "group by EmpNo  having (sum(isnull(AddDays , '0'))  - sum(isnull(MinDays , '0')) ) > 0";
            AutoDTable = objdata.RptEmployeeMultipleDetails(query);
            if (AutoDTable.Rows.Count > 0)
            {
                DT.Rows[i]["MinDays"] = AutoDTable.Rows[0]["MDays"].ToString();
            }
        }
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

}
