﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstMessTiming : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Mess Deduction";
            Mess_Agent_Load();
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
        }

        Load_Data_Grade();
        Load_Data_Days();
    }
    private void Mess_Agent_Load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMessAgentName.Items.Clear();
        query = "select (MessAgent) as Names from MessTime_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMessAgentName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Names"] = "0";
        dr["Names"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMessAgentName.DataTextField = "Names";
        ddlMessAgentName.DataValueField = "Names";
        ddlMessAgentName.DataBind();
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

        string query;
        string EmpType = "";
        DataTable DT = new DataTable();
        DataTable dt_Type = new DataTable();

        query = "select * from MessTime_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'  And MessAgent='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from MessTime_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MessAgent='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Mess Details Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }





        Load_Data_Grade();
    }
    private void Load_Data_Grade()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select MessAgent,MessAmt from MessTime_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
    private void Load_Data_Days()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select MessAgent,MessMonth,MessWroked,FineAmt from MessDaysMst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    protected void GridViewDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string Query = "";
        DataTable dt = new DataTable();
        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        string AgentName = commandArgs[0];
        string Monthdays = commandArgs[1];
        Query = "select * from MessDaysMst where MessAgent='" + AgentName + "' and MessMonth='" + Monthdays + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            Query = "Delete from MessDaysMst where MessAgent='" + AgentName + "' and MessMonth='" + Monthdays + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(Query);
        }
        Load_Data_Days();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully.');", true);
    }
    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        //string query;
        //DataTable DT = new DataTable();


        //query = "select * from MstGrade where GradeID='" + e.CommandName.ToString() + "'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //if (DT.Rows.Count > 0)
        //{
        //    txtGradeID.Value = DT.Rows[0]["GradeID"].ToString();
        //    txtGradeName.Text = DT.Rows[0]["GradeName"].ToString();


        //    btnSave.Text = "Update";
        //}
        //else
        //{
        //    txtGradeID.Value = "";
        //    txtGradeName.Text = "";

        //    btnSave.Text = "Save";
        //}
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;

        if (txtMessAgent.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the MessAgent Name.');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (txtMessAmt.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Mess Amount.');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            query = "Select * From MessTime_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MessAgent='" + txtMessAgent.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                query = "Delete From MessTime_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MessAgent='" + txtMessAgent.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }

            query = "Insert into MessTime_Mst(CompCode,LocCode,MessAgent,MessAmt)";
            query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + txtMessAgent.Text.ToUpper() + "','" + txtMessAmt.Text + "')";
            objdata.RptEmployeeMultipleDetails(query);
            Load_Data_Grade();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
            Clear_All_Field();
            Mess_Agent_Load();
        }

        
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        //txtType.SelectedValue = "-Select-";
        txtMessAgent.Text = "";
        txtMessAmt.Text = "";
        //txtGradeID.Value = "";
        //txtGradeName.Text = "";

        btnSave.Text = "Save";
    }
    protected void btnDaysSave_Click(object sender, EventArgs e)
    {
        try
        {
            string SaveMode = "Insert";
            string query = "";
            DataTable DT = new DataTable();
            bool ErrFlag = false;

            if (ddlMessAgentName.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Mess Agent Name.');", true);
                ErrFlag = true;
            }
            else if (txtMonthDays.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Month Work Days.');", true);
                ErrFlag = true;
            }
            else if (txtWorkedDays.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Worked Days.');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                query = "select * from MessDaysMst where MessAgent='" + ddlMessAgentName.SelectedValue + "' and MessMonth='" + txtMonthDays.Text + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count > 0)
                {
                    query = "delete from MessDaysMst where MessAgent='" + ddlMessAgentName.SelectedValue + "' and MessMonth='" + txtMonthDays.Text + "'";
                    DT = objdata.RptEmployeeMultipleDetails(query);
                    SaveMode = "Update";
                }
                query = "insert into MessDaysMst(MessAgent,MessMonth,MessWroked,FineAmt,Ccode,Lcode) Values(";
                query = query + "'" + ddlMessAgentName.SelectedValue + "','" + txtMonthDays.Text + "','" + txtWorkedDays.Text + "','" + txtFineAmt.Text + "','" + SessionCcode + "','" + SessionLcode + "')";
                objdata.RptEmployeeMultipleDetails(query);
                Mess_Agent_Load();
                txtMonthDays.Text = "";
                txtWorkedDays.Text = "";
                txtFineAmt.Text = "";
                Load_Data_Days();
                if (SaveMode == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Added Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Updated Successfully');", true);
                }
            }


        }
        catch (Exception)
        {
            
            throw;
        }
    }
    protected void btnDaysClear_Click(object sender, EventArgs e)
    {
        Mess_Agent_Load();
        txtMonthDays.Text = "";
        txtWorkedDays.Text = "";
    }
}
