﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class WeeklyOTMain : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionEpay;
    string SessionRights;
    string SSQL = "";
    string SessionAdmin;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "HR | Employee Details";
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Menu-EmployeeProfile"));

            Session.Remove("EmpNo");
            Session.Remove("Date");
        }
        Load_data();
    }

    private void CreateEmployeeDisplay(string TransDate)
    {
        SSQL = "";
        SSQL = "Select EmpNo,NoHrs,Convert(varchar,TransDate,105) as TransDate,TimeIn,TimeOut,EmpName,Shift,netAmount from [" + SessionEpay + "]..OverTime where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and convert(datetime,TransDate,105)=convert(datetime,'" + TransDate + "',105)";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void lbtnAdd_Click(object sender, EventArgs e)
    {
        Session.Remove("EmpNo");
        Session.Remove("Date");
        Response.Redirect("WeeklyOTHrs.aspx");
    }

    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        Session.Remove("EmpNo");
        Session.Remove("Date");

        SSQL = "";
        SSQL = "Select EmpNo,Convert(varchar,TransDate,105) as TransDate from [" + SessionEpay + "]..OverTime where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and EmpNo='" + e.CommandName + "'";
        SSQL = SSQL + " and Convert(datetime,TransDate,103)=Convert(datetime,'" + e.CommandArgument + "',105)";

        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            Session["EmpNo"] = dt.Rows[0]["EmpNo"].ToString();
            Session["Date"] = dt.Rows[0]["TransDate"].ToString();
            Response.Redirect("WeeklyOTHrs.aspx");
        }
    }

    protected void btnDelete_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..overtime where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and EmpNo='"+e.CommandName.ToString()+"'";
        SSQL = SSQL + " and Convert(datetime,TransDate,103)=Convert(datetime,'" + e.CommandArgument.ToString() + "',103)";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            SSQL = "";
            SSQL = "Delete from [" + SessionEpay + "]..OverTime where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            SSQL = SSQL + " and Empno='"+e.CommandName.ToString()+"'";
            SSQL = SSQL + " and Convert(datetime,TransDate,103)=Convert(datetime,'" + e.CommandArgument.ToString() + "',103)";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('OT Deleted Successfully');", true);
        }
        Load_data();
    }

    private void Load_data()
    {
        if (txtOTDate.Text == "")
            CreateEmployeeDisplay((DateTime.Now.AddDays(-1)).ToString("dd/MM/yyyy"));
        else
            CreateEmployeeDisplay(txtOTDate.Text);
    }

    protected void btnOTSearch_Click(object sender, EventArgs e)
    {
        bool ErrFlg = false;
        if (txtOTDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsg('Please Select the OT Date!!!')", true);
            ErrFlg = true;
            return;
        }
        if (!ErrFlg)
        {
            CreateEmployeeDisplay(txtOTDate.Text);
        }
    }
}