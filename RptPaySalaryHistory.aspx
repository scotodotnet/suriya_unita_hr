<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="RptPaySalaryHistory.aspx.cs" Inherits="RptPaySalaryHistory" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <link href="assets/plugins/ModalPopup/CSS/Popup.css" rel="stylesheet" type="text/css"/>
<script>
    $(document).ready(function() {
        //alert('hi');
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        });
    });
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });

                $('.select2').select2();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
 <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Report</a></li>
				<li class="active">Salary History</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Salary History</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Salary History</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                    
                     <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlcategory" class="form-control select2" AutoPostBack="true" 
                                        style="width:100%;" onselectedindexchanged="ddlcategory_SelectedIndexChanged">
								 <asp:ListItem Value="0" Text="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="1" Text="STAFF">STAFF</asp:ListItem>
								 <asp:ListItem Value="2" Text="LABOUR">LABOUR</asp:ListItem>
								</asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Wages</label>
								 <asp:DropDownList runat="server" ID="ddldept" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddldept_SelectedIndexChanged">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee No</label>
								 <asp:DropDownList runat="server" ID="txtEmployeeNo" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="txtEmployeeNo_SelectedIndexChanged">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Name</label>
								 
                               <asp:TextBox ID="txtEmployeeName" runat="server" class="form-control"></asp:TextBox>
								
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Token No</label>
								 
                               <asp:TextBox ID="txtTokenNo" runat="server" class="form-control"></asp:TextBox>
								
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-2 -->
                              <div class="col-md-2">
								<div class="form-group">
								 <%--<label>Token No</label>--%>
								 <asp:TextBox ID="txtTokenSearch" Visible="false" runat="server" class="form-control"></asp:TextBox>
								</div>
                               </div>
                           <!-- end col-2 -->
                             <!-- begin col-4 -->
                                <div class="col-md-2">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" Visible="false" id="btnTokenSearch" Text="Search" class="btn btn-success" onclick="btnTokenSearch_Click"/>
							     </div>
                               </div>
                              <!-- end col-2 -->
                        </div>
                       <!-- end row -->
                         <!-- begin row -->
                        <div class="row">
                        
                       
                         <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>From Date</label>
								 <asp:TextBox ID="txtFromDate" runat="server" class="form-control datepicker"></asp:TextBox>
								 </div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>To Date</label>
								<asp:TextBox ID="txtToDate" runat="server" class="form-control datepicker"></asp:TextBox>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                         <!-- end row -->
                   
                       <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnReportView" Text="Report View" class="btn btn-success" onclick="btnReportView_Click"/>
							     </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->


</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

