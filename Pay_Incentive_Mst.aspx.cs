﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Pay_Incentive_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_WagesType();
          
        }
        Load_Data_Incentive();

        Load_Full_Night_Grid();
    }

    private void Load_Full_Night_Grid()
    {
        Query = "";
        Query = "select * from [" + SessionPayroll + "]..FullNight_Incentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(Query);
        Repeater2.DataSource = dt;
        Repeater2.DataBind();
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        string Category_Str = "0";
        if (ddlcategory.SelectedItem.Text == "STAFF")
        {
            Category_Str = "1";
        }
        else if (ddlcategory.SelectedItem.Text == "LABOUR")
        {
            Category_Str = "2";
        }
        query = "select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + Category_Str + "'";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Suriya_UnitA_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private void Load_Data_Incentive()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select WI.WorkerDays,WI.WorkerAmt,WI.MonthDays,MT.EmpType,WI.WorkingDays from [" + SessionPayroll + "]..WorkerIncentive_mst WI inner join MstEmployeeType MT on WI.EmpType=MT.EmpTypeCd where WI.Ccode='" + SessionCcode + "' And WI.Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
       
    }
    
   
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

        string query;
        string EmpType = "";
        DataTable DT = new DataTable();
        DataTable dt_Type = new DataTable();

        string[] cmdArg_Split = e.CommandArgument.ToString().Split(',');
        string[] cmdName_Split = e.CommandName.ToString().Split(',');
        EmpType = cmdArg_Split[0].ToString();
        string MonthDays = cmdArg_Split[1].ToString();
        string workerDays = cmdName_Split[0].ToString();
        string workingDays = cmdName_Split[1].ToString();

        query = "";
        query = "select EmpTypeCd from MstEmployeeType where EmpType='" + EmpType + "'";
        dt_Type = objdata.RptEmployeeMultipleDetails(query);
        if (dt_Type.Rows.Count > 0)
        {
            query = "select * from [" + SessionPayroll + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'  And EmpType='" + dt_Type.Rows[0]["EmpTypeCd"].ToString() + "' and WorkerDays='" + workerDays.ToString() + "' and MonthDays='" + MonthDays.ToString() + "' and WorkingDays='" + workingDays.ToString() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                query = "delete from [" + SessionPayroll + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpType='" + dt_Type.Rows[0]["EmpTypeCd"].ToString() + "' and WorkerDays='" + workerDays.ToString() + "' and MonthDays='" + MonthDays.ToString() + "' and WorkingDays='" + workingDays.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Incentive Deleted Successfully...!');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Type Not Match..!');", true);
        
        }
        Load_Data_Incentive();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string MessFlag = "Insert";

            if ((txtDaysOfMonth.Text.Trim() == "") || (txtDaysOfMonth.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Day of Month');", true);
                ErrFlag = true;
            }
            else if ((txtMinDaysWorked.Text.Trim() == "") || (txtMinDaysWorked.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Minimum Days Worked');", true);
                ErrFlag = true;
            }
            else if ((txtIncent_Amount.Text.Trim() == "") || (txtIncent_Amount.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Incentive Amount');", true);
                ErrFlag = true;
            }
            else if (ddlcategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                string Category_Str = "0";
                if (ddlcategory.SelectedItem.Text == "STAFF")
                {
                    Category_Str = "1";
                }
                else if (ddlcategory.SelectedItem.Text == "LABOUR")
                {
                    Category_Str = "2";
                }

                DataTable dt = new DataTable();
                Query = "Select * from [" + SessionPayroll + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Catname='" + Category_Str + "' and EmpType='" + txtEmployeeType.SelectedValue + "' and MonthDays='" + txtDaysOfMonth.Text + "' and WorkerDays='" + txtMinDaysWorked.Text + "' and WorkingDays='"+txtFixedWorkDays.Text+"'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [" + SessionPayroll + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  and MonthDays='" + txtDaysOfMonth.Text + "' and Catname='" + Category_Str + "' and EmpType='" + txtEmployeeType.SelectedValue + "' and MonthDays='" + txtDaysOfMonth.Text + "' and WorkerDays='" + txtMinDaysWorked.Text + "' and WorkingDays='" + txtFixedWorkDays.Text + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MessFlag = "Update";
                }
                Query = "Insert Into [" + SessionPayroll + "]..WorkerIncentive_mst (Ccode,Lcode,WorkerDays,WorkerAmt,MonthDays,Catname,EmpType,WorkingDays)";
                Query = Query + "values ('" + SessionCcode + "','" + SessionLcode + "','" + txtMinDaysWorked.Text + "','" + txtIncent_Amount.Text + "','" + txtDaysOfMonth.Text + "','" + Category_Str + "','" + txtEmployeeType.SelectedValue + "','" + txtFixedWorkDays.Text + "')";
                objdata.RptEmployeeMultipleDetails(Query);

              
                if (MessFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
                Load_Data_Incentive();
                txtMinDaysWorked.Text = "0";
                txtDaysOfMonth.Text = "0";
                txtIncent_Amount.Text = "0";
                txtFixedWorkDays.Text = "0";
            }
        }
        catch (Exception Ex)
        { 
        
        }
    }
    
    protected void btnSaveFullNight_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtFUllNightAmt.Text == "" && txtFUllNightAmt.Text == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter the Full Night Incentive Amount');", true);
            ErrFlag = true;
        }
        if (txtFUllNightEligibleDays.Text == "" && txtFUllNightEligibleDays.Text == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter the Full Night Eligible Days');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            Query = "";
            Query = "Select * from [" + SessionPayroll + "]..FullNight_Incentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            Query = Query + " and Days='" + txtFUllNightEligibleDays.Text + "'";
            DataTable dt_Check = new DataTable();
            dt_Check = objdata.RptEmployeeMultipleDetails(Query);
            if (dt_Check.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Full Night Incentive Details Already Present');", true);
            }
        }
        if (!ErrFlag)
        {
            Query = "";
            Query = "Insert into [" + SessionPayroll + "]..FullNight_Incentive (Ccode,Lcode,Days,Amt)";
            Query = Query + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtFUllNightEligibleDays.Text + "','" + txtFUllNightAmt.Text + "')";
            objdata.RptEmployeeMultipleDetails(Query);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Full Night Incentive Details Saved Successfully');", true);
            Load_Full_Night_Grid();
        }
    }

    protected void btnClearFullNight_Click(object sender, EventArgs e)
    {
        txtFUllNightEligibleDays.Text = "0";
        txtFUllNightAmt.Text = "0";
    }

    protected void btnDeleteEnquiry_Grid_FullNight_Command(object sender, CommandEventArgs e)
    {
        Query = "";
        Query = "Select * from [" + SessionPayroll + "]..FullNight_Incentive where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        Query = Query + " and Days='"+e.CommandArgument.ToString()+"'";
        DataTable dt_check = new DataTable();
        dt_check = objdata.RptEmployeeMultipleDetails(Query);
        if (dt_check.Rows.Count > 0)
        {
            Query = "Delete from [" + SessionPayroll + "]..FullNight_Incentive where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            Query = Query + " and Days='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(Query);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Full Night Incentive Details Deleted Successfully');", true);
            Load_Full_Night_Grid();
        }
    }
}
