﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;
using System.IO;

public partial class RptAdbanceShow : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string AgentName;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin; string SessionUserType;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate;
    string ToDate;
    string salaryType;
    string Emp_ESI_Code;
    string SessionPayroll;
    string EmployeeType; string EmployeeTypeCd;
    string PayslipType;
    string Report_Types;
    string TokenNo;
    string PFTypeGet;

    string Left_Employee = "0";
    string Left_Date = "";
    string Get_Report_Type = "0";
    string Salary_CashOrBank;
    string Get_Division_Name = "";

    string Other_state = "";
    string Non_Other_state = "";
    string Basic_Report_Date = "";
    string Basic_Report_Type = "OLD";
    string ExemptedStaff = "";
    string ReportName = "";
    string Months = "";
    string Years = "";
    string TypePF = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(constr);
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        Load_DB();
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        //str_cate = Request.QueryString["Cate"].ToString();
        ReportName = Request.QueryString["BName"].ToString();

        if (ReportName == "Advance Report")
        {

            DataTable dt = new DataTable();
            DataTable dt_Query = new DataTable();

            query = " Select AP.EmpNo,(EM.MachineID) as MachineID,(EM.DeptName) as Dept,(EM.FirstName) as Name,(EM.Wages) as Wages_Type,ID,(Amount) as AdvanceAmt,Convert(varchar,TransDate,105) as AdvanceDate, ";
            query = query + " Case Completed when 'N' then 'NO' Else 'YES' ";
            query = query + " End as Completed,(DueMonth) as Duration,(BalanceAmount) as BalanceAmt,convert(varchar,Modifieddate,105) as Modifieddate ";
            query = query + " from [Suriya_UnitA_Epay]..AdvancePayment AP inner join [Then_Spay]..Employee_Mst EM on AP.EmpNo=EM.EmpNo where Ccode='" + SessionCcode + "' and Completed='N'";
            query = query + " and Lcode='" + SessionLcode + "' and  BalanceAmount>'0'";
            //dt = objdata.RptEmployeeMultipleDetails(query);

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {
                DataTable dt_Cmp = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt_Cmp = objdata.RptEmployeeMultipleDetails(query);
                if (dt_Cmp.Rows.Count > 0)
                {
                    CmpName = dt_Cmp.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt_Cmp.Rows[0]["Address1"].ToString() + ", " + dt_Cmp.Rows[0]["Address2"].ToString() + ", " + dt_Cmp.Rows[0]["Location"].ToString() + "-" + dt_Cmp.Rows[0]["Pincode"].ToString());
                }
                SqlCommand cmdComp = new SqlCommand(query, con);
                SqlDataAdapter sdaComp = new SqlDataAdapter(cmdComp);

                con.Open();
                DataTable dt_2 = new DataTable();
                sdaComp.Fill(dt_2);
                con.Close();

                rd.Load(Server.MapPath("crystal/AdvanceReportNew.rpt"));
                rd.SetDataSource(dt_1);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";


                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.AllowedExportFormats = formats;

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
            

        }
        else if (ReportName == "Advance Report DateWise")
        {
            fromdate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            EmployeeType = Request.QueryString["EmpType"].ToString();

            DataTable dt = new DataTable();
            DataTable dt_Query = new DataTable();

            query = " Select AP.EmpNo,(EM.MachineID) as MachineID,(EM.DeptName) as Dept,(EM.FirstName) as Name,(EM.Wages) as Wages_Type,ID,(Amount) as AdvanceAmt,Convert(varchar,TransDate,105) as AdvanceDate, ";
            query = query + " Case Completed when 'N' then 'NO' Else 'YES' ";
            query = query + " End as Completed,(DueMonth) as Duration,(BalanceAmount) as BalanceAmt,convert(varchar,Modifieddate,105) as Modifieddate ";
            query = query + " from [Suriya_UnitA_Epay]..AdvancePayment AP inner join [Then_Spay]..Employee_Mst EM on AP.EmpNo=EM.EmpNo where Ccode='" + SessionCcode + "' and Completed='N'";
            query = query + " and Lcode='" + SessionLcode + "' and convert(datetime,AP.TransDate,103)>=convert(datetime,'" + fromdate + "',103) and ";
            query = query + " convert(datetime,AP.TransDate,103)<=convert(datetime,'" + ToDate + "',103) ";
            if (EmployeeType != "")
            {
                query = query + " and EM.Wages='" + EmployeeType + "'";
            }
            //dt = objdata.RptEmployeeMultipleDetails(query);

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {
                DataTable dt_Cmp = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt_Cmp = objdata.RptEmployeeMultipleDetails(query);
                if (dt_Cmp.Rows.Count > 0)
                {
                    CmpName = dt_Cmp.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt_Cmp.Rows[0]["Address1"].ToString() + ", " + dt_Cmp.Rows[0]["Address2"].ToString() + ", " + dt_Cmp.Rows[0]["Location"].ToString() + "-" + dt_Cmp.Rows[0]["Pincode"].ToString());
                }
                SqlCommand cmdComp = new SqlCommand(query, con);
                SqlDataAdapter sdaComp = new SqlDataAdapter(cmdComp);

                con.Open();
                DataTable dt_2 = new DataTable();
                sdaComp.Fill(dt_2);
                con.Close();

                rd.Load(Server.MapPath("crystal/AdvanceReport.rpt"));
                rd.SetDataSource(dt_1);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";
                rd.DataDefinition.FormulaFields["FromDate"].Text = "'" + fromdate + "'";
                rd.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";


                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.AllowedExportFormats = formats;

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }


        }
        else if (ReportName == "PF List")
        {

            Months = Request.QueryString["Months"].ToString();
            Years = Request.QueryString["yr"].ToString();
            fromdate = Request.QueryString["fromdate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            TypePF = Request.QueryString["TypePF"].ToString();

            if (TypePF == "0") // Staff Category
            {
                query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                                         " (SalDet.LOPDays) as WorkedDays,(SalDet.FFDA) as Basic_SM,(SalDet.BasicAndDANew) as BasicandDA,(SalDet.BasicHRA) as HR,(SalDet.Deduction3) as MediAllow, (SalDet.Deduction4) as EduAllow, " +
                                         " (SalDet.GrossEarnings) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4) as TotalDeductions," +
                                         " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4)) as New_Tot_Amt,EmpDet.DeptName as Department  " +
                                         " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                         " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                         " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                         " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + Years + "'  and " +
                                         " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + Years + "' and " +
                                         " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                         " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='STAFF'" +
                                         " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + fromdate + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";

                query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA," +
               " EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Deduction3,SalDet.Deduction4, " +
               " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI " +
               " Order by cast(EmpDet.ExistingCode as int) Asc";
            }
            else if (TypePF == "1") //Fiter, Driver and Security
            {
                query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                                          " (SalDet.LOPDays) as WorkedDays,(SalDet.FFDA) as Basic_SM,(SalDet.BasicAndDANew) as BasicandDA,(SalDet.BasicHRA) as HR,(SalDet.Deduction3) as MediAllow, (SalDet.Deduction4) as EduAllow, " +
                                          " (SalDet.GrossEarnings) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4) as TotalDeductions," +
                                          " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4)) as New_Tot_Amt,EmpDet.DeptName as Department " +
                                         " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                         " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                         " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                         " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + Years + "'  and " +
                                         " AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + Years + "' and " +
                                         " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                         " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and (EmpDet.Wages='FITTER & ELECTRICIANS' or EmpDet.Wages='SECURITY' or EmpDet.Wages='DRIVERS')" +
                                         " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + fromdate + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";

                query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA," +
               " EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Deduction3,SalDet.Deduction4, " +
               " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI " +
               " Order by cast(EmpDet.ExistingCode as int) Asc";
            }
            else  // All Labour
            {
                query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.Wages," +
                    "MstDpt.DeptName as DepartmentNm, sum(cast(SalDet.LOPDays as decimal(18,2))) as WorkedDays,sum(cast(SalDet.Basic_SM as decimal(18,2))) as Basic_SM,sum(cast(SalDet.BasicAndDANew as decimal(18,2))) as BasicandDA," +
                    "sum(cast(SalDet.BasicHRA as decimal(18,2))) as HR,sum(cast(SalDet.New_Mess_Amt as decimal(18,2))) as MediAllow, sum(cast(SalDet.Deduction4 as decimal(18,2))) as EduAllow,sum(cast(SalDet.GrossEarnings as decimal(18,2))) as GrossEarningsOT," +
                    "sum(cast(SalDet.ProvidentFund as decimal(18,2))) as PF,sum(cast(SalDet.Advance as decimal(18,2))) as Advance,sum(cast(SalDet.ESI as decimal(18,2))) as ESI," +
                    "(sum(cast(SalDet.ProvidentFund as decimal(18,2))) + sum(cast(SalDet.ESI as decimal(18,2))) + sum(cast(SalDet.advance as decimal(18,2))) + sum(cast(SalDet.New_Mess_Amt as decimal(18,2))) + sum(cast(SalDet.Deduction4 as decimal(18,2)))) as TotalDeductions, " +
                    "((sum(SalDet.GrossEarnings))- (sum(SalDet.ProvidentFund) + sum(SalDet.ESI) + sum(SalDet.advance) + sum(SalDet.Deduction3) + sum(SalDet.Deduction4))) as New_Tot_Amt" +
                    " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    //" inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                    " where SalDet.Month='" + Months + "' AND SalDet.FinancialYear='" + Years + "'  and " +
                    //" AttnDet.Months='" + Months + "' AND AttnDet.FinancialYear='" + Years + "' and " +
                    //" SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                    " SalDet.Lcode='" + SessionLcode + "' And " +
                    " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and (EmpDet.Wages='LOCAL' or EmpDet.Wages='TAMIL BOYS' or EmpDet.Wages='TAMIL GIRLS' or EmpDet.Wages='CANTEEN' or EmpDet.Wages='HINDI BOYS' or EmpDet.Wages='HINDI GIRLS' or EmpDet.Wages='15DAYS' or EmpDet.Wages='WEEKLY')" +
                    " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + fromdate + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";

                query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,EmpDet.Wages " +
                    " having sum(cast(SalDet.LOPDays as decimal(18,2))) > 0" +
               " Order by cast(EmpDet.ExistingCode as int) Asc";
            }

            DataTable dt_1 = new DataTable();
            dt_1 = objdata.RptEmployeeMultipleDetails(query);

            if (dt_1.Rows.Count > 0)
            {
                DataTable dt_Cmp = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt_Cmp = objdata.RptEmployeeMultipleDetails(query);
                if (dt_Cmp.Rows.Count > 0)
                {
                    CmpName = dt_Cmp.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt_Cmp.Rows[0]["Address1"].ToString() + ", " + dt_Cmp.Rows[0]["Address2"].ToString() + ", " + dt_Cmp.Rows[0]["Location"].ToString() + "-" + dt_Cmp.Rows[0]["Pincode"].ToString());
                }
                SqlCommand cmdComp = new SqlCommand(query, con);
                SqlDataAdapter sdaComp = new SqlDataAdapter(cmdComp);

                con.Open();
                DataTable dt_2 = new DataTable();
                sdaComp.Fill(dt_2);
                con.Close();

                rd.Load(Server.MapPath("Payslip_New_Component/PFListAll.rpt"));
                rd.SetDataSource(dt_1);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                //rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";
                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "PL LIST FROM : " + fromdate + " - " + "TO : " + " " + ToDate + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.AllowedExportFormats = formats;

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }


        }
        else if (ReportName == "Voucher")
        {
            TokenNo = Request.QueryString["EmpNo"].ToString();
            query = query + "select (AD.EmpNo) as EmpCode,(EM.FirstName) as Name,(AD.Amount) as AdvanceAmt,CONVERT(varchar(10),AD.TransDate,103) as AdvanceDate  from [" + SessionPayroll + "]..AdvancePayment AD inner join Employee_Mst EM on AD.EmpNo=EM.EmpNo where AD.Completed='S' and  AD.EmpNo='" + TokenNo + "' and AD.Ccode='" + SessionCcode + "' and AD.Lcode='" + SessionLcode + "'";
            DataTable dt_1 = new DataTable();
            dt_1 = objdata.RptEmployeeMultipleDetails(query);
            if (dt_1.Rows.Count > 0)
            {
                string WordString = Words(Convert.ToInt32(dt_1.Rows[0]["AdvanceAmt"])).ToString();

                DataTable dt_Cmp = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt_Cmp = objdata.RptEmployeeMultipleDetails(query);
                if (dt_Cmp.Rows.Count > 0)
                {
                    CmpName = dt_Cmp.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt_Cmp.Rows[0]["Address1"].ToString() + ", " + dt_Cmp.Rows[0]["Address2"].ToString() + ", " + dt_Cmp.Rows[0]["Location"].ToString() + "-" + dt_Cmp.Rows[0]["Pincode"].ToString());
                }
                SqlCommand cmdComp = new SqlCommand(query, con);
                SqlDataAdapter sdaComp = new SqlDataAdapter(cmdComp);

                con.Open();
                DataTable dt_2 = new DataTable();
                sdaComp.Fill(dt_2);
                con.Close();

                rd.Load(Server.MapPath("crystal/AdvanceVoucher.rpt"));
                rd.SetDataSource(dt_1);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["Words"].Text = "'" + WordString + "'";
                //rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";


                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.AllowedExportFormats = formats;

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            { 
            
            }

        }

        else if (ReportName == "Company Payslip")
        {
            str_cate = Request.QueryString["Cate"].ToString();
            str_month = Request.QueryString["Months"].ToString();
            str_yr = Request.QueryString["yr"].ToString();
            fromdate = Request.QueryString["fromdate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            Report_Types = Request.QueryString["ReportFormat"].ToString();
            AgentName = Request.QueryString["AgentName"].ToString();
            //ReportName = Request.QueryString["ReportName"].ToString(); 
            salaryType = Request.QueryString["Salary"].ToString();
            EmployeeTypeCd = Request.QueryString["EmpTypeCd"].ToString();
            EmployeeType = Request.QueryString["EmpType"].ToString();


            if (EmployeeTypeCd == "2") { EmployeeType = "STAFF"; }
            if (EmployeeTypeCd == "3") { EmployeeType = "FITTER & ELECTRICIANS"; }
            if (EmployeeTypeCd == "4") { EmployeeType = "SECURITY"; }
            if (EmployeeTypeCd == "5") { EmployeeType = "DRIVERS"; }
            if (EmployeeTypeCd == "6") { EmployeeType = "LOCAL"; }
            if (EmployeeTypeCd == "7") { EmployeeType = "TAMIL BOYS"; }
            if (EmployeeTypeCd == "8") { EmployeeType = "TAMIL GIRLS"; }
            if (EmployeeTypeCd == "9") { EmployeeType = "CANTEEN"; }
            if (EmployeeTypeCd == "10") { EmployeeType = "HINDI BOYS"; }
            if (EmployeeTypeCd == "11") { EmployeeType = "HINDI GIRLS"; }
            if (EmployeeTypeCd == "12") { EmployeeType = "15DAYS"; }
            if (EmployeeTypeCd == "13") { EmployeeType = "WEEKLY"; }

            int YR = 0;
            string report_head = "";
            if (str_month == "January")
            {
                YR = Convert.ToInt32(str_yr);
                YR = YR + 1;
            }
            else if (str_month == "February")
            {
                YR = Convert.ToInt32(str_yr);
                YR = YR + 1;
            }
            else if (str_month == "March")
            {
                YR = Convert.ToInt32(str_yr);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(str_yr);
            }

            if (salaryType == "2")
            {
                report_head = "PAYSLIP FOR THE MONTH OF " + str_month.ToUpper() + " " + YR.ToString();
            }
            else
            {
                report_head = "PAYSLIP FOR THE MONTH OF " + fromdate.ToString() + " - " + ToDate.ToString();
            }


            salaryType = Request.QueryString["Salary"].ToString();



            DataTable dt_Query = new DataTable();
            //Check PF Category
            if (salaryType.ToString() != "0")
            {

            }
            else
            {

                if (Report_Types == "2")
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,(SalDet.Words) as Basic_Incentive_Text," +
                                 " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive," +
                                 " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                                 " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                                 " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                                 " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                                 " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                 " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                 " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                 " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and " +
                                 " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                                 " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And convert(datetime,AttnDet.FromDate,103)>=convert(datetime,'" + fromdate + "',103) and convert(datetime,AttnDet.ToDate,103)>=convert(datetime,'" + ToDate + "',103) And " +
                                 " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.AgentName='" + AgentName + "'" +
                                 " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes') and convert(datetime,SalDet.FromDate,103)>=convert(datetime,'" + fromdate + "',103) and convert(datetime,SalDet.ToDate,103)>=convert(datetime,'" + ToDate + "',103) ";

                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive," +
                   " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt,SalDet.Words," +
                   " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                   " Order by cast(EmpDet.ExistingCode as int) Asc";
                }
                else
                {
                    if ((EmployeeTypeCd == "10") || (EmployeeTypeCd == "11") || (EmployeeTypeCd == "9") || (EmployeeTypeCd == "6") || (EmployeeTypeCd == "7") || (EmployeeTypeCd == "8"))
                    {
                        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,(SalDet.Words) as Basic_Incentive_Text," +
                               " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive," +
                               " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                               " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                               " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                               " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                               " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                               " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                               " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                               " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                               " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                               " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                               " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'" +
                               " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')";

                        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive," +
                       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt,SalDet.Words," +
                       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                       " Order by cast(EmpDet.ExistingCode as int) Asc";
                    }
                    else if ((EmployeeTypeCd == "12") || (EmployeeTypeCd == "13"))
                    {
                        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,(SalDet.Words) as Basic_Incentive_Text," +
                                  " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive," +
                                  " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                                  " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                                  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                                  " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                  " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                                  " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And convert(datetime,AttnDet.FromDate,103)>=convert(datetime,'" + fromdate + "',103) and convert(datetime,AttnDet.ToDate,103)<=convert(datetime,'" + ToDate + "',103) And " +
                                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'" +
                                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes') and convert(datetime,SalDet.FromDate,103)>=convert(datetime,'" + fromdate + "',103) and convert(datetime,SalDet.ToDate,103)<=convert(datetime,'" + ToDate + "',103) ";

                        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive," +
                       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt,SalDet.Words," +
                       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                       " Order by cast(EmpDet.ExistingCode as int) Asc";
                    }
                    else if ((EmployeeTypeCd == "2") || (EmployeeTypeCd == "3") || (EmployeeTypeCd == "4") || (EmployeeTypeCd == "5"))
                    {
                        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,(EmpDet.BaseSalary + EmpDet.VPF) as Basic_SM,SalDet.Dayincentive," +
                                  " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA) as NonPFWages,(SalDet.Words) as Basic_Incentive_Text," +
                                  " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                                  " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                                  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advance, " +
                                  " (SalDet.New_Cash_Amt) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt " +
                                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                  " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                                  " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'" +
                                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')";

                        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive,SalDet.Words," +
                       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt,EmpDet.BaseSalary,EmpDet.VPF," +
                       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                       " Order by cast(EmpDet.ExistingCode as int) Asc";
                    }
                }

                dt_Query = objdata.RptEmployeeMultipleDetails(query);
            }

            if (dt_Query.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }
                SqlCommand cmdComp = new SqlCommand(query, con);
                SqlDataAdapter sdaComp = new SqlDataAdapter(cmdComp);

                con.Open();
                DataTable dt_1 = new DataTable();
                sdaComp.Fill(dt_1);
                con.Close();


                rd.Load(Server.MapPath("Payslip_New_Component/New_Tpsm_Payslip.rpt"));
                rd.SetDataSource(dt_Query);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "PAY SLIP FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.AllowedExportFormats = formats;

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);

            }

        }
        else if (ReportName == "AGENT WISE WORKER REPORT")
        {
            DataTable dt_Query = new DataTable();
            string OneDay;
            string PerDay;

            AgentName = Request.QueryString["AgentName"].ToString();



            query = "";
            query = query + "select (ExistingCode) as ExistingCode,(FirstName) as EmpName,DeptCode,(DeptName) as DepartmentNm,Wages,(BaseSalary + VPF) as FullSalary,'' as  BaseSalary from Employee_Mst where AgentName='" + AgentName + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            dt_Query = objdata.RptEmployeeMultipleDetails(query);
            if (dt_Query.Rows.Count > 0)
            {
                for (int i = 0; i < dt_Query.Rows.Count; i++)
                {
                    if ((dt_Query.Rows[i]["Wages"].ToString() == "STAFF") || (dt_Query.Rows[i]["Wages"].ToString() == "FITTER & ELECTRICIANS") || (dt_Query.Rows[i]["Wages"].ToString() == "SECURITY") || (dt_Query.Rows[i]["Wages"].ToString() == "DRIVERS"))
                    {
                        OneDay = dt_Query.Rows[i]["FullSalary"].ToString();
                        PerDay = (Convert.ToDecimal(OneDay) / Convert.ToDecimal(26)).ToString();
                        PerDay = (Math.Round(Convert.ToDecimal(PerDay), 0, MidpointRounding.AwayFromZero)).ToString();
                        dt_Query.Rows[i]["BaseSalary"] = PerDay;

                    }
                    else
                    {
                        OneDay = dt_Query.Rows[i]["FullSalary"].ToString();
                        PerDay = (Convert.ToDecimal(OneDay)).ToString();
                        PerDay = (Math.Round(Convert.ToDecimal(PerDay), 0, MidpointRounding.AwayFromZero)).ToString();
                        dt_Query.Rows[i]["BaseSalary"] = PerDay;
                    }
                }


                DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }
                SqlCommand cmdComp = new SqlCommand(query, con);
                SqlDataAdapter sdaComp = new SqlDataAdapter(cmdComp);

                con.Open();
                DataTable dt_1 = new DataTable();
                sdaComp.Fill(dt_1);
                con.Close();


                rd.Load(Server.MapPath("crystal/AgentWise.rpt"));
                rd.SetDataSource(dt_Query);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";
                rd.DataDefinition.FormulaFields["AgentName"].Text = "'" + AgentName + "'";

                //rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "PAY SLIP FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.AllowedExportFormats = formats;

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        else if (ReportName == "AGENT WISE WORKER REPORT EXCEL")
        {
            DataTable dt_Query = new DataTable();
            string OneDay;
            string PerDay;
            string From_Date_Str = "";
            string To_Date_Str = "";
            AgentName = Request.QueryString["AgentName"].ToString();
            From_Date_Str = Request.QueryString["FromDate"].ToString();
            To_Date_Str = Request.QueryString["ToDate"].ToString();



            query = "";
            query = query + "Select EM.ExistingCode,Em.FirstName,EM.DeptName,EM.Wages,(EM.BaseSalary + EM.VPF) as FullSalary,'' as  BaseSalary,";
            query = query + " isnull(sum(LD.Present),0) as Days_Count,convert(date,EM.DOJ,105) as DOJ from Employee_Mst EM";
            query = query + " inner join LogTime_Days LD on LD.CompCode=EM.CompCode And LD.LocCode=EM.LocCode And LD.MachineID=EM.MachineID And LD.ExistingCode=EM.ExistingCode";
            query = query + " where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            query = query + " And LD.CompCode='" + SessionCcode + "' And LD.LocCode='" + SessionLcode + "'";
            query = query + " And EM.AgentName='" + AgentName + "'";
            query = query + " And CONVERT(DATETIME,LD.Attn_Date,105) >= CONVERT(DATETIME,'" + From_Date_Str + "',105)";
            query = query + " And CONVERT(DATETIME,LD.Attn_Date,105) <= CONVERT(DATETIME,'" + To_Date_Str + "',105)";
            query = query + " group by EM.ExistingCode,Em.FirstName,EM.DeptName,EM.Wages,EM.BaseSalary,EM.VPF,EM.DOJ";
            query = query + " having isnull(sum(LD.Present),0) > 0";
            dt_Query = objdata.RptEmployeeMultipleDetails(query);

            if (dt_Query.Rows.Count > 0)
            {
                string attachment = "attachment;filename=Agent_Wise_Employee_Report.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

                DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);

                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='7'>");
                Response.Write("" + CmpName + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='7'>");
                Response.Write("" + SessionLcode + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='7'>");
                Response.Write("" + Cmpaddress + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='7'>");
                Response.Write("AGENT NAME : " + AgentName.ToString().ToUpper() + " - From Date : " + From_Date_Str + " ToDate : " + To_Date_Str);
                Response.Write("</td>");
                Response.Write("</tr></table>");

                Response.Write("<table border='1'><tr><td>S.NO</td><td>EMP.NO</td><td>EMP.NAME</td><td>DEPARTMENT</td><td>CATEGORY</td><td>BASIC</td><td>WORKED DAYS</td><td>DATE OF JOINING</td>");
                Response.Write("</tr></table>");
                for (int i = 0; i < dt_Query.Rows.Count; i++)
                {
                    if ((dt_Query.Rows[i]["Wages"].ToString() == "STAFF") || (dt_Query.Rows[i]["Wages"].ToString() == "FITTER & ELECTRICIANS") || (dt_Query.Rows[i]["Wages"].ToString() == "SECURITY") || (dt_Query.Rows[i]["Wages"].ToString() == "DRIVERS"))
                    {
                        OneDay = dt_Query.Rows[i]["FullSalary"].ToString();
                        PerDay = (Convert.ToDecimal(OneDay) / Convert.ToDecimal(26)).ToString();
                        PerDay = (Math.Round(Convert.ToDecimal(PerDay), 0, MidpointRounding.AwayFromZero)).ToString();
                        dt_Query.Rows[i]["BaseSalary"] = PerDay;
                    }
                    else
                    {
                        OneDay = dt_Query.Rows[i]["FullSalary"].ToString();
                        PerDay = (Convert.ToDecimal(OneDay)).ToString();
                        PerDay = (Math.Round(Convert.ToDecimal(PerDay), 0, MidpointRounding.AwayFromZero)).ToString();
                        dt_Query.Rows[i]["BaseSalary"] = PerDay;
                    }
                    Response.Write("<table border='1'><tr>");
                    Response.Write("<td>" + (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString() + "</td>");
                    Response.Write("<td>" + dt_Query.Rows[i]["ExistingCode"].ToString() + "</td>");
                    Response.Write("<td>" + dt_Query.Rows[i]["FirstName"].ToString() + "</td>");
                    Response.Write("<td>" + dt_Query.Rows[i]["DeptName"].ToString() + "</td>");
                    Response.Write("<td>" + dt_Query.Rows[i]["Wages"].ToString() + "</td>");
                    Response.Write("<td>" + dt_Query.Rows[i]["BaseSalary"].ToString() + "</td>");
                    Response.Write("<td>" + dt_Query.Rows[i]["Days_Count"].ToString() + "</td>");
                    Response.Write("<td>" + Convert.ToDateTime(dt_Query.Rows[i]["DOJ"].ToString()).ToString("dd/MM/yyyy") + "</td>");
                    Response.Write("</tr></table>");
                }

                Response.End();
                Response.Clear();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }
        else if (ReportName == "Emp Wise Report")
        {
            TokenNo = Request.QueryString["EmpNo"].ToString();


            DataTable DataCell = new DataTable();
            DataTable dt = new DataTable();
            DataTable dt_Pay = new DataTable();
            DataCell.Columns.Add("EmpNo");
            DataCell.Columns.Add("EmpName");
            DataCell.Columns.Add("Debit");
            DataCell.Columns.Add("Credit");
            DataCell.Columns.Add("Date");


            query = "";
            query = query + " select EM.EmpNo,(EM.FirstName) as EmpName,AP.Amount,convert(varchar(10),AP.TransDate,103) as Dates from [Suriya_UnitA_Epay]..AdvancePayment AP inner join Employee_Mst EM on AP.EmpNo=EM.EmpNo  ";
            query = query + " Where AP.EmpNo='" + TokenNo + "' order by TransDate asc";
            //SQL = SQL + " convert(datetime,TransDate,103)<=convert(datetime,'" + txtToDate.Text + "',103)";
            dt = objdata.RptEmployeeMultipleDetails(query);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataCell.NewRow();
                    DataCell.Rows.Add();

                    DataCell.Rows[i]["EmpNo"] = dt.Rows[i]["EmpNo"].ToString();
                    DataCell.Rows[i]["EmpName"] = dt.Rows[i]["EmpName"].ToString();
                    DataCell.Rows[i]["Debit"] = dt.Rows[i]["Amount"].ToString();
                    DataCell.Rows[i]["Date"] = dt.Rows[i]["Dates"].ToString();
                    DataCell.Rows[i]["Credit"] = "0";
                }
            }

            query = "";
            query = query + " select EM.EmpNo,(EM.FirstName) as EmpName,AP.Amount,convert(varchar(10),AP.TransDate,103) as Dates from [Suriya_UnitA_Epay]..Advancerepayment AP inner join Employee_Mst EM on AP.EmpNo=EM.EmpNo  ";
            query = query + " Where AP.EmpNo='" + TokenNo + "' order by TransDate asc ";
            //SQL = SQL + " convert(datetime,TransDate,103)<=convert(datetime,'" + txtToDate.Text + "',103)";
            dt_Pay = objdata.RptEmployeeMultipleDetails(query);
            if (dt_Pay.Rows.Count > 0)
            {
                for (int i = 0; i < dt_Pay.Rows.Count; i++)
                {
                    DataCell.NewRow();
                    DataCell.Rows.Add();

                    DataCell.Rows[DataCell.Rows.Count - 1]["EmpNo"] = dt_Pay.Rows[i]["EmpNo"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["EmpName"] = dt_Pay.Rows[i]["EmpName"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["Debit"] = "0";
                    DataCell.Rows[DataCell.Rows.Count - 1]["Date"] = dt_Pay.Rows[i]["Dates"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["Credit"] = dt_Pay.Rows[i]["Amount"].ToString();
                }
            }

            if (DataCell.Rows.Count > 0)
            {
                DataTable dt_Cmp = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt_Cmp = objdata.RptEmployeeMultipleDetails(query);
                if (dt_Cmp.Rows.Count > 0)
                {
                    CmpName = dt_Cmp.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt_Cmp.Rows[0]["Address1"].ToString() + ", " + dt_Cmp.Rows[0]["Address2"].ToString() + ", " + dt_Cmp.Rows[0]["Location"].ToString() + "-" + dt_Cmp.Rows[0]["Pincode"].ToString());
                }
                SqlCommand cmdComp = new SqlCommand(query, con);
                SqlDataAdapter sdaComp = new SqlDataAdapter(cmdComp);

                con.Open();
                DataTable dt_2 = new DataTable();
                sdaComp.Fill(dt_2);
                con.Close();

                rd.Load(Server.MapPath("Payslip_New_Component/EmployeeClosingLedger.rpt"));
                rd.SetDataSource(DataCell);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";


                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.AllowedExportFormats = formats;

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Suriya_UnitA_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private string Words(int number)
    {
        if (number == 0)
            return "ZERO";
        if (number < 0)
            return "minus " + Words(Math.Abs(number));
        string words = "";
        if ((number / 1000000) > 0)
        {
            words += Words(number / 1000000) + " MILLION ";
            number %= 1000000;
        }
        if ((number / 100000) > 0)
        {
            words += Words(number / 100000) + "LAKH";
            number %= 100000;
        }
        if ((number / 1000) > 0)
        {
            words += Words(number / 1000) + " THOUSAND ";
            number %= 1000;
        }
        if ((number / 100) > 0)
        {
            words += Words(number / 100) + " HUNDRED ";
            number %= 100;
        }
        if (number > 0)
        {
            if (words != "")
                words += "AND ";
            var unitsMap = new[] { "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN" };
            var tensMap = new[] { "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY" };

            if (number < 20)
                words += unitsMap[number];
            else
            {
                words += tensMap[number / 10];
                if ((number % 10) > 0)
                    words += " " + unitsMap[number % 10];
            }
        }
        return words;
    }
}
