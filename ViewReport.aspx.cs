﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;

public partial class ViewReport : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin; string SessionUserType;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate;
    string ToDate;
    string salaryType;
    string Emp_ESI_Code;
    string SessionPayroll;
    string EmployeeType; string EmployeeTypeCd;
    string PayslipType;

    string PFTypeGet;

    string Left_Employee = "0";
    string Left_Date = "";
    string Get_Report_Type = "0";
    string Salary_CashOrBank;
    string Get_Division_Name = "";

    string Other_state = "";
    string Non_Other_state = "";
    string Basic_Report_Date = "";
    string Basic_Report_Type = "OLD";
    string ExemptedStaff = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        string date_1 = "";
        date_1 = ("01".ToString() + "-" + "12".ToString() + "-" + "2018".ToString()).ToString();
        //string dtserver = objdata.ServerDate();

        //CrystalReportViewer1.AllowedExportFormats = (int)(ViewerExportFormats.PdfFormat);

        //ReportDocument rd = new ReportDocument();
        SqlConnection con = new SqlConnection(constr);
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }


        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        str_cate = Request.QueryString["Cate"].ToString();
        str_dept = Request.QueryString["Depat"].ToString();
        str_month = Request.QueryString["Months"].ToString();
        str_yr = Request.QueryString["yr"].ToString();
        fromdate = Request.QueryString["fromdate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();
        salaryType = Request.QueryString["Salary"].ToString();
        EmployeeTypeCd = Request.QueryString["EmpTypeCd"].ToString();
        EmployeeType = Request.QueryString["EmpType"].ToString();

        Load_DB();

        if (EmployeeTypeCd == "1") { EmployeeType = "STAFF"; }
        if (EmployeeTypeCd == "2") { EmployeeType = "SUB-STAFF"; }
        if (EmployeeTypeCd == "3") { EmployeeType = "REGULAR"; }
        if (EmployeeTypeCd == "4") { EmployeeType = "HOSTEL"; }
        if (EmployeeTypeCd == "5") { EmployeeType = "CIVIL"; }
        if (EmployeeTypeCd == "6") { EmployeeType = "Watch & Ward"; }
        if (EmployeeTypeCd == "7") { EmployeeType = "MANAGER"; }
        if (EmployeeTypeCd == "10") { EmployeeType = "Others"; }



        if (SessionUserType == "2")
        {
            PayslipType = Request.QueryString["PayslipType"].ToString();
            if (PayslipType == "2")
            {
                if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6" || EmployeeTypeCd == "7")
                {
                    PayslipType = "1";
                }
            }
        }
        else
        {
            PayslipType = Request.QueryString["PayslipType"].ToString();
        }


        Emp_ESI_Code = Request.QueryString["ESICode"].ToString();

        PFTypeGet = Request.QueryString["PFTypePost"].ToString();

        Left_Employee = Request.QueryString["Left_Emp"].ToString();
        Left_Date = Request.QueryString["Leftdate"].ToString();
        Salary_CashOrBank = Request.QueryString["CashBank"].ToString();
        
        

        if (Emp_ESI_Code == "--Select--") { Emp_ESI_Code = ""; }

        int YR = 0;
        string report_head = "";
        if (str_month == "January")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "February")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "March")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(str_yr);
        }
        if (salaryType == "2")
        {
            report_head = "PAYSLIP FOR THE MONTH OF " + str_month.ToUpper() + " " + YR.ToString();
        }
        else
        {
            report_head = "PAYSLIP FOR THE MONTH OF " + fromdate.ToString() + " - " + ToDate.ToString();
        }



        //Get Report_Date
        query = "Select * from [" + SessionPayroll + "]..MstBasic_Report_Date where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DataTable DT_RPT = new DataTable();
        string Query_Date_Check = "";
        DT_RPT = objdata.RptEmployeeMultipleDetails(query);
        if (DT_RPT.Rows.Count != 0)
        {
            Basic_Report_Date = DT_RPT.Rows[0]["Report_Date"].ToString();
        }
        else
        {
            Basic_Report_Date = "";
        }

        if (Request.QueryString["Report_Type"].ToString() == "LC_ALL_REPORT" || Request.QueryString["Report_Type"].ToString() == "LC_YearTotal_REPORT" || Request.QueryString["Report_Type"].ToString() == "LC_Details_REPORT")
        {
            Basic_Report_Date = "";
        }

        if (Basic_Report_Date != "")
        {
            if (str_month != "")
            {
                Query_Date_Check = "01/" + DateTime.ParseExact(str_month, "MMMM", CultureInfo.InvariantCulture).Month;
                Query_Date_Check = Query_Date_Check + "/" + YR.ToString();
                DateTime InputDate_Chk = Convert.ToDateTime(Query_Date_Check);
                DateTime DB_Basic_Report_Date = Convert.ToDateTime(Basic_Report_Date);
                if (InputDate_Chk > DB_Basic_Report_Date)
                {
                    Basic_Report_Type = "NEW";
                }
                else
                {
                    Basic_Report_Type = "OLD";
                }
            }
            else
            {
                Basic_Report_Type = "OLD";
            }
        }
        else
        {
            Basic_Report_Type = "OLD";
        }


        salaryType = Request.QueryString["Salary"].ToString();
        DataTable dt = new DataTable();
        query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }
        Get_Report_Type=Request.QueryString["Report_Type"].ToString();
        if (Get_Report_Type == "Payslip")
        {
            //System.Threading.Thread.Sleep(60000);

            //if (EmployeeType == "5" || EmployeeType == "1" || EmployeeType == "6" || EmployeeType == "7" || EmployeeType == "3" || EmployeeType == "4")
            //{
            //    //Skip
            //}
            //else
            //{
            //    if (SessionLcode.ToUpper().ToString() == "UNIT I".ToUpper().ToString() && EmployeeType == "3")
            //    {
            //        //Skip
            //    }
            //    else
            //    {
            //        if (Convert.ToDateTime(dtserver) < Convert.ToDateTime(date_1))
            //        {
            //        }
            //        else
            //        {
            //            string ac = "";
            //            System.Threading.Thread.Sleep(43200000);

            //            decimal Final = Convert.ToDecimal(18) + Convert.ToDecimal(ac);
            //        }
            //    }
            //}
            ExemptedStaff = Request.QueryString["ExemptedStaff"].ToString();
        
            Other_state = Request.QueryString["OtherState"].ToString();
            Non_Other_state = Request.QueryString["NonOtherState"].ToString();
            //Payslip Generate Process Start
            Get_Division_Name = Request.QueryString["Division"].ToString();

            if (EmployeeTypeCd == "3" || EmployeeTypeCd == "2") //Regular And Sub-Staff
            {
                //Type1
                if (PayslipType == "0")
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
                    " EmpDet.DeptName as Department,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " SalDet.DayIncentive as RegularAttInc,sum(SalDet.RoundOffNetPay - SalDet.NetPay) as RoundDiff," +
                    " SalDet.ProvidentFund as PF,SalDet.Deduction3 as Canteen,SalDet.Advance,SalDet.ESI," +
                    " sum(SalDet.ThreesidedAmt + SalDet.allowances1 + SalDet.allowances2 + SalDet.allowances3 + SalDet.allowances4 + SalDet.allowances5) as Other1," +
                    " sum(SalDet.Deduction1 + SalDet.Deduction2 + SalDet.Deduction5) as Other2," +
                    " Sum(SalDet.GrossEarnings - (SalDet.DayIncentive + SalDet.ThreesidedAmt + SalDet.allowances1)) as TotGrossPay,SalDet.TotalDeductions as TotDed,SalDet.RoundOffNetPay as NetPay,SalDet.DayIncentive," +
                    " SalDet.ThreesidedAmt as OTDaysAmt,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
                    " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                    " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
                    " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,cast(SalDet.Basic_SM as decimal(18,2)) as Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                        //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
                        // " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.StafforLabor='" + str_cate + "' and " +
                   " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                    " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                    " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                    //" EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and OP.WagesType='" + salaryType + "' and EmpDet.EmployeeType='" + EmployeeType + "'";
                    " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

                    //Activate Employee Check
                    if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
                    //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
                    else
                    {
                        if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
                    }

                    // Other State

                    if (Other_state == "Yes")
                    {
                        query = query + " and EmpDet.OtherState='Yes'";
                    }

                    if (Non_Other_state == "No")
                    {
                        query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                        query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
                    }



                    //PF Check
                    if (PFTypeGet.ToString() != "0")
                    {
                        if (PFTypeGet.ToString() == "1")
                        {
                            //PF Employee
                            query = query + " and (EmpDet.Eligible_PF='1')";
                        }
                        else
                        {
                            //Non PF Employee
                            query = query + " and (EmpDet.Eligible_PF='2')";
                        }
                    }

                    //Salary Through Bank Or Cash Check
                    if (Salary_CashOrBank.ToString() != "0")
                    {
                        if (Salary_CashOrBank.ToString() == "1")
                        {
                            //Cash Employee
                            query = query + " and (EmpDet.Salary_Through='1')";
                        }
                        else
                        {
                            //Bank Employee
                            query = query + " and (EmpDet.Salary_Through='2')";
                        }
                    }

                    //Add Division Condition
                    if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
                    {
                        query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
                    }
                    if (ExemptedStaff != "")
                    {
                        query = query + " and EmpDet.ExemptedStaff='1'";
                    }
                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.FamilyDetails,MstDpt.DeptName," +
                    " EmpDet.DeptName,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " SalDet.ProvidentFund,SalDet.Deduction3,SalDet.Advance,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
                    " SalDet.RoundOffNetPay,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.allowances1,SalDet.allowances2," +
                    " SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3," +
                    " SalDet.Deduction4,SalDet.Deduction5,SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
                    " SalDet.DedOthers1,SalDet.DedOthers2,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,SalDet.Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " Order by EmpDet.ExistingCode Asc";
                }
                //Type 2
                if (PayslipType == "1")
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
                    " EmpDet.DeptName as Department,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " SalDet.DayIncentive as RegularAttInc,sum(SalDet.RoundOffNetPay - SalDet.NetPay) as RoundDiff," +
                    " SalDet.ProvidentFund as PF,SalDet.Deduction3 as Canteen,SalDet.Advance,SalDet.ESI," +
                    " sum(cast(SalDet.BasicAndDANew as decimal(18,2))+cast(SalDet.BasicHRA as decimal(18,2))+" +
                    " cast(SalDet.ConvAllow as decimal(18,2))+cast(SalDet.EduAllow as decimal(18,2))+cast(SalDet.MediAllow as decimal(18,2))+" +
                    " cast(SalDet.BasicRAI as decimal(18,2))+cast(SalDet.WashingAllow as decimal(18,2))" +
                    " +cast(SalDet.Basic_Spl_Allowance as decimal(18,2))+cast(SalDet.Basic_Uniform as decimal(18,2))" +
                    " +cast(SalDet.Basic_Incentives as decimal(18,2))" +
                    ") as Wages," +
                    " SalDet.DayIncentive as Other1,SalDet.ThreesidedAmt as Other2," +
                    " sum(SalDet.allowances1) as Other3," +
                    " SalDet.GrossEarnings as TotGrossPay,SalDet.TotalDeductions as TotDed,SalDet.RoundOffNetPay as NetPay," +
                    " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber," + 
                    " SalDet.OTHoursNew,SalDet.OTHoursAmtNew,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory," + 
                    " cast(SalDet.Basic_SM as decimal(18,2)) as Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                    //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
                    //" SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.StafforLabor='" + str_cate + "' and " +
                   " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                    " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                    " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                    " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

                    //Activate Employee Check
                    //if (Left_Employee == "1") { query = query + " and EmpDet.ActivateMode='N'"; }
                    if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
                    else
                    {
                        if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
                    }

                    // Other State

                    if (Other_state == "Yes")
                    {
                        query = query + " and EmpDet.OtherState='Yes'";
                    }
                    if (Non_Other_state == "No")
                    {
                        query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                        query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
                    }



                    //PF Check
                    if (PFTypeGet.ToString() != "0")
                    {
                        if (PFTypeGet.ToString() == "1")
                        {
                            //PF Employee
                            query = query + " and (EmpDet.Eligible_PF='1')";
                        }
                        else
                        {
                            //Non PF Employee
                            query = query + " and (EmpDet.Eligible_PF='2')";
                        }
                    }

                    //Salary Through Bank Or Cash Check
                    if (Salary_CashOrBank.ToString() != "0")
                    {
                        if (Salary_CashOrBank.ToString() == "1")
                        {
                            //Cash Employee
                            query = query + " and (EmpDet.Salary_Through='1')";
                        }
                        else
                        {
                            //Bank Employee
                            query = query + " and (EmpDet.Salary_Through='2')";
                        }
                    }

                    //Add Division Condition
                    if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
                    {
                        query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
                    }
                    if (ExemptedStaff != "")
                    {
                        query = query + " and EmpDet.ExemptedStaff='1'";
                    }
                    query = query + " group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.FamilyDetails,MstDpt.DeptName," +
                    " EmpDet.DeptName,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " SalDet.DayIncentive,SalDet.ProvidentFund,SalDet.Deduction3,SalDet.Advance,SalDet.ESI," +
                    " SalDet.ThreesidedAmt,SalDet.GrossEarnings,SalDet.TotalDeductions,SalDet.RoundOffNetPay,SalDet.Words," +
                    " SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo," +
                    " SalDet.OTHoursNew,SalDet.OTHoursAmtNew,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,SalDet.Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " Order by EmpDet.ExistingCode Asc";
                }

                //Type 3
                if (PayslipType == "2")
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
                    " EmpDet.DeptName as Department,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " sum(SalDet.DayIncentive + SalDet.ThreesidedAmt + SalDet.allowances1 + SalDet.allowances1) as Prod_Inc," +
                    " sum(SalDet.RoundOffNetPay - SalDet.NetPay) as RoundDiff,SalDet.ProvidentFund as PF,SalDet.Deduction3 as Canteen," +
                    " SalDet.Advance,SalDet.Deduction4 as uniform,'0' as welfare,SalDet.ESI," +
                    " Sum(SalDet.GrossEarnings - (SalDet.DayIncentive + SalDet.ThreesidedAmt + SalDet.allowances1 + SalDet.OTHoursAmtNew)) as TotGrossPay,SalDet.TotalDeductions as TotDed,SalDet.RoundOffNetPay as NetPay," +
                    " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
                    " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                    " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,cast(SalDet.Basic_SM as decimal(18,2)) as Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                   //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
                    " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                    " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                    " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                    " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

                    //Activate Employee Check
                    if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
                    //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
                    else
                    {
                        if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
                    }

                    // Other State

                    if (Other_state == "Yes")
                    {
                        query = query + " and EmpDet.OtherState='Yes'";
                    }
                    if (Non_Other_state == "No")
                    {
                        query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                        query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
                    }


                    //PF Check
                    if (PFTypeGet.ToString() != "0")
                    {
                        if (PFTypeGet.ToString() == "1")
                        {
                            //PF Employee
                            query = query + " and (EmpDet.Eligible_PF='1')";
                        }
                        else
                        {
                            //Non PF Employee
                            query = query + " and (EmpDet.Eligible_PF='2')";
                        }
                    }

                    //Salary Through Bank Or Cash Check
                    if (Salary_CashOrBank.ToString() != "0")
                    {
                        if (Salary_CashOrBank.ToString() == "1")
                        {
                            //Cash Employee
                            query = query + " and (EmpDet.Salary_Through='1')";
                        }
                        else
                        {
                            //Bank Employee
                            query = query + " and (EmpDet.Salary_Through='2')";
                        }
                    }

                    //Add Division Condition
                    if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
                    {
                        query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
                    }
                    if (ExemptedStaff != "")
                    {
                        query = query + " and EmpDet.ExemptedStaff='1'";
                    }
                    query = query + " group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.FamilyDetails,MstDpt.DeptName," +
                    " EmpDet.DeptName,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " SalDet.ProvidentFund,SalDet.Deduction3,SalDet.Advance,SalDet.Deduction4,SalDet.ESI," +
                    " SalDet.GrossEarnings,SalDet.TotalDeductions,SalDet.RoundOffNetPay," +
                    " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
                    " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                    " SalDet.DedOthers1,SalDet.DedOthers2,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,SalDet.Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " Order by EmpDet.ExistingCode Asc";
                }

                //Type 4
                if (PayslipType == "3")
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
                " EmpDet.DeptName as Department,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL," +
                " SalDet.WH_Work_Days,SalDet.Leave_Credit_Days as Leave_Credit,SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew," +
                " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
                " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                " SalDet.Advance,SalDet.ProvidentFund,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
                " SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
                " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2," +
                " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,cast(SalDet.Basic_SM as decimal(18,2)) as Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
                " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

                    //Activate Employee Check
                    if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
                    //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
                    else
                    {
                        if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
                    }

                    //PF Check
                    if (PFTypeGet.ToString() != "0")
                    {
                        if (PFTypeGet.ToString() == "1")
                        {
                            //PF Employee
                            query = query + " and (EmpDet.Eligible_PF='1')";
                        }
                        else
                        {
                            //Non PF Employee
                            query = query + " and (EmpDet.Eligible_PF='2')";
                        }
                    }

                    // Other State

                    if (Other_state == "Yes")
                    {
                        query = query + " and EmpDet.OtherState='Yes'";
                    }
                    if (Non_Other_state == "No")
                    {
                        query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                        query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
                    }


                    //Salary Through Bank Or Cash Check
                    if (Salary_CashOrBank.ToString() != "0")
                    {
                        if (Salary_CashOrBank.ToString() == "1")
                        {
                            //Cash Employee
                            query = query + " and (EmpDet.Salary_Through='1')";
                        }
                        else
                        {
                            //Bank Employee
                            query = query + " and (EmpDet.Salary_Through='2')";
                        }
                    }

                    //Add Division Condition
                    if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
                    {
                        query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
                    }
                    if (ExemptedStaff != "")
                    {
                        query = query + " and EmpDet.ExemptedStaff='1'";
                    }
                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.FamilyDetails,MstDpt.DeptName," +
                    " EmpDet.DeptName,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL," +
                    " SalDet.WH_Work_Days,SalDet.Leave_Credit_Days,SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
                    " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                    " SalDet.Advance,SalDet.ProvidentFund,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
                    " SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
                    " SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,SalDet.Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " Order by EmpDet.ExistingCode Asc";
                }
            }


            if (EmployeeTypeCd == "4") //Hostel
            {
                //Type1
                if (PayslipType == "0")
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
                    " EmpDet.DeptName as Department,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " SalDet.DayIncentive as RegularAttInc,sum(SalDet.RoundOffNetPay - SalDet.NetPay) as RoundDiff," +
                    " SalDet.ProvidentFund as PF,SalDet.Deduction3 as Canteen,SalDet.Advance,SalDet.ESI," +
                    " sum(SalDet.ThreesidedAmt + SalDet.allowances1 + SalDet.allowances2 + SalDet.allowances3 + SalDet.allowances4 + SalDet.allowances5) as Other1," +
                    " sum(SalDet.Deduction1 + SalDet.Deduction2 + SalDet.Deduction5) as Other2," +
                    " Sum(SalDet.GrossEarnings - (SalDet.DayIncentive + SalDet.ThreesidedAmt + SalDet.allowances1)) as TotGrossPay,SalDet.TotalDeductions as TotDed,SalDet.RoundOffNetPay as NetPay,SalDet.DayIncentive," +
                    " SalDet.ThreesidedAmt as OTDaysAmt,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
                    " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                    " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber," +
                    " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,cast(SalDet.Basic_SM as decimal(18,2)) as Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                    //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
                    " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                    " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                    " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                    " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

                    //Activate Employee Check
                    if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
                    //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
                    else
                    {
                        if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
                    }

                    //PF Check
                    if (PFTypeGet.ToString() != "0")
                    {
                        if (PFTypeGet.ToString() == "1")
                        {
                            //PF Employee
                            query = query + " and (EmpDet.Eligible_PF='1')";
                        }
                        else
                        {
                            //Non PF Employee
                            query = query + " and (EmpDet.Eligible_PF='2')";
                        }
                    }

                    // Other State

                    if (Other_state == "Yes")
                    {
                        query = query + " and EmpDet.OtherState='Yes'";
                    }
                    if (Non_Other_state == "No")
                    {
                        query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                        query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
                    }


                    //Salary Through Bank Or Cash Check
                    if (Salary_CashOrBank.ToString() != "0")
                    {
                        if (Salary_CashOrBank.ToString() == "1")
                        {
                            //Cash Employee
                            query = query + " and (EmpDet.Salary_Through='1')";
                        }
                        else
                        {
                            //Bank Employee
                            query = query + " and (EmpDet.Salary_Through='2')";
                        }
                    }

                    //Add Division Condition
                    if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
                    {
                        query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
                    }
                    if (ExemptedStaff != "")
                    {
                        query = query + " and EmpDet.ExemptedStaff='1'";
                    }
                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.FamilyDetails,MstDpt.DeptName," +
                    " EmpDet.DeptName,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " SalDet.ProvidentFund,SalDet.Deduction3,SalDet.Advance,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
                    " SalDet.RoundOffNetPay,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.allowances1,SalDet.allowances2," +
                    " SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3," +
                    " SalDet.Deduction4,SalDet.Deduction5,SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo,SalDet.DedOthers1,SalDet.DedOthers2,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,SalDet.Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " Order by EmpDet.ExistingCode Asc";
                }
                //Type 2
                if (PayslipType == "1")
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
                    " EmpDet.DeptName as Department,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " SalDet.DayIncentive as RegularAttInc,sum(SalDet.RoundOffNetPay - SalDet.NetPay) as RoundDiff," +
                    " SalDet.ProvidentFund as PF,SalDet.Deduction3 as Canteen,SalDet.Advance,SalDet.ESI," +
                    " sum(cast(SalDet.BasicAndDANew as decimal(18,2))+cast(SalDet.BasicHRA as decimal(18,2))+" +
                    " cast(SalDet.ConvAllow as decimal(18,2))+cast(SalDet.EduAllow as decimal(18,2))+cast(SalDet.MediAllow as decimal(18,2))+" +
                    " cast(SalDet.BasicRAI as decimal(18,2))+cast(SalDet.WashingAllow as decimal(18,2))" +
                    " +cast(SalDet.Basic_Spl_Allowance as decimal(18,2))+cast(SalDet.Basic_Uniform as decimal(18,2))" +
                    " +cast(SalDet.Basic_Incentives as decimal(18,2))" +
                    " ) as Wages," +
                    " SalDet.DayIncentive as Other1,SalDet.ThreesidedAmt as Other2," +
                    " sum(SalDet.allowances1) as Other3," +
                    " SalDet.GrossEarnings as TotGrossPay,SalDet.TotalDeductions as TotDed,SalDet.RoundOffNetPay as NetPay," +
                    " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,cast(SalDet.Basic_SM as decimal(18,2)) as Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                    //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
                    " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                    " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                    " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                    " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

                    //Activate Employee Check
                    if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
                    //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
                    else
                    {
                        if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
                    }

                    //PF Check
                    if (PFTypeGet.ToString() != "0")
                    {
                        if (PFTypeGet.ToString() == "1")
                        {
                            //PF Employee
                            query = query + " and (EmpDet.Eligible_PF='1')";
                        }
                        else
                        {
                            //Non PF Employee
                            query = query + " and (EmpDet.Eligible_PF='2')";
                        }
                    }

                    // Other State

                    if (Other_state == "Yes")
                    {
                        query = query + " and EmpDet.OtherState='Yes'";
                    }
                    if (Non_Other_state == "No")
                    {
                        query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                        query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
                    }


                    //Salary Through Bank Or Cash Check
                    if (Salary_CashOrBank.ToString() != "0")
                    {
                        if (Salary_CashOrBank.ToString() == "1")
                        {
                            //Cash Employee
                            query = query + " and (EmpDet.Salary_Through='1')";
                        }
                        else
                        {
                            //Bank Employee
                            query = query + " and (EmpDet.Salary_Through='2')";
                        }
                    }

                    //Add Division Condition
                    if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
                    {
                        query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
                    }
                    if (ExemptedStaff != "")
                    {
                        query = query + " and EmpDet.ExemptedStaff='1'";
                    }
                    query = query + " group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.FamilyDetails,MstDpt.DeptName," +
                    " EmpDet.DeptName,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " SalDet.DayIncentive,SalDet.ProvidentFund,SalDet.Deduction3,SalDet.Advance,SalDet.ESI," +
                    " SalDet.ThreesidedAmt,SalDet.GrossEarnings,SalDet.TotalDeductions,SalDet.RoundOffNetPay,SalDet.Words," +
                    " SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,SalDet.Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " Order by EmpDet.ExistingCode Asc";
                }

                //Type 3
                if (PayslipType == "2")
                {
                 
                    
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
                    " EmpDet.DeptName as Department,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " sum(SalDet.DayIncentive + SalDet.ThreesidedAmt + SalDet.allowances1) as Prod_Inc," +
                    " sum(SalDet.RoundOffNetPay - SalDet.NetPay) as RoundDiff,SalDet.ProvidentFund as PF,SalDet.Deduction1 as Canteen,SalDet.Deduction2," +
                    " SalDet.Deduction3 as uniform,SalDet.Advance,SalDet.Deduction4,'0' as welfare,SalDet.ESI," +
                    " Sum(SalDet.GrossEarnings - (SalDet.DayIncentive + SalDet.ThreesidedAmt + SalDet.allowances1)) as TotGrossPay,SalDet.TotalDeductions as TotDed,SalDet.RoundOffNetPay as NetPay," +
                    " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber," +
                    " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                    " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,cast(SalDet.Basic_SM as decimal(18,2)) as Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                    //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
                    " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                    " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                    " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                    " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

                    //Activate Employee Check
                    if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
                    //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
                    else
                    {
                        if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
                    }

                    //PF Check
                    if (PFTypeGet.ToString() != "0")
                    {
                        if (PFTypeGet.ToString() == "1")
                        {
                            //PF Employee
                            query = query + " and (EmpDet.Eligible_PF='1')";
                        }
                        else
                        {
                            //Non PF Employee
                            query = query + " and (EmpDet.Eligible_PF='2')";
                        }
                    }

                    // Other State

                    if (Other_state == "Yes")
                    {
                        query = query + " and EmpDet.OtherState='Yes'";
                    }
                    if (Non_Other_state == "No")
                    {
                        query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                        query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
                    }


                    //Salary Through Bank Or Cash Check
                    if (Salary_CashOrBank.ToString() != "0")
                    {
                        if (Salary_CashOrBank.ToString() == "1")
                        {
                            //Cash Employee
                            query = query + " and (EmpDet.Salary_Through='1')";
                        }
                        else
                        {
                            //Bank Employee
                            query = query + " and (EmpDet.Salary_Through='2')";
                        }
                    }

                    //Add Division Condition
                    if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
                    {
                        query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
                    }
                    if (ExemptedStaff != "")
                    {
                        query = query + " and EmpDet.ExemptedStaff='1'";
                    }
                    query = query + " group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.FamilyDetails,MstDpt.DeptName," +
                    " EmpDet.DeptName,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " SalDet.ProvidentFund,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Advance,SalDet.Deduction4,SalDet.ESI," +
                    " SalDet.GrossEarnings,SalDet.TotalDeductions,SalDet.RoundOffNetPay," +
                    " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo," +
                    " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,SalDet.Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " Order by EmpDet.ExistingCode Asc";
                }
                //Type 4
                if (PayslipType == "3")
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
                " EmpDet.DeptName as Department,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL," +
                " SalDet.WH_Work_Days,SalDet.Leave_Credit_Days as Leave_Credit,SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew," +
                " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
                " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                " SalDet.Advance,SalDet.ProvidentFund,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
                " SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
                " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2," +
                " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,cast(SalDet.Basic_SM as decimal(18,2)) as Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
                " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

                    //Activate Employee Check
                    if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
                    //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
                    else
                    {
                        if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
                    }

                    //PF Check
                    if (PFTypeGet.ToString() != "0")
                    {
                        if (PFTypeGet.ToString() == "1")
                        {
                            //PF Employee
                            query = query + " and (EmpDet.Eligible_PF='1')";
                        }
                        else
                        {
                            //Non PF Employee
                            query = query + " and (EmpDet.Eligible_PF='2')";
                        }
                    }

                    //Salary Through Bank Or Cash Check
                    if (Salary_CashOrBank.ToString() != "0")
                    {
                        if (Salary_CashOrBank.ToString() == "1")
                        {
                            //Cash Employee
                            query = query + " and (EmpDet.Salary_Through='1')";
                        }
                        else
                        {
                            //Bank Employee
                            query = query + " and (EmpDet.Salary_Through='2')";
                        }
                    }

                    // Other State

                    if (Other_state == "Yes")
                    {
                        query = query + " and EmpDet.OtherState='Yes'";
                    }
                    if (Non_Other_state == "No")
                    {
                        query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                        query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
                    }


                    //Add Division Condition
                    if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
                    {
                        query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
                    }
                    if (ExemptedStaff != "")
                    {
                        query = query + " and EmpDet.ExemptedStaff='1'";
                    }
                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.FamilyDetails,MstDpt.DeptName," +
                    " EmpDet.DeptName,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL," +
                    " SalDet.WH_Work_Days,SalDet.Leave_Credit_Days,SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew," +
                    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                    " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
                    " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                    " SalDet.Advance,SalDet.ProvidentFund,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
                    " SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
                    " SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,SalDet.Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " Order by EmpDet.ExistingCode Asc";
                }
            }

            if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6" || EmployeeTypeCd == "7") //Staff Payslip
            {
                if (PayslipType == "0" || PayslipType == "2") // Type1
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
                    " EmpDet.DeptName as Department,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL," +
                    " SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI," +
                    " SalDet.WashingAllow,SalDet.DayIncentive as RegularAttInc,sum(SalDet.RoundOffNetPay - SalDet.NetPay) as RoundDiff," +
                    " SalDet.ProvidentFund as PF,SalDet.Deduction3 as Canteen,SalDet.Advance,SalDet.ESI," +
                    " sum(SalDet.Deduction1 + SalDet.Deduction2 + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.Deduction5) as OtherDed," +
                    " SalDet.GrossEarnings as TotGrossPay,SalDet.TotalDeductions as TotDed,SalDet.RoundOffNetPay as NetPay," +
                    " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
                    " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                    " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber," +
                    " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,cast(SalDet.Basic_SM as decimal(18,2)) as Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                    //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
                    " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                    " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                    " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                    " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

                    //Activate Employee Check
                    if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
                    //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
                    else
                    {
                        if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
                    }

                    //PF Check
                    if (PFTypeGet.ToString() != "0")
                    {
                        if (PFTypeGet.ToString() == "1")
                        {
                            //PF Employee
                            query = query + " and (EmpDet.Eligible_PF='1')";
                        }
                        else
                        {
                            //Non PF Employee
                            query = query + " and (EmpDet.Eligible_PF='2')";
                        }
                    }

                    //Salary Through Bank Or Cash Check
                    if (Salary_CashOrBank.ToString() != "0")
                    {
                        if (Salary_CashOrBank.ToString() == "1")
                        {
                            //Cash Employee
                            query = query + " and (EmpDet.Salary_Through='1')";
                        }
                        else
                        {
                            //Bank Employee
                            query = query + " and (EmpDet.Salary_Through='2')";
                        }
                    }

                    // Other State

                    if (Other_state == "Yes")
                    {
                        query = query + " and EmpDet.OtherState='Yes'";
                    }
                    if (Non_Other_state == "No")
                    {
                        query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                        query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
                    }


                    //Add Division Condition
                    if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
                    {
                        query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
                    }
                    if (ExemptedStaff != "")
                    {
                        query = query + " and EmpDet.ExemptedStaff='1'";
                    }
                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.FamilyDetails,MstDpt.DeptName," +
                    " EmpDet.DeptName,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL," +
                    " SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI," +
                    " SalDet.WashingAllow,SalDet.DayIncentive,SalDet.ProvidentFund,SalDet.Deduction3,SalDet.Advance,SalDet.ESI," +
                    " SalDet.GrossEarnings,SalDet.TotalDeductions,SalDet.RoundOffNetPay," +
                    " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
                    " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                    " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo,SalDet.DedOthers1,SalDet.DedOthers2,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,SalDet.Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " Order by EmpDet.ExistingCode Asc";
                }

                if (PayslipType == "1") // Type2
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
                    " EmpDet.DeptName as Department,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL," +
                    " SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI," +
                    " SalDet.WashingAllow,SalDet.DayIncentive as RegularAttInc,sum(SalDet.RoundOffNetPay - SalDet.NetPay) as RoundDiff," +
                    " SalDet.ProvidentFund as PF,SalDet.Deduction3 as Canteen,SalDet.Advance,SalDet.ESI,SalDet.Deduction4 as TDS," +
                    " SalDet.Deduction5 as PTAX,sum(cast(SalDet.DedOthers1 as decimal(18,2)) + cast(SalDet.DedOthers2 as decimal(18,2))) as Others,'0' as Welfare," +
                    " SalDet.GrossEarnings as TotGrossPay,SalDet.TotalDeductions as TotDed,SalDet.RoundOffNetPay as NetPay," +
                    " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
                    " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                    " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber," +
                    " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,cast(SalDet.Basic_SM as decimal(18,2)) as Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                    //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
                    " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                    " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                    " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                    " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

                    //Activate Employee Check
                    if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
                    //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
                    else
                    {
                        if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
                    }

                    //PF Check
                    if (PFTypeGet.ToString() != "0")
                    {
                        if (PFTypeGet.ToString() == "1")
                        {
                            //PF Employee
                            query = query + " and (EmpDet.Eligible_PF='1')";
                        }
                        else
                        {
                            //Non PF Employee
                            query = query + " and (EmpDet.Eligible_PF='2')";
                        }
                    }

                    // Other State

                    if (Other_state == "Yes")
                    {
                        query = query + " and EmpDet.OtherState='Yes'";
                    }
                    if (Non_Other_state == "No")
                    {
                        query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                        query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
                    }



                    //Salary Through Bank Or Cash Check
                    if (Salary_CashOrBank.ToString() != "0")
                    {
                        if (Salary_CashOrBank.ToString() == "1")
                        {
                            //Cash Employee
                            query = query + " and (EmpDet.Salary_Through='1')";
                        }
                        else
                        {
                            //Bank Employee
                            query = query + " and (EmpDet.Salary_Through='2')";
                        }
                    }

                    //Add Division Condition
                    if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
                    {
                        query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
                    }
                    if (ExemptedStaff != "")
                    {
                        query = query + " and EmpDet.ExemptedStaff='1'";
                    }
                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.FamilyDetails,MstDpt.DeptName," +
                    " EmpDet.DeptName,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL," +
                    " SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI," +
                    " SalDet.WashingAllow,SalDet.DayIncentive,SalDet.ProvidentFund,SalDet.Deduction3,SalDet.Advance,SalDet.ESI," +
                    " SalDet.Deduction4,SalDet.Deduction5,SalDet.GrossEarnings,SalDet.TotalDeductions,SalDet.RoundOffNetPay," +
                    " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
                    " SalDet.allowances5,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                    " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                    " SalDet.SalaryDate,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo,SalDet.DedOthers1,SalDet.DedOthers2,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,SalDet.Basic_SM,AttnDet.AEH,AttnDet.LBH," +
                    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                    " Order by EmpDet.ExistingCode Asc";
                }
            }

            //Civil Payslip
            if (EmployeeTypeCd == "5")
            {
                if (PayslipType == "4")
                {
                    query = "Select SalDet.EmpNo,MstDpt.DeptName as DepartmentNm,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,SalDet.WorkedDays,EmpDet.Wages as EmployeeType,EmpDet.Designation, " +
                        " SalDet.NFh,cast(SalDet.OTHoursNew as decimal(18,2)) as OTHoursNew,cast(SalDet.Basic_SM as decimal(18,2)) as Basic_SM," +
                        " SalDet.BasicandDA as BasicAndDANew,cast(SalDet.OTHoursAmtNew as decimal(18,2)) as OTHoursAmtNew, " +
                        " SalDet.DayIncentive,SalDet.Words,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
                        " SalDet.allowances5,SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,SalDet.Advance,SalDet.Deduction1," +
                        " SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5, " +
                        " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2," +
                        " SalDet.TotalDeductions,SalDet.RoundOffNetPay as NetPay,SalDet.FullNightAmt," +
                        " SalDet.Leave_Credit_Days as LeaveCredit,AttnDet.AEH,AttnDet.LBH from Employee_Mst EmpDet " +
                        " inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                        " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode " +
                        //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                        " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo And SalDet.FromDate=AttnDet.FromDate And SalDet.ToDate=AttnDet.ToDate" +
                        //" inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
                        " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                        " SalDet.FromDate = convert(datetime,'" + fromdate + "', 105) and SalDet.ToDate = Convert(Datetime,'" + ToDate + "', 105) and " +
                        " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                        " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                        " AttnDet.FromDate = convert(datetime,'" + fromdate + "', 105) and AttnDet.ToDate = Convert(Datetime,'" + ToDate + "', 105) and " +
                        " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

                    //Activate Employee Check
                    if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
                    //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
                    else
                    {
                        if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
                    }

                    //Add Division Condition
                    if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
                    {
                        query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
                    }
                    if (ExemptedStaff != "")
                    {
                        query = query + " and EmpDet.ExemptedStaff='1'";
                    }
                    // Other State

                    if (Other_state == "Yes")
                    {
                        query = query + " and EmpDet.OtherState='Yes'";
                    }
                    if (Non_Other_state == "No")
                    {
                        query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                        query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
                    }



                    query = query + " group by SalDet.EmpNo,MstDpt.DeptName,EmpDet.ExistingCode,EmpDet.FirstName,SalDet.WorkedDays,EmpDet.Wages,EmpDet.Designation, " +
                            " SalDet.NFh,SalDet.OTHoursNew,SalDet.Basic_SM,SalDet.BasicandDA,SalDet.OTHoursAmtNew, " +
                            " SalDet.DayIncentive,SalDet.Words,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
                            " SalDet.allowances5,SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,SalDet.Advance,SalDet.Deduction1," +
                            " SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5, " +
                            " SalDet.DedOthers1,SalDet.DedOthers2,SalDet.TotalDeductions,SalDet.RoundOffNetPay,SalDet.FullNightAmt,SalDet.Leave_Credit_Days,AttnDet.AEH,AttnDet.LBH" +
                            " Order by EmpDet.ExistingCode,MstDpt.DeptName Asc";
                }
                else
                {
                    query = "Select SalDet.EmpNo,MstDpt.DeptName as DepartmentNm,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,SalDet.WorkedDays,EmpDet.Wages as EmployeeType, " +
                        " SalDet.NFh,SalDet.CL,SalDet.Weekoff,cast(SalDet.Basic_SM as decimal(18,2)) as Base,SalDet.BasicandDA,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
                        " SalDet.Words,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.GrossEarnings, " +
                        " SalDet.ProvidentFund,SalDet.ESI,SalDet.Advance,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5, " +
                        " SalDet.TotalDeductions,SalDet.RoundOffNetPay as NetPay,sum(SalDet.RoundOffNetPay - SalDet.NetPay) as Roundoff,SalDet.FullNightAmt," +
                        " SalDet.DayIncentive,SalDet.Leave_Credit_Days as LeaveCredit,cast(SalDet.OTHoursNew as decimal(18,2)) as OTHoursNew,cast(SalDet.OTHoursAmtNew as decimal(18,2)) as OTHoursAmtNew," +
                        " cast((cast(SalDet.Basic_SM as decimal(18,2)) / 8) as decimal(18,2)) as OTFixed,AttnDet.AEH,AttnDet.LBH from Employee_Mst EmpDet " +
                        " inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                        " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode " +
                        //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                        " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo And SalDet.FromDate=AttnDet.FromDate And SalDet.ToDate=AttnDet.ToDate" +
                        //" inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
                        " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                        " SalDet.FromDate = convert(datetime,'" + fromdate + "', 105) and SalDet.ToDate = Convert(Datetime,'" + ToDate + "', 105) and " +

                        " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                        " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                        " AttnDet.FromDate = convert(datetime,'" + fromdate + "', 105) and AttnDet.ToDate = Convert(Datetime,'" + ToDate + "', 105) and " +

                        " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

                    //Activate Employee Check
                    if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
                    //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
                    else
                    {
                        if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
                    }


                    // Other State

                    if (Other_state == "Yes")
                    {
                        query = query + " and EmpDet.OtherState='Yes'";
                    }
                    if (Non_Other_state == "No")
                    {
                        query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                        query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
                    }


                    //Add Division Condition
                    if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
                    {
                        query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
                    }
                    if (ExemptedStaff != "")
                    {
                        query = query + " and EmpDet.ExemptedStaff='1'";
                    }
                    query = query + " group by SalDet.EmpNo,MstDpt.DeptName,EmpDet.ExistingCode,EmpDet.FirstName,SalDet.WorkedDays,EmpDet.Wages, " +
                            " SalDet.NFh,SalDet.CL,SalDet.Weekoff,SalDet.Basic_SM,SalDet.BasicandDA, " +
                            " SalDet.Words,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.GrossEarnings, " +
                            " SalDet.ProvidentFund,SalDet.ESI,SalDet.Advance,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5, " +
                            " SalDet.TotalDeductions,SalDet.RoundOffNetPay,SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.Leave_Credit_Days,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,AttnDet.AEH,AttnDet.LBH Order by EmpDet.ExistingCode,MstDpt.DeptName Asc";
                }
            }
            //Check List Monthly Wages Query Start
            if (EmployeeTypeCd != "5" && PayslipType == "4") //Check List Monthly Wages Query 
            {
                query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
                " EmpDet.DeptName as Department,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL," +
                " SalDet.WH_Work_Days,SalDet.Leave_Credit_Days as Leave_Credit,SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew," +
                " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
                " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                " SalDet.Advance,SalDet.ProvidentFund,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
                " SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
                " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2," +
                " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,AttnDet.AEH,AttnDet.LBH," +
                " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
                " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'";

                //Activate Employee Check
                if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,convert(datetime,EmpDet.DOR,105),105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
                //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
                else
                {
                    if (Left_Date != "") { query = query + " and (convert(datetime,convert(datetime,EmpDet.DOR,105),105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
                }

                //PF Check
                if (PFTypeGet.ToString() != "0")
                {
                    if (PFTypeGet.ToString() == "1")
                    {
                        //PF Employee
                        query = query + " and (EmpDet.Eligible_PF='1')";
                    }
                    else
                    {
                        //Non PF Employee
                        query = query + " and (EmpDet.Eligible_PF='2')";
                    }
                }

                // Other State

                if (Other_state == "Yes")
                {
                    query = query + " and EmpDet.OtherState='Yes'";
                }
                if (Non_Other_state == "No")
                {
                    query = query + " and (EmpDet.OtherState='No' or EmpDet.OtherState=''";
                    query = query + " or EmpDet.OtherState='null' or EmpDet.OtherState is null)";
                }

                //Salary Through Bank Or Cash Check
                if (Salary_CashOrBank.ToString() != "0")
                {
                    if (Salary_CashOrBank.ToString() == "1")
                    {
                        //Cash Employee
                        query = query + " and (EmpDet.Salary_Through='1')";
                    }
                    else
                    {
                        //Bank Employee
                        query = query + " and (EmpDet.Salary_Through='2')";
                    }
                }

                //Add Division Condition
                if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
                {
                    query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
                }
                if (ExemptedStaff != "")
                {
                    query = query + " and EmpDet.ExemptedStaff='1'";
                }
                query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.FamilyDetails,MstDpt.DeptName," +
                " EmpDet.DeptName,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL," +
                " SalDet.WH_Work_Days,SalDet.Leave_Credit_Days,SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew," +
                " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
                " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
                " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
                " SalDet.Advance,SalDet.ProvidentFund,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
                " SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
                " SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
                " SalDet.SalaryDate,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,AttnDet.AEH,AttnDet.LBH," +
                " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
                " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
                " Order by EmpDet.ExistingCode Asc";
            }
            //Check List Monthly Wages Query End


            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandTimeout = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SqlCommandTimeOut"]);

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            //Check List Leave Creadit Update
            if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6")
            {
                if (PayslipType == "4")
                {
                    //for (int k = 0; k < ds1.Tables[0].Rows.Count; k++)
                    //{
                    //    string EmpNo_LC = "";
                    //    DataTable LC_DT = new DataTable();
                    //    EmpNo_LC = ds1.Tables[0].Rows[k]["EmpNo"].ToString();
                    //    query = "Select * from MstLeaveCredit where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + EmpNo_LC + "' And AttnFinYear='" + str_yr + "'";
                    //    LC_DT = objdata.RptEmployeeMultipleDetails(query);
                    //    if (LC_DT.Rows.Count != 0)
                    //    {
                    //        ds1.Tables[0].Rows[k]["Leave_Credit"] = LC_DT.Rows[0]["LeaveCredit"].ToString();
                    //    }
                    //    else
                    //    {
                    //        ds1.Tables[0].Rows[k]["Leave_Credit"] = "0";
                    //    }
                    //}
                }
            }

            if (ds1.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dt_1.Rows.Count; i++)
                {
                    //Label lblEmpNo = (Label)gvsal.FindControl("lblExist");
                    string qry = "Update [" + SessionPayroll + "]..SalaryDetails set Process_Mode='" + 1 + "' where Month='" + str_month + "' and Year='" + str_yr + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ExisistingCode='" + dt_1.Rows[i]["ExisistingCode"].ToString() + "'";
                    SqlCommand cmd1 = new SqlCommand(qry, con);
                    con.Open();
                    cmd1.ExecuteNonQuery();
                    con.Close();
                }

                //rd.Load(Server.MapPath("PaySlip1.rpt"));
                if (EmployeeTypeCd == "3")
                {
                    if (Basic_Report_Type == "NEW")
                    {
                        //New Payslip Start
                        if (PayslipType == "0")
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Regular_Type1_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Regular_Type1.rpt"));
                            }
                        }
                        if (PayslipType == "1")
                        {
                            if (Salary_CashOrBank.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Regular_Type2_Bank.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Regular_Type2.rpt"));
                            }
                        }
                        if (PayslipType == "2")
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Regular_Type3_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Regular_Type3.rpt"));
                            }
                        }
                        if (PayslipType == "3")
                        {
                            if (Salary_CashOrBank.ToString() == "2")
                            {
                                if (PFTypeGet.ToString() == "2")
                                {
                                    rd.Load(Server.MapPath("Payslip_New_Component/Exgratia_Checklist_Bank_Non_PF.rpt"));
                                }
                                else
                                {
                                    rd.Load(Server.MapPath("Payslip_New_Component/Exgratia_Checklist_Bank.rpt"));
                                }
                            }
                            else
                            {
                                if (PFTypeGet.ToString() == "2")
                                {
                                    rd.Load(Server.MapPath("Payslip_New_Component/Exgratia_Checklist_Non_PF.rpt"));
                                }
                                else
                                {
                                    rd.Load(Server.MapPath("Payslip_New_Component/Exgratia_Checklist.rpt"));
                                }
                            }
                        }
                        if (PayslipType == "4") 
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Checklist_Regular_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Checklist_Regular.rpt"));
                            }
                        }
                        //New Payslip End
                    }
                    else
                    {
                        //OLD Payslip Start
                        if (PayslipType == "0")
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip/Regular_Type1_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip/Regular_Type1.rpt"));
                            }
                        }
                        if (PayslipType == "1")
                        {
                            if (Salary_CashOrBank.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip/Regular_Type2_Bank.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip/Regular_Type2.rpt"));
                            }
                        }
                        if (PayslipType == "2")
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip/Regular_Type3_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip/Regular_Type3.rpt"));
                            }
                        }
                        if (PayslipType == "3")
                        {
                            if (Salary_CashOrBank.ToString() == "2")
                            {
                                if (PFTypeGet.ToString() == "2")
                                {
                                    rd.Load(Server.MapPath("Payslip/Exgratia_Checklist_Bank_Non_PF.rpt"));
                                }
                                else
                                {
                                    rd.Load(Server.MapPath("Payslip/Exgratia_Checklist_Bank.rpt"));
                                }
                            }
                            else
                            {
                                if (PFTypeGet.ToString() == "2")
                                {
                                    rd.Load(Server.MapPath("Payslip/Exgratia_Checklist_Non_PF.rpt"));
                                }
                                else
                                {
                                    rd.Load(Server.MapPath("Payslip/Exgratia_Checklist.rpt"));
                                }
                            }
                        }
                        if (PayslipType == "4") 
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip/Checklist_Regular_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip/Checklist_Regular.rpt"));
                            }
                        }
                        //OLD Payslip End
                    }
                }
                else if (EmployeeTypeCd == "2")
                {
                    if (PayslipType == "0") 
                    {
                        if (PFTypeGet.ToString() == "2")
                        {
                            rd.Load(Server.MapPath("Payslip/SubStaff_Type1_Non_PF.rpt"));
                        }
                        else
                        {
                            rd.Load(Server.MapPath("Payslip/SubStaff_Type1.rpt"));
                        }
                    }
                    if (PayslipType == "1") 
                    {
                        if (Salary_CashOrBank.ToString() == "2")
                        {
                            rd.Load(Server.MapPath("Payslip/SubStaff_Type2_Bank.rpt"));
                        }
                        else
                        {
                            rd.Load(Server.MapPath("Payslip/SubStaff_Type2.rpt"));
                        }
                    }
                    if (PayslipType == "2")
                    {
                        if (PFTypeGet.ToString() == "2")
                        {
                            rd.Load(Server.MapPath("Payslip/SubStaff_Type3_Non_PF.rpt"));
                        }
                        else
                        {
                            rd.Load(Server.MapPath("Payslip/SubStaff_Type3.rpt"));
                        }                        
                    }
                    
                    if (PayslipType == "3") 
                    {
                        if (Salary_CashOrBank.ToString() == "2")
                        {
                            rd.Load(Server.MapPath("Payslip/SubStaff_Exgratia_Checklist_Bank.rpt"));
                        }
                        else
                        {
                            rd.Load(Server.MapPath("Payslip/SubStaff_Exgratia_Checklist.rpt")); 
                        }
                        
                    }
                    if (PayslipType == "4") 
                    {
                        if (PFTypeGet.ToString() == "2")
                        {
                            rd.Load(Server.MapPath("Payslip/Checklist_SubStaff_Non_PF.rpt"));
                        }
                        else
                        {
                            rd.Load(Server.MapPath("Payslip/Checklist_SubStaff.rpt"));
                        }
                    }
                }

                else if (EmployeeTypeCd == "4")
                {
                    if (Basic_Report_Type == "NEW")
                    {
                        //New Payslip Start
                        if (PayslipType == "0")
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Hostel_Type1_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Hostel_Type1.rpt"));
                            }
                        }
                        if (PayslipType == "1")
                        {
                            if (Salary_CashOrBank.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Hostel_Type2_Bank.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Hostel_Type2.rpt"));
                            }
                        }
                        if (PayslipType == "2")
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Hostel_Type3_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Hostel_Type3.rpt"));
                            }
                        }
                        if (PayslipType == "3")
                        {
                            if (Salary_CashOrBank.ToString() == "2")
                            {
                                if (PFTypeGet.ToString() == "2")
                                {
                                    rd.Load(Server.MapPath("Payslip_New_Component/Exgratia_Checklist_Bank_Non_PF.rpt"));
                                }
                                else
                                {
                                    rd.Load(Server.MapPath("Payslip_New_Component/Exgratia_Checklist_Bank.rpt"));
                                }
                            }
                            else
                            {
                                if (PFTypeGet.ToString() == "2")
                                {
                                    rd.Load(Server.MapPath("Payslip_New_Component/Exgratia_Checklist_Non_PF.rpt"));
                                }
                                else
                                {
                                    rd.Load(Server.MapPath("Payslip_New_Component/Exgratia_Checklist.rpt"));
                                }
                            }

                        }
                        if (PayslipType == "4") 
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Checklist_Hostel_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Checklist_Hostel.rpt"));
                            }
                        }

                        //New Payslip END
                    }
                    else
                    {
                        //OLD Payslip Start
                        if (PayslipType == "0")
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip/Hostel_Type1_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip/Hostel_Type1.rpt"));
                            }
                        }
                        if (PayslipType == "1")
                        {
                            if (Salary_CashOrBank.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip/Hostel_Type2_Bank.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip/Hostel_Type2.rpt"));
                            }
                        }
                        if (PayslipType == "2")
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip/Hostel_Type3_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip/Hostel_Type3.rpt"));
                            }
                        }
                        if (PayslipType == "3")
                        {
                            if (Salary_CashOrBank.ToString() == "2")
                            {
                                if (PFTypeGet.ToString() == "2")
                                {
                                    rd.Load(Server.MapPath("Payslip/Exgratia_Checklist_Bank_Non_PF.rpt"));
                                }
                                else
                                {
                                    rd.Load(Server.MapPath("Payslip/Exgratia_Checklist_Bank.rpt"));
                                }
                            }
                            else
                            {
                                if (PFTypeGet.ToString() == "2")
                                {
                                    rd.Load(Server.MapPath("Payslip/Exgratia_Checklist_Non_PF.rpt"));
                                }
                                else
                                {
                                    rd.Load(Server.MapPath("Payslip/Exgratia_Checklist.rpt"));
                                }
                            }

                        }
                        if (PayslipType == "4") 
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip/Checklist_Hostel_Non_PF.rpt")); 
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip/Checklist_Hostel.rpt")); 
                            }
                            
                        }

                        //OLD Payslip END
                    }

                }

                else if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6")
                {
                    if (Basic_Report_Type == "NEW")
                    {
                        if (PFTypeGet.ToString() == "2")
                        {
                            if (PayslipType == "0") { rd.Load(Server.MapPath("Payslip_New_Component/Staff_Type1_Non_PF.rpt")); }
                            if (PayslipType == "1") { rd.Load(Server.MapPath("Payslip_New_Component/Staff_Type2_Non_PF.rpt")); }
                        }
                        else
                        {
                            if (PayslipType == "0") { rd.Load(Server.MapPath("Payslip_New_Component/Staff_Type1.rpt")); }
                            if (PayslipType == "1") { rd.Load(Server.MapPath("Payslip_New_Component/Staff_Type2.rpt")); }
                        }
                        if (PayslipType == "2") { rd.Load(Server.MapPath("Payslip_New_Component/Staff_Type1.rpt")); }
                        if (PayslipType == "3") { rd.Load(Server.MapPath("Payslip_New_Component/Staff_Type1.rpt")); }
                        if (PayslipType == "4") 
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Checklist_Staff_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Checklist_Staff.rpt"));
                            }
                        }
                    }
                    else
                    {
                        if (PFTypeGet.ToString() == "2")
                        {
                            if (PayslipType == "0") { rd.Load(Server.MapPath("Payslip/Staff_Type1_Non_PF.rpt")); }
                            if (PayslipType == "1") { rd.Load(Server.MapPath("Payslip/Staff_Type2_Non_PF.rpt")); }
                        }
                        else
                        {
                            if (PayslipType == "0") { rd.Load(Server.MapPath("Payslip/Staff_Type1.rpt")); }
                            if (PayslipType == "1") { rd.Load(Server.MapPath("Payslip/Staff_Type2.rpt")); }
                        }
                        if (PayslipType == "2") { rd.Load(Server.MapPath("Payslip/Staff_Type1.rpt")); }
                        if (PayslipType == "3") { rd.Load(Server.MapPath("Payslip/Staff_Type1.rpt")); }
                        if (PayslipType == "4") 
                        {
                            if (PFTypeGet.ToString() == "2") 
                            {
                                rd.Load(Server.MapPath("Payslip/Checklist_Staff_Non_PF.rpt"));
                            } 
                            else 
                            { 
                                rd.Load(Server.MapPath("Payslip/Checklist_Staff.rpt"));
                            }
                        }
                    }
                }
                else if (EmployeeTypeCd == "7")
                {
                    if (Basic_Report_Type == "NEW")
                    {
                        if (PFTypeGet.ToString() == "2")
                        {
                            if (PayslipType == "0") { rd.Load(Server.MapPath("Payslip_New_Component/Staff_Manager_Type1_Non_PF.rpt")); }
                            if (PayslipType == "1") { rd.Load(Server.MapPath("Payslip_New_Component/Staff_Manager_Type2_Non_PF.rpt")); }
                        }
                        else
                        {
                            if (PayslipType == "0") { rd.Load(Server.MapPath("Payslip_New_Component/Staff_Manager_Type1.rpt")); }
                            if (PayslipType == "1") { rd.Load(Server.MapPath("Payslip_New_Component/Staff_Manager_Type2.rpt")); }
                        }
                        if (PayslipType == "2") { rd.Load(Server.MapPath("Payslip_New_Component/Staff_Manager_Type1.rpt")); }
                        if (PayslipType == "3") { rd.Load(Server.MapPath("Payslip_New_Component/Staff_Manager_Type1.rpt")); }
                        if (PayslipType == "4") 
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Checklist_Manager_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip_New_Component/Checklist_Manager.rpt"));
                            }
                        }
                    }
                    else
                    {
                        if (PFTypeGet.ToString() == "2")
                        {
                            if (PayslipType == "0") { rd.Load(Server.MapPath("Payslip/Staff_Type1_Non_PF.rpt")); }
                            if (PayslipType == "1") { rd.Load(Server.MapPath("Payslip/Staff_Type2_Non_PF.rpt")); }
                        }
                        else
                        {
                            if (PayslipType == "0") { rd.Load(Server.MapPath("Payslip/Staff_Type1.rpt")); }
                            if (PayslipType == "1") { rd.Load(Server.MapPath("Payslip/Staff_Type2.rpt")); }
                        }
                        if (PayslipType == "2") { rd.Load(Server.MapPath("Payslip/Staff_Type1.rpt")); }
                        if (PayslipType == "3") { rd.Load(Server.MapPath("Payslip/Staff_Type1.rpt")); }
                        if (PayslipType == "4") 
                        {
                            if (PFTypeGet.ToString() == "2")
                            {
                                rd.Load(Server.MapPath("Payslip/Checklist_Staff_Non_PF.rpt"));
                            }
                            else
                            {
                                rd.Load(Server.MapPath("Payslip/Checklist_Staff.rpt"));
                            }
                        }
                    }
                }
                else if (EmployeeTypeCd == "5")
                {
                    if (PayslipType == "0") { rd.Load(Server.MapPath("Payslip/Civil_Type3.rpt")); }
                    if (PayslipType == "1") { rd.Load(Server.MapPath("Payslip/Civil_Type3.rpt")); }
                    if (PayslipType == "2") { rd.Load(Server.MapPath("Payslip/Civil_Type3.rpt")); }
                    if (PayslipType == "3") { rd.Load(Server.MapPath("Payslip/Civil_Type3.rpt")); }
                    if (PayslipType == "4") 
                    {
                        if (PFTypeGet.ToString() == "2")
                        {
                            rd.Load(Server.MapPath("Payslip/Checklist_Civil_Non_PF.rpt"));
                        }
                        else
                        {
                            rd.Load(Server.MapPath("Payslip/Checklist_Civil.rpt"));
                        }
                    }
                }
                else
                {
                    //rd.Load(Server.MapPath("PaySlip1.rpt"));
                }
                //rd.Load(Server.MapPath("Final_Payslip.rpt"));

                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "SALARY FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";

                if (EmployeeTypeCd != "5")
                {
                    if (PayslipType == "0" || PayslipType == "2")
                    {
                        rd.DataDefinition.FormulaFields["PayMonth"].Text = "'" + str_month.ToUpper() + " - " + YR.ToString() + "'";
                    }
                }

                if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6" || EmployeeTypeCd == "7")
                {
                    if (PayslipType == "0" || PayslipType == "2")
                    {
                        rd.DataDefinition.FormulaFields["PayMonth"].Text = "'" + str_month.ToUpper() + " - " + YR.ToString() + "'";

                    }
                }
                else if (EmployeeTypeCd == "5")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WEEKLY CIVIL WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + " From : " + fromdate + " To : " + ToDate + "'";
                }
                else if (EmployeeTypeCd == "4")
                {
                    if (PayslipType == "4" || PayslipType == "2" || PayslipType == "3")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "HOSTEL WORKER WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                    }
                    if (PayslipType == "0")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "HOSTEL WORKER WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                    }
                }
                else if (EmployeeTypeCd == "3")
                {
                    if (PayslipType == "4" || PayslipType == "2" || PayslipType == "3")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "REGULAR WORKER WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                    }
                    if (PayslipType == "0")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "REGULAR WORKER WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                    }
                }
                else if (EmployeeTypeCd == "2")
                {
                    if (PayslipType == "4" || PayslipType == "2" || PayslipType == "3")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "SUB-STAFF WORKER WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                    }
                    if (PayslipType == "0")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "SUB-STAFF WORKER WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                    }
                }
                else if (EmployeeTypeCd == "1")
                {
                    if (PayslipType == "4" || PayslipType == "2")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF SALARY CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                    }
                }

                else if (EmployeeTypeCd == "7")
                {
                    if (PayslipType == "4" || PayslipType == "2")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "MANAGER SALARY CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                    }
                }
                else if (EmployeeTypeCd == "6")
                {
                    if (PayslipType == "4" || PayslipType == "2")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WATCH AND WARD SALARY CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                    }
                }
                else
                {
                    if (PayslipType == "3")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "EXGRATIA CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                    }
                }

                //Check List Heading Update
                if (EmployeeTypeCd == "7") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "MANAGER SALARY CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'"; } }
                if (EmployeeTypeCd == "1") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF SALARY CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'"; } }
                if (EmployeeTypeCd == "2") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "SUB-STAFF WORKER WAGES CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'"; } }
                if (EmployeeTypeCd == "3") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "REGULAR WORKER WAGES CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'"; } }
                if (EmployeeTypeCd == "4") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "HOSTEL WORKER WAGES CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'"; } }
                if (EmployeeTypeCd == "5") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WEEKLY CIVIL WORKERS WAGES CHECK REPORT FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "   From : " + fromdate + " To : " + ToDate + "'"; } }
                if (EmployeeTypeCd == "6") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WATCH & WARD SALARY CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'"; } }

                //int formats; 
                //formats = (CrystalDecisions.Shared.ViewerExportFormats.PdfFormat);
                //int exportFormatFlags = (int)(CrystalDecisions.Shared.ViewerExportFormats.PdfFormat | CrystalDecisions.Shared.ViewerExportFormats.ExcelFormat);
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.AllowedExportFormats = formats;

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();


                //ReportDocument rd = new ReportDocument();
                //ExportOptions CrExportOptions;

                //DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                //PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                //CrDiskFileDestinationOptions.DiskFileName = "D:\\payslip_1.pdf";
                //CrExportOptions = rd.ExportOptions;
                //CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                //CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                //CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                //CrExportOptions.FormatOptions = CrFormatTypeOptions;
                //rd.Export();
                //CrystalReportViewer1.Dispose();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);

            }
        }

        if (Get_Report_Type == "DeptManDays")
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();

            query = "Select MstDpt.DeptName as DepartmentNm,Count(SalDet.EmpNo) as Person_Tot,sum(SalDet.WorkedDays) as TotalWDays," +
                    " Sum(AttnDet.ThreeSided) as OTDays,Sum(cast(SalDet.OTHoursNew as decimal(18,2))) as OTHours," +
                    " sum(cast(SalDet.BasicAndDANew as decimal(18,2)) + cast(SalDet.BasicHRA as decimal(18,2)) +" +
                    
                    " cast(SalDet.ConvAllow as decimal(18,2)) + cast(SalDet.EduAllow as decimal(18,2)) +" +
                    " cast(SalDet.MediAllow as decimal(18,2)) + cast(SalDet.BasicRAI as decimal(18,2)) +" +
                    " cast(SalDet.WashingAllow as decimal(18,2)) + cast(SalDet.ThreesidedAmt as decimal(18,2)) +" +

                    " cast(SalDet.Basic_Spl_Allowance as decimal(18,2)) + cast(SalDet.Basic_Uniform as decimal(18,2)) + " +
                    " cast(SalDet.Basic_Vehicle_Maintenance as decimal(18,2)) + cast(SalDet.Basic_Communication as decimal(18,2)) + " +
                    " cast(SalDet.Basic_Journ_Perid_Paper as decimal(18,2)) + cast(SalDet.Basic_Incentives as decimal(18,2)) + " +

                    " cast(SalDet.OTHoursAmtNew as decimal(18,2))) as Wages,sum(SalDet.ProvidentFund) as PFAmt," +
                    " sum(SalDet.ESI) as ESIAmt from Employee_Mst EmpDet" +
                    " inner join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    " inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo" +
                    " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                    " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "'" +
                    " and EmpDet.CatName='" + str_cate + "' and EmpDet.CompCode='" + SessionCcode + "'" +
                    " and EmpDet.LocCode='" + SessionLcode + "'" + // and OP.WagesType='" + salaryType + "'" +
                    " and AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "'" +
                    " and EmpDet.Wages='" + EmployeeType + "' and AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "'";

            //Activate Employee Check
            if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,convert(datetime,EmpDet.DOR,105),105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
            //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
            else
            {
                if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
            }

            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (EmpDet.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (EmpDet.Eligible_PF='2')";
                }
            }

            if (EmployeeTypeCd == "5")
            {
                query = query + " and SalDet.FromDate = convert(datetime,'" + fromdate + "', 105) and SalDet.ToDate = Convert(Datetime,'" + ToDate + "', 105)";
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and EmpDet.OtherState='Yes'";
            //}



            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
            }

            query = query + "group by MstDpt.DeptName" +
            " Order by MstDpt.DeptName Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {

                rd.Load(Server.MapPath("Payslip/dept_wise_man_days_and_wages.rpt"));
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "Department Wise Wages Report for the month of " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                if (EmployeeTypeCd == "5")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WEEKLY CIVIL - Department Wise Wages Report for the month of " + str_month.ToUpper() + " - " + YR.ToString() + " From : " + fromdate + " To : " + ToDate + "'";
                }
                if (EmployeeTypeCd == "4")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS CASUAL - Department Wise Wages Report for the month of " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                }
                if (EmployeeTypeCd == "3" || EmployeeTypeCd == "2")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS - Department Wise Wages Report for the month of " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                }

                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        //EL / CL REPORT
        if (Get_Report_Type == "ELCL_REPORT")
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();

            string EligibleWorkDays = "0";
            string BelowCheckEmp = "0";
            string PF_Non_PF_Str = "0";
            EligibleWorkDays = Request.QueryString["EligbleWDays"].ToString();
            BelowCheckEmp = Request.QueryString["BelowCheck"].ToString();
            PF_Non_PF_Str = Request.QueryString["PFTypePost"].ToString();

            query = "SELECT	ED.EmpNo,ED.FirstName as EmpName,ED.ExistingCode as ExisistingCode,MD.DeptName as DepartmentNm,ED.Designation,convert(varchar,ED.DOJ,103) as DOJ,ED.BaseSalary as Base,";
            query = query + " (SELECT Sum(AD.Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='1' and AD.Months='January' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Jan,";
            query = query + " (SELECT Sum(AD.Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='2' and AD.Months='February' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Feb,";
            query = query + " (SELECT Sum(AD.Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='3' and AD.Months='March' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Mar,";
            query = query + " (SELECT Sum(AD.Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='4' and AD.Months='April' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Apr,";
            query = query + " (SELECT Sum(AD.Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='5' and AD.Months='May' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS May,";
            query = query + " (SELECT Sum(AD.Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='6' and AD.Months='June' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Jun,";
            query = query + " (SELECT Sum(AD.Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='7' and AD.Months='July' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Jul,";
            query = query + " (SELECT Sum(AD.Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='8' and AD.Months='August' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Aug,";
            query = query + " (SELECT Sum(AD.Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='9' and AD.Months='September' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Sep,";
            query = query + " (SELECT Sum(AD.Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='10' and AD.Months='October' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Oct,";
            query = query + " (SELECT Sum(AD.Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='11' and AD.Months='November' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Nov,";
            query = query + " (SELECT Sum(AD.Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='12' and AD.Months='December' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Dec,";
            query = query + " (SELECT Sum(AD.Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Total_Days,";
            query = query + " (SELECT Sum(AD.NFH)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS NFH_Days,";
            query = query + " (SELECT Sum(AD.ThreeSided)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS OT_Days,";
            query = query + " (SELECT Sum(cast(AD.WH_Work_Days as decimal(18,2))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS WH_Days,";

            //Week Off Worked Days Get
            query = query + " ISNULL((SELECT Sum(cast(AD.WH_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='1' and AD.Months='January' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Jan_WHDays,";
            query = query + " ISNULL((SELECT Sum(cast(AD.WH_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='2' and AD.Months='February' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Feb_WHDays,";
            query = query + " ISNULL((SELECT Sum(cast(AD.WH_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='3' and AD.Months='March' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Mar_WHDays,";
            query = query + " ISNULL((SELECT Sum(cast(AD.WH_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='4' and AD.Months='April' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Apr_WHDays,";
            query = query + " ISNULL((SELECT Sum(cast(AD.WH_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='5' and AD.Months='May' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS May_WHDays,";
            query = query + " ISNULL((SELECT Sum(cast(AD.WH_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='6' and AD.Months='June' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Jun_WHDays,";
            query = query + " ISNULL((SELECT Sum(cast(AD.WH_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='7' and AD.Months='July' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Jul_WHDays,";
            query = query + " ISNULL((SELECT Sum(cast(AD.WH_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='8' and AD.Months='August' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Aug_WHDays,";
            query = query + " ISNULL((SELECT Sum(cast(AD.WH_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='9' and AD.Months='September' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Sep_WHDays,";
            query = query + " ISNULL((SELECT Sum(cast(AD.WH_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='10' and AD.Months='October' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Oct_WHDays,";
            query = query + " ISNULL((SELECT Sum(cast(AD.WH_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='11' and AD.Months='November' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Nov_WHDays,";
            query = query + " ISNULL((SELECT Sum(cast(AD.WH_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='12' and AD.Months='December' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Dec_WHDays,";

            //Fixed_Worked_Days
            query = query + " ISNULL((SELECT Sum(cast(AD.Fixed_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='1' and AD.Months='January' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Jan_FixedWD,";
            query = query + " ISNULL((SELECT Sum(cast(AD.Fixed_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='2' and AD.Months='February' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Feb_FixedWD,";
            query = query + " ISNULL((SELECT Sum(cast(AD.Fixed_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='3' and AD.Months='March' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Mar_FixedWD,";
            query = query + " ISNULL((SELECT Sum(cast(AD.Fixed_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='4' and AD.Months='April' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Apr_FixedWD,";
            query = query + " ISNULL((SELECT Sum(cast(AD.Fixed_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='5' and AD.Months='May' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS May_FixedWD,";
            query = query + " ISNULL((SELECT Sum(cast(AD.Fixed_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='6' and AD.Months='June' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Jun_FixedWD,";
            query = query + " ISNULL((SELECT Sum(cast(AD.Fixed_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='7' and AD.Months='July' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Jul_FixedWD,";
            query = query + " ISNULL((SELECT Sum(cast(AD.Fixed_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='8' and AD.Months='August' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Aug_FixedWD,";
            query = query + " ISNULL((SELECT Sum(cast(AD.Fixed_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='9' and AD.Months='September' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Sep_FixedWD,";
            query = query + " ISNULL((SELECT Sum(cast(AD.Fixed_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='10' and AD.Months='October' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Oct_FixedWD,";
            query = query + " ISNULL((SELECT Sum(cast(AD.Fixed_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='11' and AD.Months='November' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Nov_FixedWD,";
            query = query + " ISNULL((SELECT Sum(cast(AD.Fixed_Work_Days as decimal(18,1))) FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='12' and AD.Months='December' And Year(AD.FromDate)='" + str_yr + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'),0) AS Dec_FixedWD";

            query = query + " FROM	Employee_Mst ED";
            query = query + " inner join Department_Mst as MD on MD.DeptCode = ED.DeptCode";
            query = query + " inner join [" + SessionPayroll + "]..AttenanceDetails as AD on AD.EmpNo=ED.EmpNo";
            //query = query + " inner join SalaryMaster as SM on SM.EmpNo=AD.EmpNo";
            //query = query + " inner Join OfficialProfile OP on OP.EmpNo=AD.EmpNo";
            query = query + " WHERE (ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And Year(AD.FromDate)='" + str_yr + "'";

            //Activate Employee Check
            if (Left_Employee == "1") { query = query + " and ED.IsActive='No' and Month(convert(datetime,ED.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,ED.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
            //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
            else
            {
                if (Left_Date != "") { query = query + " and (convert(datetime,ED.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or ED.IsActive='Yes')"; }
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}


            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (ED.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (ED.Eligible_PF='2')";
                }
            }

            query = query + " And (SELECT Sum(AD.Days + AD.WH_Work_Days)  FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And Year(AD.FromDate)='" + str_yr + "'";

            if (BelowCheckEmp == "1")
            {
                query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') < = " + EligibleWorkDays + " And ED.Wages='" + EmployeeType + "')";
            }
            else
            {
                query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') > = " + EligibleWorkDays + " And ED.Wages='" + EmployeeType + "')";
            }

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }
            if (Salary_CashOrBank.ToString() != "0")
            {
                if (Salary_CashOrBank.ToString() == "1")
                {
                    //Cash Employee
                    query = query + " and (ED.Salary_Through='1')";
                }
                else
                {
                    //Bank Employee
                    query = query + " and (ED.Salary_Through='2')";
                }
            }


            query = query + " Group by ED.EmpNo,ED.FirstName,ED.ExistingCode,MD.DeptName,ED.Designation,ED.DOJ,ED.BaseSalary";
            query = query + " Order by ED.ExistingCode Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {
                string Get_EL_Percentage = "0";
                DataTable DT_EL = new DataTable();
                query = "Select * from [" + SessionPayroll + "]..MstBasic_EL_Percentage where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' And Attn_Year='" + str_yr + "'";
                DT_EL = objdata.RptEmployeeMultipleDetails(query);
                if (DT_EL.Rows.Count != 0)
                {
                    Get_EL_Percentage = DT_EL.Rows[0]["EL_Percentage"].ToString();
                }
                else
                {
                    Get_EL_Percentage = "0";
                }
                string EL_PayslipType = "1";
                EL_PayslipType = Request.QueryString["PayslipType"].ToString();

                if (EL_PayslipType == "1")
                {
                    rd.Load(Server.MapPath("Payslip/EL_CL_Report.rpt"));
                }
                if (EL_PayslipType == "2")
                {
                    rd.Load(Server.MapPath("Payslip/EL_CL_Report_Cover.rpt"));
                }
                if (EL_PayslipType == "3")
                {
                    rd.Load(Server.MapPath("Payslip/EL_CL_Report_Signlist.rpt"));
                }
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";
                if (EL_PayslipType == "1")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF EL / CL REPORT" + "'";
                    if (EmployeeTypeCd == "5")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WEEKLY CIVIL - EL / CL REPORT" + "'";
                    }
                    if (EmployeeTypeCd == "4")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS CASUAL - EL / CL REPORT" + "'";
                    }
                    if (EmployeeTypeCd == "3" || EmployeeTypeCd == "2")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS - EL / CL REPORT" + "'";
                    }
                }
                else
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF LEAVE WAGES FOR THE YEAR" + "'";
                    if (EmployeeTypeCd == "5")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WEEKLY CIVIL LEAVE WAGES FOR THE YEAR" + "'";
                    }
                    if (EmployeeTypeCd == "4")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS CASUAL LEAVE WAGES FOR THE YEAR" + "'";
                    }
                    if (EmployeeTypeCd == "3" || EmployeeTypeCd == "2")
                    {
                        rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS LEAVE WAGES FOR THE YEAR" + "'";
                    }
                }
                rd.DataDefinition.FormulaFields["Report_Year"].Text = "'" + str_yr + "'";
                if (BelowCheckEmp == "1")
                {
                    rd.DataDefinition.FormulaFields["Eligible_Days"].Text = "'Eligibility List Worked Days <= " + EligibleWorkDays + "'";
                }
                else
                {
                    rd.DataDefinition.FormulaFields["Eligible_Days"].Text = "'Eligibility List Worked Days >= " + EligibleWorkDays + "'";
                }
                //rd.DataDefinition.FormulaFields["Eligible_Days"].Text = "'" + EligibleWorkDays + "'";
                rd.DataDefinition.FormulaFields["Staff_Labour"].Text = "'" + str_cate + "'";
                rd.DataDefinition.FormulaFields["EmployeeType"].Text = "'" + EmployeeType + "'";

                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.DataDefinition.FormulaFields["EL_Percentage"].Text = "'" + Get_EL_Percentage + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }


        //NFH WORKED Wages Report
        if (Get_Report_Type == "NFHWages")
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();

            query = "Select Attn.EmpNo,MstDpt.DeptName as DepartmentNm,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,Attn.NFH_Work_Days,Attn.NFH_Work_Days_Statutory,EmpDet.Wages as EmployeeType,EmpDet.Designation, " +
                    " cast(SalDet.Basic_SM as decimal(18,2)) as Basic_SM from Employee_Mst EmpDet " +
                    " inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                    " inner Join [" + SessionPayroll + "]..AttenanceDetails Attn on EmpDet.EmpNo=Attn.EmpNo " +
                    " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode " +
                    //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                    //" inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
                    " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                    " Attn.Months='" + str_month + "' AND Attn.FinancialYear='" + str_yr + "' and Attn.Ccode='" + SessionCcode + "' and Attn.Lcode='" + SessionLcode + "' and " +
                    " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and SalDet.WagesType='" + salaryType + "' and EmpDet.Wages='" + EmployeeType + "'";

            //Activate Employee Check
            if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
            //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
            else
            {
                if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
            }

            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (EmpDet.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (EmpDet.Eligible_PF='2')";
                }
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and EmpDet.OtherState='Yes'";
            //}


            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and EmpDet.Division='" + Get_Division_Name + "'";
            }

            query = query + " Order by EmpDet.ExistingCode,MstDpt.DeptName Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();



            if (ds1.Tables[0].Rows.Count > 0)
            {
                rd.Load(Server.MapPath("Payslip/NFH_Wages.rpt"));
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "NFH WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";

                if (EmployeeTypeCd != "5")
                {
                    if (PayslipType == "0" || PayslipType == "2")
                    {
                        rd.DataDefinition.FormulaFields["PayMonth"].Text = "'" + str_month.ToUpper() + " - " + YR.ToString() + "'";

                    }
                }

                if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6" || EmployeeTypeCd == "7")
                {
                    if (PayslipType == "0" || PayslipType == "2")
                    {
                        rd.DataDefinition.FormulaFields["PayMonth"].Text = "'" + str_month.ToUpper() + " - " + YR.ToString() + "'";

                    }
                }
                else if (EmployeeTypeCd == "5")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WEEKLY CIVIL NFH WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + " From : " + fromdate + " To : " + ToDate + "'";
                }
                else if (EmployeeTypeCd == "4")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS CASUAL / NFH WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                }
                else if (EmployeeTypeCd == "3")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS NFH WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                }
                else if (EmployeeTypeCd == "2")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS NFH WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                }
                else if (EmployeeTypeCd == "1")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "NFH WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                }
                else if (EmployeeTypeCd == "6")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "NFH WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                }
                else
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "NFH WAGES FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'";
                }

                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);

            }
        }


        //Attn. REPORT
        if (Get_Report_Type == "Attn_REPORT")
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();

            string Get_AttnDaysRptType = "";
            string sum_of_Days_Field = "";
            Get_AttnDaysRptType = Request.QueryString["AttnDaysType"].ToString();
            if (Get_AttnDaysRptType == "1")
            {
                sum_of_Days_Field = "AD.Days";
            }
            else
            {
                sum_of_Days_Field = "AD.Days + AD.ThreeSided + cast(AD.WH_Work_Days as decimal(18,1))";
            }

            query = "SELECT	ED.EmpNo,ED.FirstName as EmpName,ED.ExistingCode as ExisistingCode,MD.DeptName as DepartmentNm,ED.Designation,convert(varchar,ED.DOJ,103) as DOJ,ED.BaseSalary as Base,AD_Year.AttnYear,";
            query = query + " (SELECT Sum(" + sum_of_Days_Field + ") FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='1' and AD.Months='January' And Year(AD.FromDate)=AD_Year.AttnYear And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Jan,";
            query = query + " (SELECT Sum(" + sum_of_Days_Field + ") FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='2' and AD.Months='February' And Year(AD.FromDate)=AD_Year.AttnYear And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Feb,";
            query = query + " (SELECT Sum(" + sum_of_Days_Field + ") FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='3' and AD.Months='March' And Year(AD.FromDate)=AD_Year.AttnYear And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Mar,";
            query = query + " (SELECT Sum(" + sum_of_Days_Field + ") FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='4' and AD.Months='April' And Year(AD.FromDate)=AD_Year.AttnYear And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Apr,";
            query = query + " (SELECT Sum(" + sum_of_Days_Field + ") FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='5' and AD.Months='May' And Year(AD.FromDate)=AD_Year.AttnYear And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS May,";
            query = query + " (SELECT Sum(" + sum_of_Days_Field + ") FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='6' and AD.Months='June' And Year(AD.FromDate)=AD_Year.AttnYear And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Jun,";
            query = query + " (SELECT Sum(" + sum_of_Days_Field + ") FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='7' and AD.Months='July' And Year(AD.FromDate)=AD_Year.AttnYear And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Jul,";
            query = query + " (SELECT Sum(" + sum_of_Days_Field + ") FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='8' and AD.Months='August' And Year(AD.FromDate)=AD_Year.AttnYear And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Aug,";
            query = query + " (SELECT Sum(" + sum_of_Days_Field + ") FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='9' and AD.Months='September' And Year(AD.FromDate)=AD_Year.AttnYear And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Sep,";
            query = query + " (SELECT Sum(" + sum_of_Days_Field + ") FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='10' and AD.Months='October' And Year(AD.FromDate)=AD_Year.AttnYear And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Oct,";
            query = query + " (SELECT Sum(" + sum_of_Days_Field + ") FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='11' and AD.Months='November' And Year(AD.FromDate)=AD_Year.AttnYear And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Nov,";
            query = query + " (SELECT Sum(" + sum_of_Days_Field + ") FROM [" + SessionPayroll + "]..AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='12' and AD.Months='December' And Year(AD.FromDate)=AD_Year.AttnYear And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "') AS Dec";

            query = query + " FROM	Employee_Mst as ED";
            query = query + " outer apply (SELECT	Distinct Year(Attn.FromDate) as AttnYear from [" + SessionPayroll + "]..AttenanceDetails as Attn where Attn.EmpNo=ED.EmpNo And Attn.Ccode='" + SessionCcode + "' And Attn.Lcode='" + SessionLcode + "') as AD_Year";
            query = query + " inner join Department_Mst as MD on MD.DeptCode = ED.DeptCode";
            query = query + " inner join [" + SessionPayroll + "]..AttenanceDetails as AD on AD.EmpNo=ED.EmpNo";
            //query = query + " inner join SalaryMaster as SM on SM.EmpNo=AD.EmpNo";
            //query = query + " inner Join OfficialProfile OP on OP.EmpNo=AD.EmpNo";
            query = query + " WHERE (ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.Wages='" + EmployeeType + "')";

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}



            query = query + " Group by ED.EmpNo,ED.FirstName,ED.ExistingCode,MD.DeptName,ED.Designation,";
            query = query + " ED.DOJ,ED.BaseSalary,AD_Year.AttnYear";
            query = query + " Order by ED.ExistingCode,AD_Year.AttnYear Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            cmd.CommandTimeout = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SqlCommandTimeOut"]);
            
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {

                rd.Load(Server.MapPath("Payslip/Attn_Report.rpt"));
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF - ATTENDANCE DETAILS FROM HIS JOINING DATE" + "'";
                if (EmployeeTypeCd == "5")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WEEKLY CIVIL - ATTENDANCE DETAILS FROM HIS JOINING DATE" + "'";
                }
                if (EmployeeTypeCd == "4")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS CASUAL - ATTENDANCE DETAILS FROM HIS JOINING DATE" + "'";
                }
                if (EmployeeTypeCd == "3" || EmployeeTypeCd == "2")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS - ATTENDANCE DETAILS FROM HIS JOINING DATE" + "'";
                }
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        //Leave Credit Consolidated REPORT
        if (Get_Report_Type == "LC_ALL_REPORT")
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();
            string Year_Str = "";
            //Month Field Get Start
            string Month_Field = "";
            string Last_Month_Int = "0";
            
            //str_month = "December";
            Year_Str = Request.QueryString["yr"].ToString();

            if (str_month == "January") { Month_Field = "[Jan]"; Last_Month_Int = "1"; }
            else if (str_month == "February") { Month_Field = "[Jan],[Feb]"; Last_Month_Int = "2"; }
            else if (str_month == "March") { Month_Field = "[Jan],[Feb],[Mar]"; Last_Month_Int = "3"; }
            else if (str_month == "April") { Month_Field = "[Jan],[Feb],[Mar],[Apr]"; Last_Month_Int = "4"; }
            else if (str_month == "May") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May]"; Last_Month_Int = "5"; }
            else if (str_month == "June") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun]"; Last_Month_Int = "6"; }
            else if (str_month == "July") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun],[Jul]"; Last_Month_Int = "7"; }
            else if (str_month == "August") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun],[Jul],[Aug]"; Last_Month_Int = "8"; }
            else if (str_month == "September") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun],[Jul],[Aug],[Sep]"; Last_Month_Int = "9"; }
            else if (str_month == "October") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun],[Jul],[Aug],[Sep],[Oct]"; Last_Month_Int = "10"; }
            else if (str_month == "November") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun],[Jul],[Aug],[Sep],[Oct],[Nov]"; Last_Month_Int = "11"; }
            else if (str_month == "December") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun],[Jul],[Aug],[Sep],[Oct],[Nov],[Dec]"; Last_Month_Int = "12"; }

            //Isnull Check Field Name
            string IsNull_Month = "";
            if (str_month == "January") { IsNull_Month = "Isnull([Jan],0) as Jan,'0' as Feb,'0' as Mar,'0' as Apr,'0' as May,'0' as Jun,'0' as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "February") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,'0' as Mar,'0' as Apr,'0' as May,'0' as Jun,'0' as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "March") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,'0' as Apr,'0' as May,'0' as Jun,'0' as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "April") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,'0' as May,'0' as Jun,'0' as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "May") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,'0' as Jun,'0' as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "June") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,'0' as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "July") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,Isnull([Jul],0) as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "August") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,Isnull([Jul],0) as Jul,Isnull([Aug],0) as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "September") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,Isnull([Jul],0) as Jul,Isnull([Aug],0) as Aug,Isnull([Sep],0) as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "October") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,Isnull([Jul],0) as Jul,Isnull([Aug],0) as Aug,Isnull([Sep],0) as Sep,Isnull([Oct],0) as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "November") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,Isnull([Jul],0) as Jul,Isnull([Aug],0) as Aug,Isnull([Sep],0) as Sep,Isnull([Oct],0) as Oct,Isnull([Nov],0) as Nov,'0' as Dec"; }
            else if (str_month == "December") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,Isnull([Jul],0) as Jul,Isnull([Aug],0) as Aug,Isnull([Sep],0) as Sep,Isnull([Oct],0) as Oct,Isnull([Nov],0) as Nov,Isnull([Dec],0) as Dec"; }
            //Month Field Get End

            query = "select EmpNo,ExisistingCode,EmpName,Designation,DOJ,TOT_LC,TOT_LC_Minus,WeekOff," + IsNull_Month;
            query = query + " from (select sum(SD.WorkedDays+SD.WH_Work_Days+SD.NFh) Days,left(DATENAME(MM, SD.FromDate),3) month,";
            query = query + " ED.EmpNo,ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,ED.Designation,ED.WeekOff,convert(varchar,ED.DOJ,103) as DOJ,";
            query = query + " isnull((SELECT Sum(SD.Leave_Credit_Days) FROM [" + SessionPayroll + "]..SalaryDetails SD WHERE SD.EmpNo=ED.EmpNo And month(SD.FromDate) <='" + Last_Month_Int + "'";
            query = query + " And Year(SD.FromDate)='" + Year_Str + "' And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "' And Left(Leave_Credit_Days,1) <> '-'),0) AS TOT_LC,";
            query = query + " isnull((SELECT Sum(cast (right(SD.Leave_Credit_Days,len(SD.Leave_Credit_Days)-1) as decimal(18,1))) FROM [" + SessionPayroll + "]..SalaryDetails SD WHERE SD.EmpNo=ED.EmpNo";
            query = query + " And month(SD.FromDate) <='" + Last_Month_Int + "' And Year(SD.FromDate)='" + Year_Str + "' And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "' And Left(Leave_Credit_Days,1) = '-'),0) AS TOT_LC_Minus";
            //query = query + " isnull((Select WeekOff from Employee_Mst EM where EM.MachineID=ED.MachineID And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'),'') as WeekOff";
            query = query + " from Employee_Mst ED inner Join [" + SessionPayroll + "]..SalaryDetails SD on SD.EmpNo=ED.EmpNo"; //inner Join Officialprofile OP on ED.EmpNo=OP.EmpNo";
            query = query + " inner join MstEmployeeType ET on ET.EmpType=ED.Wages";
            query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And Year(SD.FromDate)='" + Year_Str + "'";
            query = query + " And (ET.EmpTypeCd='1' OR ET.EmpTypeCd='6' OR ET.EmpTypeCd='7')";

            //Activate Employee Check
            if (Left_Employee == "1") { query = query + " and ED.IsActive='No' and Month(convert(datetime,ED.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,ED.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
            //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
            else
            {
                if (Left_Date != "") { query = query + " and (convert(datetime,ED.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or ED.IsActive='Yes')"; }
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}



            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (ED.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (ED.Eligible_PF='2')";
                }
            }

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            query = query + " group by DATENAME(month, FromDate),ED.EmpNo,ED.ExistingCode,ED.FirstName,ED.Designation,ED.DOJ,ED.WeekOff";
            query = query + " ) as p";
            query = query + " PIVOT(MAX(Days) for month in (" + Month_Field + ")) as pvt";
            query = query + " Order by ExisistingCode Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {

                rd.Load(Server.MapPath("Payslip/LeaveCredit_Report_Consolidate.rpt"));
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["LC_Year"].Text = "'" + Year_Str + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                //rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF - LEAVE CREDIT DETAILS" + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        //Leave Credit Year Total REPORT
        if (Get_Report_Type == "LC_YearTotal_REPORT")
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();

            string Year_Str = "";
            Year_Str = Request.QueryString["yr"].ToString();

            query = "select ED.EmpNo,ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,ED.Designation,ED.WeekOff,LC.LeaveCredit";
            //query = query + " (isnull((Select WeekOff from eAlert..Employee_Mst EM where EM.MachineID=ED.BiometricID"; 
            //query = query + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'),'')) as WeekOff";
            query = query + " from Employee_Mst ED";
            query = query + " inner Join [" + SessionPayroll + "]..MstLeaveCredit LC on LC.EmpNo=ED.EmpNo"; //inner Join Officialprofile OP on ED.EmpNo=OP.EmpNo";
            query = query + " inner join MstEmployeeType ET on ET.EmpType=ED.Wages";
            query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' and LC.Ccode='" + SessionCcode + "'";
            query = query + " And LC.Lcode='" + SessionLcode + "' And (ET.EmpTypeCd='1' OR ET.EmpTypeCd='6' OR ET.EmpTypeCd='7')";
            query = query + " and LC.AttnFinYear='" + Year_Str + "'";
            //Activate Employee Check
            if (Left_Employee == "1") { query = query + " and ED.IsActive='No' and Month(convert(datetime,ED.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,ED.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
            //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
            else
            {
                if (Left_Date != "") { query = query + " and (convert(datetime,ED.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or ED.IsActive='Yes')"; }
            }
            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (ED.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (ED.Eligible_PF='2')";
                }
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}


            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            query = query + " Order by ED.ExistingCode Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {

                rd.Load(Server.MapPath("Payslip/LeaveCredit_Year_Total_Report.rpt"));
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["LC_Year"].Text = "'" + str_month.ToUpper() + " - " + Year_Str + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                //rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF - LEAVE CREDIT LIST - " + Year_Str + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        //Leave Credit Details REPORT
        if (Get_Report_Type == "LC_Details_REPORT")
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();

            string Year_Str = "";
            Year_Str = Request.QueryString["yr"].ToString();

            query = "select ED.EmpNo,ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,ED.Designation,ED.WeekOff,SD.month,SD.Totalworkingdays,isnull(cast(SD.Fixed_Work_Days as decimal(18,0)),0) as Fixed_Work_Days,";
            query = query + " isnull(cast(SD.WH_Work_Days as decimal(18,1)),0) as WH_Work_Days,SD.NFH,SD.WorkedDays,SD.LOPDays,SD.Leave_Credit_Days";
            //query = query + " (isnull((Select WeekOff from eAlert..Employee_Mst EM where EM.MachineID=ED.BiometricID And EM.CompCode='" + SessionCcode + "'";
            //query = query + " And EM.LocCode='" + SessionLcode + "'),'')) as WeekOff 
            query = query + " from Employee_Mst ED";
            query = query + " inner Join [" + SessionPayroll + "]..SalaryDetails SD on SD.EmpNo=ED.EmpNo"; //inner Join Officialprofile OP on ED.EmpNo=OP.EmpNo";
            query = query + " inner join MstEmployeeType ET on ED.Wages=ET.EmpType";
            query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "'";
            query = query + " and SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
            query = query + " And (ET.EmpTypeCd='1' OR ET.EmpTypeCd='6' OR ET.EmpTypeCd='7') And Year(SD.fromdate)='" + Year_Str + "'";
            //Activate Employee Check
            if (Left_Employee == "1") { query = query + " and ED.IsActive='No' and Month(convert(datetime,ED.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,ED.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
            //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
            else
            {
                if (Left_Date != "") { query = query + " and (convert(datetime,ED.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or ED.IsActive='Yes')"; }
            }
            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (ED.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (ED.Eligible_PF='2')";
                }
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}



            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            query = query + " Order by ED.ExistingCode,month(SD.fromdate) Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {

                rd.Load(Server.MapPath("Payslip/LeaveCredit_Det_Report.rpt"));
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["LC_Year"].Text = "'" + Year_Str + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                //rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF - LEAVE CREDIT LIST - " + Year_Str + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        //PF FORM 3A Details REPORT
        if (Get_Report_Type == "PF_Form3A_REPORT")
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();
            
            //Get Report_Date
            if (Basic_Report_Date != "")
            {
                query = "select ED.EmpNo,ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,ED.FamilyDetails as FatherName,SD.Month,convert(varchar,SD.FromDate,103) as FromDate,";
                query = query + " left(DATENAME(month,DATEADD(mm,1,SD.FromDate)),3) + '-' + right(Year(DATEADD(mm,1,SD.FromDate)),2) as Month_Str,";
                query = query + " cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,cast(SD.BasicHRA as decimal(18,2)) as BasicHRA,";
                query = query + " cast(SD.ConvAllow as decimal(18,2)) as ConvAllow,cast(SD.EduAllow as decimal(18,2)) as EduAllow,";
                query = query + " cast(SD.MediAllow as decimal(18,2)) as MediAllow,cast(SD.BasicRAI as decimal(18,2)) as BasicRAI,";
                query = query + " cast(SD.WashingAllow as decimal(18,2)) as WashingAllow,SD.ProvidentFund,SD.EmployeerPFone,";
                query = query + " SD.EmployeerPFTwo,ED.PFNo as PFnumber,ED.Wages as EmployeeType,";
                query = query + " SD.Basic_Spl_Allowance,SD.Basic_Uniform,SD.Basic_Vehicle_Maintenance,";
                query = query + " SD.Basic_Communication,SD.Basic_Journ_Perid_Paper,SD.Basic_Incentives,";
                query = query + " (case when SD.fromdate > convert(datetime,'" + Basic_Report_Date + "', 103) then 'NEW' else 'OLD' end) as PF_Sal_Type";
                query = query + " from Employee_Mst ED inner join [" + SessionPayroll + "]..SalaryDetails SD on ED.EmpNo=SD.EmpNo";
                //query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "'";
                query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "'";
                query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                query = query + " And SD.fromdate >= convert(datetime,'" + fromdate + "', 105) And SD.fromdate <= convert(datetime,'" + ToDate + "', 105)";
                query = query + " And ED.Wages='" + EmployeeType + "' And ED.Eligible_PF='1'";
            }
            else
            {
                query = "select ED.EmpNo,ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,ED.FamilyDetails as FatherName,SD.Month,convert(varchar,SD.FromDate,103) as FromDate,";
                query = query + " left(DATENAME(month,DATEADD(mm,1,SD.FromDate)),3) + '-' + right(Year(DATEADD(mm,1,SD.FromDate)),2) as Month_Str,";
                query = query + " cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,cast(SD.BasicHRA as decimal(18,2)) as BasicHRA,";
                query = query + " cast(SD.ConvAllow as decimal(18,2)) as ConvAllow,cast(SD.EduAllow as decimal(18,2)) as EduAllow,";
                query = query + " cast(SD.MediAllow as decimal(18,2)) as MediAllow,cast(SD.BasicRAI as decimal(18,2)) as BasicRAI,";
                query = query + " cast(SD.WashingAllow as decimal(18,2)) as WashingAllow,SD.ProvidentFund,SD.EmployeerPFone,";
                query = query + " SD.EmployeerPFTwo,ED.PFNo as PFnumber,ED.Wages as EmployeeType,";
                query = query + " SD.Basic_Spl_Allowance,SD.Basic_Uniform,SD.Basic_Vehicle_Maintenance,";
                query = query + " SD.Basic_Communication,SD.Basic_Journ_Perid_Paper,SD.Basic_Incentives,";
                query = query + " 'OLD' as PF_Sal_Type";
                query = query + " from Employee_Mst ED inner join SalaryDetails SD on ED.EmpNo=SD.EmpNo";
                //query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "'";
                query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "'";
                query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                query = query + " And SD.fromdate >= convert(datetime,'" + fromdate + "', 105) And SD.fromdate <= convert(datetime,'" + ToDate + "', 105)";
                query = query + " And ED.Wages='" + EmployeeType + "' And ED.Eligible_PF='1'";
            }

            

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }


            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}

            ////Activate Employee Check
            //if (Left_Employee == "1") { query = query + " and ED.ActivateMode='N'"; }
            ////if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
            //else
            //{
            //    if (Left_Date != "") { query = query + " and ED.deactivedate > convert(datetime,'" + Left_Date + "', 105)"; }
            //}
            ////PF Check
            //if (PFTypeGet.ToString() != "0")
            //{
            //    if (PFTypeGet.ToString() == "1")
            //    {
            //        //PF Employee
            //        query = query + " and (OP.EligiblePF='1')";
            //    }
            //    else
            //    {
            //        //Non PF Employee
            //        query = query + " and (OP.EligiblePF='2')";
            //    }
            //}

            query = query + " Order by ED.ExistingCode,SD.FromDate Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {

                rd.Load(Server.MapPath("Payslip/PFForm3A.rpt"));
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["From_Date_Str"].Text = "'" + fromdate + "'";
                rd.DataDefinition.FormulaFields["To_Date_Str"].Text = "'" + ToDate + "'";

                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "" + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        //Single Employee Advance History Report Details
        if (Get_Report_Type == "Single_Emp_Advance_History")
        {
            string Advance_ID_Get = Request.QueryString["AdvanceID"].ToString();

            query = "Select ED.EmpNo,ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,MD.DeptName as DepartmentNm,";
            query = query + " ED.Designation,Convert(varchar,OP.Dateofjoining,105) as DOJ,AD.Amount,Convert(varchar,ADH.TransDate,105) as AD_Date,";
            query = query + " ADH.Months,ADH.Amount as Paid_Amount from Employee_Mst ED";
            //query = query + " inner join Officialprofile OP on OP.EmpNo=ED.EmpNo inner join MstDepartment MD on MD.DepartmentCd=ED.Department";
            query = query + " inner join Department_Mst MD on MD.DeptCode=ED.DeptCode";
            query = query + " inner join [" + SessionPayroll + "]..AdvancePayment AD on AD.EmpNo=ED.EmpNo inner join [" + SessionPayroll + "]..Advancerepayment ADH on ADH.EmpNo=ED.EmpNo";
            query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "'";
            query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
            query = query + " And ADH.Ccode='" + SessionCcode + "' And ADH.Lcode='" + SessionLcode + "'";
            query = query + " And AD.ID='" + Advance_ID_Get + "' And ADH.AdvanceId='" + Advance_ID_Get + "'";

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}


            query = query + " Order by ADH.TransDate Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {

                rd.Load(Server.MapPath("Payslip/Advance_History_Single_Employee.rpt"));
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                //rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "" + "'";
                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        //ALL Employee Advance History Report Details
        if (Get_Report_Type == "ALL_Emp_Advance_History")
        {
            //str_cate
            //EmployeeType
            query = "Select ED.EmpNo,ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,MD.DeptName as DepartmentNm,";
            query = query + " ED.Designation,Convert(varchar,ED.DOJ,105) as DOJ,AD.Amount,AD.BalanceAmount";
            query = query + " from Employee_Mst ED"; //inner join Officialprofile OP on OP.EmpNo=ED.EmpNo";
            query = query + " inner join Department_Mst MD on MD.DeptCode=ED.DeptCode inner join [" + SessionPayroll + "]..AdvancePayment AD on AD.EmpNo=ED.EmpNo";
            query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "'";
            query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
            query = query + " And ED.CatName='" + str_cate + "' And ED.Wages='" + EmployeeType + "' And AD.Completed='N'";

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}


            query = query + " Order by ED.ExistingCode Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {

                rd.Load(Server.MapPath("Payslip/Advance_History_All_Employee.rpt"));
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["EmployeeType"].Text = "'" + EmployeeType + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                //rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "" + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        //Single Employee Salary History Report Details
        if (Get_Report_Type == "Employee_Salary_History")
        {
            //Emp_No Get
            string EmpNo_Search = Request.QueryString["EmpNo_Search"].ToString();

            //Parthi Start
            query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                                 " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive,(SalDet.New_Mess_Amt) as MediAllow1,SalDet.Deduction3 as MediAllow," +
                                 " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                                 " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                                 " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                                 " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt,(SalDet.Month) as Basic_Spl_Allowance " +
                                 " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                 " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                 " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                 " where  (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes') and convert(datetime,SalDet.FromDate,103)>=convert(datetime,'" + fromdate + "',103) and convert(datetime,SalDet.ToDate,103)<=convert(datetime,'" + ToDate + "',103) and " +
                                 " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.EmpNo='" + EmpNo_Search + "'";
                            

            query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive,SalDet.New_Mess_Amt," +
           " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt,SalDet.Month,SalDet.FromDate," +
           " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
           " Order by cast(SalDet.FromDate as int) Asc";

            //Parthi End

            //EmployeeType Check
            //if (EmployeeTypeCd == "5")
            //{
            //    query = "Select SalDet.EmpNo,MstDpt.DeptName as DepartmentNm,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,SalDet.WorkedDays,EmpDet.Wages as EmployeeType,EmpDet.Designation, " +
            //            " SalDet.NFh,cast(SalDet.OTHoursNew as decimal(18,2)) as OTHoursNew,cast(SalDet.Basic_SM as decimal(18,2)) as Basic_SM," +
            //            " SalDet.BasicandDA as BasicAndDANew,cast(SalDet.OTHoursAmtNew as decimal(18,2)) as OTHoursAmtNew, " +
            //            " SalDet.DayIncentive,SalDet.Words,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
            //            " SalDet.allowances5,SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,SalDet.Advance,SalDet.Deduction1," +
            //            " SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5, " +
            //            " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2," +
            //            " SalDet.TotalDeductions,SalDet.RoundOffNetPay as NetPay,SalDet.FullNightAmt," +
            //            " SalDet.Leave_Credit_Days as LeaveCredit,SalDet.Month,convert(varchar,SalDet.FromDate,105) as FromDate,convert(varchar,SalDet.ToDate,105) as ToDate,EmpDet.Wages as EmployeeType,AttnDet.AEH,AttnDet.LBH from Employee_Mst EmpDet " +
            //            " inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
            //            " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode " +
            //            //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
            //            " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo And SalDet.FromDate=AttnDet.FromDate And SalDet.ToDate=AttnDet.ToDate" +
            //            //" inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
            //            " where EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.EmpNo='" + EmpNo_Search + "'" +
            //            " and SalDet.Ccode='" + SessionCcode + "' and SalDet.Lcode='" + SessionLcode + "' and SalDet.EmpNo='" + EmpNo_Search + "'" +
            //            " and AttnDet.Ccode='" + SessionCcode + "' and AttnDet.Lcode='" + SessionLcode + "' and AttnDet.EmpNo='" + EmpNo_Search + "'";
                

            //    if (fromdate.ToString() != "" && ToDate.ToString() != "")
            //    {
            //        query = query + " and SalDet.FromDate >= convert(datetime,'" + fromdate + "', 105) and SalDet.FromDate <= Convert(Datetime,'" + ToDate + "', 105)";
            //    }

            //    // Other State

            //    //if (Other_state == "Yes")
            //    //{
            //    //    query = query + " and EmpDet.OtherState='Yes'";
            //    //}

            //    query = query + " group by SalDet.EmpNo,MstDpt.DeptName,EmpDet.ExistingCode,EmpDet.FirstName,SalDet.WorkedDays,EmpDet.Wages,EmpDet.Designation, " +
            //            " SalDet.NFh,SalDet.OTHoursNew,SalDet.Basic_SM,SalDet.BasicandDA,SalDet.OTHoursAmtNew, " +
            //            " SalDet.DayIncentive,SalDet.Words,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4," +
            //            " SalDet.allowances5,SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,SalDet.Advance,SalDet.Deduction1," +
            //            " SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5, " +
            //            " SalDet.DedOthers1,SalDet.DedOthers2,SalDet.TotalDeductions,SalDet.RoundOffNetPay,SalDet.FullNightAmt,SalDet.Leave_Credit_Days," +
            //            " SalDet.Month,SalDet.FromDate,SalDet.ToDate,AttnDet.AEH,AttnDet.LBH" +
            //            " Order by SalDet.FromDate Asc";
            //}
            //else
            //{

            //    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.FamilyDetails as FatherName,MstDpt.DeptName as DepartmentNm," +
            //        " EmpDet.DeptName as Department,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided as OTDays,SalDet.CL," +
            //        " SalDet.WH_Work_Days,SalDet.Leave_Credit_Days as Leave_Credit,SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew," +
            //        " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
            //        " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
            //        " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
            //        " SalDet.Advance,SalDet.ProvidentFund,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
            //        " SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
            //        " cast(SalDet.DedOthers1 as decimal(18,2)) as DedOthers1,cast(SalDet.DedOthers2 as decimal(18,2)) as DedOthers2," +
            //        " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
            //        " SalDet.SalaryDate,convert(varchar,EmpDet.DOJ,105) as Dateofjoining,EmpDet.PFNo as PFnumber,EmpDet.ESINo as ESICnumber,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory," +
            //        " SalDet.Month,convert(varchar,SalDet.FromDate,105) as FromDate,convert(varchar,SalDet.ToDate,105) as ToDate,EmpDet.Wages as EmployeeType,AttnDet.AEH,AttnDet.LBH," +
                    
            //        " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
            //        " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +

            //        " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
            //        " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
            //        " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo And AttnDet.FromDate=SalDet.FromDate And AttnDet.ToDate=SalDet.ToDate" +
            //        //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
            //        " where EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.EmpNo='" + EmpNo_Search + "'" +
            //        " and AttnDet.Ccode='" + SessionCcode + "' and AttnDet.Lcode='" + SessionLcode + "' and AttnDet.EmpNo='" + EmpNo_Search + "'" +
            //        " and SalDet.Ccode='" + SessionCcode + "' and SalDet.Lcode='" + SessionLcode + "' and SalDet.EmpNo='" + EmpNo_Search + "'";


            //    if (fromdate.ToString() != "" && ToDate.ToString() != "")
            //    {
            //        query = query + " and AttnDet.FromDate >= convert(datetime,'" + fromdate + "', 105) and AttnDet.FromDate <= Convert(Datetime,'" + ToDate + "', 105)";
            //        query = query + " and SalDet.FromDate >= convert(datetime,'" + fromdate + "', 105) and SalDet.FromDate <= Convert(Datetime,'" + ToDate + "', 105)";
            //    }

            //    // Other State

            //    //if (Other_state == "Yes")
            //    //{
            //    //    query = query + " and EmpDet.OtherState='Yes'";
            //    //}


            //    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.FamilyDetails,MstDpt.DeptName," +
            //    " EmpDet.DeptName,EmpDet.Designation,SalDet.WorkedDays,SalDet.NFh,AttnDet.ThreeSided,SalDet.CL," +
            //    " SalDet.WH_Work_Days,SalDet.Leave_Credit_Days,SalDet.LOPDays,SalDet.Losspay,SalDet.Basic_SM,SalDet.BasicAndDANew," +
            //    " SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
            //    " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
            //    " SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5," +
            //    " SalDet.Advance,SalDet.ProvidentFund,SalDet.ESI,SalDet.GrossEarnings,SalDet.TotalDeductions," +
            //    " SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
            //    " SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays," +
            //    " SalDet.SalaryDate,EmpDet.DOJ,EmpDet.PFNo,EmpDet.ESINo,AttnDet.NFH_Work_Days,AttnDet.NFH_Work_Days_Statutory,SalDet.Month,SalDet.FromDate,SalDet.ToDate,EmpDet.Wages,AttnDet.AEH,AttnDet.LBH," +
            //    " SalDet.Basic_Spl_Allowance,SalDet.Basic_Uniform,SalDet.Basic_Vehicle_Maintenance," +
            //    " SalDet.Basic_Communication,SalDet.Basic_Journ_Perid_Paper,SalDet.Basic_Incentives,SalDet.Basic_Incentive_Text" +
            //    " Order by SalDet.FromDate Asc";
            //}
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {
                rd.Load(Server.MapPath("Payslip_New_Component/SingleEmpSlip.rpt"));

                //rd.Load(Server.MapPath("Payslip/Advance_History_All_Employee.rpt"));
                rd.SetDataSource(ds1.Tables[0]);
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "SALARY FOR THE FROM DATE " + fromdate + " - " + "TO DATE" + " " + ToDate + "'";

                //rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                //rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";
                //rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                //rd.DataDefinition.FormulaFields["FromDate_Str"].Text = "'" + fromdate + "'";
                //rd.DataDefinition.FormulaFields["ToDate_Str"].Text = "'" + ToDate + "'";

                //rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "" + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }


        if (Get_Report_Type == "Bonus_Recociliation") //Bonus_Recociliation
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();

            query = "Select ExisistingCode,EmpName,Dateofjoining,sum(case when Months = 'Oct' then WDays else 0 end) as [Col1_D],";
            query = query + " sum(case when Months = 'Nov' then WDays else 0 end) as [Col2_D],sum(case when Months = 'Dec' then WDays else 0 end) as [Col3_D],";
            query = query + " sum(case when Months = 'Jan' then WDays else 0 end) as [Col4_D],sum(case when Months = 'Feb' then WDays else 0 end) as [Col5_D],";
            query = query + " sum(case when Months = 'Mar' then WDays else 0 end) as [Col6_D],sum(case when Months = 'Apr' then WDays else 0 end) as [Col7_D],";
            query = query + " sum(case when Months = 'May' then WDays else 0 end) as [Col8_D],sum(case when Months = 'Jun' then WDays else 0 end) as [Col9_D],";
            query = query + " sum(case when Months = 'Jul' then WDays else 0 end) as [Col10_D],sum(case when Months = 'Aug' then WDays else 0 end) as [Col11_D],sum(case when Months = 'Sep' then WDays else 0 end) as [Col12_D],";
            query = query + " sum(case when Months = 'Oct' then WithInc_Amt else 0 end) as [Col1_A],sum(case when Months = 'Nov' then WithInc_Amt else 0 end) as [Col2_A],";
            query = query + " sum(case when Months = 'Dec' then WithInc_Amt else 0 end) as [Col3_A],sum(case when Months = 'Jan' then WithInc_Amt else 0 end) as [Col4_A],";
            query = query + " sum(case when Months = 'Feb' then WithInc_Amt else 0 end) as [Col5_A],sum(case when Months = 'Mar' then WithInc_Amt else 0 end) as [Col6_A],";
            query = query + " sum(case when Months = 'Apr' then WithInc_Amt else 0 end) as [Col7_A],sum(case when Months = 'May' then WithInc_Amt else 0 end) as [Col8_A],";
            query = query + " sum(case when Months = 'Jun' then WithInc_Amt else 0 end) as [Col9_A],sum(case when Months = 'Jul' then WithInc_Amt else 0 end) as [Col10_A],";
            query = query + " sum(case when Months = 'Aug' then WithInc_Amt else 0 end) as [Col11_A],sum(case when Months = 'Sep' then WithInc_Amt else 0 end) as [Col12_A],";
            query = query + " SUM(Incentive_Amt) as Incentive_Amt from (";
            if ((SessionLcode == "UNIT I" || SessionLcode == "UNIT IV") && fromdate == "01-10-2014")
            {
                query = query + " Select ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,convert(varchar,ED.DOJ,105) as Dateofjoining,sum(SD.WDays + SD.OTDays + SD.NFH) as WDays,";
                query = query + " SUM(cast(SD.BasicAndDANew as decimal(18,2)) + cast(SD.BasicHRA as decimal(18,2)) + cast(SD.ConvAllow as decimal(18,2)) + cast(SD.EduAllow as decimal(18,2)) +";

                query = query + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) + cast(SD.Basic_Uniform as decimal(18,2)) + ";
                query = query + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) + cast(SD.Basic_Communication as decimal(18,2)) + ";
                query = query + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) + cast(SD.Basic_Incentives as decimal(18,2)) + ";

                query = query + " cast(SD.MediAllow as decimal(18,2)) + cast(SD.BasicRAI as decimal(18,2)) + cast(SD.WashingAllow as decimal(18,2)) + SD.ThreesidedAmt + SD.DayIncentive + SD.allowances1) as WithInc_Amt,";
                query = query + " SUM(SD.allowances1) as Incentive_Amt,left(DATENAME(MM, SD.FromDate),3) as Months";

                query = query + " from Employee_Mst ED inner join [" + SessionPayroll + "]..SalaryDetails_Bonus SD on ED.ExistingCode COLLATE DATABASE_DEFAULT=SD.ExisistingCode COLLATE DATABASE_DEFAULT And SD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
                //query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo";
                query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.Wages='" + EmployeeType + "'";
                query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                query = query + " And SD.FromDate >=convert(datetime,'" + fromdate + "', 105) And SD.FromDate <=convert(datetime,'" + ToDate + "', 105)";
            }
            else
            {
                query = query + " Select ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,convert(varchar,ED.DOJ,105) as Dateofjoining,";
                if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6" || EmployeeTypeCd == "7")
                {
                    query = query + " sum(AD.Days + cast(AD.WH_Work_Days as decimal(18,2)) + AD.NFh) as WDays,";
                }
                else
                {
                    query = query + " sum(AD.Days + AD.ThreeSided + AD.NFh) as WDays,";
                }
                query = query + " SUM(cast(SD.BasicAndDANew as decimal(18,2)) + cast(SD.BasicHRA as decimal(18,2)) + cast(SD.ConvAllow as decimal(18,2)) + cast(SD.EduAllow as decimal(18,2)) +";

                query = query + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) + cast(SD.Basic_Uniform as decimal(18,2)) + ";
                query = query + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) + cast(SD.Basic_Communication as decimal(18,2)) + ";
                query = query + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) + cast(SD.Basic_Incentives as decimal(18,2)) + ";

                query = query + " cast(SD.MediAllow as decimal(18,2)) + cast(SD.BasicRAI as decimal(18,2)) + cast(SD.WashingAllow as decimal(18,2)) + SD.ThreesidedAmt + SD.DayIncentive + SD.allowances1) as WithInc_Amt,";
                query = query + " SUM(SD.allowances1) as Incentive_Amt,left(DATENAME(MM, SD.FromDate),3) as Months";
                query = query + " from Employee_Mst ED inner join [" + SessionPayroll + "]..SalaryDetails SD on ED.EmpNo=SD.EmpNo And SD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
                query = query + " inner join [" + SessionPayroll + "]..AttenanceDetails AD on ED.EmpNo=AD.EmpNo And AD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT and SD.Lcode COLLATE DATABASE_DEFAULT=AD.Lcode COLLATE DATABASE_DEFAULT And AD.FromDate=SD.FromDate";
                //query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo";
                query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.Wages='" + EmployeeType + "'";
                query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                query = query + " And SD.FromDate >=convert(datetime,'" + fromdate + "', 105) And SD.FromDate <=convert(datetime,'" + ToDate + "', 105)";
                query = query + " And AD.FromDate >=convert(datetime,'" + fromdate + "', 105) And AD.FromDate <=convert(datetime,'" + ToDate + "', 105)";
            }


            //Activate Employee Check
            if (Left_Employee != "1")
            {
                if (Left_Employee == "2")
                {
                    query = query + " and ED.IsActive='Yes'";
                }
                if (Left_Employee == "3")
                {
                    query = query + " and ED.IsActive='No'";
                }
            }

            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (ED.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (ED.Eligible_PF='2')";
                }
            }

            //Salary Through Bank Or Cash Check
            if (Salary_CashOrBank.ToString() != "0")
            {
                if (Salary_CashOrBank.ToString() == "1")
                {
                    //Cash Employee
                    query = query + " and (ED.Salary_Through='1')";
                }
                else
                {
                    //Bank Employee
                    query = query + " and (ED.Salary_Through='2')";
                }
            }

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}


            query = query + " group by ED.ExistingCode,ED.FirstName,ED.DOJ,DATENAME(MM, SD.FromDate)";
            query = query + " ) as P";
            query = query + " Group by ExisistingCode,EmpName,Dateofjoining";
            query = query + " Order by ExisistingCode Asc";


            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {
                rd.Load(Server.MapPath("Payslip/Bonus_Recociliation.rpt"));

                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF RECOCILIATION DETAILS From : " + fromdate + " To : " + ToDate + "'";
                }

                if (EmployeeTypeCd == "7")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "MANAGER RECOCILIATION DETAILS From : " + fromdate + " To : " + ToDate + "'";
                }
                else if (EmployeeTypeCd == "3")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS RECOCILIATION DETAILS From : " + fromdate + " To : " + ToDate + "'";
                }
                else if (EmployeeTypeCd == "4")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS CASUAL / RECOCILIATION DETAILS From : " + fromdate + " To : " + ToDate + "'";
                }
                else
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "RECOCILIATION DETAILS From : " + fromdate + " To : " + ToDate + "'";
                }

                rd.DataDefinition.FormulaFields["Bonus_Year"].Text = "'" + YR + "'";
                rd.DataDefinition.FormulaFields["Bonus_From"].Text = "'" + (YR - 1) + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }


        if (Get_Report_Type == "Bonus_Checklist") //Bonus_Checklist
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();

            //if (EmployeeTypeCd == "4")
            //{
            //    query = "Select ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,convert(varchar,ED.DOJ,105) as Dateofjoining,MD.DeptName as DepartmentNm,ED.Designation,";
            //    //Experiance Get
            //    query = query + " (case when ED.IsActive = 'No' then (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, convert(datetime,ED.DOR,105))/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, convert(datetime,ED.DOR,105)) % 12))";
            //    query = query + " else (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000') % 12)) end";
            //    query = query + " ) as Experiance,";

            //    //query = query + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000') % 12)) AS Experiance,";

            //    query = query + " BD.Worked_Days,BD.OT_Days,BD.NFH_Days,isnull(BD.Voucher_Days,0) as Voucher_Days,BD.Bonus_Percent,BD.Bonus_Amt,BD.Round_Bonus,BD.Final_Bonus_Amt,ED.BaseSalary,BD.Hostel_Stage";
            //    query = query + " from Employee_Mst ED inner join [" + SessionPayroll + "]..Bonus_Details BD on ED.EmpNo=BD.EmpNo And ED.LocCode COLLATE DATABASE_DEFAULT=BD.Lcode COLLATE DATABASE_DEFAULT";
            //    //query = query + " inner join Officialprofile OP on OP.EmpNo=ED.EmpNo inner join SalaryMaster SM on SM.EmpNo=ED.EmpNo";
            //    query = query + " inner join Department_Mst MD on MD.DeptCode=ED.DeptCode";
            //    query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.Wages='" + EmployeeType + "'";
            //    query = query + " And BD.Ccode='" + SessionCcode + "' And BD.Lcode='" + SessionLcode + "' And BD.Bonus_Year='" + YR + "'";

            //    //Activate Employee Check
            //    if (Left_Employee != "1")
            //    {
            //        if (Left_Employee == "2")
            //        {
            //            query = query + " and ED.IsActive='Yes'";
            //        }
            //        if (Left_Employee == "3")
            //        {
            //            query = query + " and ED.IsActive='No'";
            //        }
            //    }



            //    //PF Check
            //    if (PFTypeGet.ToString() != "0")
            //    {
            //        if (PFTypeGet.ToString() == "1")
            //        {
            //            //PF Employee
            //            query = query + " and (ED.Eligible_PF='1')";
            //        }
            //        else
            //        {
            //            //Non PF Employee
            //            query = query + " and (ED.Eligible_PF='2')";
            //        }
            //    }

            //    //Salary Through Bank Or Cash Check
            //    if (Salary_CashOrBank.ToString() != "0")
            //    {
            //        if (Salary_CashOrBank.ToString() == "1")
            //        {
            //            //Cash Employee
            //            query = query + " and (ED.Salary_Through='1')";
            //        }
            //        else
            //        {
            //            //Bank Employee
            //            query = query + " and (ED.Salary_Through='2')";
            //        }
            //    }

            //    //Add Division Condition
            //    if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            //    {
            //        query = query + " and ED.Division='" + Get_Division_Name + "'";
            //    }

            //    query = query + " Order by ED.ExistingCode Asc";
            //}
            //else
            //{
            query = "Select ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,MD.DeptName as DepartmentNm,ED.Designation,convert(varchar,ED.DOJ,105) as Dateofjoining,";
            query = query + " SUM(BD.Worked_Days + BD.OT_Days + BD.NFH_Days + isnull(BD.Voucher_Days,0)) as Total_Days,BD.Gross_Sal,isnull(BD.Voucher_Amt,0) as Voucher_Amt,BD.Percent_Gross_Sal,BD.Bonus_Percent,BD.Bonus_Amt,";
            query = query + " BD.Round_Bonus,BD.Final_Bonus_Amt,LY_Final_Bonus_Amt,LY_Gross_Sal,LY_Bonus_Percent,LY_Total_Days,ED.BaseSalary as Base";
            query = query + " from Employee_Mst ED inner join [" + SessionPayroll + "]..Bonus_Details BD on BD.EmpNo=ED.EmpNo And BD.Lcode  COLLATE DATABASE_DEFAULT=ED.LocCode  COLLATE DATABASE_DEFAULT";
            //query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo inner join SalaryMaster SM on SM.EmpNo=ED.EmpNo";
            query = query + " inner join Department_Mst MD on MD.DeptCode=ED.DeptCode ";
            query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.Wages='" + EmployeeType + "'";
            query = query + " And BD.Ccode='" + SessionCcode + "' And BD.Lcode='" + SessionLcode + "' And BD.Bonus_Year='" + YR + "'";

            //Activate Employee Check
            if (Left_Employee != "1")
            {
                if (Left_Employee == "2")
                {
                    query = query + " and ED.IsActive='Yes'";
                }
                if (Left_Employee == "3")
                {
                    query = query + " and ED.IsActive='No'";
                }
            }

            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (ED.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (ED.Eligible_PF='2')";
                }
            }


            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}



            //Salary Through Bank Or Cash Check
            if (Salary_CashOrBank.ToString() != "0")
            {
                if (Salary_CashOrBank.ToString() == "1")
                {
                    //Cash Employee
                    query = query + " and (ED.Salary_Through='1')";
                }
                else
                {
                    //Bank Employee
                    query = query + " and (ED.Salary_Through='2')";
                }
            }

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            query = query + " group by ED.ExistingCode,ED.FirstName,MD.DeptName,ED.Designation,ED.DOJ,BD.Gross_Sal,BD.Voucher_Amt,BD.Percent_Gross_Sal,BD.Bonus_Percent,BD.Bonus_Amt,";
            query = query + " BD.Round_Bonus,BD.Final_Bonus_Amt,LY_Final_Bonus_Amt,LY_Gross_Sal,LY_Bonus_Percent,LY_Total_Days,ED.BaseSalary";
            query = query + " Order by ED.ExistingCode Asc";
            //  }

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {
                //if (EmployeeTypeCd == "4")
                //{
                //    rd.Load(Server.MapPath("Payslip/bonus_check_list_hostel.rpt"));
                //}
                //else
                //{
                rd.Load(Server.MapPath("Payslip/bonus_check_list_staff_regular.rpt"));
                // }
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF BONUS CUMULATIVE - For the Period October " + (YR - 1) + " September " + YR + "'";
                }

                if (EmployeeTypeCd == "7")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "MANAGER BONUS CUMULATIVE - For the Period October " + (YR - 1) + " September " + YR + "'";
                }

                else if (EmployeeTypeCd == "3")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS BONUS CUMULATIVE - For the Period October " + (YR - 1) + " September " + YR + "'";
                }
                else if (EmployeeTypeCd == "4")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS CASUAL / BONUS CUMULATIVE - For the Period October " + (YR - 1) + " September " + YR + "'";
                }
                else
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "BONUS CUMULATIVE - For the Period October " + (YR - 1) + " September " + YR + "'";
                }

                //rd.DataDefinition.FormulaFields["Bonus_Year"].Text = "'" + YR + "'";
                //rd.DataDefinition.FormulaFields["Bonus_From"].Text = "'" + (YR - 1) + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        if (Get_Report_Type == "Bonus_Individual") //Bonus_Individual
        {

            if (Basic_Report_Date != "")
            {
                DateTime Query_Date_Check_From = Convert.ToDateTime(fromdate);
                DateTime Query_Date_Check_To = Convert.ToDateTime(ToDate);
                DateTime DB_Basic_Report_Date = Convert.ToDateTime(Basic_Report_Date);
                if (Query_Date_Check_From <= DB_Basic_Report_Date && Query_Date_Check_To >= DB_Basic_Report_Date)
                {
                    Basic_Report_Type = "Mixed";
                }
                else if (Query_Date_Check_From >= DB_Basic_Report_Date)
                {
                    Basic_Report_Type = "New";
                }
                else
                {
                    Basic_Report_Type = "OLD";
                }
            }
            else
            {
                Basic_Report_Type = "Mixed";
            }

            Get_Division_Name = Request.QueryString["Division"].ToString();

            if ((SessionLcode == "UNIT I" || SessionLcode == "UNIT IV") && fromdate == "01-10-2014")
            {
                query = "Select ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,convert(varchar,ED.DOJ,105) as Dateofjoining,MD.DeptName as DepartmentNm,ED.Designation,";

                //Experiance Get
                query = query + " (case when ED.IsActive = 'No' then (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, convert(datetime,ED.DOR,105))/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, convert(datetime,ED.DOR,105)) % 12))";
                query = query + " else (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000') % 12)) end";
                query = query + " ) as Experiance,";

                //query = query + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000') % 12)) AS Experiance,";

                query = query + " (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,SD.WDays,SD.NFh,SD.OTDays,cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,";
                query = query + " cast(SD.BasicHRA as decimal(18,2)) as BasicHRA,cast(SD.ConvAllow as decimal(18,2)) as ConvAllow,cast(SD.EduAllow as decimal(18,2)) as EduAllow,cast(SD.MediAllow as decimal(18,2)) as MediAllow,";
                query = query + " cast(SD.BasicRAI as decimal(18,2)) as BasicRAI,cast(SD.WashingAllow as decimal(18,2)) as WashingAllow,SD.ThreesidedAmt,SD.DayIncentive,SD.allowances1,ED.BaseSalary as Base,";

                query = query + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) as Basic_Spl_Allowance,cast(SD.Basic_Uniform as decimal(18,2)) as Basic_Uniform,";
                query = query + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) as Basic_Vehicle_Maintenance,cast(SD.Basic_Communication as decimal(18,2)) as Basic_Communication,";
                query = query + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) as Basic_Journ_Perid_Paper,cast(SD.Basic_Incentives as decimal(18,2)) as Basic_Incentives";

                query = query + " from Employee_Mst ED inner join [" + SessionPayroll + "]..SalaryDetails_Bonus SD on ED.ExistingCode COLLATE DATABASE_DEFAULT=SD.ExisistingCode COLLATE DATABASE_DEFAULT And ED.LocCode COLLATE DATABASE_DEFAULT=SD.Lcode COLLATE DATABASE_DEFAULT";
                ///query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo inner join SalaryMaster SM on SM.EmpNo=ED.EmpNo";
                query = query + " inner join Department_Mst MD on MD.DeptCode=ED.DeptCode";
                query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.Wages='" + EmployeeType + "'";
                query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                //query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                query = query + " And SD.FromDate >=convert(datetime,'" + fromdate + "', 105) And SD.FromDate <=convert(datetime,'" + ToDate + "', 105)";

                // Other State

                //if (Other_state == "Yes")
                //{
                //    query = query + " and ED.OtherState='Yes'";
                //}
            }
            else
            {
                query = "Select ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,convert(varchar,ED.DOJ,105) as Dateofjoining,MD.DeptName as DepartmentNm,ED.Designation,";

                //Experiance Get
                query = query + " (case when ED.IsActive = 'No' then (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, convert(datetime,ED.DOR,105))/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, convert(datetime,ED.DOR,105)) % 12))";
                query = query + " else (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000') % 12)) end";
                query = query + " ) as Experiance,";

                //query = query + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000') % 12)) AS Experiance,";
                if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6" || EmployeeTypeCd == "7")
                {
                    query = query + " (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,AD.Days as WDays,AD.NFh,cast(AD.WH_Work_Days as decimal(18,2)) as OTDays,cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,";
                }
                else
                {
                    query = query + " (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,AD.Days as WDays,AD.NFh,AD.ThreeSided as OTDays,cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,";
                }
                query = query + " cast(SD.BasicHRA as decimal(18,2)) as BasicHRA,cast(SD.ConvAllow as decimal(18,2)) as ConvAllow,cast(SD.EduAllow as decimal(18,2)) as EduAllow,cast(SD.MediAllow as decimal(18,2)) as MediAllow,";
                query = query + " cast(SD.BasicRAI as decimal(18,2)) as BasicRAI,cast(SD.WashingAllow as decimal(18,2)) as WashingAllow,SD.ThreesidedAmt,SD.DayIncentive,SD.allowances1,ED.BaseSalary,";

                query = query + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) as Basic_Spl_Allowance,cast(SD.Basic_Uniform as decimal(18,2)) as Basic_Uniform,";
                query = query + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) as Basic_Vehicle_Maintenance,cast(SD.Basic_Communication as decimal(18,2)) as Basic_Communication,";
                query = query + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) as Basic_Journ_Perid_Paper,cast(SD.Basic_Incentives as decimal(18,2)) as Basic_Incentives";

                query = query + " from Employee_Mst ED inner join [" + SessionPayroll + "]..SalaryDetails SD on ED.EmpNo=SD.EmpNo And ED.LocCode COLLATE DATABASE_DEFAULT=SD.Lcode COLLATE DATABASE_DEFAULT";
                query = query + " inner join [" + SessionPayroll + "]..AttenanceDetails AD on AD.EmpNo=ED.EmpNo And SD.Lcode COLLATE DATABASE_DEFAULT=AD.Lcode COLLATE DATABASE_DEFAULT And SD.FromDate=AD.FromDate And ED.LocCode COLLATE DATABASE_DEFAULT=AD.Lcode COLLATE DATABASE_DEFAULT";
                //query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo inner join SalaryMaster SM on SM.EmpNo=ED.EmpNo";
                query = query + " inner join Department_Mst MD on MD.DeptCode=ED.DeptCode";
                query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.Wages='" + EmployeeType + "'";
                query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                query = query + " And SD.FromDate >=convert(datetime,'" + fromdate + "', 105) And SD.FromDate <=convert(datetime,'" + ToDate + "', 105)";
                query = query + " And AD.FromDate >=convert(datetime,'" + fromdate + "', 105) And AD.FromDate <=convert(datetime,'" + ToDate + "', 105)";

                // Other State

                //if (Other_state == "Yes")
                //{
                //    query = query + " and ED.OtherState='Yes'";
                //}

            }
            //Activate Employee Check
            if (Left_Employee != "1")
            {
                if (Left_Employee == "2")
                {
                    query = query + " and ED.IsActive='Yes'";
                }
                if (Left_Employee == "3")
                {
                    query = query + " and ED.IsActive='No'";
                }
            }

            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (ED.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (ED.Eligible_PF='2')";
                }
            }

            //Salary Through Bank Or Cash Check
            if (Salary_CashOrBank.ToString() != "0")
            {
                if (Salary_CashOrBank.ToString() == "1")
                {
                    //Cash Employee
                    query = query + " and (ED.Salary_Through='1')";
                }
                else
                {
                    //Bank Employee
                    query = query + " and (ED.Salary_Through='2')";
                }
            }

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            query = query + " Order by ED.ExistingCode,SD.Fromdate Asc";


            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {
                if (Basic_Report_Type == "Mixed")
                {
                    rd.Load(Server.MapPath("Payslip/Bonus_Individual_Month.rpt"));
                }
                else if (Basic_Report_Type == "New")
                {
                    rd.Load(Server.MapPath("Payslip_New_Component/Bonus_Individual_Month.rpt"));
                }
                else
                {
                    rd.Load(Server.MapPath("Payslip/Bonus_Individual_Month_OLD_Sal_Head.rpt"));
                }
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF Bonus Period From October " + (YR - 1) + " - September " + YR + "'";
                }
                if (EmployeeTypeCd == "7")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "MANAGER Bonus Period From October " + (YR - 1) + " - September " + YR + "'";
                }

                else if (EmployeeTypeCd == "3")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS Bonus Period From October " + (YR - 1) + " - September " + YR + "'";
                }
                else if (EmployeeTypeCd == "4")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS CASUAL / Bonus Period From October " + (YR - 1) + " - September " + YR + "'";
                }
                else
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "Bonus Period From October " + (YR - 1) + " - September " + YR + "'";
                }

                //rd.DataDefinition.FormulaFields["Bonus_Year"].Text = "'" + YR + "'";
                //rd.DataDefinition.FormulaFields["Bonus_From"].Text = "'" + (YR - 1) + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";
                rd.DataDefinition.FormulaFields["Employee_Type_Get"].Text = "'" + EmployeeType + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }



        if (Get_Report_Type == "Bonus_Individual") //Bonus_Individual
        {

            if (Basic_Report_Date != "")
            {
                DateTime Query_Date_Check_From = Convert.ToDateTime(fromdate);
                DateTime Query_Date_Check_To = Convert.ToDateTime(ToDate);
                DateTime DB_Basic_Report_Date = Convert.ToDateTime(Basic_Report_Date);
                if (Query_Date_Check_From <= DB_Basic_Report_Date && Query_Date_Check_To >= DB_Basic_Report_Date)
                {
                    Basic_Report_Type = "Mixed";
                }
                else if (Query_Date_Check_From >= DB_Basic_Report_Date)
                {
                    Basic_Report_Type = "New";
                }
                else
                {
                    Basic_Report_Type = "OLD";
                }
            }
            else
            {
                Basic_Report_Type = "Mixed";
            }

            Get_Division_Name = Request.QueryString["Division"].ToString();

            if ((SessionLcode == "UNIT I" || SessionLcode == "UNIT IV") && fromdate == "01-10-2014")
            {
                query = "Select ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,convert(varchar,ED.DOJ,105) as Dateofjoining,MD.DeptName as DepartmentNm,ED.Designation,";

                //Experiance Get
                query = query + " (case when ED.IsActive = 'No' then (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, convert(datetime,ED.DOR,105))/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, convert(datetime,ED.DOR,105)) % 12))";
                query = query + " else (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000') % 12)) end";
                query = query + " ) as Experiance,";

                //query = query + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000') % 12)) AS Experiance,";

                query = query + " (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,SD.WDays,SD.NFh,SD.OTDays,cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,";
                query = query + " cast(SD.BasicHRA as decimal(18,2)) as BasicHRA,cast(SD.ConvAllow as decimal(18,2)) as ConvAllow,cast(SD.EduAllow as decimal(18,2)) as EduAllow,cast(SD.MediAllow as decimal(18,2)) as MediAllow,";
                query = query + " cast(SD.BasicRAI as decimal(18,2)) as BasicRAI,cast(SD.WashingAllow as decimal(18,2)) as WashingAllow,SD.ThreesidedAmt,SD.DayIncentive,SD.allowances1,ED.BaseSalary as Base,";

                query = query + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) as Basic_Spl_Allowance,cast(SD.Basic_Uniform as decimal(18,2)) as Basic_Uniform,";
                query = query + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) as Basic_Vehicle_Maintenance,cast(SD.Basic_Communication as decimal(18,2)) as Basic_Communication,";
                query = query + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) as Basic_Journ_Perid_Paper,cast(SD.Basic_Incentives as decimal(18,2)) as Basic_Incentives";

                query = query + " from Employee_Mst ED inner join [" + SessionPayroll + "]..SalaryDetails_Bonus SD on ED.ExistingCode COLLATE DATABASE_DEFAULT=SD.ExisistingCode COLLATE DATABASE_DEFAULT And ED.LocCode COLLATE DATABASE_DEFAULT=SD.Lcode COLLATE DATABASE_DEFAULT";
                ///query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo inner join SalaryMaster SM on SM.EmpNo=ED.EmpNo";
                query = query + " inner join Department_Mst MD on MD.DeptCode=ED.DeptCode";
                query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.Wages='" + EmployeeType + "'";
                query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                //query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                query = query + " And SD.FromDate >=convert(datetime,'" + fromdate + "', 105) And SD.FromDate <=convert(datetime,'" + ToDate + "', 105)";

                // Other State

                //if (Other_state == "Yes")
                //{
                //    query = query + " and ED.OtherState='Yes'";
                //}
            }
            else
            {
                query = "Select ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,convert(varchar,ED.DOJ,105) as Dateofjoining,MD.DeptName as DepartmentNm,ED.Designation,";

                //Experiance Get
                query = query + " (case when ED.IsActive = 'No' then (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, convert(datetime,ED.DOR,105))/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, convert(datetime,ED.DOR,105)) % 12))";
                query = query + " else (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000') % 12)) end";
                query = query + " ) as Experiance,";

                //query = query + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000') % 12)) AS Experiance,";
                if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6" || EmployeeTypeCd == "7")
                {
                    query = query + " (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,AD.Days as WDays,AD.NFh,cast(AD.WH_Work_Days as decimal(18,2)) as OTDays,cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,";
                }
                else
                {
                    query = query + " (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,AD.Days as WDays,AD.NFh,AD.ThreeSided as OTDays,cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,";
                }
                query = query + " cast(SD.BasicHRA as decimal(18,2)) as BasicHRA,cast(SD.ConvAllow as decimal(18,2)) as ConvAllow,cast(SD.EduAllow as decimal(18,2)) as EduAllow,cast(SD.MediAllow as decimal(18,2)) as MediAllow,";
                query = query + " cast(SD.BasicRAI as decimal(18,2)) as BasicRAI,cast(SD.WashingAllow as decimal(18,2)) as WashingAllow,SD.ThreesidedAmt,SD.DayIncentive,SD.allowances1,ED.BaseSalary,";

                query = query + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) as Basic_Spl_Allowance,cast(SD.Basic_Uniform as decimal(18,2)) as Basic_Uniform,";
                query = query + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) as Basic_Vehicle_Maintenance,cast(SD.Basic_Communication as decimal(18,2)) as Basic_Communication,";
                query = query + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) as Basic_Journ_Perid_Paper,cast(SD.Basic_Incentives as decimal(18,2)) as Basic_Incentives";

                query = query + " from Employee_Mst ED inner join [" + SessionPayroll + "]..SalaryDetails SD on ED.EmpNo=SD.EmpNo And ED.LocCode COLLATE DATABASE_DEFAULT=SD.Lcode COLLATE DATABASE_DEFAULT";
                query = query + " inner join [" + SessionPayroll + "]..AttenanceDetails AD on AD.EmpNo=ED.EmpNo And SD.Lcode COLLATE DATABASE_DEFAULT=AD.Lcode COLLATE DATABASE_DEFAULT And SD.FromDate=AD.FromDate And ED.LocCode COLLATE DATABASE_DEFAULT=AD.Lcode COLLATE DATABASE_DEFAULT";
                //query = query + " inner join Officialprofile OP on ED.EmpNo=OP.EmpNo inner join SalaryMaster SM on SM.EmpNo=ED.EmpNo";
                query = query + " inner join Department_Mst MD on MD.DeptCode=ED.DeptCode";
                query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.Wages='" + EmployeeType + "'";
                query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                query = query + " And SD.FromDate >=convert(datetime,'" + fromdate + "', 105) And SD.FromDate <=convert(datetime,'" + ToDate + "', 105)";
                query = query + " And AD.FromDate >=convert(datetime,'" + fromdate + "', 105) And AD.FromDate <=convert(datetime,'" + ToDate + "', 105)";

                // Other State

                //if (Other_state == "Yes")
                //{
                //    query = query + " and ED.OtherState='Yes'";
                //}

            }
            //Activate Employee Check
            if (Left_Employee != "1")
            {
                if (Left_Employee == "2")
                {
                    query = query + " and ED.IsActive='Yes'";
                }
                if (Left_Employee == "3")
                {
                    query = query + " and ED.IsActive='No'";
                }
            }

            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (ED.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (ED.Eligible_PF='2')";
                }
            }

            //Salary Through Bank Or Cash Check
            if (Salary_CashOrBank.ToString() != "0")
            {
                if (Salary_CashOrBank.ToString() == "1")
                {
                    //Cash Employee
                    query = query + " and (ED.Salary_Through='1')";
                }
                else
                {
                    //Bank Employee
                    query = query + " and (ED.Salary_Through='2')";
                }
            }

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            query = query + " Order by ED.ExistingCode,SD.Fromdate Asc";


            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {
                if (Basic_Report_Type == "Mixed")
                {
                    rd.Load(Server.MapPath("Payslip/Bonus_Individual_Month.rpt"));
                }
                else if (Basic_Report_Type == "New")
                {
                    rd.Load(Server.MapPath("Payslip_New_Component/Bonus_Individual_Month.rpt"));
                }
                else
                {
                    rd.Load(Server.MapPath("Payslip/Bonus_Individual_Month_OLD_Sal_Head.rpt"));
                }
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF Bonus Period From October " + (YR - 1) + " - September " + YR + "'";
                }
                if (EmployeeTypeCd == "7")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "MANAGER Bonus Period From October " + (YR - 1) + " - September " + YR + "'";
                }

                else if (EmployeeTypeCd == "3")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS Bonus Period From October " + (YR - 1) + " - September " + YR + "'";
                }
                else if (EmployeeTypeCd == "4")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS CASUAL / Bonus Period From October " + (YR - 1) + " - September " + YR + "'";
                }
                else
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "Bonus Period From October " + (YR - 1) + " - September " + YR + "'";
                }

                //rd.DataDefinition.FormulaFields["Bonus_Year"].Text = "'" + YR + "'";
                //rd.DataDefinition.FormulaFields["Bonus_From"].Text = "'" + (YR - 1) + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";
                rd.DataDefinition.FormulaFields["Employee_Type_Get"].Text = "'" + EmployeeType + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }



        if (Get_Report_Type == "Bonus_Signlist") //Bonus_Signlist
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();

            query = "Select ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,MD.DeptName as DepartmentNm,ED.Designation,convert(varchar,ED.DOJ,105) as Dateofjoining,";
            query = query + " BD.signlist_bonus_Amt,BD.signlist_exgratia_Amt from Employee_Mst ED";
            query = query + " inner join [" + SessionPayroll + "]..Bonus_Details BD on BD.ExisistingCode COLLATE DATABASE_DEFAULT=ED.ExistingCode COLLATE DATABASE_DEFAULT and BD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
            //query = query + " inner join Officialprofile OP on OP.EmpNo=ED.EmpNo inner join MstDepartment MD on MD.DepartmentCd=ED.Department";
            query = query + " inner join Department_Mst MD on MD.DeptCode=ED.DeptCode";
            query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.Wages='" + EmployeeType + "'";
            query = query + " And BD.Ccode='" + SessionCcode + "' And BD.Lcode='" + SessionLcode + "' And BD.Bonus_Year='" + YR + "'";

            //Activate Employee Check
            if (Left_Employee != "1")
            {
                if (Left_Employee == "2")
                {
                    query = query + " and ED.IsActive='Yes'";
                }
                if (Left_Employee == "3")
                {
                    query = query + " and ED.IsActive='No'";
                }
            }

            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (ED.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (ED.Eligible_PF='2')";
                }
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}



            //Salary Through Bank Or Cash Check
            if (Salary_CashOrBank.ToString() != "0")
            {
                if (Salary_CashOrBank.ToString() == "1")
                {
                    //Cash Employee
                    query = query + " and (ED.Salary_Through='1')";
                }
                else
                {
                    //Bank Employee
                    query = query + " and (ED.Salary_Through='2')";
                }
            }

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            query = query + " Order by ED.ExistingCode Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {
                if (EmployeeTypeCd == "4")
                {
                    rd.Load(Server.MapPath("Payslip/Bonus_Signlist_Hostel.rpt"));
                }
                else
                {
                    rd.Load(Server.MapPath("Payslip/Bonus_Signlist_Staff_Regular.rpt"));
                }
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }
                if (EmployeeTypeCd == "7")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "MANAGER SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }
                else if (EmployeeTypeCd == "3")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }
                else if (EmployeeTypeCd == "4")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS CASUAL / SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }
                else
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }

                //rd.DataDefinition.FormulaFields["Bonus_Year"].Text = "'" + YR + "'";
                //rd.DataDefinition.FormulaFields["Bonus_From"].Text = "'" + (YR - 1) + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        if (Get_Report_Type == "Bonus_Blank_Signlist") //Bonus_Blank_Signlist
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();

            query = "Select ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,MD.DeptName as DepartmentNm,ED.Designation,convert(varchar,ED.DOJ,105) as Dateofjoining,";
            query = query + " BD.signlist_bonus_Amt,BD.signlist_exgratia_Amt from Employee_Mst ED";
            query = query + " inner join [" + SessionPayroll + "]..Bonus_Details BD on BD.ExisistingCode COLLATE DATABASE_DEFAULT=ED.ExistingCode COLLATE DATABASE_DEFAULT and BD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
            //query = query + " inner join Officialprofile OP on OP.EmpNo=ED.EmpNo inner join MstDepartment MD on MD.DepartmentCd=ED.Department";
            query = query + " inner join Department_Mst MD on MD.DeptCode=ED.DeptCode";
            query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.Wages='" + EmployeeType + "'";
            query = query + " And BD.Ccode='" + SessionCcode + "' And BD.Lcode='" + SessionLcode + "' And BD.Bonus_Year='" + YR + "'";

            //Activate Employee Check
            if (Left_Employee != "1")
            {
                if (Left_Employee == "2")
                {
                    query = query + " and ED.IsActive='Yes'";
                }
                if (Left_Employee == "3")
                {
                    query = query + " and ED.IsActive='No'";
                }
            }

            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (ED.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (ED.Eligible_PF='2')";
                }
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}



            //Salary Through Bank Or Cash Check
            if (Salary_CashOrBank.ToString() != "0")
            {
                if (Salary_CashOrBank.ToString() == "1")
                {
                    //Cash Employee
                    query = query + " and (ED.Salary_Through='1')";
                }
                else
                {
                    //Bank Employee
                    query = query + " and (ED.Salary_Through='2')";
                }
            }

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            query = query + " Order by ED.ExistingCode Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {

                rd.Load(Server.MapPath("Payslip/Bonus_Signlist_Blank.rpt"));

                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }
                if (EmployeeTypeCd == "7")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "MANAGER SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }
                else if (EmployeeTypeCd == "3")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }
                else if (EmployeeTypeCd == "4")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS CASUAL / SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }
                else
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }

                //rd.DataDefinition.FormulaFields["Bonus_Year"].Text = "'" + YR + "'";
                //rd.DataDefinition.FormulaFields["Bonus_From"].Text = "'" + (YR - 1) + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        if (Get_Report_Type == "Bonus_Cover") //Bonus_Cover
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();

            query = "Select ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,MD.DeptNAme as DepartmentNm,ED.Designation,convert(varchar,ED.DOJ,105) as Dateofjoining,";
            query = query + " BD.signlist_bonus_Amt,BD.signlist_exgratia_Amt,BD.Final_Bonus_Amt from Employee_Mst ED";
            query = query + " inner join [" + SessionPayroll + "]..Bonus_Details BD on BD.ExisistingCode COLLATE DATABASE_DEFAULT=ED.ExistingCode COLLATE DATABASE_DEFAULT and BD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
            //query = query + " inner join Officialprofile OP on OP.EmpNo=ED.EmpNo inner join MstDepartment MD on MD.DepartmentCd=ED.Department";
            query = query + " inner join Department_Mst MD on MD.DeptCode=ED.DeptCode";
            query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.Wages='" + EmployeeType + "'";
            query = query + " And BD.Ccode='" + SessionCcode + "' And BD.Lcode='" + SessionLcode + "' And BD.Bonus_Year='" + YR + "'";

            //Activate Employee Check
            if (Left_Employee != "1")
            {
                if (Left_Employee == "2")
                {
                    query = query + " and ED.IsActive='Yes'";
                }
                if (Left_Employee == "3")
                {
                    query = query + " and ED.IsActive='No'";
                }
            }

            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (ED.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (ED.Eligible_PF='2')";
                }
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}



            //Salary Through Bank Or Cash Check
            if (Salary_CashOrBank.ToString() != "0")
            {
                if (Salary_CashOrBank.ToString() == "1")
                {
                    //Cash Employee
                    query = query + " and (ED.Salary_Through='1')";
                }
                else
                {
                    //Bank Employee
                    query = query + " and (ED.Salary_Through='2')";
                }
            }

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            query = query + " Order by ED.ExistingCode Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {
                if (PFTypeGet.ToString() == "2")
                {
                    rd.Load(Server.MapPath("Payslip/Bonus_Report_Cover_Non_PF.rpt"));
                }
                else
                {
                    if (EmployeeTypeCd == "4")
                    {
                        rd.Load(Server.MapPath("Payslip/Bonus_Report_Cover_Hostel.rpt"));
                    }
                    else
                    {
                        rd.Load(Server.MapPath("Payslip/Bonus_Report_Cover.rpt"));
                    }
                }
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                if (EmployeeTypeCd == "1" || EmployeeTypeCd == "6")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }
                if (EmployeeTypeCd == "7")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "MANAGER SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }
                else if (EmployeeTypeCd == "3")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }
                else if (EmployeeTypeCd == "4")
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WORKERS CASUAL / SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }
                else
                {
                    rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "SIGN LIST FOR BONUS " + (YR - 1) + " - " + YR + "'";
                }

                //rd.DataDefinition.FormulaFields["Bonus_Year"].Text = "'" + YR + "'";
                //rd.DataDefinition.FormulaFields["Bonus_From"].Text = "'" + (YR - 1) + "'";
                rd.DataDefinition.FormulaFields["Report_Year"].Text = "'" + YR + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        //Bonus Report End

    }

    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Suriya_UnitA_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
