﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
public partial class New_PF_List : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
   
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionPayroll;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    string SSQL = "";
    DataTable dt = new DataTable();

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        Load_DB();

        if (!IsPostBack)
        {
            Months_load();

            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                //txtLCYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Suriya_UnitA_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }
    protected void gvSalary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnPFList_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            int YR = 0;
            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            
            if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                SSQL = "";
                SSQL = SSQL + " Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName, ";
                SSQL = SSQL + " cast(EmpDet.UAN as varchar(20)) as UAN,sum(cast(SalDet.BasicAndDANew as decimal(18,2))) as BasicandDA, ";
                SSQL = SSQL + "CASE WHEN EmpDet.Wages='DRIVERS' THEN (sum(cast(SalDet.GrossEarnings as decimal(18,2)))-sum(cast(SalDet.allowances3 as decimal(18,2))))";
                SSQL = SSQL + " ELSE sum(cast(SalDet.GrossEarnings as decimal(18,2))) END AS GrossEarnings,";
                SSQL = SSQL + " sum(cast(SalDet.ProvidentFund as decimal(18,2))) as PF,sum(cast(SalDet.EmployeerPFone as decimal(18,2))) as EmployeerPFone,";
                SSQL = SSQL + " sum(cast(SalDet.EmployeerPFTwo as decimal(18,2))) as EmployeerPFTwo,sum(cast(SalDet.ESI as decimal(18,2))) as ESI ";
                SSQL = SSQL + " from Employee_Mst EmpDet inner Join [Suriya_UnitA_Epay]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo  ";
                SSQL = SSQL + " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
                SSQL = SSQL + " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "'  and  SalDet.Lcode='" + SessionLcode + "' And  ";
                SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Eligible_PF='1' ";
                SSQL = SSQL + " group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,EmpDet.UAN,EmpDet.Wages  ";
                SSQL = SSQL + " having sum(cast(SalDet.LOPDays as decimal(18,2))) > 0 Order by cast(EmpDet.ExistingCode as int) Asc ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    GvPF.DataSource = dt;
                    GvPF.DataBind();
                    string attachment = "attachment;filename=PFList.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    grid.HeaderStyle.Font.Bold = true;
                    System.IO.StringWriter stw = new System.IO.StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    GvPF.RenderControl(htextw);

                    Response.Write(stw.ToString());

                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
                }


            }
        }
        catch (Exception Ex)
        {
            
            throw;
        }



    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
    }
    protected void btnESI_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            int YR = 0;
            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }


            if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                SSQL = "";
                SSQL = SSQL + " Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName, ";
                SSQL = SSQL + " cast(EmpDet.UAN as varchar(20)) as UAN,sum(cast(SalDet.BasicAndDANew as decimal(18,2))) as BasicandDA, ";
                SSQL = SSQL + " sum(cast(SalDet.GrossEarnings as decimal(18,2))) as GrossEarnings,";
                SSQL = SSQL + " sum(cast(SalDet.ProvidentFund as decimal(18,2))) as PF,sum(cast(SalDet.EmployeerPFone as decimal(18,2))) as EmployeerPFone,";
                SSQL = SSQL + " sum(cast(SalDet.EmployeerPFTwo as decimal(18,2))) as EmployeerPFTwo,sum(cast(SalDet.ESI as decimal(18,2))) as ESI ";
                SSQL = SSQL + " from Employee_Mst EmpDet inner Join [Suriya_UnitA_Epay]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo  ";
                SSQL = SSQL + " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode ";
                SSQL = SSQL + " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "'  and  SalDet.Lcode='" + SessionLcode + "' And  ";
                SSQL = SSQL + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Eligible_ESI='1' ";
                SSQL = SSQL + " group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,EmpDet.UAN  ";
                SSQL = SSQL + " having sum(cast(SalDet.LOPDays as decimal(18,2))) > 0 Order by cast(EmpDet.ExistingCode as int) Asc ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    GvESI.DataSource = dt;
                    GvESI.DataBind();
                    string attachment = "attachment;filename=ESIList.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    grid.HeaderStyle.Font.Bold = true;
                    System.IO.StringWriter stw = new System.IO.StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    GvESI.RenderControl(htextw);

                    Response.Write(stw.ToString());

                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
                }


            }
        }
        catch (Exception Ex)
        {

            throw;
        }
    }
}
