﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;

using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Drawing;

public partial class MDDashBoard : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionRights = Session["Rights"].ToString();
        }
        Load_DB();
        if (!IsPostBack)
        {
            Load_Location();
            txtLocation.SelectedValue = SessionLcode;
            Current_Year_Employee_Join();
            Current_Year_Employee_RecruiterType();
            Employee_Trainee_Level_Det();
            Salary_Strengtn_Unitwise();

        }

    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Suriya_UnitA_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    public void Load_Location()
    {
        string SSQL = "";
        DataTable dtempty = new DataTable();
        txtLocation.DataSource = dtempty;
        txtLocation.DataBind();
        DataTable dt = new DataTable();
        SSQL = "Select LocCode as LCode from Location_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        txtLocation.DataSource = dt;
        txtLocation.DataTextField = "LCode";
        txtLocation.DataValueField = "LCode";
        txtLocation.DataBind();

        txtlevelLoc.DataSource = dt;
        txtlevelLoc.DataTextField = "LCode";
        txtlevelLoc.DataValueField = "LCode";
        txtlevelLoc.DataBind();
    }

    private void Current_Year_Employee_Join()
    {
        DataTable DT1 = new DataTable();
        DataTable dt = new DataTable();
        string query = "";

        query = "Select isnull([Yes],0) as YES,isnull([No],0) as No from ";
        query = query + "(select (IsActive) as Shift_Join, count(EmpNo) as present ";
        query = query + "from Employee_Mst where CompCode='" + SessionCcode + "'";
        query = query + " And DATEPART(yyyy, (CONVERT(DATETIME,DOJ, 103))) = DATEPART(yyyy, GETDATE()) ";
        query = query + "group by  IsActive ) as p  pivot (max(present)";
        query = query + " for Shift_Join in ( [Yes],[No] )) as pvtt";
        DT1 = objdata.RptEmployeeMultipleDetails(query);

        if (DT1.Rows.Count != 0)
        {
            txtNewJoin.Text = DT1.Rows[0]["YES"].ToString();
            txtLeft.Text = DT1.Rows[0]["No"].ToString();
        }


        query = "Select LocCode,isnull([Yes],0) as YES,isnull([No],0) as No";
        query = query + " from (select LocCode ,(IsActive) as Shift_Join, count(EmpNo) as present ";
        query = query + " from Employee_Mst where CompCode='" + SessionCcode + "'";
        query = query + " And DATEPART(yyyy, (CONVERT(DATETIME,DOJ, 103))) = DATEPART(yyyy, GETDATE()) ";
        query = query + " group by LocCode, IsActive ) as p ";
        query = query + " pivot (max(present) for Shift_Join in ( [Yes],[No] )) as pvtt";
       
        dt = objdata.RptEmployeeMultipleDetails(query);
        Current_Year_Emp_Join.DataSource = dt;
        Current_Year_Emp_Join.ChartAreas.Clear();
        Current_Year_Emp_Join.Series.Clear();
        Current_Year_Emp_Join.Legends.Clear();
        Current_Year_Emp_Join.ChartAreas.Add("ChartArea2");
        Current_Year_Emp_Join.ChartAreas[0].AxisX.Title = "UNIT";
        Current_Year_Emp_Join.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Verdasa",11,System.Drawing.FontStyle.Bold);

        //Current_Year_Emp_Join.ChartAreas[0].AxisY.Title = "Join/Left";
        Current_Year_Emp_Join.ChartAreas[0].AxisY.TitleFont = new System.Drawing.Font("Verdasa", 11, System.Drawing.FontStyle.Bold);

        Current_Year_Emp_Join.ChartAreas[0].BorderDashStyle = ChartDashStyle.Solid;
        Current_Year_Emp_Join.ChartAreas[0].BorderWidth=2;

        
        Current_Year_Emp_Join.Series.Add("Join");
        Current_Year_Emp_Join.Series[0].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
        Current_Year_Emp_Join.Series[0].XValueMember = "LocCode";
        Current_Year_Emp_Join.Series[0].YValueMembers = "YES";

        Current_Year_Emp_Join.Series[0].IsVisibleInLegend = true;
        Current_Year_Emp_Join.Series[0].IsValueShownAsLabel = true;
        Current_Year_Emp_Join.Series[0].ToolTip = "Data Point Y value:#VALY(G)";

        Current_Year_Emp_Join.Series[0].BorderWidth = 3;
        Current_Year_Emp_Join.Series[0].Color = Color.Green;


        Current_Year_Emp_Join.Series.Add("Left");
        Current_Year_Emp_Join.Series[1].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
        Current_Year_Emp_Join.Series[1].XValueMember = "LocCode";
        Current_Year_Emp_Join.Series[1].YValueMembers = "No";


        Current_Year_Emp_Join.Series[1].IsVisibleInLegend = true;
        Current_Year_Emp_Join.Series[1].IsValueShownAsLabel = true;
        Current_Year_Emp_Join.Series[1].ToolTip = "Data Point Y value:#VALY(G)";

        Current_Year_Emp_Join.Series[1].BorderWidth = 3;
        Current_Year_Emp_Join.Series[1].Color = Color.Red;

        Current_Year_Emp_Join.Legends.Add("Legend1");
        Current_Year_Emp_Join.Legends[0].Enabled = true;
        Current_Year_Emp_Join.Legends[0].Docking = Docking.Bottom;
        Current_Year_Emp_Join.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        Current_Year_Emp_Join.Series[0].LegendText = "JOIN";

        Current_Year_Emp_Join.Legends[0].Enabled = true;
        Current_Year_Emp_Join.Legends[0].Docking = Docking.Bottom;
        Current_Year_Emp_Join.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        Current_Year_Emp_Join.Series[1].LegendText = "LEFT";

        //Current_Year_Emp_Join.Series["Series1"].XValueMember = "LocCode";
        //Current_Year_Emp_Join.Series["Series1"].YValueMembers = "YES";
        //Current_Year_Emp_Join.Series["Series2"].XValueMember = "LocCode";
        //Current_Year_Emp_Join.Series["Series2"].YValueMembers = "YES";

        //this.Current_Year_Emp_Join.Series[0]["PieLabelStyle"] = "Outside";
        //this.Current_Year_Emp_Join.ChartAreas[0].Area3DStyle.Enable3D = true;
        //this.Current_Year_Emp_Join.ChartAreas[0].Area3DStyle.Inclination = 1;
        //this.Current_Year_Emp_Join.Series[0].BorderWidth = 1;
        //this.Current_Year_Emp_Join.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        //this.Current_Year_Emp_Join.Legends.Add("Legend1");
        //this.Current_Year_Emp_Join.Legends[0].Enabled = true;
        //this.Current_Year_Emp_Join.Legends[0].Docking = Docking.Bottom;
        //this.Current_Year_Emp_Join.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        //this.Current_Year_Emp_Join.Series[0].LegendText = "#PERCENT";

        //this.Current_Year_Emp_Join.Series[1]["PieLabelStyle"] = "Outside";
        //this.Current_Year_Emp_Join.ChartAreas[0].Area3DStyle.Enable3D = true;
        //this.Current_Year_Emp_Join.ChartAreas[0].Area3DStyle.Inclination = 1;
        //this.Current_Year_Emp_Join.Series[1].BorderWidth = 1;
        //this.Current_Year_Emp_Join.Series[1].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        //this.Current_Year_Emp_Join.Legends.Add("Legend2");
        //this.Current_Year_Emp_Join.Legends[1].Enabled = true;
        //this.Current_Year_Emp_Join.Legends[1].Docking = Docking.Bottom;
        //this.Current_Year_Emp_Join.Legends[1].Alignment = System.Drawing.StringAlignment.Center;
        //this.Current_Year_Emp_Join.Series[1].LegendText = "#PERCENT";
        ////this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        //Current_Year_Emp_Join.DataBind();
    }

    private void Current_Year_Employee_RecruiterType()
    {
        DataTable dt = new DataTable();
        string query = "";

        query = "Select RecuriterName ,count(EmpNo) as present ";
        query = query + " from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + txtLocation.SelectedValue + "'";
        query = query + " And DATEPART(yyyy, (CONVERT(DATETIME,DOJ, 103))) = DATEPART(yyyy, GETDATE()) ";
        query = query + " And IsActive='Yes'";
        query = query + " And RecuritmentThro='Recruitment Officer'";
        query = query + " group by RecuriterName";

        dt = objdata.RptEmployeeMultipleDetails(query);
        Current_Year_Emp_Left.DataSource = dt;
        Current_Year_Emp_Left.Series["Series1"].XValueMember = "RecuriterName";
        Current_Year_Emp_Left.Series["Series1"].YValueMembers = "present";

        this.Current_Year_Emp_Left.Series[0]["PieLabelStyle"] = "Outside";
        this.Current_Year_Emp_Left.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Current_Year_Emp_Left.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Current_Year_Emp_Left.Series[0].BorderWidth = 1;
        this.Current_Year_Emp_Left.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Current_Year_Emp_Left.Legends.Clear();
        this.Current_Year_Emp_Left.Legends.Add("Legend1");
        this.Current_Year_Emp_Left.Legends[0].Enabled = true;
        this.Current_Year_Emp_Left.Legends[0].Docking = Docking.Bottom;
        this.Current_Year_Emp_Left.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Current_Year_Emp_Left.Series[0].LegendText = "#PERCENT";
        //this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Current_Year_Emp_Left.DataBind();
    }

    private void Employee_Trainee_Level_Det()
    {
        DataTable dt = new DataTable();
        string query = "";

        query = "Select EmpLevel ,count(EmpNo) as present ";
        query = query + " from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + txtlevelLoc.SelectedValue + "'";
        query = query + " And IsActive='Yes' ";
        query = query + " group by EmpLevel";

        dt = objdata.RptEmployeeMultipleDetails(query);
        Employee_Trainee_Level.DataSource = dt;
        Employee_Trainee_Level.Series["Series1"].XValueMember = "EmpLevel";
        Employee_Trainee_Level.Series["Series1"].YValueMembers = "present";

        this.Employee_Trainee_Level.Series[0]["PieLabelStyle"] = "Outside";
        this.Employee_Trainee_Level.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Employee_Trainee_Level.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Employee_Trainee_Level.Series[0].BorderWidth = 1;
        this.Employee_Trainee_Level.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Employee_Trainee_Level.Legends.Clear();
        this.Employee_Trainee_Level.Legends.Add("Legend1");
        this.Employee_Trainee_Level.Legends[0].Enabled = true;
        this.Employee_Trainee_Level.Legends[0].Docking = Docking.Bottom;
        this.Employee_Trainee_Level.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Employee_Trainee_Level.Series[0].LegendText = "#PERCENT";
        //this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Employee_Trainee_Level.DataBind();
    }

    private void Salary_Strengtn_Unitwise()
    {
        DataTable DT1 = new DataTable();
        DataTable dt = new DataTable();
        string query = "";
        string Month = "";
        string Year = "";

        query = "Select DATEPART(YEAR,(GETDATE()-DATEPART(DD,(GETDATE())))) as Year,DateName(MONTH,(GETDATE()-DATEPART(DD,(GETDATE())))) as Month";
        DT1 = objdata.RptEmployeeMultipleDetails(query);
        
        if (DT1.Rows.Count != 0)
        {
            Month = DT1.Rows[0]["Month"].ToString();
            Year = DT1.Rows[0]["Year"].ToString();
        }

        query = "Select Lcode, sum(RoundOffNetPay) as present ";
        query = query + " from [" + SessionPayroll + "]..SalaryDetails where Ccode='" + SessionCcode + "'";
        query = query + " And Year = '" + Year + "' And Month='" + Month + "'";
        query = query + " group by Lcode";
   

        dt = objdata.RptEmployeeMultipleDetails(query);
        Salary_Strength_Unitwise.DataSource = dt;
        Salary_Strength_Unitwise.Series["Series1"].XValueMember = "Lcode";
        Salary_Strength_Unitwise.Series["Series1"].YValueMembers = "present";

        this.Salary_Strength_Unitwise.Series[0]["PieLabelStyle"] = "Outside";
        this.Salary_Strength_Unitwise.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Salary_Strength_Unitwise.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Salary_Strength_Unitwise.Series[0].BorderWidth = 1;
        this.Salary_Strength_Unitwise.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Salary_Strength_Unitwise.Legends.Clear();
        this.Salary_Strength_Unitwise.Legends.Add("Legend1");
        this.Salary_Strength_Unitwise.Legends[0].Enabled = true;
        this.Salary_Strength_Unitwise.Legends[0].Docking = Docking.Bottom;
        this.Salary_Strength_Unitwise.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Salary_Strength_Unitwise.Series[0].LegendText = "#PERCENT";
        //this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Salary_Strength_Unitwise.DataBind();
    }

    protected void txtLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        Current_Year_Employee_Join();
        Current_Year_Employee_RecruiterType();
        Employee_Trainee_Level_Det();
        Salary_Strengtn_Unitwise();
    }

    protected void txtlevelLoc_SelectedIndexChanged(object sender, EventArgs e)
    {
        Current_Year_Employee_Join();
        Current_Year_Employee_RecruiterType();
        Employee_Trainee_Level_Det();
        Salary_Strengtn_Unitwise();
    }
}
