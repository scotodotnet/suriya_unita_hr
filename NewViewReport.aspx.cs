﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;
public partial class NewViewReport : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string AgentName;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin; string SessionUserType;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate;
    string ToDate;
    string salaryType;
    string Emp_ESI_Code;
    string SessionPayroll;
    string EmployeeType; string EmployeeTypeCd;
    string PayslipType;
    string Report_Types;

    string PFTypeGet;

    string Left_Employee = "0";
    string Left_Date = "";
    string Get_Report_Type = "0";
    string Salary_CashOrBank;
    string Get_Division_Name = "";

    string Other_state = "";
    string Non_Other_state = "";
    string Basic_Report_Date = "";
    string Basic_Report_Type = "OLD";
    string ExemptedStaff = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        string date_1 = "";
        date_1 = ("01".ToString() + "-" + "12".ToString() + "-" + "2018".ToString()).ToString();
        //string dtserver = objdata.ServerDate();

        //CrystalReportViewer1.AllowedExportFormats = (int)(ViewerExportFormats.PdfFormat);

        //ReportDocument rd = new ReportDocument();


        SqlConnection con = new SqlConnection(constr);
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }


        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        str_cate = Request.QueryString["Cate"].ToString();
        //str_dept = Request.QueryString["Depat"].ToString();
        str_month = Request.QueryString["Months"].ToString();
        str_yr = Request.QueryString["yr"].ToString();
        fromdate = Request.QueryString["fromdate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();
        Report_Types = Request.QueryString["ReportFormat"].ToString();
        AgentName = Request.QueryString["AgentName"].ToString();


        salaryType = Request.QueryString["Salary"].ToString();
        EmployeeTypeCd = Request.QueryString["EmpTypeCd"].ToString();
        EmployeeType = Request.QueryString["EmpType"].ToString();

        Load_DB();

        if (EmployeeTypeCd == "2") { EmployeeType = "STAFF"; }
        if (EmployeeTypeCd == "3") { EmployeeType = "FITTER & ELECTRICIANS"; }
        if (EmployeeTypeCd == "4") { EmployeeType = "SECURITY"; }
        if (EmployeeTypeCd == "5") { EmployeeType = "DRIVERS"; }
        if (EmployeeTypeCd == "6") { EmployeeType = "LOCAL"; }
        if (EmployeeTypeCd == "7") { EmployeeType = "TAMIL BOYS"; }
        if (EmployeeTypeCd == "8") { EmployeeType = "TAMIL GIRLS"; }
        if (EmployeeTypeCd == "9") { EmployeeType = "CANTEEN"; }
        if (EmployeeTypeCd == "10") { EmployeeType = "HINDI BOYS"; }
        if (EmployeeTypeCd == "11") { EmployeeType = "HINDI GIRLS"; }
        if (EmployeeTypeCd == "12") { EmployeeType = "15DAYS"; }
        if (EmployeeTypeCd == "13") { EmployeeType = "WEEKLY"; }

        if (SessionUserType == "2")
        {
            PayslipType = "1";
        }
        else
        {
            PayslipType = Request.QueryString["PayslipType"].ToString();
        }
        int YR = 0;
        string report_head = "";
        if (str_month == "January")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "February")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "March")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(str_yr);
        }

        if (salaryType == "2")
        {
            report_head = "PAYSLIP FOR THE MONTH OF " + str_month.ToUpper() + " " + YR.ToString();
        }
        else
        {
            report_head = "PAYSLIP FOR THE MONTH OF " + fromdate.ToString() + " - " + ToDate.ToString();
        }
        
        salaryType = Request.QueryString["Salary"].ToString();

        Get_Report_Type = Request.QueryString["Report_Type"].ToString();
        if (Get_Report_Type == "Payslip")
        {

            //Write Query Start

            DataTable dt_Query = new DataTable();
            //Check PF Category
            if (salaryType.ToString() != "0")
            {
                
            }
            else
            {
                if (Report_Types == "2")
                {
                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.Salary_Through,MstDpt.DeptName as DepartmentNm,(SalDet.New_Mess_Amt) as MediAllow," +
                                 " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive," +
                                 " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                                 " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                                 " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                                 " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                                 " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                 " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                 " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                 " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and " +
                                 " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                                 " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And convert(datetime,AttnDet.FromDate,103)>=convert(datetime,'" + fromdate + "',103) and convert(datetime,AttnDet.ToDate,103)>=convert(datetime,'" + ToDate + "',103) And " +
                                 " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.AgentName='" + AgentName + "'" +
                                 " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes') and convert(datetime,SalDet.FromDate,103)>=convert(datetime,'" + fromdate + "',103) and convert(datetime,SalDet.ToDate,103)>=convert(datetime,'" + ToDate + "',103) ";

                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.Salary_Through,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive,SalDet.New_Mess_Amt," +
                   " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                   " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                   " Order by cast(EmpDet.ExistingCode as int) Asc";
                }
                else
                {
                    if ((EmployeeTypeCd == "10") || (EmployeeTypeCd == "11") || (EmployeeTypeCd == "9") || (EmployeeTypeCd == "7") || (EmployeeTypeCd == "8"))
                    {
                        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.Salary_Through,MstDpt.DeptName as DepartmentNm," +
                               " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive,(SalDet.New_Mess_Amt) as MediAllow," +
                               " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                               " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                               " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                               " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                               " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                               " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                               " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                               " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                               " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                               " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                               " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'" +
                               " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')";

                        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.Salary_Through,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive,SalDet.New_Mess_Amt," +
                       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                       " Order by cast(EmpDet.ExistingCode as int) Asc";
                    }
                    else if ((EmployeeTypeCd == "12") || (EmployeeTypeCd == "13") || (EmployeeTypeCd == "6"))
                    {
                        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.Salary_Through,MstDpt.DeptName as DepartmentNm," +
                                  " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive,(SalDet.New_Mess_Amt) as MediAllow1,SalDet.Deduction3 as MediAllow," +
                                  " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                                  " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                                  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                                  " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                  " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                                  " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And convert(datetime,AttnDet.FromDate,103)>=convert(datetime,'" + fromdate + "',103) and convert(datetime,AttnDet.ToDate,103)<=convert(datetime,'" + ToDate + "',103) And " +
                                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'" +
                                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes') and convert(datetime,SalDet.FromDate,103)>=convert(datetime,'" + fromdate + "',103) and convert(datetime,SalDet.ToDate,103)<=convert(datetime,'" + ToDate + "',103) ";

                        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.Salary_Through,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive,SalDet.New_Mess_Amt," +
                       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                       " Order by cast(EmpDet.ExistingCode as int) Asc";
                    }
                    else if ((EmployeeTypeCd == "2") || (EmployeeTypeCd == "3") || (EmployeeTypeCd == "4"))
                    {
                        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.Salary_Through,MstDpt.DeptName as DepartmentNm,(EmpDet.BaseSalary + EmpDet.VPF) as Basic_SM,SalDet.Dayincentive,SalDet.Deduction3 as MediAllow," +
                                  " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA) as NonPFWages," +
                                  " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                                  " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                                  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advance, " +
                                  " (SalDet.New_Cash_Amt) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt " +
                                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                  " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                                  " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'" +
                                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')";

                        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.Salary_Through,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive,SalDet.New_Mess_Amt," +
                       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt,EmpDet.BaseSalary,EmpDet.VPF," +
                       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                       " Order by cast(EmpDet.ExistingCode as int) Asc";
                    }
                    else if (EmployeeTypeCd == "5")
                    {
                        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.Salary_Through,MstDpt.DeptName as DepartmentNm,(EmpDet.BaseSalary + EmpDet.VPF) as Basic_SM,SalDet.Dayincentive,SalDet.Deduction3 as MediAllow," +
                                  " SalDet.WorkedDays,EmpDet.DeptName as Department,(SalDet.OTHoursNew + SalDet.Manual_OT) as OTHoursNew,(SalDet.BasicandDA) as NonPFWages," +
                                  " SalDet.allowances3 as OTHoursAmtNew,SalDet.OTHoursAmtNew as OTAMT,(SalDet.BasicandDA + SalDet.allowances3) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                                  " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                                  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advance, " +
                                  " (SalDet.New_Cash_Amt) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt " +
                                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                  " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and EmpDet.CatName='" + str_cate + "' and " +
                                  " AttnDet.Months='" + str_month + "' AND AttnDet.FinancialYear='" + str_yr + "' and " +
                                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + EmployeeType + "'" +
                                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')";

                        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.Salary_Through,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive,SalDet.New_Mess_Amt," +
                       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.allowances3,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt,EmpDet.BaseSalary,EmpDet.VPF," +
                       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Manual_OT,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                       " Order by cast(EmpDet.ExistingCode as int) Asc";
                    }
                }

                dt_Query = objdata.RptEmployeeMultipleDetails(query);
            }

            
         
            //Query End

            


            //SqlCommand cmd = new SqlCommand(query, con);
            //cmd.CommandTimeout = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SqlCommandTimeOut"]);

            //SqlDataAdapter sda = new SqlDataAdapter(cmd);
            //DataSet ds1 = new DataSet();
            //con.Open();

            //sda.Fill(ds1);
            
            //con.Close();



            if (dt_Query.Rows.Count > 0)
            {

                DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }
                SqlCommand cmdComp = new SqlCommand(query, con);
                SqlDataAdapter sdaComp = new SqlDataAdapter(cmdComp);

                con.Open();
                DataTable dt_1 = new DataTable();
                sdaComp.Fill(dt_1);
                con.Close();

                if (EmployeeTypeCd == "5")
                {
                    rd.Load(Server.MapPath("Payslip_New_Component/New_Payslip_Driver.rpt"));
                }
                else
                {
                    rd.Load(Server.MapPath("Payslip_New_Component/New_Payslip.rpt"));
                }
                rd.SetDataSource(dt_Query);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "SALARY FOR THE FROM DATE " + fromdate + " - " + "TO DATE" + " " + ToDate + "'";

                //Check List Heading Update
                //if (EmployeeTypeCd == "7") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "MANAGER SALARY CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'"; } }
                //if (EmployeeTypeCd == "1") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF SALARY CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'"; } }
                //if (EmployeeTypeCd == "2") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "SUB-STAFF WORKER WAGES CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'"; } }
                //if (EmployeeTypeCd == "3") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "REGULAR WORKER WAGES CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'"; } }
                //if (EmployeeTypeCd == "4") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "HOSTEL WORKER WAGES CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'"; } }
                //if (EmployeeTypeCd == "5") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WEEKLY CIVIL WORKERS WAGES CHECK REPORT FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "   From : " + fromdate + " To : " + ToDate + "'"; } }
                //if (EmployeeTypeCd == "6") { if (PayslipType == "4") { rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "WATCH & WARD SALARY CHECKLIST FOR THE MONTH OF " + str_month.ToUpper() + " - " + YR.ToString() + "'"; } }

                

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.AllowedExportFormats = formats;

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();


            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);

            }
        }

        //Leave Credit Consolidated REPORT
        if (Get_Report_Type == "LC_ALL_REPORT")
        {
            Get_Division_Name = Request.QueryString["Division"].ToString();
            string Year_Str = "";
            //Month Field Get Start
            string Month_Field = "";
            string Last_Month_Int = "0";

            //str_month = "December";
            Year_Str = Request.QueryString["yr"].ToString();

            if (str_month == "January") { Month_Field = "[Jan]"; Last_Month_Int = "1"; }
            else if (str_month == "February") { Month_Field = "[Jan],[Feb]"; Last_Month_Int = "2"; }
            else if (str_month == "March") { Month_Field = "[Jan],[Feb],[Mar]"; Last_Month_Int = "3"; }
            else if (str_month == "April") { Month_Field = "[Jan],[Feb],[Mar],[Apr]"; Last_Month_Int = "4"; }
            else if (str_month == "May") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May]"; Last_Month_Int = "5"; }
            else if (str_month == "June") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun]"; Last_Month_Int = "6"; }
            else if (str_month == "July") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun],[Jul]"; Last_Month_Int = "7"; }
            else if (str_month == "August") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun],[Jul],[Aug]"; Last_Month_Int = "8"; }
            else if (str_month == "September") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun],[Jul],[Aug],[Sep]"; Last_Month_Int = "9"; }
            else if (str_month == "October") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun],[Jul],[Aug],[Sep],[Oct]"; Last_Month_Int = "10"; }
            else if (str_month == "November") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun],[Jul],[Aug],[Sep],[Oct],[Nov]"; Last_Month_Int = "11"; }
            else if (str_month == "December") { Month_Field = "[Jan],[Feb],[Mar],[Apr],[May],[Jun],[Jul],[Aug],[Sep],[Oct],[Nov],[Dec]"; Last_Month_Int = "12"; }

            //Isnull Check Field Name
            string IsNull_Month = "";
            if (str_month == "January") { IsNull_Month = "Isnull([Jan],0) as Jan,'0' as Feb,'0' as Mar,'0' as Apr,'0' as May,'0' as Jun,'0' as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "February") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,'0' as Mar,'0' as Apr,'0' as May,'0' as Jun,'0' as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "March") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,'0' as Apr,'0' as May,'0' as Jun,'0' as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "April") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,'0' as May,'0' as Jun,'0' as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "May") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,'0' as Jun,'0' as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "June") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,'0' as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "July") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,Isnull([Jul],0) as Jul,'0' as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "August") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,Isnull([Jul],0) as Jul,Isnull([Aug],0) as Aug,'0' as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "September") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,Isnull([Jul],0) as Jul,Isnull([Aug],0) as Aug,Isnull([Sep],0) as Sep,'0' as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "October") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,Isnull([Jul],0) as Jul,Isnull([Aug],0) as Aug,Isnull([Sep],0) as Sep,Isnull([Oct],0) as Oct,'0' as Nov,'0' as Dec"; }
            else if (str_month == "November") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,Isnull([Jul],0) as Jul,Isnull([Aug],0) as Aug,Isnull([Sep],0) as Sep,Isnull([Oct],0) as Oct,Isnull([Nov],0) as Nov,'0' as Dec"; }
            else if (str_month == "December") { IsNull_Month = "Isnull([Jan],0) as Jan,Isnull([Feb],0) as Feb,Isnull([Mar],0) as Mar,Isnull([Apr],0) as Apr,Isnull([May],0) as May,Isnull([Jun],0) as Jun,Isnull([Jul],0) as Jul,Isnull([Aug],0) as Aug,Isnull([Sep],0) as Sep,Isnull([Oct],0) as Oct,Isnull([Nov],0) as Nov,Isnull([Dec],0) as Dec"; }
            //Month Field Get End

            query = "select EmpNo,ExisistingCode,EmpName,Designation,DOJ,TOT_LC,TOT_LC_Minus,WeekOff," + IsNull_Month;
            query = query + " from (select sum(SD.WorkedDays+SD.WH_Work_Days+SD.NFh) Days,left(DATENAME(MM, SD.FromDate),3) month,";
            query = query + " ED.EmpNo,ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,ED.Designation,ED.WeekOff,convert(varchar,ED.DOJ,103) as DOJ,";
            query = query + " isnull((SELECT Sum(SD.Leave_Credit_Days) FROM [" + SessionPayroll + "]..SalaryDetails SD WHERE SD.EmpNo=ED.EmpNo And month(SD.FromDate) <='" + Last_Month_Int + "'";
            query = query + " And Year(SD.FromDate)='" + Year_Str + "' And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "' And Left(Leave_Credit_Days,1) <> '-'),0) AS TOT_LC,";
            query = query + " isnull((SELECT Sum(cast (right(SD.Leave_Credit_Days,len(SD.Leave_Credit_Days)-1) as decimal(18,1))) FROM [" + SessionPayroll + "]..SalaryDetails SD WHERE SD.EmpNo=ED.EmpNo";
            query = query + " And month(SD.FromDate) <='" + Last_Month_Int + "' And Year(SD.FromDate)='" + Year_Str + "' And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "' And Left(Leave_Credit_Days,1) = '-'),0) AS TOT_LC_Minus";
            //query = query + " isnull((Select WeekOff from Employee_Mst EM where EM.MachineID=ED.MachineID And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'),'') as WeekOff";
            query = query + " from Employee_Mst ED inner Join [" + SessionPayroll + "]..SalaryDetails SD on SD.EmpNo=ED.EmpNo"; //inner Join Officialprofile OP on ED.EmpNo=OP.EmpNo";
            query = query + " inner join MstEmployeeType ET on ET.EmpType=ED.Wages";
            query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And Year(SD.FromDate)='" + Year_Str + "'";
            query = query + " And (ET.EmpTypeCd='1' OR ET.EmpTypeCd='6' OR ET.EmpTypeCd='7')";

            //Activate Employee Check
            if (Left_Employee == "1") { query = query + " and ED.IsActive='No' and Month(convert(datetime,ED.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,ED.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
            //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
            else
            {
                if (Left_Date != "") { query = query + " and (convert(datetime,ED.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or ED.IsActive='Yes')"; }
            }

            // Other State

            //if (Other_state == "Yes")
            //{
            //    query = query + " and ED.OtherState='Yes'";
            //}



            //PF Check
            if (PFTypeGet.ToString() != "0")
            {
                if (PFTypeGet.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (ED.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (ED.Eligible_PF='2')";
                }
            }

            //Add Division Condition
            if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
            {
                query = query + " and ED.Division='" + Get_Division_Name + "'";
            }

            query = query + " group by DATENAME(month, FromDate),ED.EmpNo,ED.ExistingCode,ED.FirstName,ED.Designation,ED.DOJ,ED.WeekOff";
            query = query + " ) as p";
            query = query + " PIVOT(MAX(Days) for month in (" + Month_Field + ")) as pvt";
            query = query + " Order by ExisistingCode Asc";

            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataSet ds1 = new DataSet();
            con.Open();
            sda.Fill(ds1);
            DataTable dt_1 = new DataTable();
            sda.Fill(dt_1);
            con.Close();

            if (ds1.Tables[0].Rows.Count != 0)
            {

                rd.Load(Server.MapPath("Payslip/LeaveCredit_Report_Consolidate.rpt"));
                rd.SetDataSource(ds1.Tables[0]);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
                rd.DataDefinition.FormulaFields["LC_Year"].Text = "'" + Year_Str + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";

                //rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "STAFF - LEAVE CREDIT DETAILS" + "'";
                rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }

        


    }

  
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Suriya_UnitA_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }

}
