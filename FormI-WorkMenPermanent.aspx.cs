﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class FormI_WorkMenPermanent : System.Web.UI.Page
{

    string fromdate;
    string todate;
    string Wages;
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    string SessionUserType;
    string SSQL = "";
    string mIpAddress_IN;
    string mIpAddress_OUT;
    DataTable dsEmployee = new DataTable();
    DateTime date1 = new DateTime();
    DateTime date2 = new DateTime();
    string RegNo;
    DataTable dtdlocation = new DataTable();
    string Address1;
    string Address2;
    string city;
    string Pincode;
    string PF_DOJ_Get_str;
    DateTime PF_DOJ_Get;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-FORM I WORK MEN PERMANENT REPORT ";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            fromdate = Request.QueryString["FromDate"].ToString();
            todate = Request.QueryString["ToDate"].ToString();
            string TempWages = Request.QueryString["WagesType"].ToString();
            Wages = TempWages.Replace("_", "&");
            
            //DataTable dtIPaddress = new DataTable();
            //dtIPaddress = objdata.BelowFourHours(SessionCcode.ToString(), SessionLcode.ToString());


            //if (dtIPaddress.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dtIPaddress.Rows.Count; i++)
            //    {
            //        if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
            //        {
            //            mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
            //        }
            //        else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
            //        {
            //            mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
            //        }
            //    }
            //}
            string Location_Full_Address_Join="";
            DataTable dt = new DataTable();
            string Compname = "";
            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count != 0)
            {
                SessionCompanyName = dt.Rows[0]["CompName"].ToString();
            }

            System.Web.UI.WebControls.DataGrid grid =
                                 new System.Web.UI.WebControls.DataGrid();

            AutoDTable.Columns.Add("1");
            AutoDTable.Columns.Add("2");
            AutoDTable.Columns.Add("3");
            AutoDTable.Columns.Add("4");
            AutoDTable.Columns.Add("5");
            AutoDTable.Columns.Add("6");
            AutoDTable.Columns.Add("7");
            AutoDTable.Columns.Add("8");
            AutoDTable.Columns.Add("9");
            AutoDTable.Columns.Add("10");

            FormIworkMenPermanentReport();

            SSQL = "Select * from Location_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            dtdlocation = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dtdlocation.Rows.Count > 0)
            {
                RegNo = dtdlocation.Rows[0]["Register_No"].ToString();
                Address1 = dtdlocation.Rows[0]["Add1"].ToString();
                Address2 = dtdlocation.Rows[0]["Add2"].ToString();
                city = dtdlocation.Rows[0]["City"].ToString();
                Pincode = dtdlocation.Rows[0]["Pincode"].ToString();

                if (Address1 != "")
                {
                    Location_Full_Address_Join = Location_Full_Address_Join + Address1;
                }
                if (Address2 != "")
                {
                    Location_Full_Address_Join = Location_Full_Address_Join + "," + Address2;
                }
                if (city != "")
                {
                    Location_Full_Address_Join = Location_Full_Address_Join + "," + city;
                }
                if (Pincode != "")
                {
                    Location_Full_Address_Join = Location_Full_Address_Join + "," + Pincode;
                }
            }
            else
            {
                RegNo = "";
            }

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=FormIWorkmenPermanent.xls";
          
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table border='1'>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='3'  border='0'>");
            Response.Write("</td>");

            Response.Write("<td colspan='4'>");
            Response.Write("<a style=\"font-weight:bold\">Form-I</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='3'  border: 0;>");
            Response.Write("</td>");

            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='3' BorderColor='White'>");
            Response.Write("</td>");

            Response.Write("<td colspan='4'>");
            Response.Write("<a style=\"font-weight:bold\">(See Sub - rule (1) Rule 6 )</a>");
            Response.Write("</td>");
            Response.Write("<td colspan='3' BorderColor='White'>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='3'>");
            Response.Write("</td>");
            Response.Write("<td colspan='4'>");
            Response.Write("<a style=\"font-weight:bold\">REGISTER OF WORKMEN</a>");
            Response.Write("</td>");
            Response.Write("<td colspan='3'>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td rowspan='2' colspan='3' align='center'>");
            Response.Write("<a style=\"font-weight:bold\">Name and address of the factory :</a>");
            Response.Write("</td>");
            Response.Write("<td Border=true  colspan='4' align='center'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
            Response.Write("  ");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");

            Response.Write("<td Border=true  colspan='2' align='center'>");
            Response.Write("<a style=\"font-weight:bold\">Reg No: "+ RegNo +"</a>");
            Response.Write("</td>");
            Response.Write("<td Border=true  colspan='1' align='center'>");
            Response.Write("<a style=\"font-weight:bold\">Month & Year: " + date1.ToString("MMMM") + "--" + date1.Year.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='4'>");
            Response.Write("<a style=\"font-weight:bold\">" + Location_Full_Address_Join.ToString() + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='3'>");
            Response.Write("</td>");

            Response.Write("<td Border=true  colspan='4' align='center'>");
            Response.Write("<a style=\"font-weight:bold\">( To be maintained by the employer of Industrial establishment )</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='3'>");
            Response.Write("</td>");

            Response.Write("</tr>");


            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\" align='center'>S.No</a>");
            Response.Write("</td>");

            Response.Write("<td style='width:50px;'>");
            Response.Write("<a style=\"font-weight:bold\">Token No</a>");
            Response.Write("</td>");

            Response.Write("<td style='width:50px;'>");
            Response.Write("<a style=\"font-weight:bold\">Name and Adress of the workmen</a>");
            Response.Write("</td>");

            Response.Write("<td style='width:50px;'>");
            Response.Write("<a style=\"font-weight:bold\">Department & Designation of the workmen</a>");
            Response.Write("</td>");

            Response.Write("<td style='width:50px;'>");
            Response.Write("<a style=\"font-weight:bold\" style=\'word-wrap: break-word\'>Wether permananet temporary Causual Badili, (or) Apprentice other than those covered under the Apprentice Act 1951</a>");
            Response.Write("</td>");

            Response.Write("<td style='width:50px;'>");
            Response.Write("<a style=\"font-weight:bold\">Date of first entry into Service</a>");
            Response.Write("</td>");

            Response.Write("<td style='width:50px;'>");
            Response.Write("<a style=\"font-weight:bold\">Date on which he completed 480 days of service</a>");
            Response.Write("</td>");

            Response.Write("<td style='width:50px;'>");
            Response.Write("<a style=\"font-weight:bold\">Date on which made Permanent</a>");
            Response.Write("</td>");

            Response.Write("<td style='width:50px;'>");
            Response.Write("<a style=\"font-weight:bold\">Remarks</a>");
            Response.Write("</td>");

            Response.Write("<td style='width:50px;'>");
            Response.Write("<a style=\"font-weight:bold\">Signature of the Workmen with date ( to attest the entries)</a>");
            Response.Write("</td>");

            Response.Write("</tr>");

            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }


    public void FormIworkMenPermanentReport()
    {
        DataTable dtdlocation = new DataTable();
        date1 = Convert.ToDateTime(fromdate);
        date2 = Convert.ToDateTime(todate);
        int dayCount = (int)((date2 - date1).TotalDays);

        SSQL = "";
        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And Wages='" + Wages + "' And IsActive='Yes'";
        SSQL = SSQL + " And (CONVERT(DateTime,PFDOJ, 103) >= '" + date1.ToString("yyyy/MM/dd") + "'";
        SSQL = SSQL + " and (CONVERT(DateTime,PFDOJ, 103) <= '" + date2.ToString("yyyy/MM/dd") + "'))";
        //Check User Type
        if (SessionUserType == "2")
        {
            SSQL = SSQL + " And Eligible_PF='1'";
        }
        SSQL = SSQL + " Order by ExistingCode Asc";


        dsEmployee = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dsEmployee.Rows.Count > 0)
        {
            for (int i = 0; i < dsEmployee.Rows.Count; i++)
             {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();

                    AutoDTable.Rows[i]["1"] = (i + 1).ToString();
                    AutoDTable.Rows[i]["2"] = dsEmployee.Rows[i]["MachineID"].ToString();
                    AutoDTable.Rows[i]["3"] = dsEmployee.Rows[i]["FirstName"].ToString() + dsEmployee.Rows[i]["LastName"].ToString() + "," + dsEmployee.Rows[i]["Address1"].ToString();
                    AutoDTable.Rows[i]["4"] = dsEmployee.Rows[i]["DeptName"].ToString() + dsEmployee.Rows[i]["Designation"].ToString();
                    AutoDTable.Rows[i]["5"] = "Casual worker";
                  

                    //Get PF DOJ
                    //PF CHeck
                 
                    if(dsEmployee.Rows[i]["Eligible_PF"].ToString() == "1")
                    {
                        if(dsEmployee.Rows[i]["PFDOJ"].ToString() != "")
                        {
                          PF_DOJ_Get_str = dsEmployee.Rows[i]["PFDOJ"].ToString();
                          PF_DOJ_Get = Convert.ToDateTime(PF_DOJ_Get_str);
                        }
                    }
                    else if(dsEmployee.Rows[i]["PFNo"].ToString() != "" && dsEmployee.Rows[i]["PFNo"].ToString() != "0" && dsEmployee.Rows[i]["PFNo"].ToString() != "o" && dsEmployee.Rows[i]["PFNo"].ToString() != "A" && dsEmployee.Rows[i]["PFNo"].ToString() != "a")
                    {
                        if(dsEmployee.Rows[i]["PFDOJ"].ToString() != "")
                        {
                          PF_DOJ_Get_str = dsEmployee.Rows[i]["PFDOJ"].ToString();
                          PF_DOJ_Get = Convert.ToDateTime(PF_DOJ_Get_str);
                        }
                    }

                AutoDTable.Rows[i]["6"] = PF_DOJ_Get.ToString("dd/MM/yyyy");
                AutoDTable.Rows[i]["7"] = dsEmployee.Rows[i]["Emp_Permn_Date"].ToString();
                AutoDTable.Rows[i]["8"] = "";
                AutoDTable.Rows[i]["9"] = "";
                AutoDTable.Rows[i]["10"] = "";
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}