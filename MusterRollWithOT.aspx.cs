﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
public partial class MusterRollWithOT : System.Web.UI.Page
{

    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    string SessionUserType;
    string SSQL = "";

    DataTable dt = new DataTable();
    DataTable dsEmployee = new DataTable();
    DataTable mDataSet = new DataTable();
    Int32 Total_Calculate = 0;
    string[] Time_Minus_Value_Check;

    int shiftCount;
    string Final_Shift_Final = "";
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string FindWages;

    DataTable dt_OT = new DataTable();
    string OT_Above_Eight = "0";
    string OT_Above_Four = "0";
    string OT_Below_Four = "0";
    string OT_Total_Hours = "0";
    string oldWages;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-OT Report Between Dates";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            //SessionUserType = Session["UserType"].ToString();


            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            FindWages = Request.QueryString["wages"].ToString();

            oldWages = Request.QueryString["Wages"].ToString();
            if (oldWages == "FITTER _ ELECTRICIANS")
            {
                FindWages = oldWages.Replace('_', '&');
            }
            else
            {
                FindWages = Request.QueryString["Wages"].ToString();
            }


            System.Web.UI.WebControls.DataGrid grid =
                                  new System.Web.UI.WebControls.DataGrid();

            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("MachineID_Enc");
            AutoDTable.Columns.Add("EmpNo");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("FirstName");
            AutoDTable.Columns.Add("Wages");

            DataCells.Columns.Add("DeptName");
            DataCells.Columns.Add("EmpNo");
            DataCells.Columns.Add("ExistingCode");
            DataCells.Columns.Add("FirstName");



            Fill_Day_Attd_Between_Dates_OT();


            grid.DataSource = DataCells;
            grid.DataBind();

            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt.Rows[0]["CompName"].ToString();


            string attachment = "attachment;filename=OTReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + name + "</a>");
            Response.Write("  ");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">OT REPORT BETWEEN DATES</a>");
            Response.Write("  ");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">FROM:" + FromDate + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">TO:" + ToDate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
    }
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    public string Right_Val(string Value, int Length)
    {
        // Recreate a RIGHT function for string manipulation
        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }

    public void Fill_Day_Attd_Between_Dates_OT()
    {


        string SSQL = "";

        SSQL = "";
        SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID,MachineID_Encrypt As MachineID_Enc";
        SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
        SSQL = SSQL + ",isnull(FirstName,'') as [FirstName],isnull(Wages,'') as [Wages]";
        SSQL = SSQL + " from Employee_Mst  Where Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes' And OTEligible='1' ";
        // SSQL = SSQL + " convert(datetime,Attn_Date_Str,103)>=CONVERT(datetime,'" + FromDate + "',103) and convert(datetime,Attn_Date_Str,103)<=CONVERT(datetime,'" + ToDate + "',103) and Total_Hrs<>'0.0'";

        if (SessionUserType == "2")
        {
            SSQL = SSQL + " And IsNonAdmin='1'";
        }
        if (FindWages != "")
        {
            SSQL = SSQL + " And Wages='" + FindWages + "'";
        }
        SSQL = SSQL + " Order By cast(MachineID as int) asc";

        dsEmployee = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dsEmployee.Rows.Count <= 0)
            return;
        int i1 = 0;

        for (int j = 0; j < dsEmployee.Rows.Count; j++)
        {
            AutoDTable.NewRow();
            AutoDTable.Rows.Add();
            AutoDTable.Rows[i1][0] = dsEmployee.Rows[j]["DeptName"].ToString();
            AutoDTable.Rows[i1][1] = dsEmployee.Rows[j]["MachineID"].ToString();
            AutoDTable.Rows[i1][2] = dsEmployee.Rows[j]["MachineID_Enc"].ToString();
            AutoDTable.Rows[i1][3] = dsEmployee.Rows[j]["EmpNo"].ToString();
            AutoDTable.Rows[i1][4] = dsEmployee.Rows[j]["ExistingCode"].ToString();
            AutoDTable.Rows[i1][5] = dsEmployee.Rows[j]["FirstName"].ToString();
            AutoDTable.Rows[i1][6] = dsEmployee.Rows[j]["Wages"].ToString();
            i1++;
        }


        writeAttend_OT_Count();

    }

    public void writeAttend_OT_Count()
    {
        int intI = 1;
        int intK = 1;
        int intCol = 0;
        DataTable DS_InTime = new DataTable();//Shift Check  variable
        string Final_InTime;
        DataTable Shift_DS = new DataTable();
        TimeSpan InTime_TimeSpan;
        Boolean Shift_Check_blb = false;
        string Shift_Start_Time;
        string Shift_End_Time;
        DateTime ShiftdateStartIN;
        DateTime ShiftdateEndIN;
        DateTime InTime_Check;
        DateTime InToTime_Check;
        string Final_Shift;
        DateTime EmpdateIN;//End Shift check Variable 



        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;



        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

            DataCells.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

            daycount -= 1;
            daysAdded += 1;
        }

        intI = 4;
        intK = 1;

        int DaysCountTotal1 = (int)((Date2 - date1).TotalDays);
        int EndCount1 = 7 + DaysCountTotal1 + 1;

        AutoDTable.Columns.Add("Total Hours");
        DataCells.Columns.Add("Total Hours");
        int k = 0;


        for (int intRow = 0; intRow < AutoDTable.Rows.Count; intRow++)
        {

            if (AutoDTable.Rows[intRow]["EmpNo"].ToString() == "1513")
            {
                string StopProcess = "";
            }
            intK = 1;
            int colIndex = intK;
            bool isPresent = false;
            string Total_TIme_work = "00:00";
            DateTime TempDateTime;
            TimeSpan TempTimeSpan;
            decimal Total_Calculate = 0;
            decimal TotalDays = 0;
            string DaysCalc = "";
            ArrayList OT_Array_Value = new ArrayList();
            OT_Array_Value.Clear();
            intK = 7;


            string Wages_Type = AutoDTable.Rows[intRow]["Wages"].ToString();


            intK = 1;

            //Add OT Time
            int day_col = 7;
            int day_col1 = 4;
            DataCells.NewRow();
            DataCells.Rows.Add();
            for (intCol = 0; intCol <= daysAdded - 1; intCol++)
            {
                isPresent = false;
                string Machine_ID_Str = "";
                string OT_Week_OFF_Machine_No = null;
                string Date_Value_Str = "";
                string Date_Value_Str1 = "";
                DataTable mLocalDS = new DataTable();
                string Time_IN_Str = "";
                string Time_Out_Str = "";
                string Total_Time_get = "";
                string Emp_Total_Work_Time_1 = "00:00";
                string Final_OT_Work_Time_1 = "00:00";
                TimeSpan ts_get;
                Int32 time_Check_dbl = 0;
                TimeSpan Total_Hr = new TimeSpan();
                string PerDays = "";
                OT_Array_Value.Add("0");

                DateTime dtime = date1.AddDays(intCol);
                DataTable Log_DS = new DataTable();

                //Get Days

                
                SSQL = "";
                SSQL = "Select LTD.MachineID,LTD.ExistingCode,EM.FirstName,(CASE WHEN LTD.Present = 1.0 THEN 'X' WHEN LTD.Present = 0.5 THEN 'H' else 'A' End) as Presents,";
                SSQL = SSQL + " EM.DOJ,EM.Weekoff from LogTime_Days LTD inner join Employee_Mst EM on";
                SSQL = SSQL + " EM.CompCode=LTD.CompCode And EM.LocCode=LTD.LocCode And LTD.MachineID=EM.MachineID";
                SSQL = SSQL + " where LTD.CompCode='" + SessionCcode + "' And LTD.LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' and LTD.MachineID='" + AutoDTable.Rows[intRow]["MachineID"].ToString() + "'";
                SSQL = SSQL + " And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103))";
                SSQL = SSQL + " and CONVERT(DATETIME,LTD.Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(dtime).ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And CONVERT(DATETIME,LTD.Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(dtime).ToString("dd/MM/yyyy") + "',103) ";
                SSQL = SSQL + " And LTD.Present!='0.0'";
                SSQL = SSQL + " Group by LTD.MachineID,LTD.ExistingCode,EM.FirstName,EM.DOJ,EM.Weekoff,LTD.Present ";
                SSQL = SSQL + " Order by cast(LTD.ExistingCode as int) Asc";

                Log_DS = objdata.RptEmployeeMultipleDetails(SSQL);

                if (Log_DS.Rows.Count > 0) { DaysCalc = Log_DS.Rows[0]["Presents"].ToString(); } else { DaysCalc = "A"; }



                //Get OT Hours only
                //OT Hours Above working 8 hours 

                SSQL = "";
                SSQL = SSQL + "select isnull(sum(NoHrs), '0') as NoHrs from [Suriya_UnitA_Epay]..OverTime where EmpNo='" + AutoDTable.Rows[intRow]["MachineID"].ToString() + "' ";
                SSQL = SSQL + " and CONVERT(DATETIME,TransDate,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtime).ToString("dd/MM/yyyy") + "',103)";
                dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_OT.Rows.Count > 0) { OT_Above_Eight = dt_OT.Rows[0]["NoHrs"].ToString(); } else { OT_Above_Eight = "0"; }


                //if ((Wages_Type == "STAFF") || (Wages_Type == "FITTER & ELECTRICIANS") || (Wages_Type == "SECURITY") || (Wages_Type == "DRIVERS"))
                //{
                //    // Aboue 8 Hours
                //    SSQL = "";
                //    SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(9 as decimal(18,1)))), '0') as OT_8Hours ";
                //    SSQL = SSQL + " from LogTime_Days ";
                //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtime).ToString("dd/MM/yyyy") + "',103)";
                //    //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    SSQL = SSQL + " And Present ='1' and MachineID='" + AutoDTable.Rows[intRow]["MachineID"].ToString() + "' and  Total_Hrs > 9";
                //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    if (dt_OT.Rows.Count > 0) { OT_Above_Eight = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Eight = "0"; }


                //    // Aboue 4 Hours and Below 8 Hours
                //    SSQL = "";
                //    SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(4 as decimal(18,1)))), '0') as OT_8Hours ";
                //    SSQL = SSQL + " from LogTime_Days ";
                //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtime).ToString("dd/MM/yyyy") + "',103)";
                //    //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    SSQL = SSQL + " And (Present ='1' or Present ='0.5') and MachineID='" + AutoDTable.Rows[intRow]["MachineID"].ToString() + "' and  Total_Hrs > 4 and Total_Hrs < 7";
                //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    if (dt_OT.Rows.Count > 0) { OT_Above_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Four = "0"; }


                //    // Below 4
                //    SSQL = "";
                //    SSQL = "Select isnull(SUM(CAST(Total_Hrs as decimal(18,1))), '0') as OT_8Hours ";
                //    SSQL = SSQL + " from LogTime_Days ";
                //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtime).ToString("dd/MM/yyyy") + "',103)";
                //    //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    SSQL = SSQL + " And TypeName!='Leave' and MachineID='" + AutoDTable.Rows[intRow]["MachineID"].ToString() + "' and  Total_Hrs < 4 ";
                //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }


                //    if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }
                //}
                //else
                //{
                //    // Aboue 8 Hours
                //    SSQL = "";
                //    SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(8 as decimal(18,1)))), '0') as OT_8Hours ";
                //    SSQL = SSQL + " from LogTime_Days ";
                //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtime).ToString("dd/MM/yyyy") + "',103)";
                //    //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    SSQL = SSQL + " And Present ='1' and MachineID='" + AutoDTable.Rows[intRow]["MachineID"].ToString() + "' and  Total_Hrs > 8";
                //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    if (dt_OT.Rows.Count > 0) { OT_Above_Eight = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Eight = "0"; }


                //    // Aboue 4 Hours and Below 8 Hours
                //    SSQL = "";
                //    SSQL = "Select isnull(SUM((CAST(Total_Hrs as decimal(18,1)) - cast(4 as decimal(18,1)))), '0') as OT_8Hours ";
                //    SSQL = SSQL + " from LogTime_Days ";
                //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtime).ToString("dd/MM/yyyy") + "',103)";
                //    //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    SSQL = SSQL + " And (Present ='1' or Present ='0.5') and MachineID='" + AutoDTable.Rows[intRow]["MachineID"].ToString() + "' and  Total_Hrs > 4 and Total_Hrs < 7";
                //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    if (dt_OT.Rows.Count > 0) { OT_Above_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Above_Four = "0"; }


                //    // Below 4
                //    SSQL = "";
                //    SSQL = "Select isnull(SUM(CAST(Total_Hrs as decimal(18,1))), '0') as OT_8Hours ";
                //    SSQL = SSQL + " from LogTime_Days ";
                //    SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //    SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(dtime).ToString("dd/MM/yyyy") + "',103)";
                //    //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                //    SSQL = SSQL + " And TypeName!='Leave' and MachineID='" + AutoDTable.Rows[intRow]["MachineID"].ToString() + "' and  Total_Hrs < 4 ";
                //    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                //    if (dt_OT.Rows.Count > 0) { OT_Below_Four = dt_OT.Rows[0]["OT_8Hours"].ToString(); } else { OT_Below_Four = "0"; }
                //}

                OT_Total_Hours = (Convert.ToDecimal(OT_Above_Eight) + Convert.ToDecimal(0) + Convert.ToDecimal(0)).ToString();


                if (OT_Array_Value[intCol] == "0")
                {
                    AutoDTable.Rows[intRow][day_col] = DaysCalc + "/" + OT_Total_Hours;
                    DataCells.Rows[k][day_col1] = DaysCalc + "/" + OT_Total_Hours;

                    //AutoDTable.Rows[intRow][day_col] = "<span style=color:green><b>" + DaysCalc + "/" + OT_Total_Hours + "</b></span>";
                    //DataCells.Rows[k][day_col1] = "<span style=color:green><b>" + DaysCalc + "/" + OT_Total_Hours + "</b></span>";

                }

                else
                {
                    AutoDTable.Rows[intRow][day_col] = DaysCalc + "/" + OT_Total_Hours;
                    DataCells.Rows[k][day_col1] = DaysCalc + "/" + OT_Total_Hours;
                    //AutoDTable.Rows[intRow][day_col] = OT_Array_Value[intCol];
                    //DataCells.Rows[k][day_col1] = OT_Array_Value[intCol];
                }
                if (DaysCalc == "X") { PerDays = "1"; } else if (DaysCalc == "H") { PerDays = "0.5"; } else { PerDays = "0"; }
                Total_Calculate = (Convert.ToDecimal(Total_Calculate) + Convert.ToDecimal(OT_Total_Hours));
                TotalDays = (Convert.ToDecimal(TotalDays) + Convert.ToDecimal(PerDays));

                //OT_Total_Hours = (Convert.ToDecimal(OT_Above_Eight) + Convert.ToDecimal(OT_Above_Four) + Convert.ToDecimal(OT_Below_Four)).ToString();

                //OT_Hours = OT_Total_Hours;

                //string tot_hr = AutoDTable.Rows[intRow][day_col].ToString();

                string tot_hr = OT_Total_Hours;

                //if (tot_hr != "")
                //{
                //    TimeSpan temp1 = Convert.ToDateTime(string.Format("{0:hh:mm}", tot_hr)).TimeOfDay;
                //    Total_Hr = Total_Hr.Add(temp1);
                //}

                intK += 1;
                day_col += 1;
                day_col1 += 1;

            }

            AutoDTable.Rows[intRow]["Total Hours"] = TotalDays + "&" + Total_Calculate;
            //AutoDTable.Rows[intRow]["Total Hours"] = (string.Format("{0:t}", 0));

            //DataCells.Rows[k]["Total Hours"] = (string.Format("{0:t}", 0)); //tot_var.ToString();// String.Format("{0:hh:mm tt}", AutoDTable.Rows[intRow]["Total Hours"]);
            DataCells.Rows[k]["Total Hours"] = TotalDays + "&" + Total_Calculate; //tot_var.ToString();// String.Format("{0:hh:mm tt}", AutoDTable.Rows[intRow]["Total Hours"]);
            DataCells.Rows[k]["DeptName"] = AutoDTable.Rows[intRow]["DeptName"];
            DataCells.Rows[k]["EmpNo"] = AutoDTable.Rows[intRow]["EmpNo"];
            DataCells.Rows[k]["ExistingCode"] = AutoDTable.Rows[intRow]["ExistingCode"];
            DataCells.Rows[k]["FirstName"] = AutoDTable.Rows[intRow]["FirstName"];

            intI += 1;
            k += 1;
            Total_Calculate = 0;
            TotalDays = 0;
        }
    }


}
