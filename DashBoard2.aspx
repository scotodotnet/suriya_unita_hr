﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="DashBoard2.aspx.cs" Inherits="DashBoard2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<asp:UpdatePanel ID="UpdatePanel5" runat="server">
<ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			
			<!-- begin page-header -->
			<h1 class="page-header">DashBoard </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
		 <div class="row">
        <div class="col-sm-6"><!-- Current Month Sales Count Wise Start -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">UNIT I</h4>
                </div>
                <div class="panel-body">
                    <div>                    
                        <div>
                           <asp:Chart ID="UnitoneChart" runat="server" Compression="100" 
                                EnableViewState="True" Width="480px">
                                 <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea2" ChartType="Pie" Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea2"></asp:ChartArea>
                                </chartareas>
                               <%-- <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Column"  Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                    <asp:Series Name="Series2" ChartArea="ChartArea1" ChartType="Column"  Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                    
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                                </chartareas>--%>
                            </asp:Chart>
                        </div>
                    </div>   
                </div>
            </div>
        </div><!-- col-sm-6 --><!-- Current Month Sales Count Wise End -->
        
          <div class="col-sm-6"><!-- Current Month Sales Count Wise Start -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">UNIT II</h4>
                </div>
                <div class="panel-body">
                    <div>                    
                        <div>
                           <asp:Chart ID="UnittwoCart" runat="server" Compression="100" 
                                EnableViewState="True" Width="480px">
                                 <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea3" ChartType="Pie" Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea3"></asp:ChartArea>
                                </chartareas>
                            
                            </asp:Chart>
                        </div>
                    </div>   
                </div>
            </div>
        </div><!-- col-sm-6 --><!-- Current Month Sales Count Wise End -->
        
        
    
        
        
    </div><!-- Row -->
                <!-- end col-12 -->
                
                
               <!-- begin row -->
		 <div class="row">
        <div class="col-sm-6"><!-- Current Month Sales Count Wise Start -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">UNIT III</h4>
                </div>
                <div class="panel-body">
                    <div>                    
                        <div>
                           <asp:Chart ID="unitthreeChart" runat="server" Compression="100" 
                                EnableViewState="True" Width="480px">
                                 <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea4" ChartType="Pie" Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea4"></asp:ChartArea>
                                </chartareas>
                               <%-- <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea1" ChartType="Column"  Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                    <asp:Series Name="Series2" ChartArea="ChartArea1" ChartType="Column"  Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                    
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                                </chartareas>--%>
                            </asp:Chart>
                        </div>
                    </div>   
                </div>
            </div>
        </div><!-- col-sm-6 --><!-- Current Month Sales Count Wise End -->
        
          <div class="col-sm-6"><!-- Current Month Sales Count Wise Start -->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">UNIT IV</h4>
                </div>
                <div class="panel-body">
                    <div>                    
                        <div>
                           <asp:Chart ID="unitfourChart" runat="server" Compression="100" 
                                EnableViewState="True" Width="480px">
                                 <series>
                                    <asp:Series Name="Series1" ChartArea="ChartArea5" ChartType="Pie" Label="#VALX: #VALY"
                                        IsXValueIndexed="True">
                                    </asp:Series>
                                </series>
                                <chartareas>
                                    <asp:ChartArea Name="ChartArea5"></asp:ChartArea>
                                </chartareas>
                            
                            </asp:Chart>
                        </div>
                    </div>   
                </div>
            </div>
        </div><!-- col-sm-6 --><!-- Current Month Sales Count Wise End -->
        
        
    
        
        
    </div><!-- Row -->
      
        
        
  
                
                
                
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>



</asp:Content>