﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;

public partial class AttendanceUpload : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    AttenanceClass objatt = new AttenanceClass();
    string SessionCcode;
    string SessionLcode;
    string WagesType;
    string SessionUserType;
    string SessionEpay;
    string Query = "";

    string TempNFH = "0";
    string TempCL = "0";
    string TempThreeSide = "0";
    string TempFull = "0";
    string TempHalf = "0";
    string TempHome = "0";
    string TempOTNew = "";
    string TempWH_Work_Days = "0";
    string TempFixed_Days = "0";
    string TempNFH_Work_Days = "0";
    string TempNFH_Work_Manual = "0";
    string TempNFH_Work_Station = "0";
    string TempNFH_Count_Days = "0";
    string TempLBH_Count_Days = "0";

    SqlConnection con;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }
    }
    public void Load_TwoDates()
    {
        if (ddlMonths.SelectedValue != "-Select-" && ddlfinance.SelectedValue != "-Select-")
        {

            decimal Month_Total_days = 0;
            string Month_Last = "";
            string Year_Last = "0";
            string Temp_Years = "";
            string[] Years;
            string FromDate = "";
            string ToDate = "";

            Temp_Years = ddlfinance.SelectedItem.Text;
            Years = Temp_Years.Split('-');
            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
            {
                Month_Total_days = 31;
            }
            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
            {
                Month_Total_days = 30;
            }

            else if (ddlMonths.SelectedValue == "February")
            {
                int yrs = (Convert.ToInt32(Years[0]) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                }
            }
            switch (ddlMonths.SelectedItem.Text)
            {
                case "January": Month_Last = "01";
                    break;
                case "February": Month_Last = "02";
                    break;
                case "March": Month_Last = "03";
                    break;
                case "April": Month_Last = "04";
                    break;
                case "May": Month_Last = "05";
                    break;
                case "June": Month_Last = "06";
                    break;
                case "July": Month_Last = "07";
                    break;
                case "August": Month_Last = "08";
                    break;
                case "September": Month_Last = "09";
                    break;
                case "October": Month_Last = "10";
                    break;
                case "November": Month_Last = "11";
                    break;
                case "December": Month_Last = "12";
                    break;
                default:
                    break;
            }

            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "February") || (ddlMonths.SelectedValue == "March"))
            {
                Year_Last = Years[1];
            }
            else
            {
                Year_Last = Years[0];
            }
            FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
            ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

            txtFrom.Text = FromDate.ToString();
            txtTo.Text = ToDate.ToString();

        }
        else
        {
            txtFrom.Text = "";
            txtTo.Text = "";
        }

        
    }
    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void ddlfinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        try
        {

            if (ddlMonths.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }

            else if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtNfh.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To NFh Days Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

            }
            if (!ErrFlag)
            {
                int Month_Mid_Total_Days_Count = 0;
                Month_Mid_Total_Days_Count = (int)((Convert.ToDateTime(txtTo.Text) - Convert.ToDateTime(txtFrom.Text)).TotalDays);
                Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                txtdays.Text = Month_Mid_Total_Days_Count.ToString();
                decimal total = (Convert.ToDecimal(txtdays.Text.Trim()));
                int total_check = 0;
                decimal days = 0;
                string from_date_check = txtFrom.Text.ToString();
                string to_date_check = txtTo.Text.ToString();
                from_date_check = from_date_check.Replace("/", "-");
                to_date_check = to_date_check.Replace("/", "-");
                txtFrom.Text = from_date_check;
                txtTo.Text = to_date_check;

                if (FileUpload.HasFile)
                {
                    FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

                }

                if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                {
                    days = 31;
                }
                else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                {
                    days = 30;
                }

                else if (ddlMonths.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                    if ((yrs % 4) == 0)
                    {
                        days = 29;
                    }
                    else
                    {
                        days = 28;
                    }
                }
                if (total > days)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            SqlConnection cn = new SqlConnection(constr);

                            //Get Employee Master Details

                            DataTable dt_Emp_Mst = new DataTable();
                            string MachineID = dt.Rows[j][0].ToString();
                            string Query_Emp_mst = "";
                            Query_Emp_mst = "Select ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptName as DepartmentNm, '2' as Wagestype,";
                            Query_Emp_mst = Query_Emp_mst + " ED.MachineID as BiometricID from Employee_Mst ED inner Join Department_Mst MD on MD.DeptCode=ED.DeptCode";
                            Query_Emp_mst = Query_Emp_mst + " where ED.CompCode='" + SessionCcode + "'";
                            Query_Emp_mst = Query_Emp_mst + " and ED.LocCode='" + SessionLcode + "' and ED.IsActive='Yes'";
                            Query_Emp_mst = Query_Emp_mst + " and ED.MachineID='" + MachineID + "'";
                            dt_Emp_Mst = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);

                            if (dt_Emp_Mst.Rows.Count == 0)
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Token No Not Found in Employee Master. The Employee No is " + MachineID + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }

                            string Department = dt_Emp_Mst.Rows[0]["DepartmentNm"].ToString();
                            string EmpNo = dt_Emp_Mst.Rows[0]["EmpNo"].ToString();

                            string ExistingCode = dt.Rows[j][1].ToString();
                            string FirstName = dt.Rows[j][2].ToString();
                            string WorkingDays = dt.Rows[j][3].ToString();
                            string CL = dt.Rows[j][4].ToString();
                            string AbsentDays = dt.Rows[j][5].ToString();
                            string OT_Days = dt.Rows[j][6].ToString();
                            string SpgAllow = dt.Rows[j][7].ToString();
                            string Home = dt.Rows[j][8].ToString();
                            string weekoff = "0";//dt.Rows[j][9].ToString();
                            string HalfNight = dt.Rows[j][18].ToString();
                            string FUllNight = dt.Rows[j][19].ToString();

                            if (MachineID == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Machine ID. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }

                            else if (ExistingCode == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing Code. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Existing Code. The Roe Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (FirstName == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Name. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (WorkingDays == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Working Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }

                            else if (CL == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the CL Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (AbsentDays == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the N/FH Days properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (OT_Days == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (HalfNight == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Half Night Shift Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (FUllNight == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Full Night Shift Days. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            else if (Home == "")
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Canteen Days Minus. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }


                            else if ((Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text)) > 31)
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }

                            cn.Open();
                            string qry_dpt = "Select DeptName as DepartmentNm from Department_Mst where DeptName = '" + Department + "'";
                            SqlCommand cmd_dpt = new SqlCommand(qry_dpt, cn);
                            SqlDataReader sdr_dpt = cmd_dpt.ExecuteReader();
                            if (sdr_dpt.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();
                            string qry_dpt1 = "Select ED.DeptCode as Department from Employee_Mst ED inner Join Department_Mst MSt on Mst.DeptCode=ED.DeptCode where Mst.DeptName = '" + Department + "' and ED.EmpNo='" + EmpNo + "'";
                            SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                            SqlDataReader sdr_dpt1 = cmd_dpt1.ExecuteReader();
                            if (sdr_dpt1.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            cn.Open();

                            cn.Close();
                            cn.Open();
                            string qry_empNo = "Select EmpNo from Employee_Mst where EmpNo= '" + EmpNo + "' and ExistingCode = '" + ExistingCode + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                            SqlCommand cmd_Emp = new SqlCommand(qry_empNo, cn);
                            SqlDataReader sdr_Emp = cmd_Emp.ExecuteReader();
                            if (sdr_Emp.HasRows)
                            {
                            }
                            else
                            {
                                j = j + 2;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Details Properly. The Row Number is " + j + "');", true);
                                //System.Windows.Forms.MessageBox.Show("Enter the Employee Details Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                ErrFlag = true;
                            }
                            cn.Close();
                            total = (Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text.Trim()) + Convert.ToDecimal(CL) + Convert.ToDecimal(AbsentDays));
                            days = 0;
                            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                            {
                                days = 31;
                            }
                            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                            {
                                days = 30;
                            }
                            else if (ddlMonths.SelectedValue == "February")
                            {
                                int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                                if ((yrs % 4) == 0)
                                {
                                    days = 29;
                                }
                                else
                                {
                                    days = 28;
                                }
                            }
                            total_check = Convert.ToInt32(total);
                            if (total_check > Convert.ToInt32(days))
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                                ErrFlag = true;
                            }

                            decimal Att_sum = (Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text.Trim()) + Convert.ToDecimal(AbsentDays) + Convert.ToDecimal(CL));
                            int Att_Sum_Check = 0;
                            Att_Sum_Check = Convert.ToInt32(Att_sum);
                            if (Convert.ToInt32(days) < Att_Sum_Check)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Attenance Details Properly ...');", true);
                                ErrFlag = true;
                            }

                            if (!ErrFlag)
                            {

                                //Get Employee Master Details
                                //DataTable dt_Emp_Mst = new DataTable();
                                //string MachineID = dt.Rows[i][0].ToString();
                                //string Query_Emp_mst = "";
                                Query_Emp_mst = "Select ED.Wages, ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptName as DepartmentNm, '2' as Wagestype,";
                                Query_Emp_mst = Query_Emp_mst + " ED.MachineID as BiometricID from Employee_Mst ED inner Join Department_Mst MD on MD.DeptCode=ED.DeptCode";
                                Query_Emp_mst = Query_Emp_mst + " where ED.CompCode='" + SessionCcode + "'";
                                Query_Emp_mst = Query_Emp_mst + " and ED.LocCode='" + SessionLcode + "' and ED.IsActive='Yes'";
                                Query_Emp_mst = Query_Emp_mst + " and ED.MachineID='" + MachineID + "'";
                                dt_Emp_Mst = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);

                                //SqlConnection cn = new SqlConnection(constr);
                                cn.Open();
                                qry_dpt1 = "Select DeptCode as DepartmentCd from Department_Mst where DeptName = '" + dt_Emp_Mst.Rows[0]["DepartmentNm"].ToString() + "'";
                                cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                                DepartmentCode = Convert.ToString(cmd_dpt1.ExecuteScalar());
                                string qry_emp = "Select AT.EmpNo from [" + SessionEpay + "]..AttenanceDetails AT inner JOin [" + SessionEpay + "]..SalaryDetails SD on SD.EmpNo=AT.EmpNo where AT.EmpNo='" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and " +
                                                 "AT.Months='" + ddlMonths.Text + "' and AT.FinancialYear='" + ddlfinance.SelectedValue + "' and AT.Ccode='" + SessionCcode + "' and AT.Lcode='" + SessionLcode + "' and SD.Month='" + ddlMonths.Text + "'" +
                                                 " and SD.FinancialYear='" + ddlfinance.SelectedValue + "' and SD.Process_Mode='1' and SD.Ccode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "' and convert(datetime,AT.TODate,105) >= convert(datetime,'" + txtFrom.Text + "',105)";
                                //string qry_emp = "Select EmpNo from AttenanceDetails where EmpNo = '" + dt.Rows[i][2].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                SqlCommand cmd_verify = new SqlCommand(qry_emp, cn);
                                Name_Upload = Convert.ToString(cmd_verify.ExecuteScalar());
                                string qry_emp1 = "Select EmpNo from [" + SessionEpay + "]..AttenanceDetails where EmpNo = '" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(datetime,TODate,105) >= convert(datetime,'" + txtFrom.Text + "',105)";
                                SqlCommand cmd_verify1 = new SqlCommand(qry_emp1, cn);
                                Name_Upload1 = Convert.ToString(cmd_verify1.ExecuteScalar());
                                if (dt_Emp_Mst.Rows[0]["Wages"].ToString().ToUpper() == "CIVIL".ToUpper().ToString())
                                {
                                    WagesType = "1";
                                }
                                else
                                {
                                    WagesType = "2";
                                }

                                objatt.department = DepartmentCode;
                                objatt.EmpNo = dt_Emp_Mst.Rows[0]["EmpNo"].ToString();
                                objatt.Exist = dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString();
                                objatt.EmpName = dt_Emp_Mst.Rows[0]["EmpName"].ToString();
                                objatt.days = dt.Rows[j][3].ToString();

                                objatt.Months = ddlMonths.Text;

                                if (dt.Rows[j][13].ToString().Trim() == "")
                                {
                                    objatt.TotalDays = (Convert.ToInt32(txtdays.Text)).ToString();
                                }
                                else
                                {
                                    objatt.TotalDays = dt.Rows[j][13].ToString();
                                }

                                objatt.finance = ddlfinance.SelectedValue;
                                objatt.Ccode = SessionCcode;
                                objatt.Lcode = SessionLcode;
                                objatt.nfh = txtNfh.Text.Trim();
                                objatt.Absent = "0";

                                if (dt.Rows[j][5].ToString().Trim() == "")      //   N/FH
                                {
                                    TempNFH = "0";
                                }
                                else
                                {
                                    TempNFH = dt.Rows[j][5].ToString();
                                }

                                if (dt.Rows[j][4].ToString().Trim() == "")
                                {
                                    TempCL = "0";
                                }

                                else
                                {
                                    TempCL = dt.Rows[j][4].ToString();
                                }

                                if (dt.Rows[j][6].ToString().Trim() == "") //OT DAYS
                                {
                                    TempThreeSide = "0";
                                }

                                else
                                {
                                    TempThreeSide = dt.Rows[j][6].ToString();
                                }

                                if (dt.Rows[j][8].ToString().Trim() == "") { TempHome = "0"; } else { TempHome = dt.Rows[j][8].ToString(); }
                                //if (dt.Rows[i][9].ToString().Trim() == "") { objatt.weekoff = "0"; } else { objatt.weekoff = dt.Rows[i][9].ToString(); }
                                objatt.weekoff = "0";
                                if (dt.Rows[j][9].ToString().Trim() == "") { TempOTNew = "0"; } else { TempOTNew = dt.Rows[j][9].ToString(); }

                                objatt.workingdays = txtdays.Text;

                                if (dt.Rows[j][10].ToString().Trim() == "") { TempWH_Work_Days = "0"; } else { TempWH_Work_Days = dt.Rows[j][10].ToString(); }
                                if (dt.Rows[j][11].ToString().Trim() == "") { TempFixed_Days = "0"; } else { TempFixed_Days = dt.Rows[j][11].ToString(); }
                                if (dt.Rows[j][12].ToString().Trim() == "") { TempNFH_Work_Days = "0"; } else { TempNFH_Work_Days = dt.Rows[j][12].ToString(); }

                                if (dt.Rows[j][14].ToString().Trim() == "") { TempNFH_Work_Manual = "0"; } else { TempNFH_Work_Manual = dt.Rows[j][14].ToString(); }
                                if (dt.Rows[j][15].ToString().Trim() == "") { TempNFH_Work_Station = "0"; } else { TempNFH_Work_Station = dt.Rows[j][15].ToString(); }
                                if (dt.Rows[j][16].ToString().Trim() == "") { TempNFH_Count_Days = "0"; } else { TempNFH_Count_Days = dt.Rows[j][16].ToString(); }
                                if (dt.Rows[j][17].ToString().Trim() == "") { TempLBH_Count_Days = "0"; } else { TempLBH_Count_Days = dt.Rows[j][17].ToString(); }
                                if (dt.Rows[j][18].ToString().Trim() == "") { TempHalf = "0"; } else { TempHalf = dt.Rows[j][18].ToString(); }
                                if (dt.Rows[j][19].ToString().Trim() == "") { TempFull = "0"; } else { TempFull = dt.Rows[j][19].ToString(); }
                                cn.Close();

                                string TempDate = "getdate()";
                                if (Name_Upload1 == dt_Emp_Mst.Rows[0]["EmpNo"].ToString())
                                {
                                    string Del = "Delete from [" + SessionEpay + "]..AttenanceDetails where EmpNo = '" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(Datetime,FromDate,105) = Convert(Datetime,'" + txtFrom.Text.Trim() + "', 105)";
                                    cn.Open();
                                    SqlCommand cmd_del = new SqlCommand(Del, cn);
                                    cmd_del.ExecuteNonQuery();
                                    cn.Close();
                                    DateTime MyDate = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                    DateTime MyDate1 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);

                                    Query = "";
                                    Query = Query + "Insert into [" + SessionEpay + "]..AttenanceDetails (DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,";
                                    Query = Query + "TotalDays,Ccode,Lcode,NFh,WorkingDays,CL,AbsentDays,weekoff,FromDate,ToDate,Modeval,home,";
                                    Query = Query + "halfNight,FullNight,ThreeSided,OTHoursNew,Fixed_Work_Days,WH_Work_Days,NFH_Work_Days,";
                                    Query = Query + "NFH_Work_Days_Incentive,NFH_Work_Days_Statutory,AEH,LBH) values (";
                                    Query = Query + "'" + DepartmentCode + "','" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "','" + dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString() + "',";
                                    Query = Query + "'" + dt.Rows[j][3].ToString() + "','" + ddlMonths.Text + "','" + ddlfinance.SelectedValue + "',convert(datetime," + TempDate + ",105),";
                                    Query = Query + "'" + dt.Rows[j][13].ToString() + "','" + SessionCcode + "','" + SessionLcode + "','" + TempNFH + "','" + txtdays.Text + "','" + TempCL + "',";
                                    Query = Query + "'0','0',convert(datetime,'" + txtFrom.Text + "',105),convert(datetime,'" + txtTo.Text + "',105),'" + WagesType + "','" + TempHome + "',";
                                    Query = Query + "'" + TempHalf + "','" + TempFull + "','" + TempThreeSide + "','" + TempOTNew + "','" + TempFixed_Days + "','" + TempWH_Work_Days + "','" + TempNFH_Work_Days + "',";
                                    Query = Query + "'" + TempNFH_Work_Manual + "','" + TempNFH_Work_Station + "','" + TempNFH_Count_Days + "','" + TempLBH_Count_Days + "')";
                                    objdata.RptEmployeeMultipleDetails(Query);
                                    //objdata.Attenance_insert(objatt, MyDate, MyDate1, rbsalary.SelectedValue);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                }
                                else
                                {
                                    DateTime MyDate = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                    DateTime MyDate1 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                                    Query = "";
                                    Query = Query + "Insert into [" + SessionEpay + "]..AttenanceDetails (DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,";
                                    Query = Query + "TotalDays,Ccode,Lcode,NFh,WorkingDays,CL,AbsentDays,weekoff,FromDate,ToDate,Modeval,home,";
                                    Query = Query + "halfNight,FullNight,ThreeSided,OTHoursNew,Fixed_Work_Days,WH_Work_Days,NFH_Work_Days,";
                                    Query = Query + "NFH_Work_Days_Incentive,NFH_Work_Days_Statutory,AEH,LBH) values (";
                                    Query = Query + "'" + DepartmentCode + "','" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "','" + dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString() + "',";
                                    Query = Query + "'" + dt.Rows[j][3].ToString() + "','" + ddlMonths.Text + "','" + ddlfinance.SelectedValue + "',convert(datetime," + TempDate + ",105),";
                                    Query = Query + "'" + dt.Rows[j][13].ToString() + "','" + SessionCcode + "','" + SessionLcode + "','" + TempNFH + "','" + txtdays.Text + "','" + TempCL + "',";
                                    Query = Query + "'0','0',convert(datetime,'" + txtFrom.Text + "',105),convert(datetime,'" + txtTo.Text + "',105),'" + WagesType + "','" + TempHome + "',";
                                    Query = Query + "'" + TempHalf + "','" + TempFull + "','" + TempThreeSide + "','" + TempOTNew + "','" + TempFixed_Days + "','" + TempWH_Work_Days + "','" + TempNFH_Work_Days + "',";
                                    Query = Query + "'" + TempNFH_Work_Manual + "','" + TempNFH_Work_Station + "','" + TempNFH_Count_Days + "','" + TempLBH_Count_Days + "')";
                                    objdata.RptEmployeeMultipleDetails(Query);

                                    //objdata.Attenance_insert(objatt, MyDate, MyDate1, rbsalary.SelectedValue);
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                }
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                sSourceConnection.Close();

                                //System.Windows.Forms.MessageBox.Show("Uploaded SuccessFully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

                            }
                            if (ErrFlag == true)
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload...');", true);
                                //System.Windows.Forms.MessageBox.Show("Your File Not Uploaded", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string StaffLabour = "";

        if (ddlCategory.SelectedValue == "1")
        {
            StaffLabour = "1";
        }
        else if (ddlCategory.SelectedValue == "2")
        {
            StaffLabour = "2";
        }
        else
        {
            StaffLabour = "0";
        }


        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        query = "select EmpType from MstEmployeeType where EmpCategory='" + StaffLabour + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpType";
        ddlEmployeeType.DataBind();
    }
    protected void btnDownload_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlCategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }
            else if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }

            else if (ddlEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else if (ddlEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else
            {
                string staff_lab = "";
                string Employee_Type = "";
                if (ddlCategory.SelectedValue == "1")
                {
                    staff_lab = "S";
                }
                else if (ddlCategory.SelectedValue == "2")
                {
                    staff_lab = "L";
                }
                Employee_Type = ddlEmployeeType.SelectedValue.ToString();
                DataTable dt = new DataTable();

                Query = "";
                Query = Query + "select EM.EmpNo,(EM.FirstName) as EmpName,(EM.MachineID) as MachineNo,(EM.ExistingCode) as ExisistingCode,(DM.DeptName) as DepartmentNm from Employee_Mst EM inner join Department_Mst DM on EM.DeptCode=DM.DeptCode ";
                Query = Query + "where EM.LocCode='" + SessionLcode + "' and EM.CompCode='" + SessionCcode + "' and EM.IsActive='Yes' ";
                Query = Query + "and EM.Wages='" + ddlEmployeeType.SelectedItem.Text.ToString() + "'";
                Query = Query + " Order by EM.ExistingCode Asc";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                gvEmp.DataSource = dt;
                gvEmp.DataBind();

                if (dt.Rows.Count > 0)
                {
                    string attachment = "attachment;filename=Attendance.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";

                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    gvEmp.RenderControl(htextw);
                    Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                
                }
            }
        }
        catch (Exception)
        {
            
            throw;
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
}
