﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class WeeklyOTHours : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;

    double Final_Count;

    string PerDayAmt;

    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable OTHrs_DS = new DataTable();


    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;
    ReportDocument report = new ReportDocument();
    DataSet ds = new DataSet();
    DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- WEEKLY OT HOURS";

                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");

            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();

            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();

            SessionUserType = Session["Isadmin"].ToString();
            Division = Request.QueryString["Division"].ToString();
            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");

            //WagesType = Request.QueryString["Wages"].ToString();

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            if (SessionUserType == "2")
            {
                NonAdminGetAttdTable_Weekly_OTHOURS();
            }

            else
            {
                GetAttdTable_Weekly_OTHOURS();
            }


            ds.Tables.Add(AutoDTable);

            report.Load(Server.MapPath("crystal/ManualOTReport.rpt"));

            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            string name = dt.Rows[0]["CompName"].ToString();
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.DataDefinition.FormulaFields["Ccode"].Text = "'" + name.ToString() + "'";
            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";

            //DataTable DT_Inc = new DataTable();
            //SSQL = "Select *from OT_Incentive_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            //DT_Inc = objdata.RptEmployeeMultipleDetails(SSQL);
            //string INCAMT = "0";

            //if (DT_Inc.Rows.Count != 0)
            //{
            //    INCAMT = DT_Inc.Rows[0]["OTIncAmt"].ToString();
            //}
            //report.DataDefinition.FormulaFields["IncentiveAmt"].Text = "'" + INCAMT.ToString() + "'";

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

            //string[]  ColHead_Spilit;

            ////Col Head 1
            //DateTime Fromdayy=new DateTime();

            //Fromdayy = Convert.ToDateTime(date1.AddDays(0).ToString("yyyy/MM/dd"));
            //ColHead_Spilit = Fromdayy.ToString("yyyy/MM/dd").Split('/');

            // report.DataDefinition.FormulaFields["ColHead1"].Text = "'" + ColHead_Spilit[2].ToString() + "'";

            ////Col Head 2
            // Fromdayy = Convert.ToDateTime(date1.AddDays(1).ToString("yyyy/MM/dd"));
            // ColHead_Spilit = Fromdayy.ToString("yyyy/MM/dd").Split('/');
            // report.DataDefinition.FormulaFields["ColHead2"].Text = "'" + ColHead_Spilit[2].ToString() + "'";

            ////Col Head 3
            // Fromdayy = Convert.ToDateTime(date1.AddDays(2).ToString("yyyy/MM/dd"));
            // ColHead_Spilit = Fromdayy.ToString("yyyy/MM/dd").Split('/');
            // report.DataDefinition.FormulaFields["ColHead3"].Text = "'" + ColHead_Spilit[2].ToString() + "'";

            ////Col Head 4
            // Fromdayy = Convert.ToDateTime(date1.AddDays(3).ToString("yyyy/MM/dd"));
            // ColHead_Spilit = Fromdayy.ToString("yyyy/MM/dd").Split('/');
            // report.DataDefinition.FormulaFields["ColHead4"].Text = "'" + ColHead_Spilit[2].ToString() + "'";
            ////Col Head 5
            // Fromdayy = Convert.ToDateTime(date1.AddDays(4).ToString("yyyy/MM/dd"));
            // ColHead_Spilit = Fromdayy.ToString("yyyy/MM/dd").Split('/');
            // report.DataDefinition.FormulaFields["ColHead5"].Text = "'" + ColHead_Spilit[2].ToString() + "'";
            ////Col Head 6
            // Fromdayy = Convert.ToDateTime(date1.AddDays(5).ToString("yyyy/MM/dd"));
            // ColHead_Spilit = Fromdayy.ToString("yyyy/MM/dd").Split('/');
            // report.DataDefinition.FormulaFields["ColHead6"].Text = "'" + ColHead_Spilit[2].ToString() + "'";
            ////Col Head 7
            // Fromdayy = Convert.ToDateTime(date1.AddDays(6).ToString("yyyy/MM/dd"));
            // ColHead_Spilit = Fromdayy.ToString("yyyy/MM/dd").Split('/');
            // report.DataDefinition.FormulaFields["ColHead7"].Text = "'" + ColHead_Spilit[2].ToString() + "'";


        }
    }

    public void GetAttdTable_Weekly_OTHOURS()
    {
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("FromDate");
        AutoDTable.Columns.Add("ToDate");
        AutoDTable.Columns.Add("Col1");
        AutoDTable.Columns.Add("Col2");
        AutoDTable.Columns.Add("Col3");
        AutoDTable.Columns.Add("Col4");
        AutoDTable.Columns.Add("Col5");
        AutoDTable.Columns.Add("Col6");
        AutoDTable.Columns.Add("Col7");
        AutoDTable.Columns.Add("Basic");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("OTHrs");
        AutoDTable.Columns.Add("Designation");
        AutoDTable.Columns.Add("WagesType");
        AutoDTable.Columns.Add("TokenNo");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("EmpName");
        AutoDTable.Columns.Add("SPGTotalHrs");
        AutoDTable.Columns.Add("OTHrs_Rate");
        AutoDTable.Columns.Add("BasicAmt");

        DataTable DateTable = new DataTable();
        DataRow datetRow;
        int i;
        DateTable.Columns.Add("OTDate");


        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);
        //for(i=0;i<7;i++)
        //{
        //    datetRow=DateTable.NewRow();
        //    date1 = Convert.ToDateTime(FromDate);
        //    DateTime dayy = Convert.ToDateTime(date1.AddDays(i).ToShortDateString());
        //    string OT_Date_Str = (Convert.ToDateTime(date1.AddDays(i).ToString("yyyy/MM/dd"))).ToString();
        //    datetRow["OTDate"] = OT_Date_Str.ToString();
        //    DateTable.Rows.Add(datetRow);

        //}

        DataRow dtRow;


        //Get Weekly OT Employee Details

        DateTime Fromdayy = Convert.ToDateTime(date1.AddDays(0).ToShortDateString());
        string Date1 = Fromdayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
        DateTime Todayy = Convert.ToDateTime(Date2.AddDays(0).ToShortDateString());
        string date2 = Todayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);


        SSQL = "";
        SSQL = SSQL + "select (EM.EmpNo) as TokenNo,EM.MachineID,(EM.FirstName) as EmpName,(EM.Wages) as WagesType, ";
        SSQL = SSQL + "(EM.Designation) as Designation,(EM.DeptName) as DeptName,sum(OT.NoHrs) as OTHrs,sum(OT.netAmount) as Basic ";
        SSQL = SSQL + "from Employee_Mst EM inner join [Suriya_UnitA_Epay]..OverTime OT on EM.EmpNo=OT.EmpNo where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' ";
        SSQL = SSQL + "and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and NoHrs<>'0'";
        if (WagesType != "-Select-")
        {
            SSQL = SSQL + " And EM.Wages='" + WagesType + "'";
        }
        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + "and CONVERT(DATETIME,TransDate,103)>= convert(datetime,'" + FromDate + "',103) and CONVERT(DATETIME,TransDate,103)<= convert(datetime,'" + ToDate + "',103)";
        }
        SSQL = SSQL + "group by EM.EmpNo,EM.MachineID,EM.FirstName,EM.Wages,EM.Designation,EM.DeptName ";
        SSQL = SSQL + "order by cast(EM.EmpNo as int) asc ";


        // SSQL = "Select Distinct WOT.MachineID,WOT.ExistingCode as TokenNo,WOT.EmpName,WOT.Wages as WagesType,ED.DeptName,ED.Designation,(ED.BaseSalary + ED.VPF) as Basic,";
        // SSQL = SSQL + "sum(WOT.OTHrs) as OTHrs,'' as BasicAmt from  Weekly_OTHours WOT inner join Employee_Mst ED on WOT.MachineID=ED.MachineID ";
        // SSQL = SSQL + " where WOT.CompCode='" + SessionCcode + "' And WOT.LocCode='" + SessionLcode + "' ";
        // SSQL = SSQL + " And ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "'";

        // if(FromDate !="" && ToDate  !="")
        // {
        //     //SSQL = SSQL + " and WOT.OTDate between CONVERT(datetime,'" + Date1 + "',111) and CONVERT(datetime,'" + date2 + "',111)";
        //     SSQL = SSQL + "and convert(datetime,OTDate_Str,103)>=convert(datetime,'" + FromDate + "',103) and convert(datetime,OTDate_Str,103)<=convert(datetime,'" + ToDate + "',103)";

        // }
        //SSQL = SSQL + " group by WOT.MachineID,WOT.ExistingCode,ED.ExistingCode,WOT.EmpName,WOT.Wages, ED.Wages,ED.DeptName,ED.Designation,ED.BaseSalary,ED.VPF";
        //SSQL = SSQL + " Order By WOT.ExistingCode Asc";

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        for (i = 0; i < mDataSet.Rows.Count; i++)
        {
            dtRow = AutoDTable.NewRow();
            dtRow["CompanyName"] = SessionCcode;
            dtRow["LocationName"] = SessionLcode;
            dtRow["FromDate"] = FromDate;
            dtRow["ToDate"] = ToDate;
            dtRow["OTHrs"] = mDataSet.Rows[i]["OTHrs"].ToString();
            dtRow["Basic"] = mDataSet.Rows[i]["Basic"].ToString();
            dtRow["Dept"] = mDataSet.Rows[i]["DeptName"].ToString();
            dtRow["Designation"] = mDataSet.Rows[i]["Designation"].ToString();
            dtRow["WagesType"] = mDataSet.Rows[i]["WagesType"].ToString();
            dtRow["TokenNo"] = mDataSet.Rows[i]["TokenNo"].ToString();
            dtRow["MachineID"] = mDataSet.Rows[i]["MachineID"].ToString();
            dtRow["EmpName"] = mDataSet.Rows[i]["EmpName"].ToString();

            AutoDTable.Rows.Add(dtRow);
        }
    }
    public void NonAdminGetAttdTable_Weekly_OTHOURS()
    {

        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("FromDate");
        AutoDTable.Columns.Add("ToDate");
        AutoDTable.Columns.Add("Col1");
        AutoDTable.Columns.Add("Col2");
        AutoDTable.Columns.Add("Col3");
        AutoDTable.Columns.Add("Col4");
        AutoDTable.Columns.Add("Col5");
        AutoDTable.Columns.Add("Col6");
        AutoDTable.Columns.Add("Col7");
        AutoDTable.Columns.Add("Basic");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Designation");
        AutoDTable.Columns.Add("WagesType");
        AutoDTable.Columns.Add("TokenNo");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("EmpName");
        AutoDTable.Columns.Add("SPGTotalHrs");

        DataTable DateTable = new DataTable();
        DataRow datetRow;
        int i;
        DateTable.Columns.Add("OTDate");


        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);
        for (i = 0; i < 7; i++)
        {
            datetRow = DateTable.NewRow();
            date1 = Convert.ToDateTime(FromDate);
            DateTime dayy = Convert.ToDateTime(date1.AddDays(i).ToShortDateString());
            datetRow["OTDate"] = Convert.ToDateTime(date1.AddDays(i).ToString("yyyy/MM/dd"));
            DateTable.Rows.Add(datetRow);

        }

        DataRow dtRow;

        //Get Weekly OT Employee Details
        DateTime Fromdayy = Convert.ToDateTime(date1.AddDays(0).ToShortDateString());
        string Date1 = Fromdayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
        DateTime Todayy = Convert.ToDateTime(Date2.AddDays(0).ToShortDateString());
        string date2 = Todayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);

        SSQL = "Select Distinct WOT.MachineID,WOT.ExistingCode as TokenNo,WOT.EmpName,WOT.Wages as WagesType,ED.DeptName,ED.Designation,ED.BaseSalary as Basic,";
        SSQL = SSQL + "0 as Col1,0 as Col2,0 as Col3,0 as Col4,0 as Col5,0 as Col6,0 as Col7 from Weekly_OTHours WOT, Employee_Mst ED where";
        SSQL = SSQL + " ED.CompCode=WOT.CompCode and ED.LocCode=WOT.LocCode and ED.MachineID=WOT.MachineID and ED.ExistingCode=WOT.ExistingCode";
        SSQL = SSQL + " And WOT.CompCode='" + SessionCcode + "' And WOT.LocCode='" + SessionLcode + "' And WOT.Wages='" + WagesType + "'";
        SSQL = SSQL + " And ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' and ED.Eligible_PF='1'";

        if (FromDate != "" && ToDate != "")
        {
            SSQL = SSQL + " and WOT.OTDate between CONVERT(datetime,'" + Date1 + "',103) and CONVERT(datetime,'" + date2 + "',103)";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And ED.Division = '" + Division + "'";
        }
        SSQL = SSQL + " Order By WOT.ExistingCode Asc";

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        for (i = 0; i < mDataSet.Rows.Count; i++)
        {

            dtRow = AutoDTable.NewRow();
            dtRow["CompanyName"] = SessionCcode;
            dtRow["LocationName"] = SessionLcode;
            dtRow["FromDate"] = FromDate;
            dtRow["ToDate"] = ToDate;
            dtRow["Col1"] = mDataSet.Rows[i]["Col1"].ToString();
            dtRow["Col2"] = mDataSet.Rows[i]["Col2"].ToString();
            dtRow["Col3"] = mDataSet.Rows[i]["Col3"].ToString();
            dtRow["Col4"] = mDataSet.Rows[i]["Col4"].ToString();
            dtRow["Col5"] = mDataSet.Rows[i]["Col5"].ToString();
            dtRow["Col6"] = mDataSet.Rows[i]["Col6"].ToString();
            dtRow["Col7"] = mDataSet.Rows[i]["Col7"].ToString();
            dtRow["Basic"] = mDataSet.Rows[i]["Basic"].ToString();
            dtRow["Dept"] = mDataSet.Rows[i]["DeptName"].ToString();
            dtRow["Designation"] = mDataSet.Rows[i]["Designation"].ToString();
            dtRow["WagesType"] = mDataSet.Rows[i]["WagesType"].ToString();
            dtRow["TokenNo"] = mDataSet.Rows[i]["TokenNo"].ToString();
            dtRow["MachineID"] = mDataSet.Rows[i]["MachineID"].ToString();
            dtRow["EmpName"] = mDataSet.Rows[i]["EmpName"].ToString();


            //Get Spinning OT Hrs Start

            SSQL = "Select isnull(sum(cast(OTHrs as decimal(18,2))),0) as SPGTotalHrs from Weekly_OTHours where CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "'";
            SSQL = SSQL + " And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "' and SPG_Inc='True'";
            SSQL = SSQL + " and OTDate between CONVERT(datetime,'" + Date1 + "',103) and CONVERT(datetime,'" + date2 + "',103)";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["SPGTotalHrs"] = OTHrs_DS.Rows[0]["SPGTotalHrs"].ToString();
            }
            else
            {
                dtRow["SPGTotalHrs"] = 0;
            }
            //Get Spinning OT Hrs End

            //Day1 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[0][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col1"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }

            //Day2 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[1][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col2"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }
            //Day3 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[2][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col3"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }
            //Day4 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[3][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col4"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }
            //Day5 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[4][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col5"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }
            //Day6 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[5][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col6"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }
            //Day7 Add
            SSQL = "Select OTHrs from Weekly_OTHours where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[i]["MachineID"].ToString() + "' And ExistingCode='" + mDataSet.Rows[i]["TokenNo"].ToString() + "'";
            SSQL = SSQL + " And OTDate='" + DateTable.Rows[6][0] + "'";
            OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (OTHrs_DS.Rows.Count != 0)
            {
                dtRow["Col7"] = OTHrs_DS.Rows[0]["OTHrs"].ToString();
            }
            AutoDTable.Rows.Add(dtRow);
        }

    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
